<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is the base class for all of the controllers. All controllers must extend this class.
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
abstract class MOC_Controller extends CI_Controller 
{
	/**
    * Stores locally accessed data
    * @var string array 
    */
	public $data;
	
	/**
	 * Contains active member's username and pid
	 * @var mixed array
	 */
	public $userSession;
	
	/**
	 * Object for testing runtime of functions
	 * @var object
	 */
	public $timer;
		
	function __construct() {
		
        // Call the CI_Controller constructor
        parent::__construct();
		
        //$this->load->library('carabiner');
        
		$this->userSession = $this->session->all_userdata();
		
		if(!isset($this->userSession['member']) || empty($this->userSession['member'])) {
			$this->userSession['member'] = false;
		}
		
		if(!isset($this->userSession['pid']) || empty($this->userSession['pid'])) {
			$this->userSession['pid'] = false;
		}
		
		$this->data = array();
		$this->data['session'] = $this->userSession;
		
		// Initialize these arrays here first
		$this->data['css'] = array();
		$this->data['scripts'] = array();
		$this->data['inline_scripts'] = array();
		
		$this->data['page_title'] = 'Minds of Chimera';
		
		$this->timer = '';
	}
}

/**
 * @TODO finish writing and implement
 */
class LoggedIn extends MOC_Controller {

    public function __construct() {
        parent::__construct();
        if (is_logged_in() == FALSE) {
            $this->session->set_userdata('return_to', uri_string());
            $this->session->set_flashdata('message', 'You need to log in.');
            redirect('/home');
        }
    }
}

/**
 * @TODO finish writing and implement
 */
class AdminLayout extends LoggedIn {

    public function __construct() {
        parent::__construct();
        // do something
    }
}

/**
 * Calculates how long a block of functions run
 * 
 * usage:
 * 
 * $timer = new FuncTimer();
 * $start = $timer->getTime();
 * // Run some functions and return
 * $end = $timer->getTime();
 * $timer->getTotalTime($start, $end);
 * exit;
 */
class FuncTimer extends MOC_Controller {
	
	/**
	 * Stores start time in microseconds
	 * @var float
	 */
	public $start;
	
	/**
	 * Stores end time in microseconds 
	 * @var float
	 */
	public $end;
	
    public function __construct() {
        parent::__construct();
        
        $this->start = 0;
        $this->end = 0;
    }
    
    public function getStartTime() {
    	$timer = explode(' ', microtime());
    	$this->start = $timer[1] + $timer[0];
    	return $this->start;
    }
    
    public function getEndTime() {
    	$timer = explode(' ', microtime());
    	$this->end = $timer[1] + $timer[0];
    	return $this->end;
    }
    
    public function getTotalTime($start, $end) {
    	return round($end - $start, 4).' seconds';
    }
}

/* End of file  */
/* Location: ./application/core/ */
