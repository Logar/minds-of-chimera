<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
/* filename: templater.php */

/**
 * Generates a template views for all pages
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Templater
{
	/**
	 * CI -- the CodeIgniter instance
	 *
	 * @var mixed
	 * @access private
	 */
	private $CI;
	
	/**
	 * Stores the logged in username
	 * @var string
	 */
	private $user;
	
	/**
	 * Stores user's pid
	 * @var string
	 */
	private $pid;

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		global $CI;
		
		$CI =& get_instance();
		$CI->load->library('session');
		$CI->load->library('twig');
		
		$this->userSession = $CI->session->all_userdata();
		
		if(isset($this->userSession['member']) && !empty($this->userSession['member'])) {
			$this->user = $this->userSession['member'];
		} else {
			$this->user = false;
		}
		
		if(isset($this->userSession['pid']) && !empty($this->userSession['pid'])) {
			$this->pid = $this->userSession['pid'];
		} else {
			$this->pid = false;
		}
	}
	
	/**
	 * Generates the view templates for all pages
	 * 
	 * @param string $page
	 * @param string $layoutType
	 * @access public
	 */
	public function makePageTemplate($page, $layoutType, $data) {
		global $CI;
	
		$data['page_title'] = ucwords($data['page']);
	
		/**
		 * PHP script because these are more complex
		*/
		$data['content'] = $CI->load->view($page, $data, true);
	
		if($this->user !== false) {
			if(isset($this->userSession['notifyList'])) {
				$data['old_notifications'] = $this->userSession['notifyList'];
			} else {
				$data['old_notifications'] = '';
			}
			$data['notification_view'] = $CI->view('notification_view', $data, true);
		}
		
		$data['layoutType'] = $layoutType;
		
		if($layoutType == "two_cols") {
			/**
			 * Load the sidebar for the 2nd column
			 */
			if($this->user !== false) {
				if(isset($_GET['login_failed'])) {
					$data['login_failed'] = true;
				}
				if(isset($_GET['user_banned'])) {
					$data['user_banned'] = true;
				}
				$data['sidebar'] = $CI->twig->render('sidebars/login', $data, false);
			} else {
				$data['sidebar'] = $CI->load->view('sidebars/sidebar', $data, true);
			}
		}
		
		/**
		 * Let's have Twig handle the layout pages 
		 * (pages: base.html.twig, footer.html.twig, header.html.twig, and main_layout.html.twig)
		 */
		$CI->twig->render('layouts/main_layout', $data);
	}
}

?>
