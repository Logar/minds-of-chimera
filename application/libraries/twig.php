<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
/**
 * Twig class.
 */
class Twig
{

	/**
	 * twig -- the twig environment
	 *
	 * @var mixed
	 * @access private
	 */
	private $_twig;

	/**
	 * CI -- the CodeIgniter instance
	 *
	 * @var mixed
	 * @access private
	
	public function ci_function_init()
	{
	 */
	private $CI;

	/**
	 * data -- an array of data to be passed to the template context
	 *
	 * (default value: array())
	 *
	 * @var array
	 * @access private
	 */
	private $data = array(); // data passed to template

	/**
	 * loadMyFunctions -- makes CI functions available within templates
	 *
	 * @access private
	 * @return void
	*/
	private function loadMyFunctions()
	{
		$functions = get_defined_functions();
		foreach($functions['user'] as $index => $function) {
			$this->addFunction($function);
		}
	}

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
	    $this->CI->config->load('twig');

	    ini_set('include_path',
        ini_get('include_path') . PATH_SEPARATOR . APPPATH . 'libraries/Twig');
        require_once 'vendor/twig/twig/lib/Twig/Autoloader.php';
            
        log_message('debug', "Twig Autoloader Loaded");
	
		Twig_Autoloader::register();

		$loader = new Twig_Loader_Filesystem($this->CI->config->item('template_dir'));

		$this->_twig = new Twig_Environment($loader, array(
			'auto_reload' => $this->CI->config->item('auto_reload'),
			'autoescape' => $this->CI->config->item('autoescape'),
			'base_template_class' => $this->CI->config->item('base_template_class'),
			'cache' => $this->CI->config->item('cache_dir'),
			'charset' => $this->CI->config->item('charset'),
			'debug' => $this->CI->config->item('debug'),
			'optimizations' => $this->CI->config->item('optimizations'),
			'strict_variables' => $this->CI->config->item('strict_variables')
		));
		
        foreach(get_defined_functions() as $functions) {
        	foreach($functions as $function) {
        		$this->_twig->addFunction($function, new Twig_Function_Function($function));
        	}
        }
        $this->ci_function_init();
		
		
		log_message('debug', '[Twig] Library loaded -- twig templates are available for use');
	}

	/**
	 * addFunction function -- expose a single function to the templates
	 *
	 * @access public
	 * @param string $name The name of the function to be added
	 * @return void
	 */
	public function addFunction($name)
	{
		if(function_exists($name)) 
			$this->_twig->addFunction($name, new Twig_Function_Function($name));
	}

	/**
	 * render function -- Renders the template with the given context
	 *
	 * @access public
	 * @param string $name Name of template file (including paths from root template path)
	 * @param array $data (default: array())
	 * @param boolean $render Determines whether the compiled template is output or returned (default: TRUE)
	 * @return mixed
	 */
	public function render($name, $data = array(), $render = TRUE)
	{
		$template = $this->loadTemplate($name);
		$this->_preOutput();
		if ( ! $render )
			return $template->render(array_merge($this->data, $data));

		echo $template->render(array_merge($this->data, $data));
	}

	/**
	 * loadTemplate function -- load a template
	 *
	 * @access public
	 * @param string $name Name of template file (including paths from root template path)
	 * @return Twig_Template
	 */
	public function loadTemplate($name) {
		$result = $this->_twig->loadTemplate($name . '.' . $this->CI->config->item('template_ext'));
		log_message('debug', '[Twig] Template loaded -- ' . $name . '.' . $this->CI->config->item('template_ext'));
		return $result;
	}

	/**
	 * _preOutput function -- some last minute helpers
	 *
	 * @access private
	 * @return void
	 */
	private function _preOutput()
	{
		$this->data['memory_usage'] = round(memory_get_usage()/1024/1024, 2) . 'MB';
		$this->data['elapsed_time'] = $this->CI->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
	}
	
	public function ci_function_init()
	{
		$this->_twig->addFunction('base_url', new Twig_Function_Function('base_url'));
		$this->_twig->addFunction('site_url', new Twig_Function_Function('site_url'));
		$this->_twig->addFunction('current_url', new Twig_Function_Function('current_url'));
	}
}
?>
