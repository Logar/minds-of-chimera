<? if($session['member'] !== false) { ?>
<div style="float: left; padding-right: 10px;">
<div style="width: 20px; height: 20px; background: #D7EFEF; float: left; margin-right: 5px; border: 1px solid gray;"></div>= Not yet Started</div>
<div style="float: left; padding-right: 10px;">
<div style="width: 20px; height: 20px; background: #FAF8CC; float: left; margin-right: 5px; border: 1px solid #FDD017;"></div> = In progess</div>
<div style="float: left; padding-right: 10px;">
<div style="width: 20px; height: 20px; background: #c7f7be; float: left; margin-right: 5px; border: 1px solid #338800;"></div> = Completed</div>
<div class="clear"></div><br />

<?
if($activeCount < 1) {
	echo '<div class="warning-box">You currently do not have any active quests</div><br />';
}

?>
In Progress Quests: <?=$activeCount ?><br />
Completed Quests: <?=$completedCount ?><br />

Total Quests: <?=$totalQuests ?><br /><br />

Percentage of Quests Left: <?=$percentLeft ?>%<br /><br />
<? } ?>
<ol>
<?
for($i=0; $i < $totalQuests; $i++) {
	
	$lower_questName = lcfirst($questNames[$i]);
	$upper_questName = $questNames[$i];
	$story = $questDesc[$i];
	
	if(!empty($upper_questName)) {
		
		if(isset($activeQuests) && isset($completedQuests)) {

			if (in_array($lower_questName, $activeQuests, true)) {
				echo '<li><div style="margin-bottom: 5px; padding: 5px; background: #FAF8CC; border: 1px solid #FDD017;"><b>'.$upper_questName.'</b><br /><p style="width: 100%;">'.$story.'</p></div></li>';
			}
			else if (array_key_exists($upper_questName, $completedQuests)) {
				echo '<li><div style="margin-bottom: 5px; padding: 5px; background: #c7f7be; border: 1px solid #338800;"><b>'.$upper_questName.'</b><br /><p style="width: 100%;">'.$story.'</p></div></li>';
			}
			else {
				echo '<li><div style="margin-bottom: 5px; padding: 5px; background: #D7EFEF; border: 1px solid gray;"><b>'.$upper_questName.'</b><br /><p style="width: 100%;">'.$story.'</p></div></li>';
			}
		}
		else {
			echo '<li><div style="border: 1px solid gray; margin-bottom: 5px; background: #D7EFEF; padding: 5px;"><b>'.$upper_questName.'</b><br /><p style="width: 100%;">'.$story.'</p></div></li>';
		}
	}
}

?>
</ol>
