<h2 class="page_title">FAQ</h2>

<div id="sites-canvas-main-content">
<table xmlns="http://www.w3.org/1999/xhtml" cellspacing="0" class="sites-layout-name-one-column sites-layout-hbox"><tbody><tr><td class="sites-layout-tile sites-tile-name-content-1"><div dir="ltr"><h3 dir="ltr"><a name="TOC-Q:-How-do-I-play-Minds-of-Chimera-"></a><span >Q: How do I play Minds of Chimera?</span></h3>
<span >A: Glad you asked! Just follow these simple instructions!</span><br><br>
<h4 dir="ltr"><a name="TOC-Get-a-Login"></a><span >Get a Login</span></h4>
<span >The researchers will provide you with a Minecraft account. If you are interested, email </span><span >isue.moc@gmail.com</span><span > asking to be on the whitelist and to be put on the permissions file.</span><br>
<br>
<h4 dir="ltr"><a name="TOC-Get-the-Client"></a><span >Get the Client</span></h4>
<ol ><li ><span >Find the latest client here: </span><a href="http://minedev.net/BukkitContrib/Spoutcraft.jar" rel="nofollow"><span >MineCraft SpoutClient</span></a></li>
<li ><span >Run the Spoutcraft.jar file.</span></li>
<li ><span >Login with your new account.</span></li>
<li ><span >Select multiplayer</span><br>
<img height="164px;" src="https://lh5.googleusercontent.com/rqg_3HgTyK6QEV5bCm9y3L8aP1R8W-rNXC1lZ-42j5m9iD3c1UVg8x6o-iPs_IG3RWrEwAqARDz_VN3Bi-dyNnmf6RRd46FivQDDmXRqI3vLE3xwkdc" width="267px;">
<br>
<br>
</li>
<li><span > Select "Add a Server"</span><br>
<img height="100px;" src="https://lh3.googleusercontent.com/YhvQiGvYrh2KmfwxTE-yCLVTpJ6q_iohvs9usZygudzu95-brnQ4wnB16vYc0XyYZpu7LcHcD9YVTb5y11E43WIvrC6NiqhGZ5Hu-VLjGut4YhNEWfA" width="490px;">
<br>
<br>
</li>
<li><span > Enter the server IP: isue-server.eecs.ucf.edu and enter a name for it, "ISUE" or "isue-server". Hit "Add".</span><br>
<img height="348px;" src="https://lh6.googleusercontent.com/tvaa1P3uItT9ifByoRVBfxGjfXxtgXaM-jgzt3Mwyfz2RfTqRifv1V8gKAfNuMjvfH1I6krCAvyMDVcpPxlGCGqLBCVS8WBVOPbkmbZ1EhsJJxtX-hQ" width="427px;"><br>
<br>
</li>
<li><span > Double click the server to login:</span><br>
<img height="80px;" src="https://lh3.googleusercontent.com/4N8PAuJxDLSn5YeveIMb57JrG_tO_8ZM6cYsaNRNQORKCdo72lp6H8mINZBilHIMLn_p6Vt203TjFlo-Nw7EPRIiKsP2U3ZUJUyt8Pm5MQF90l8mrAU" width="427px;"><br>
<br>
</li>
<li><span > If errors arise send an email to </span><span >isue.moc@gmail.com</span><span >!</span><br>
</li>
</ol>
<br>
<h3 dir="ltr"><a name="TOC-Q:-I-don-t-know-how-to-play-minecraft-"></a><span >Q: I don't  to play minecraft!</span></h3>
<span >A: We've included a special Beginner's World just for you! It's perfect for people who are new to Minecraft, or PC games in general. Use the slash command '/warp rtut' to try it out!</span><br>
<br>
<h3 dir="ltr"><a name="TOC-Q:-How-do-I-do-slash-commands-"></a><span >Q: How do I do slash commands?</span></h3>
<span >A: Press 't' (or ENTER if that doesn't work) to get to a prompt. Now you can use special commands like /spawn to send you back to the spawn point.</span><br>
<br>
<span >Here are some special commands:</span><br>
<h4 dir="ltr"><a name="TOC-Getting-Around"></a><span >Getting Around</span></h4>
<p dir="ltr" ><span >/spawn - teleports you to the starting point of the world</span></p>
<p dir="ltr" ><span >/whereami - gives information about your location in the world</span></p>
<p dir="ltr" ><span >/warp list - Lists some of the locations where it is possible to teleport</span></p>
<p dir="ltr" ><span >/warp list 2 - Lists some of the locations where it is possible to teleport</span></p>
<p dir="ltr" ><span >/warp list 3 - Lists some of the locations where it is possible to teleport</span></p>
<p dir="ltr" ><span >/warp list 4 - Lists some of the locations where it is possible to teleport</span></p>
<p dir="ltr" ><span >/warp [location] - teleports you to a location, locations to teleport can be found using the warp list command</span></p>
<h4 dir="ltr"><a name="TOC-Permissions"></a><span >Permissions</span></h4>
<span > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/lwc - lists all of the ways certain objects can be protected, such as chests and iron doors.</span><br>
<h3 dir="ltr"><a name="TOC-Q:-How-do-I-make-my-Minecraft-character-look-cool-"></a><span >Q: How do I make my Minecraft character look cool?</span></h3>
<a href="http://minecraft.novaskin.me/" rel="nofollow"><span >Novaskin:</span></a><span > Is a great way to make your own skin or use a great one made by someone else. Click Apply when you're ready to add it to your minecraft avatar!</span><br>
<h3 dir="ltr"><a name="TOC-Q:-When-will-you-add-INSERTAWESOMEFEATUREHERE-"></a><span >Q: How will I know if a new feature has been added to the server?</span></h3>
<span >A: Keep track of the public wall on this website to be notified of all updates to Minds of Chimera! And add your own suggestions as well so we know what you want!</span><br>
<h3 dir="ltr"><a name="TOC-Resources"></a><span >Resources</span></h3>

<ul ><li ><a href="http://www.gamefaqs.com/pc/606524-minecraft/faqs/63651" rel="nofollow"><span >Minecraft Walkthrough</span></a></li>
<li ><span ><a href="http://www.gamefaqs.com/pc/606524-minecraft/faqs/61594" rel="nofollow">Strategy Guide</a></span></li></ul></div></td></tr></tbody></table>
</div>
