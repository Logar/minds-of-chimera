<h1 class="page_title float-left">Design Patterns</h1>
<div class="clearfix">
	<a href="javascript:TINY.box.show({url:'<?=SITE_URL?>misc/popup/patterns_wiki.php',width:viewport[0]-100,height:viewport[1]-50,opacity:30})" title="About Patterns" style="margin-left: 20px; margin-top: 15px; float: left;">What's a pattern?</a>
</div>
<div class="instant-msg-container" style="display: none; width: 220px;">
	<div class="instant-msg-bubble">You must log in to MineCraft to create patterns. Click on the link to learn how to install MineCraft. </div>
<img src="<?=site_url('assets/images/down-arrow.png')?>" style="position: absolute; top: 68px; right: 10px; float: right;" />
</div>
<a href="<?=SITE_URL ?>installation" class="instant-msg-link" style="float: right; padding: 5px;">How can I create a pattern?</a><br /><br />
<table class="sortable clearfix">
	<thead>
	<tr>
	<th style="width: 200px;" class=" sorttable_sorted">Name <span id="sorttable_sortfwdind">&nbsp;▾</span></th><th style="width: 150px;">Author</th><th style="width: 70px;">Rating</th><th style="width: 70px;">Used</th>
	<th style="width: 100px;">Added</th>
	</tr>
	</thead>
	<tbody>
	<? if(!isset($warningMsg)) {
		
		for($i=0; $i<sizeof($patterns); $i++) {
	?>
	<tr><td style="max-width: 195px; word-wrap: break-word;"><a href="<?=site_url('mocrater/display/'.$patterns[$i]['id'])?>"><?=$patterns[$i]['name']?></a></td><td><?=$patterns[$i]['author']?></td>
	<td><? if(!empty($patterns[$i]['total_score'])) { echo number_format($patterns[$i]['total_score'], 1); } else { echo 'N/A'; } ?></td><td></td><td><?=$patterns[$i]['createdon']?></td></tr>
	<? 
		}  
	} 
	?>
	</tbody>
</table>
<? if(isset($warningMsg)) { 
		 echo '<div style="border: 1px solid #ccc; padding: 10px;"><div class="warning-box">No patterns exist. Add some by clicking <a href="'.site_url("mocrater/add_pattern").'">here</a>.</div><div class="space_40"></div></div>';
	}
?>
