<?php
$content = '';
$style = '';
$builders = ''; $diggers = ''; $farmers = ''; $hunters = ''; $miners = ''; 
$weaponsmiths = ''; $woodcutters = ''; $fishermen = '';

$content .= '<div style="margin: 0 auto; width: 540px; max-height: 500px; overflow: auto;">';
for($i=0; $i < sizeof($results); $i++) {
		
	$curr_user = $this->userSession['member'];

	if($results[$i]['username'] == $curr_user) {
		$style='style="color: blue; font-weight: bold;"';
	}
	if($results[$i]['job'] == "Farmer") {
		$farmers .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	} else if($results[$i]['job'] == "Builder") {
		$builders .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	} else if($results[$i]['job'] == "Miner") {
		$miners .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	} else if($results[$i]['job'] == "Digger") {
		$diggers .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	} else if($results[$i]['job'] == "Woodcutter") {
		$woodcutters .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	} else if($results[$i]['job'] == "Weaponsmith") {
		$weaponsmiths .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	} else if($results[$i]['job'] == "Hunter") {
		$hunters .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	} else if($results[$i]['job'] == "Fisherman") {
		$fishermen .= '<tr><td '.$style.'>'.$results[$i]['username'].'</td><td '.$style.'>'.$results[$i]['level'].'</td></tr>';
	}
	$style='';
}

$j = 0;
if(!empty($builders)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Builders</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$builders.'</table></div>';
}
if(!empty($diggers)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Diggers</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$diggers.'</table></div>';
}
if($j == 2) {
	$content .= '<div class="clear"></div><br />';
	$j = 0;
}
if(!empty($farmers)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Farmers</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$farmers.'</table></div>';
}
if($j == 2) {
	$content .= '<div class="clear"></div><br />';
	$j = 0;
}
if(!empty($fishermen)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Fishermen</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$fishermen.'</table></div>';
}
if($j == 2) {
	$content .= '<div class="clear"></div><br />';
	$j = 0;
}
if(!empty($hunters)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Hunters</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$hunters.'</table></div>';
}
if($j == 2) {
	$content .= '<div class="clear"></div><br />';
	$j = 0;
}
if(!empty($miners)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Miners</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$miners.'</table></div>';
}
if($j == 2) {
	$content .= '<div class="clear"></div><br />';
	$j = 0;
}
if(!empty($weaponsmiths)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Weaponsmiths</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$weaponsmiths.'</table></div>';
}
if($j == 2) {
	$content .= '<div class="clear"></div><br />';
	$j = 0;
}
if(!empty($woodcutters)) {
	$j++;
	$content .= '<div class="grid_24"><h2>Woodcutters</h2><table class="table1"><tr><th class="username">Username</th><th>Level</tr>'.$woodcutters.'</table></div>';
}
$content .= '</div>';

echo content;

?>