<h2 class="page_title">Community Projects</h2>
<div id="sites-canvas-main-content">
<div class="sites-layout-name-one-column sites-layout-hbox">
<div class="sites-layout-tile sites-tile-name-content-1">
<div dir="ltr">This page is dedicated to some team projects. &nbsp;This is a place where you can tell people 
what to build for you or if you are bored to can lend a hand to an&nbsp;architect&nbsp;in need of builder.<div>
<br></div><div>List a warp to the location you want the item made, links to the structure you would like made 
and any other relevant information.</div><div>If you help out with a project put you name next to it and let 
everybody know you helped create the work of art listed.</div><div><br></div><div>Project Rome</div>
<div>warp name: rome</div><div>Project director: laithc</div><div><br></div><div>Projects Underway</div>
<div>Pantheon</div><div>
<a href="http://en.wikipedia.org/wiki/Pantheon,_Rome" rel="nofollow">http://en.wikipedia.org/wiki/Pantheon,_Rome</a></div>
<div><span>&nbsp; &nbsp; Items left to be done</span><br></div><div><span>
<span>&nbsp;&nbsp; &nbsp;<span>&nbsp; &nbsp; Clearing out exterior debris</span></span><br>
</span></div><div><span><span><span><span>&nbsp;&nbsp; &nbsp;
<span>&nbsp; &nbsp; Constructing&nbsp;entrance&nbsp;archway</span></span><br></span>
</span></span></div><div><span><span><span><span><span><span>&nbsp;&nbsp; &nbsp;
<span>&nbsp; &nbsp; Detail interior</span></span><br></span></span></span></span></span></div><div><br></div>
<div>Items to be built</div><div>Aqueduct</div><div><a href="http://en.wikipedia.org/wiki/Aqueduct" rel="nofollow">
http://en.wikipedia.org/wiki/Aqueduct</a></div><div>Public bath</div>
<div><a href="http://en.wikipedia.org/wiki/Public_bathing" rel="nofollow">http://en.wikipedia.org/wiki/Public_bathing</a></div>
<div>Colosseum</div><div>
<a href="http://en.wikipedia.org/wiki/Colosseum" rel="nofollow">http://en.wikipedia.org/wiki/Colosseum</a>
</div><div>Arch of Constatine</div><div><a href="http://en.wikipedia.org/wiki/Arch_of_Constantine" rel="nofollow">
http://en.wikipedia.org/wiki/Arch_of_Constantine</a></div></div></div></div>
</div>
