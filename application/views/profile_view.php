<br />
<? 
if(isset($errorMsg)) { 
	echo $errorMsg; 
} else {

	if(isset($owner)) {
		$ownerLowerCase = $owner;
	}
?>
<div class="row">
<div class="col_8">

<?
if(!empty($profile_img_src_big)) { $target = $profile_img_src_big; } else { $target = $profile_img_src; }
?>
<div class="img-container" id="profile_img"><div class="gallery-img-container-inner">
<?
if(isset($owner)) {
	if($owner == $session['member']) {
?>

<div id="img-result"></div>
<div id="profile_img_form_container" style="position: absolute; left: 0px; z-index: 900; top: -20px;" >
<div class="f1-upload-process" style="position: absolute; top: ><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></div>
<form name="image_upload_form" id="profile_upload_form" action="<?=site_url('main/upload_profile_img')?>" method="post" enctype="multipart/form-data"  style="display: none; position: absolute;">
	
	<div class="file-wrapper" style="width: 150px; position: absolute; left: 5px; top: 25px;">
		<input type="file" name="profile_img" id="upload-img" style="width: 150px;" />
		<div class="upload_img_overlap"><div style="padding-left: 15px;"><b>Edit Profile Picture</b></div></div>
	</div>
</form>
</div>
<? } 
} ?>
<? if(!empty($profile_img_src)) { ?>
<a href="<?=$target ?>" target="<?=$target ?>" rel="lightbox[gallery]" id="profile-img-link"><img src="<?=$profile_img_src ?>" alt="profile_image" id="profile-img" /></a>
<? } else { ?>
<a href="<?=$target ?>" target="<?=$target ?>" rel="lightbox[gallery]" id="profile-img-link"><img src="<?=site_url('assets/images/profile/steve_avatar.png')?>" alt="profile_image" id="profile-img" /></a>
<? } ?>
</div></div>
<br />
<span style="font-weight: 600;">Money: </span><?=$money?><span style="color: #AF7817;">g</span><br />
<b>Playtime</b>: <?=$total_playtime ?> hours<br /><br />
<h4 style="padding-bottom: 2px;">STATS</h4>
<table class="table1">
	<tbody>
	<tr><td class="col1"><b>Moved</b></td><td class="col2"><?=$moved?></td></tr>
	<tr><td class="col1"><b>Placed</b></td><td class="col2"><?=$bplace?></td></tr>
	<tr><td class="col1"><b>Hostiles</b></td><td class="col2"><?=$hk?></td></tr>
	<tr><td class="col1"><b>Friendly</b></td><td class="col2"><?=$fk?></td></tr>
	<tr><td class="col1"><b>PKs</b></td><td class="col2"><?=$pk?></td></tr>
	<tr><td class="col1"><b>Destroyed</b></td><td class="col2"><?=$bdestroy?></td></tr>
	<tr><td class="col1"><b>Died</b></td><td class="col2"><?=$died?></td></tr>
	</tbody>
</table>
<br />
<? 
/*
	$level_array = array();
	$level_array['combat'] = $combat_xp;
	$level_array['mining'] = $mining_xp;
	$level_array['wood'] = $wood_xp;
	$level_array['excavate'] = $excavate_xp;
	$level_array['farm'] = $farming_xp;
	$level_array['scavenge'] = $scavenge_xp;
	*/
?>

</div>
<div class="col_16">

<div style="float: left;">
<h3 style="margin-top: 15px; margin-bottom: 20px;"><a href="<?=$owner_profile_path ?>"><?=ucwords($owner)?></a></h3>
<?
	if(isset($user_status)) {
		if($user_status == NULL) {
			echo "&nbsp;(Inactive Minecraft User)";
		}
	}
	/*$highest_value = 0;
	$highest_level = array();
	
	foreach($level_array as $key=>$value) {
		if($value > $highest_value) {
			$highest_value = $value;
			$highest_level = $key; 
		}
	}
	if($highest_level == "combat") {
		if($highest_value >= 100) {
			$title = "The Warlord";
		} else {
			$title = "The Warrior";
		}
	}
	else if($highest_level == "mining") {
		$title = "The Prospector";
	}
	else if($highest_level == "wood") {
		$title = "The Lumberjack";
	}
	else if($highest_level == "excavate") {
		$title = "The Explorer";
	}
	else if($highest_level == "farm") {
		$title = "The Grower";
	}
	else if($highest_level == "scavanger") {
		$title = "The Scavanger";
	}
	if(isset($title) && !empty($title)) {
		echo '<h2 style="margin-left: 20px;"><i>'.$title.'</i></h2>';
	}*/
?>
</div>
	<? if($session['member'] != false) { ?>
	<div style="margin-top: 5px; position: relative;">
		<div style="position: relative; left: 15px; float: left;">
			<img src="<?=site_url('assets/images/feed-status-icon.png')?>" alt="status icon" style="padding-right: 5px;  position: relative; float: left; z-index: 15;" />
			<a id="post-status" style="float: left;"><b>Status</b></a>
		</div>
		<div style="position: relative; left: 35px; float: left;">
			<img src="<?=site_url('assets/images/feed-photo-icon.png')?>" alt="status icon" style="padding-right: 5px;  position: relative; float: left; z-index: 15;" />
			<a id="post-photo" style="float: left;"><b>Photo</b></a>
		</div>
		<div class="clear"></div>
		<p id="img-result"></p>
		<div id="post_form_container">
			<div class="f1-upload-process" style="position: absolute; right: 0px; top: 0px;"><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></div>
			<form class="post-form" enctype="multipart/form-data" method="post" action="<?=site_url('main/addPost/home')?>">
				<textarea name="postBox" id="post-box" class="expand" rows="1" placeholder="<? if($session['member'] !== $owner) { echo "Make a post on this profile..."; } else { echo "Tell people what you are up to...";} ?>" style="width:100%; resize: none; float: left; position: relative; z-index: 10; margin-top: -1px; box-sizing: border-box;" required></textarea>
		
				<input type="hidden" name="location" value="<?=$ownerLowerCase?>" />
				<div class="grippie"></div>
				
				<div id="post-box-bottom">
					<input type="file" name="post-feed-image" id="upload-feed-photo" class="float-left" />
					<input type="submit" value="Post" class="float-right" />
				</div>
				<div class="clear"></div>
			</form>
		</div>
	</div>
	<br />
	<? } ?>
	
	<div class="all-posts">
		<?=$post_feed ?>
	</div>
	<div id="more-posts">

		<p id="more-posts-loader" style="display: none;"><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></p>
		<ul>
			<!-- Hijack this link for the infinite scroll -->
			<!--<li class="more"><a href="." title="Traditional navigation link">Next Page</a></li>-->
		</ul>
	</div>
</div>
<div class="col_12">
<? if(isset($_SESSION['member'])) { ?>
	<div class="sidebar_container">
		<div style="z-index: 900;">
			<div style="position: relative; float: left; width: 60px;">
				<a href="<?=SITE_URL?>gallery"><span style="cursor: pointer;">Photos</span><br/><img src="<?=SITE_URL?>assets/images/photos_icon.png" alt="minecraft photos" /></a>
			</div>
			<div style="position: relative; float: left; width: 50px;">
				<a href="<?=SITE_URL?>map" id="#expand-map" title="Show map" style="cursor: pointer;"><span style="margin-left: 7px; cursor: pointer;">Maps</span><br><img src="<?=SITE_URL?>assets/images/map-icon.png" alt="map" /></a>
			</div>
			<!--<div style="position: relative; float: left; width: 60px;">
				<a href="javascript:TINY.box.show({url:'<?=SITE_URL?>main/scores?playerid=<?=$_SESSION['pid']?>',width:700,height:600,opacity:30})" title="Scores" style="cursor: pointer;"><span style="margin-left: 8px; cursor: pointer;">Scores</span><br/><img src="<?=SITE_URL?>assets/images/awards-icon.png" alt="achievements" style="margin-left: 10px;" /></a>
			</div>-->
			<div style="position: relative; float: left; width: 60px;">
				<a href="<?=SITE_URL?>convo" title="Kiosk" style="cursor: pointer;"><span style="margin-left: 10px; cursor: pointer;">Kiosk</span><img src="<?=SITE_URL?>assets/images/kiosk-icon.png" alt="kiosk" style="margin-left: 6px; margin-top: 5px;"/></a>
			</div>
			<!--<div style="position: relative; float: left; width: 60px;">
				<a href="javascript:TINY.box.show({url:'<?=SITE_URL?>main/events',width:700,height:385,opacity:30})" title="Events" style="cursor: pointer;"><span style="margin-left: -3px; cursor: pointer;">Events</span><img src="<?=SITE_URL?>assets/images/events-icon.png" alt="events" style="margin-left: 0px; margin-top: 5px;"/></a>
			</div>-->
		</div>
	</div><div class="clear"></div>
<? } ?>
<? if(!empty($jobs)) { ?>
<h4 style="padding-bottom: 2px;">My Jobs</h4>
<table class="table1">
	<tr><th style="width: 90px;"><b>Name</b></th><th style="width: 48px;"><b>Level</b></th></tr>
	<?
	
	for($i=0; $i < sizeof($jobs); $i++) {
		
		$jobname = $jobs[$i]['job'];
	?>
	<tbody>
	<tr><td style="max-width: 40px;"><?=$jobname ?></td><td style="max-width: 20px;"><?=$jobs[$i]['level'] ?></td></tr>
	</tbody>
	<? } ?>
</table>
<? } ?><br />
<div style="border: 1px solid #CCC; padding: 5px;">
<div style="background: #EEE; padding: 5px;">
	<span style="margin-right: 11px; float: left;"><img src="<?=SITE_URL?>assets/images/awards-icon.png" alt="<?=$ownerLowerCase ?> awards" style="width: 30px; height: 30px;" /></span><span style="float: left;">My Awards</span><br/>
	<p>You currently do not have any awards</p>
</div>
</div><br /><br />
<div style="border: 1px solid #CCC; padding: 5px;">
<div style="background: #EEE; padding: 5px;">
	<a href="<?=SITE_URL?>gallery/photos/<?=$ownerLowerCase?>"><span style="margin-right: 11px; float: left;"><img src="<?=SITE_URL?>assets/images/myphotos-icon.png" alt="<?=$ownerLowerCase ?> photos" /></span>
	<span style="float: left; cursor: pointer;">My Photos</span><br/></a>
	<p>You currently do not have any photos. Click <a href="<?=SITE_URL?>gallery/photos/<?=$ownerLowerCase?>"><b>here</b></a> to upload some now.</p>
</div>
</div>
</div>
</div>
<? } ?>
