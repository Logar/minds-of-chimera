<h2 class="page_title">Learn to Play</h2>
<?php

	//$dom = new DOMDocument();
	//@$dom->loadHTMLFile('https://sites.google.com/a/cwingrav.com/moc-plugins/community-pages/learning-to-play');
	//$div = $dom->getElementById('sites-canvas-main-content');
	$div = '';
	
	function get_inner_html( $node ) { 
		$innerHTML= ''; 
		$children = $node->childNodes; 
		foreach ($children as $child) { 
			$innerHTML .= $child->ownerDocument->saveXML( $child ); 
		} 

		return $innerHTML; 
	}
	if(!empty($div)) {
		$content = get_inner_html($div);
		$clean_content = utf8_decode($content);
		
		echo $clean_content;
	} else {
?>
<div id="sites-canvas-main-content">
<table xmlns="http://www.w3.org/1999/xhtml" cellspacing="0" class="sites-layout-name-one-column sites-layout-hbox"><tbody><tr><td class="sites-layout-tile sites-tile-name-content-1"><div dir="ltr">Here are some basic tips on learning to play MineCraft.<div><br></div><div><ol><li>Don't click. Hold the mouse button down to break a block. Wait for the cracks to form and then you can get the block.</li><li>Night is scary and lasts 8 minutes. Hide in a village or fortified bunker to wait it out. Day lasts 20 minutes.</li><li>Place torches to avoid monsters spawning. Torches require coal and a stick.</li><li>Get wood from trees.&nbsp;Plant saplings to grow trees. Get saplings from leaves of trees. Ah, the circle of life.</li><li>Look up recipes online in the MineCraft wiki to learn how to create useful items.</li><li>Set the location you spawn when you die by right clicking in a bed. If you are the only person in a world, and it is night, you will wake up at daybreak.</li><li>There are three worlds: Leocius, Ercilis and the dreams of Herfinita. Each is a separate play style.</li></ol><div><br></div></div><div>Leocius</div><div>This is the main world. You can't get experience for killing things, but you can buy land and build cool things.</div><div><br></div><div>Ercilis</div><div>This is the punishing world. Monsters are tougher and you can't heal. But, you can get experience orbs.</div></div></td></tr></tbody></table>
</div>
<? } ?>
