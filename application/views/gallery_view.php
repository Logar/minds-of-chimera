<h2 class="page_title">Photos</h2>
<?
if(isset($owner)) {
	if($owner == $member) { 
?>

<p id="f1_upload_process" style="display: none;">Uploading...<br/><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></p>
<p id="img-result"></p>
<div id="image_upload_form_container">
<form name="image_upload_form" id="gallery_upload_form" action="<?=site_url('gallery/upload_gallery_img')?>" method="post" enctype="multipart/form-data">
	
	<div class="file-wrapper" style="width: 78px;">
		<div id="upload-gallery-img" class="button"><img src="<?=site_url('/assets/images/up-arrow.png') ?>" alt="up arrow" style="margin-left: -5px; padding-top: 3px; margin-right: 5px;" />
		Upload
		<input type="file" name="gallery_img" id="upload-img" style="width: 100px !important; height: 35px !important; position: absolute; top: 0; left: 0;" /></div>
	</div>
</form>
</div><div style="margin-bottom: 10px;"></div>
<? } 
} ?>

<div id="gallery" style="width: 100%; margin: 0 auto;">
<span id="status"></span>
	<?php
		if(is_array($gallery_records) && !empty($gallery_records)) {
			foreach($gallery_records as $row)
			{
				$photo_id = $row['photo_id'];
				$big_img = $row['main_img_src']; 
				$small_img = $row['thumbnail_img_src'];
				$photo_owner = $row['owner'];
				$date = $row['date_created'];
				$visibility = $row['visibility'];
				
				$row = '<div class="img-container"><div class="gallery-img-container-inner">';
				
				if(isset($owner)) {
					if($photo_owner == $member) {
						$row .= '<div class="edit-item-container"><div class="edit-photo-icon-container"><div class="edit-item-icon"></div></div>
						<div class="edit-item-inner">
						<a class="delete_photo" id="photo_' . $photo_id . '" >
						<img src="' . site_url('assets/images/delete_button.png') . '" alt="delete button" />Destroy!</a><br />
						<div class="clear"></div>
						<form name="permissions-form-'.$photo_id .'" id="permissions-form-'.$photo_id .'" action="#" method="post" enctype="multipart/form-data">';
						if($visibility == "everyone") {
							$row .= '<input type="radio" name="view_permissions_'.$photo_id .'" value="everyone" checked="checked" class="visibility-setting" />Public <br />';
						} else {
							$row .= '<input type="radio" name="view_permissions_'.$photo_id .'" value="everyone" class="visibility-setting" />Public <br />';
						}
						if($visibility == "private") {
							$row .= '<input type="radio" name="view_permissions_'.$photo_id .'" value="private" checked="checked" class="visibility-setting" />Private<br />';
						} else {
							$row .= '<input type="radio" name="view_permissions_'.$photo_id .'" value="private" class="visibility-setting" />Private<br />';
						}
						$row .= '</form></div></div>';
					}
				}
				
				$row .= '<a href="'. $big_img.'" target="'. $big_img.'" id="enlarge_photo_'.$photo_id.'" class="enlarge_photo" rel="lightbox[gallery]" title="Posted by '.$photo_owner. ' on '.$date.'"><img src="'. $small_img.'" alt="minds of chimera"></a><br />'. PHP_EOL;
				$row .= '</div></div>';
				echo $row;
			}
		}
	?>
</div>
