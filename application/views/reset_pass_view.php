<h2 class="page_title">Reset Your Password</h2><div class="lb"></div><br />
<form method="post" action="<?=SITE_URL?>reg/resetPassForm" class="wp-user-form">
	<div class="float-left">
		<label class="register_label" style="width: 120px;">New Password: </label>
		<input type="password" name="pass" value="" class="float-left" size="28" tabindex="1" />
	</div><div class="clear"></div>
	<div class="float-left">
		<label class="register_label" style="width: 120px;">Re-enter New Password: </label>
		<input type="password" name="pass2" value="" class="float-left" size="28" tabindex="2" />
	</div><div class="clear"></div>
	<input type="hidden" name="auth" value="<?=$auth?>" />
	<input class="reset-button" type="submit" class="float-left" style="margin-left: 125px;" value="Reset Password" tabindex="3" />
</form>
<div class="space_40"></div>
