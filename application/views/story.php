<h2 class="page_title">The Story</h2>

<div id="sites-canvas-main-content">
<table xmlns="http://www.w3.org/1999/xhtml" cellspacing="0" class="sites-layout-name-one-column sites-layout-hbox"><tbody><tr><td class="sites-layout-tile sites-tile-name-content-1"><div dir="ltr"><div><h2 style="color:black"><a name="TOC-You-caused-the-end-of-the-world..."></a>You caused the end of the world...</h2>
<p>The Chimera was powerful and kind. He formed a world where bravery, 
survival and creativity were one. But, troubles began - and the Chimera began spending more time away from the world he formed - until he vanished. You 
find yourself training as a citizen, but all is not right. Things are afoot and the docile citizens can not bring 
themselves to admit these problems. The world itself is under siege - stressed and fractured. And at its weakest, in your ignorance, you cause the final break, ending the once utopia. <br></p><p>Three worlds now exist where once sat peace and harmony:</p><ul><li><b>The lion world of Leocius</b> - steadfast and strong. Humanity has a foothold and you have a chance to rebuild it. Meet the remaining citizens and help them in their quests. Work hard, earn the rights to the land and build back what once was.</li></ul><ul><li><b>The snake world of Ercilis</b> - dangerous and cursed. It was lost to the hordes. Zombies pour into this world and no one can last for long. There is no quarter. Get in. Get out. Hopefully with resources badly needed in Leocius. Just survive.</li></ul><ul><li><b>The goat world of Herfinita</b> - only exists in dreams. When the world shattered, the goat world took the brunt of the force. Smashed. Only shards of this world remain, dreams of those that remember it. Some are wonderful, some clever, some dreadful. As the people rebuild the Chimera's world, their dreams become more real and plentiful. Perhaps the dreams will grow and fill the void?<br></li></ul>Now... now you wake in a small port city, not even large enough for a name. Get 
your bearings. Gather your resources. Talk to the citizens. You must put back together what was destroyed by your own hand. You
 must make things right. You must reform Chimera!<br><br><h2><a name="TOC-Who-We-Are-Looking-For"></a>Who We Are Looking For</h2>
 We are looking for dedicated Minecraft players ages 18+ that are mature and follow the rules. This is a research server and we are 
 dedicated to the study and development of creativity support tools. Minecraft is our platform we are currently developing on and every tool 
 and plugin we create is being released back to the millions of players in the community. <br><br><h2><a name="TOC-Server-Features"></a>
 Server Features</h2><ul><li><b>2 survival worlds</b>: A hard survival world, Ercilis, and a 
 <a href="http://dev.bukkit.org/server-mods/Residence/" target="_blank" rel="nofollow">Residence</a> protected world, Leocius, with NPCs, quests 
 and cities.</li><li><b>Custom Instancing Worlds</b>: What would you do if you could admin your own world? We created a plugin and are working 
 on end-user tools to support your creative efforts in developing individualized worlds to share with others to play through (and automatically 
 reset after they leave). Get in-game money for highly played instances.<br></li><li><b>Deeply Connected Website</b>: Social media, World Maps, 
 Picture Sharing, Player Stats, Achievements, etc.</li><li><b>Custom Plugins</b>: Lots of em to support creative ideas. Change world physics, 
 program a robot, launch spawned mobs through the air, track your game progress, rate builds of others, regenerate blocks, etc.<br></li>
 <li><b>Comments, Critique, Ratings and Feedback</b>: Custom plugin to support the communication of design ideas, design critique and the 
 development of a Minecraft pattern language.<br></li><li><b>Popular Plugins</b>: iConomy, <a href="http://dev.bukkit.org/server-mods/jobs/" 
 target="_blank" rel="nofollow">Jobs</a>, <a href="http://dev.bukkit.org/server-mods/lwc/" target="_blank" rel="nofollow">LWC</a> (chest and 
 door protection), <a href="http://dev.bukkit.org/server-mods/citizens/" target="_blank" rel="nofollow">Citizens</a> (NPCs and quests), 
 <a href="http://dev.bukkit.org/server-mods/signshop/" target="_blank" rel="nofollow">Sign Shop</a>, 
 <a href="http://dev.bukkit.org/server-mods/transporter/" target="_blank" rel="nofollow">Transporter</a>, 
 <a href="http://dev.bukkit.org/server-mods/treefeller/" target="_blank" rel="nofollow">TreeFeller</a><br></li><li>
 <b>Utility Plugins</b>: Spout, Achievements, mmoCore (multiple), MobHealth, HawkEye, Vault<br></li><li>
	 <b>Periodic Backups</b>: Our worlds files are backed up regularly and mirrored.</li><li><b>PVP</b>: Only in Ercilis if you dare.</li>
	 </ul></div><br><div><h2><a name="TOC-Server-Information">
	 </a>Server Information<br></h2><p><b>Server IP</b>:&nbsp; mindsofchimera.com&nbsp;&nbsp;&nbsp; <i>NOTE</i>: 
	 <i>Use the <a href="http://dev.bukkit.org/client-mods/spoutcraft/" target="_blank" rel="nofollow">Spoutcraft</a> 
	 client to connect</i>.<br><b>Website</b>:&nbsp; http://mindsofchimera.com<br><b>Email</b>:&nbsp; cwingrav+moc@gmail.com<br></p>
	 <p><br></p><h2><a name="TOC-Explanation-of-Research"></a>Explanation of Research</h2><ul><li>The purpose of this research is to support 
	 creativity in the game Minecraft and release the tools back to the community. As well:</li></ul><ul><ul><li>We intend to study how 
	 creative ideas start and spread in our community. </li></ul></ul><ul><ul><li>We intend to study the language used to describe creative 
	 ideas in the game Minecraft.</li></ul></ul><ul><li>If you partake in this research, you will be asked to play Minecraft on our server, as 
	 you would any other Minecraft server, and play by the rules stated in the game. We encourage you to play the creations of other gamers, 
	 review and discuss them on our website (http://mindsofchimera.com) and develop new creations yourself.</li></ul><ul>
		 <li>Our Minecraft server is open 24/7 to play, barring any outages for maintenance. 
		 The time commitment is up to you. We ask you to play as often as you like.</li></ul><ul><li>All communication in game and on the 
		 website is logged, to support our research effort and to enforce our rules of conduct. We are not opening up those logs to others 
		 but may use some parts in academic publications.</li></ul><ul><li>You must be 18 years of age or older to 
		 take part in this research study.</li></ul></div></div></td></tr></tbody></table>
</div>
