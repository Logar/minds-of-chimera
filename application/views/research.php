<h2 class="page_title">Our Research</h2>

<div id="sites-canvas-main-content">
<table xmlns="http://www.w3.org/1999/xhtml" cellspacing="0" class="sites-layout-name-one-column sites-layout-hbox"><tbody><tr><td class="sites-layout-tile sites-tile-name-content-1"><div dir="ltr"><h3><a name="TOC-1"></a><br>
<div style="display:block;text-align:left"><a href="https://sites.google.com/a/cwingrav.com/moc-plugins/website-data/about-the-research/mocinspiration.png?attredirects=0" imageanchor="1"><img border="0" src="https://sites.google.com/a/cwingrav.com/moc-plugins/_/rsrc/1332365440107/website-data/about-the-research/mocinspiration.png?width=500" width="500"></a></div>
<br>
</h3>
<h3><a name="TOC-Creativity-and-Inspiration"></a>Creativity and Inspiration<br>
</h3>
Creativity is "the ability to produce work that is both novel and appropriate" [1, p3.]. Creative people add thoughts and ideas to a society, change it and drive it forward [2]. In a society unafraid of ideas, creative people are the cause of societal advancement. For this reason, creativity is a field of study important to, among others: science, art, business, mathematics and engineering. Creativity is usually not taught in schools [2, p 256] but it is a learned process that can be enhanced [2, chapter 20] and a game environment can provides positive reinforcement to encourage this type of thinking.<br>
<br>
<div style="display:block;text-align:left"><a href="https://sites.google.com/a/cwingrav.com/moc-plugins/website-data/about-the-research/mocinspiration2.png?attredirects=0" imageanchor="1"><img border="0" src="https://sites.google.com/a/cwingrav.com/moc-plugins/_/rsrc/1332365683757/website-data/about-the-research/mocinspiration2.png?width=520" width="520"></a></div>
<br>
<br>
Inspiration is providing the knowledge, motivation and an environment to be creative. Specifically, we inspire creativity by:<br>
<br>
1) teaching physics domain knowledge and skills to create quests<br>
2) motivating by example quests and fun educational content<br>
3) an environment easy build in and share creations<br>
4) simply asking them to be creative (which does impact)<br>
<br><h3><a name="TOC-A-Minecraft-Pattern-Language"></a>A Minecraft Pattern Language</h3>There is no common language by the players to support the discussion of the numerous creative ideas and constructs that exist in Minecraft. Is a design language based upon Christopher Alexander's Pattern Language: <br>1) found useful by Minecraft players in terms of discussions and player interactions in-game and on the server; and will it <br>2) lead to more creative works as evidenced by creativity scores, elements used in creative works and usage of patterns after exposure to the pattern.<br><br><b>Objective</b>: Develop a Minecraft pattern language, based upon Christopher Alexander's Architectural Patterns, and demonstrate its use by our players, impact on creative works and inspiration to our players.<br><b>Approach</b>: Create a plugin to create patterns, demonstrate them, rate and critique them. Create social interactions and discussions that will enable players to use this language.<br><br><br><h3><a name="TOC-References"></a>References</h3>
[1] Handbook of Creativity edited by R. J. Sternberg, Cambridge University Press, 1999.<br>
[2] Florida, R. The Rise of the Creative Class and How It's Transforming Work, Leisure,<br>
Community and Everyday Life. Basic Books, 2002.<br>
<br>
<br></div></td></tr></tbody></table>
</div>
