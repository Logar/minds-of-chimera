<h2 class="page_title">Installing the Game</h2>
<?php

	//$dom = new DOMDocument();
	//@$dom->loadHTMLFile('https://sites.google.com/a/cwingrav.com/moc-plugins/community-pages/getting-started');
	//$div = $dom->getElementById('sites-canvas-main-content');
	$div = '';
	
	function get_inner_html( $node ) { 
		$innerHTML= ''; 
		$children = $node->childNodes; 
		foreach ($children as $child) { 
			$innerHTML .= $child->ownerDocument->saveXML( $child ); 
		} 

		return $innerHTML;
	}
	if(!empty($div)) {
		$content = get_inner_html($div);
		$clean_content = utf8_decode($content);
		
		echo $clean_content;
	} else {
?>
<div id="sites-canvas-main-content">
<table xmlns="http://www.w3.org/1999/xhtml" cellspacing="0" class="sites-layout-name-one-column sites-layout-hbox"><tbody><tr><td class="sites-layout-tile sites-tile-name-content-1"><div dir="ltr"><h2><a name="TOC-Get-a-Login"></a>Get a Login</h2>
<div>Buy an account from Mojang: <a href="http://www.minecraft.net/store" target="_blank" rel="nofollow">go here</a>.<br>
</div>
<br>
<h2><a name="TOC-Get-the-Spoutcraft-Client"></a>Get the Spoutcraft Client</h2>
<div>
<div class="sites-codeblock sites-codesnippet-block"><code>Why Spoutcraft? We use the Spoutcraft client as it add extra functionality to game modders. It is a 100% replacement to the Minecraft client.</code></div>
</div>
<div>
<ol><li>Find the latest client here: <a href="http://get.spout.org" target="_blank" rel="nofollow">Download "Spoutcraft" for your operating system</a></li>
<li>Run the Spoutcraft.jar file.</li>
</ol><div><br></div>
<h2><a name="TOC-Run-the-Client"></a>Run the Client</h2>
<ol>
<li>Login with your new account.</li>
</ol><div><br></div>
<h2><a name="TOC-Select-Minecraft-Version-1.4.5"></a>Select Minecraft Version 1.4.5</h2>
<ol>
<li>Select the upper right configuration icon</li>
<li>Set it to use Minecraft version 1.4.5.</li>
</ol>
<div>
<div style="display:block;text-align:left"><a href="https://sites.google.com/a/cwingrav.com/moc-plugins/community-pages/getting-started/Screen%20Shot%202013-01-13%20at%202.09.36%20PM.png?attredirects=0" imageanchor="1"><img border="0" height="100" src="https://sites.google.com/a/cwingrav.com/moc-plugins/_/rsrc/1358104278962/community-pages/getting-started/Screen%20Shot%202013-01-13%20at%202.09.36%20PM.png?height=51&amp;width=320" width="320"></a></div>
</div>
<div></div><div class="sites-codeblock sites-codesnippet-block"><div><code>NOTE: Our server uses 1.4.5 for compatibility reasons with Bukkit and Spout. This changes periodically so if you have trouble logging in, check the server notes for the current version.</code></div></div><p><br></p><h2><a name="TOC-Connect-to-the-Server"></a>


Connect to the Server
</h2><ol><li>Select multiplayer</li>
</ol>
</div>
<blockquote style="margin:0pt 0pt 0pt 40px;padding:0px;border:currentColor">
<div>
<div style="text-align:left;display:block"><a href="https://sites.google.com/a/cwingrav.com/moc-plugins/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.34.32%20PM.png?attredirects=0" imageanchor="1"><img border="0" height="246" src="https://sites.google.com/a/cwingrav.com/moc-plugins/_/rsrc/1314733843049/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.34.32%20PM.png?height=123&amp;width=200" width="200"></a></div>
<br>
</div>
</blockquote>
<div>&nbsp; &nbsp; &nbsp;5. Select "Add a Server"</div>
<blockquote style="margin:0pt 0pt 0pt 40px;padding:0px;border:currentColor">
<div>
<div style="text-align:left;display:block"><a href="https://sites.google.com/a/cwingrav.com/moc-plugins/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.34.14%20PM.png?attredirects=0" imageanchor="1"><img border="0" height="150" src="https://sites.google.com/a/cwingrav.com/moc-plugins/_/rsrc/1314733843049/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.34.14%20PM.png?height=75&amp;width=400" width="400"></a></div>
<br>
</div>
</blockquote>
<div><br>
</div>
<div>&nbsp; &nbsp; &nbsp;6. Enter the server IP: isue-server.eecs.ucf.edu and enter a name for it, "ISUE" or "isue-server". Hit "Add".</div>
<blockquote style="margin:0pt 0pt 0pt 40px;padding:0px;border:currentColor">
<div>
<div style="text-align:left;display:block"><a href="https://sites.google.com/a/cwingrav.com/moc-plugins/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.37.38%20PM.png?attredirects=0" imageanchor="1"><img border="0" height="261" src="https://sites.google.com/a/cwingrav.com/moc-plugins/_/rsrc/1314733843049/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.37.38%20PM.png?height=261&amp;width=320" width="320"></a></div>
</div>
</blockquote>
<div><br>
</div>
<div><br>
</div>
&nbsp; &nbsp; &nbsp;7. Double click the server to login:
<blockquote style="margin:0pt 0pt 0pt 40px;padding:0px;border:currentColor">
<div>
<div style="text-align:left;display:block"><a href="https://sites.google.com/a/cwingrav.com/moc-plugins/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.38.46%20PM.png?attredirects=0" imageanchor="1"><img border="0" height="60" src="https://sites.google.com/a/cwingrav.com/moc-plugins/_/rsrc/1314733843050/community-pages/getting-started/Screen%20shot%202011-08-30%20at%203.38.46%20PM.png?height=60&amp;width=320" width="320"></a></div>
</div>
</blockquote>
<div>
<p><br>
</p>
</div></div></td></tr></tbody></table>
</div>
<? } ?>
