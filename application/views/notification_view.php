<?
	function tokenTruncate($string, $your_desired_width) {

		$parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
		$parts_count = count($parts);
		$length = 0;
		$last_part = 0;

		for (; $last_part < $parts_count; ++$last_part) {
			$length += strlen($parts[$last_part]);
			if ($length > $your_desired_width) { break; }
		}
		return implode(array_slice($parts, 0, $last_part));
	}
	$notify_arr = $old_notifications;
	$arr_size = sizeof($notify_arr);
	
	if($arr_size !== 1) {
	
		$posts = '';
		$posts = '<div id="notification-posts-top">';
		for($i=0; $i < $arr_size - 1; $i++) {
			$post_id = $notify_arr[$i]['post_id'];
			$comment_id = $notify_arr[$i]['comment_id'];
			$date = $notify_arr[$i]['date'];
			
			if($i == 0) {
				$full_date = $notify_arr[$i]['full_date'];
			}
			
			if($post_id == null || empty($post_id)) {
				$post_id = $comment_id;
			}
			
			$post_content = $notify_arr[$i]['content'];
			$len = strlen($post_content);
			
			if($len > 60) {
				$post_content = tokenTruncate($post_content, 60) . '...';
			}
			$profile_img_src = $notify_arr[$i]['profile_img_src'];
			$author = ucwords($notify_arr[$i]['post_author']);
			$profile_path = $notify_arr[$i]['profile_path'];
			
			$posts .= '<div class="list_item notification_'.$post_id.'"><a href="'.site_url('/single/notify?postid='.$post_id).'" class="no-hover-color">
			<div class="row clearfix"><div class="col_7"><img src="'.$profile_img_src .'" class="post_profile_img" alt="profile_image" /></div>
			<div class="col_29"><b>'.$author.'</b>';
			
			if ($notify_arr[$i]['type'] == "comment") {
			
				$posts .= ' commented on your post: '; 
			} else if($notify_arr[$i]['type'] == "post") {
				$posts .= ' wrote on your profile: '; 
			} else if($notify_arr[$i]['type'] == "like") {
				$posts .= ' liked your post: '; 
			}
			$posts .= '"'.$post_content.'"<br /><span class="color-666 font-11">'.$date.'</span></div></div></div><div class="clear"></div><div class="lb lb-bottom"></div>';
		}
		$posts .= '<input type="hidden" class="last-notification-date" value="'.$full_date.'" />';
		$posts .= '</div>';
		echo $posts;
	} else {
		echo '<div id="notification-posts-top"><div id="no-notifications-text">You have no notifications yet</div><div class="last-notification-date" class="hide">1976-02-29 00:00:00</div></div>';
	}
?>