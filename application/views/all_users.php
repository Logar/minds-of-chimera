<div class="clearfix">
<?
	if(isset($sub_page)) {
		
		if($sub_page == "Members") {
			$next_page = "guests";
		} else if($sub_page == "Guests") {
			$next_page = "members";
		}
		$cont = '';
		$cont .= '<div style="padding: 5px; float: left; background: #FFF8C6; margin-bottom: 10px; margin-right: 15px;">
		<a href="'.SITE_URL.'all_users/members" style="float: left; font-weight: bold; font-size: 18px; color: #000;">
		<img src="'.SITE_URL.'assets/images/single-star.png" alt="single star" style="float: left; padding-right: 3px;" />Members</a>
		</div>';
		$cont .= '<div style="padding: 5px; float: left; background: #eee; margin-bottom: 10px;">
		<a href="'.SITE_URL.'all_users/guests" style="float: left; font-weight: bold; font-size: 18px; color: #000;">Guests</a>
		</div>';

	} else {
		$cont .= '<div style="padding: 5px; float: left; background: #eee; margin-bottom: 10px;">
		<a href="'.SITE_URL.'all_users/members" style="float: left; font-weight: bold; font-size: 18px; color: #000; ">
		<img src="'.SITE_URL.'assets/images/single-star.png" alt="single star" style="float: left;" />Members</a></div>';
	}
	$cont .= '<br /><div class="clear"></div>';
	echo $cont;
	
?>
<div id="all-users">
	<div id="all-users-content">
	<? 
		if(!empty($errorMsg)) { 
			echo $errorMsg;
		} 
		else {

			for($i=0; $i < sizeof($users_array); $i++) {
				if(($sub_page == "Guests" && $users_array[$i]['user_status'] == NULL) || 
				($sub_page == "Members" && $users_array[$i]['user_status'] !== NULL)) {

					$name = $users_array[$i]['user_login'];
					$profile_img_src_small = $users_array[$i]['profile_img_src_small'];
					$profile_path = $users_array[$i]['profile_path'];
					$last_login = $users_array[$i]['last_login'];
					
					if(empty($profile_img_src_small)) {
						$profile_img_src_small = site_url('assets/images/profile/steve_avatar_small.png');
					}
					if(empty($profile_path)) {
						$profile_path = site_url('profile/'.$name);
					}
	?>
		<div>
			<div style="float: left; padding-right: 10px;">
			<a href="<?=$profile_path ?>"><img src="<?=$profile_img_src_small ?>" class="post_profile_img" /></a>
			</div>
			<span style="float: left; text-align: left;"><a href="<?=$profile_path ?>"><b><?=$name ?></b></a></span>
			<span style="float: right; padding-right: 5px;">Last Server Login: <?=$last_login?></span><br />
		</div>
	<?
			}

		}

		} 
	?>
		<div id="all-users-bottom">
		<p id="all-users-loader" style="display: none;"><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></p>
			<ul>
				<!-- Hijack this link for the infinite scroll -->
				<!--<li class="more"><a href="." title="Traditional navigation link">Next Page</a></li>-->
			</ul>
		</div>
	</div>
</div>
</div>