<h2>Welcome to the official website for the Minds of Chimera Minecraft server!</h2>

<b>Proud to state</b>: This is one of the most advanced Minecraft servers available. Multiple developers are currently creating new ways to play the game. This server is about creativity and advancing the Minecraft styles of play. Be a part of it.<br />
<div style="margin:0 auto;display:block;text-align:center">
	<img alt="chimera" src="http://mindsofchimera.com/assets/images/chimera.png" style="display:block;margin:5px auto;text-align:center" /></div>
<br />
<b>Server</b>: mindsofchimera.com<br />
<b>Website</b>: <a href="http://mindsofchimera.com" target="_blank" rel="nofollow">http://mindsofchimera.com</a><br />
<span ><br />
*** <b>Just Opening to Builders!</b> --- so not much activity, but lots of features!<br />
     ** Use the social features to send us a note about when you will be on. We will keep an eye out for you!<br />
<br />
</span>
<h3><a name="TOC-Features:"></a><span >Features:</span></h3>
<ul><li><span >Custom website displays in-game activity and provides social media.</span></li>
<li>Residence, Jobs, Citizens, Spout, Transporter, mmoCore, etc..</li>
<li>CUSTOM BUKKIT PLUGINS:</li>
<ul><li>CodeBlocks - In-game robot programming.</li>
<li>MOCGoodEats - Nutrition-aware food intake for advanced performance.</li>
<li>MOC3DImporter - Imports 3D objects in-game through a 3D scan line algorithm</li>
<li>MOCDreamCrafter - Instancing, admin-like powers to custom create worlds</li>
<li>Others: Kiosks, Rater, Researcher, Fizziks, GravitySheep, etc.<br />
</li></ul></ul>
<span ><br />
Like the plugins? Download them yourself. We are pushing the limits of Minecraft and want people to join in!<br />
<br />
We use the flexible and engaging world of Minecraft to  support three play styles, player created content and instances of play.  Learn all about the project <a href="<?=SITE_URL?>research"><b>here</b></a>, or log in if you're playing to check updates and stats!</span>
