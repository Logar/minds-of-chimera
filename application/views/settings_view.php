<h2 class="page_title">Account Settings</h2>
<ul class="settings-list">
	<li><div class="clearfix"><span class="settings-label"><b>Name</b></span><?=$name?><a href="#" class="edit">Edit</a></div></li>
	<li><div class="clearfix"><span class="settings-label"><b>Username</b></span><?=$userName?></div></li>
	<li><div class="clearfix"><span class="settings-label"><b>Email Address</b></span><?=$email?><a href="#" class="edit">Edit</a></div></li>
	<li><div class="clearfix"><span class="settings-label"><b>Password</b></span><a href="#" class="edit">Edit</a></div></li>
	<li><div class="clearfix"><span class="settings-label"><b>Email Notifications</b></span><a href="#" class="edit">Edit</a></div></li>
</ul>
<div style="margin-bottom: 150px;"></div>
