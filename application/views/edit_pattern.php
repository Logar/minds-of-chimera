<div style="max-width: 500px; word-wrap:break-word;">
<h1 class="page_title" style="float: left;"><?=$patterns[0]['name']?></h1><?=$patterns[0]['score']?></div>
<span style="float: right;">
<a href="<?=site_url('mocrater/display/'.$patterns[0]['id']) ?>">
<img src="<?=site_url('assets/images/back-button.png')?>" alt="back button" />&nbsp;back</a></span>
	by <a href="<?=site_url('profile/'.$patterns[0]['author'])?>"><?=$patterns[0]['author']?></a> on <?=$patterns[0]['createdon']?>
	<? 
		if(isset($_GET['resp'])) {
			if($_GET['resp'] == "failed") {
				echo '<br /><br /><div class="error-box">An error occurred while trying to update the pattern. Please try again.</div><br />';
			}
		} else {
			echo '<br /><br />';
		}
	?>

	<form enctype="multipart/form-data" method="post" action="<?=site_url('mocrater/update_pattern/'.$patterns[0]['id'])?>">
	<div class="clearfix" style="width: 340px; margin-right: 7px; float: left;">
		<h2>Context:</h2>
		<textarea id="limit-textarea-1" class="edit-pattern-textarea" name="context"><?=$patterns[0]['context'] ?></textarea>
		<div class="chars-left"><b><span id="chars-left-1"></span></b> characters remaining.</div>
		<h2>Problem:</h2>
		<textarea id="limit-textarea-2" class="edit-pattern-textarea" name="problem"><?=$patterns[0]['problem'] ?></textarea>
		<div class="chars-left"><b><span id="chars-left-2"></span></b> characters remaining.</div>
		<h2>Solution:</h2>
		<textarea id="limit-textarea-3" class="edit-pattern-textarea" name="solution"><?=$patterns[0]['solution'] ?></textarea>
		<div class="chars-left"><b><span id="chars-left-3"></span></b> characters remaining.</div>
		<h2>Related Patterns:</h2>
		<select multiple="multiple" class="mocrater-select" name="related-patterns[]">
			<?
				for($i=0; $i<sizeof($relatedPatterns); $i++) {
					
					if(!empty($currPatterns)) {
						$found = '';
						$found = array_search($relatedPatterns[$i]['name'], $currPatterns);
						if(!empty($found)) {				
							echo '<option class="mocrater-option" value="'.$relatedPatterns[$i]['id'].'" selected="selected"><b>'.$relatedPatterns[$i]['name'].'</b></option>';
						} else {
							echo '<option class="mocrater-option" value="'.$relatedPatterns[$i]['id'].'"><b>'.$relatedPatterns[$i]['name'].'</b></option>';
						}
					} else {
						echo '<option class="mocrater-option" value="'.$relatedPatterns[$i]['id'].'"><b>'.$relatedPatterns[$i]['name'].'</b></option>';
					}
				}
			?>
		</select>
	</div>
	<div style="float: left;">
		<div class="img-container">
			<div class="mocrater-img-container-inner">
			<? 
				if(empty($patterns[0]['screen_src'])) {
					echo '<div style="text-align: center; margin: 0 auto; padding-top: 80px;"><h3 style="color: #888;">No Image</h3></div>';
				} else {
					if(file_exists('assets/images/profile/'.$patterns[0]['screen_src'])) {
						echo '<a href="'.site_url('assets/images/profile/'.$patterns[0]['screen_src']).'" target="'.site_url('assets/images/profile/'.$patterns[0]['screen_src']).'" rel="lightbox"><img src="'.site_url('assets/images/profile/'.$patterns[0]['screen_src']).'" alt="" style="width: 240px; height: 200px;" /></a>';
					} else {
						echo '<div style="text-align: center; margin: 0 auto; padding-top: 80px;"><h3 style="color: #888;">No Image</h3></div>';
					}
				}
			?>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="space_20"></div><span style="float: right;"><a href="<?=site_url('mocrater/display/'.$patterns[0]['id'])?>">cancel</a>&nbsp;&nbsp;&nbsp;<input type="submit" value="Save" /></span>

	</form>
