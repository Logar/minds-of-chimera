<iframe src="http://mindsofchimera.com:8123" width="100%" height="100%" id="moc-map"></iframe>

<script type="text/javascript">
	var buffer = 20; //scroll bar buffer
	var iframe = document.getElementById('moc-map');
	
	function pageY(elem) {
	    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
	}
	
	function resizeIframe() {
	    var height = document.documentElement.clientHeight;
	    height -= pageY(document.getElementById('moc-map'))+ buffer ;
	    height = (height < 0) ? 0 : height;
	    document.getElementById('moc-map').style.height = height + 'px';
	}
	
	// .onload doesn't work with IE8 and older.
	if (iframe.attachEvent) {
	    iframe.attachEvent("onload", resizeIframe);
	} else {
	    iframe.onload=resizeIframe;
	}
	
	window.onresize = resizeIframe;
</script>
