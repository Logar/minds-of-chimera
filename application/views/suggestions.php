<h2 class="page_title">Suggestions</h2>
<?php

	$dom = new DOMDocument();
	@$dom->loadHTMLFile('https://sites.google.com/a/cwingrav.com/moc-plugins/issue-log');
	$div = $dom->getElementById('sites-canvas-main-content');
	
	function get_inner_html( $node ) { 
		$innerHTML= ''; 
		$children = $node->childNodes; 
		foreach ($children as $child) { 
			$innerHTML .= $child->ownerDocument->saveXML( $child ); 
		} 

		return $innerHTML; 
	}
	if(!empty($div)) {
		$content = get_inner_html($div);
		$clean_content = utf8_decode($content);
		
		echo $clean_content;
	}
?>
