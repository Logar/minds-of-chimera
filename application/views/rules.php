<h2 class="page_title">Rules of Conduct</h2>
<?php

	//$dom = new DOMDocument();
	//@$dom->loadHTMLFile('https://sites.google.com/a/cwingrav.com/moc-plugins/website-data/rules-of-conduct');
	//$div = $dom->getElementById('sites-canvas-main-content');
	$div = '';
	
	function get_inner_html( $node ) { 
		$innerHTML= ''; 
		$children = $node->childNodes; 
		foreach ($children as $child) { 
			$innerHTML .= $child->ownerDocument->saveXML( $child ); 
		} 

		return $innerHTML; 
	}
	if(!empty($div)) {
		$content = get_inner_html($div);
		$clean_content = utf8_decode($content);
		
		echo $clean_content;
	} else {
?>
<div id="sites-canvas-main-content">
<table xmlns="http://www.w3.org/1999/xhtml" cellspacing="0" class="sites-layout-name-one-column sites-layout-hbox"><tbody><tr>
	<td class="sites-layout-tile sites-tile-name-content-1">This is a research project and we operate within the rules of approved human testing by the University of Central Florida. For these reasons, you must be 18 years of age or older to play. If you have any questions about this research and its observation of your activities on this server, please see <a href="<?=SITE_URL ?>research" rel="nofollow">About the Research</a>.<br>
<br>
<h2><a name="TOC-General"></a>General</h2>
<ul><li>Use common courtesy and decency:</li>
<ul><li>Keep in the spirit of the game. Notice a bug? Let us know on the website.</li>
<li>Use chat responsibly! Spam, excessive Caps, profanity, and racism will be met with zero tolerance.</li>
<li>Lewd/profane/vulgar skins/builds (art or otherwise) are not welcome on this server. Use common sense!</li>
<li>Not everyone appreciates a joke or prank. Ask before you teleport.</li></ul></ul>
<ul><li>No "Griefing". <br>
</li>
<ul><li>Report griefing to the admins.</li>
<li>We have many tools to repair damage and backups to revert to. <br>
</li></ul></ul>
<ul><li>No unauthorized mods or hacks</li>
<ul><li>We DO NOT allow any Mod/Glitch/Hack that gives you an advantage over 
other players. <br>
</li>
<li>Ex: game exploits, flying, smart move, item spawning, 
duping blocks, speed hacking, X-Ray mods or X-ray-texturepacks.</li></ul></ul>
<ul><li>And in general...</li>
<ul><li>Do not ask to be an OP and please do not pester the mods/admins.</li>
<li>Do not ask for blocks or tools or items. <br>
</li>
<li><span style="color: red; font-weight: bold;">Do not advertise any other servers using our website without first getting our permission. We will ban you.</span></li>
<li>Follow the direction of the Admins. If you have an issue, please submit a report on the <a href="<?=SITE_URL ?>" rel="nofollow">website</a>.<br>
</li></ul></ul>
<h2><a name="TOC-Leocius"></a>Leocius</h2>
<ul><li>Claim land with residence (see the /infoguide) and build on it.</li>
<li>Consider others when you place your constructions. Allow them room to grow.</li>
<li>Do not destroy other people's work.<br>
</li>
<li>Land that is no longer owned is free to purchase. Land leases expire 30 days after the last login.<br>
</li></ul>
<h2><a name="TOC-Ercilis"></a>Ercilis</h2>
<ul><li>Just survive.<br>
</li></ul>
<h2><a name="TOC-Herfinita"></a>Herfinita</h2>
<div>
<ul><li>Do NOT destroy people's Origin worlds. This is banable.</li></ul>
<ul><li>Creating an Instance:<br>
</li>
<ul><li>Create with all the tools available to you<br>
</li>
<li>
Put it online when it is ready</li></ul></ul>
<ul><li>Playing an Instance:</li>
<ul><li>Enjoy other people's work. <br>
</li>
<li>Give good constructive comments and criticism.</li>
<li>Identify patterns and create new patterns. <br>
</li></ul></ul>
</div>
</div></div></td></tr></tbody></table>
</div>
<? } ?>
