<h2 class="page_title">Plugin Information</h2>
<br />
<div id="sites-canvas-main-content">
<h2>GravitySheep</h2>
<div>
<h3>Description</h3>
<p>GravitySheep allows players to set up blocks in the world as regions and then assign entity type as well as velocity with which that entity will be launched from that block. Create machine guns that shoot sheep or zombies at your enemies with a push of a button or through red stone wiring!</p>
<h3>Commands</h3>
<p>/gs select - toggles selection mode on/off</p>
<p>/gs listpoint - list your current selection</p>
<p>/gs create [name] - creates a new region using the selected points</p>
<p>/gs update [name] - update the bounds of the region to the selected bounds</p>
<p>/gs remove [name] - removes the region</p>
<p>/gs setvel [name] [x] [y] [z] - sets the velocity for the region</p>
<p>/gs addvel [name] [x] [y] [z] - adds to the region's velocity</p>
<p>/gs addswitch/removeswitch/clearswitch/listswitch [name] - power switches. These will be used as triggers for the region.</p>
<p>/gs addsign/removesign/clearsign/listsign [name] - sign operations. These will display settings for the region.</p>
<p>/gs entitytype [name] [type] (block id) (block data) - sets entity type for the region. Id and value used by FALLING_BLOCK ty[e</p>
<p>/gs list - lists regions</p>
<p><br>
</p>
<h2>MOCFizziks</h2></p>
<h3>Description</h3>
</div>
<div><span >MOCFizziks allows users to create regions that apply either velocity or acceleration or both to players in those regions</span></div>
<div><span >- Create cuboid regions with assigned velocity and/or acceleration that is applied to the players after they enter the region.</span><br >
<span >- Ability to turn region's power on/off via commands or with level/button/pressure plate (note: any lever/button/plate in the region will toggle power)</span><br >
<span >- Velocity and acceleration are defined in X Y Z format (Y is vertical). Value of one create a fairly high velocity so it may be more useful to use fractions. For example, 0.1 0 0 would create a fairly good push in positive X direction to the player, while 1 0 0 will basically throw the player.</span></div>
<div>
<h3>Commands</h3>
<span >/mf select - toggles select mode on/off</span><br >
<span >/mf listpoints - list your current selection</span><br >
<span >/mf create [name] - creates a new region using the selected points</span><br >
<span >/mf update [name] - update the bounds of the region to the selected bounds</span><br >
<span >/mf remove [name] - removes the region</span><br >
<span >/mf setvel [name] [x] [y] [z] - sets the velocity for the region</span><br >
<span >/mf addvel [name] [x] [y] [z] - adds to the region's velocity</span><br >
<span >/mf setacc [name] [x] [y] [z] - sets the acceleration for the region</span><br >
<span >/mf addacc [name] [x] [y] [z] - adds to the region's acceleration</span><br >
<span >/mf toggle [name] - toggles region on/off</span><br >
<span >/mf togglepower [name] - toggles use of power switches for the region on/off</span><br >
<span >/mf addswitch/removeswitch/clearswitch/listswitch - power switches</span><br >
<span >/mf addsign/removesign/clearsign/listsign - sign operations</span><br >
<span >/mf info - Prints out info about the currently selected region</span><br >
<span >/mf list - lists regions</span></div>
<div><span ><br>
</span></div>
<div><h2>MOCKiosk</h2></div>
<div>
<div>
<h3>Description</h3>
</div>
<div><span >MOCKiosk allows for a placement of kiosk blocks throughout the world that can shout out a predefined message as the user approaches, display message as the user clicks the block, or display a GUI window with text and an attached image from an external website.</span></div>
<div>
<h3>Commands</h3>
<span >/kiosk edit|delete|tp [id] - Edit|Delete|Teloport to Kiosk.</span><br >
<span >/kiosk browse - Browse Kiosks in the area.</span><br >
<span >/kiosk reload - Reloads the configuration file for the plugin.</span></div>
</div>
<div><span ><br>
</span></div>
<div>
<h3>Use</h3>
</div>
<div><span >Once you have Kiosk block, place it where you would like to create a kiosk and a GUI window with information. &nbsp;Click on your kiosk with the Convo item (red circle) to edit the kiosk settings.</span></div>
<div><span ><br>
</span></div>
<div><span >To create conversations on the website go to&nbsp;</span><a href="http://mindsofchimera.com/convo" >http://mindsofchimera.com/convo</a></div>
<div><br>
</div>
<div><br>
</div>
<div><h2>CodeBlocks</h2>
<div>
<h3>Description</h3>
<p>CodeBlocks allows user to control robots through different functions user wrote using special custom blocks.<br>
Please report any issues as well as suggestions here.<br>
</p>
<p >Each user has their own function space where they can add as many functions as they wants which can call each other or even themselves for recursion, as well as, a fleet of robots that can execute these functions. Multiple robots can run same function or each can run their own.</p>
<p ><strong >You can try out latest build on my server at server.aiwing.org</strong>&nbsp;I might be around to answer any questions etc as well</p>
<h3>Commands</h3>
<span >/cb - Opens main GUI window from which you can go to Functions, Robots, or Blocks windows.</span>
<span >/cbf - Opens Functions GUI</span>
<span >/cbr - Opens Robots GUI</span></div>
<div><span ><br>
</span></div>
<div><span >/cbb - Opens Blocks GUI - you can get custom blocks here while in any mode creative or survival.</span></div>
<div><font color="#141414" face="Trebuchet MS, Helvetica, Arial, sans-serif"><span ><br>
</span></font><span >/cbb all - will drop all possible code blocks in stacks of 64 around the player.</span></div>
<div><font color="#141414" face="Trebuchet MS, Helvetica, Arial, sans-serif"><span ><br>
</span></font><span >Pressing 'C' will open main GUI as well.</span></div>
<div><font color="#141414" face="Trebuchet MS, Helvetica, Arial, sans-serif"><span ><br>
</span></font><span >While placing any of the group blocks you will notice that all of the choices have numbers next to them. Pressing matching number on keyboard while block selector window is open will instantly place the block in the world.</span></div>
<div><br>
</div>
<div>
</div>
<div>
<h3>Use</h3>
</div>
<div><br>
</div>
<div>
<p >To get the blocks, you can use /cbb command.</p>
<p >Once you are set with blocks you start by placing a Function block down to begin a new function. When function is complete. Click the function block with Tool block to compile it. At this point you can assign it to robots. To do that, place a robot somewhere in the world, a GUI interface to control robot will appear. On the left side you will see a list of functions you have written, select one and press Run and the robot will begin executing the function.</p>
</div>
</div>
</div>
<br />
<h2>RegenBlock</h2>
<p>Define regions that will be automatically restored to their original state after a set amount of time.</p><br />

<p>To create a region issue a "/rb select" command. This will put you into selection mode.</p><br />

<p>Next, select two blocks that will be the boundaries of the region (you are selecting a cube and the blocks selected will be the opposite corners of it). After two blocks are selected with left and right clicks, issue a command "/rb region create [region name] [respawn time]".</p><br />

<p>This will create a region with the region name and re-spawn time you've specified. At this point you can type "/rb select" again to exit the selection mode.</p>
<br />
/rb list - Lists player's current selection<br />
/rb blacklist add/remove (id id id ...) - adds/removes supplied block IDs<br />
/rb reload - restores all blocks in queue and reloads the configuration file<br />
/rb select - Starts/stops player's selection mode<br />
/rb region list - lists current regions<br />
/rb region create (name) [re-spawn time] - assigns selection to a region<br />
/rb region remove (name) - removes region from the list<br />
/rb region type [typeId] - sets region type, 0 for normal, 1 for mine.<br />
/rb region sync [0/1] - sets region to regen all at once or not, 0 for not, 1 for yes.<br />
/rb region modify (name) [re-spawn time] - modify existing region<br />
/rb region modify time (name) (re-spawn time) - modify existing region's re-spawn time<br />
/rb region blacklist (name) add/remove (id id id ...) - adds/removes supplied block IDs for region<br />
/rb region feedback (name) (type) - sets feedback type for the region. 0 - none, 1 - on place, 2 - on place/remove<br />
/rb region feedback set (string) - sets string sent to player during region feedback. Use TIME to show re-spawn time.<br /><br />

For more information visit <a href="http://dev.bukkit.org/server-mods/regenblock/pages/main/">http://dev.bukkit.org/server-mods/regenblock/pages/main/</a><br />
