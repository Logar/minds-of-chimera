<div>This work is being performed out of the <a href="http://www.eecs.ucf.edu/isuelab/" target="_blank">Interactive Systems and User Experience (ISUE) Lab</a> at the <a href="http://www.ucf.edu" target="_blank">University of Central Florida</a>.
</div><br /><br />
<table>
<tr>
<td>
<div class="about-image-container">
<a href="http://www.eecs.ucf.edu/isuelab/people/jjl.php">
<div class="outer-center"><div class="inner-center"><img alt="Joseph J. LaViola Jr." src="http://www.eecs.ucf.edu/isuelab/images/photos/joe.jpg" class="about-image" /></div></div><div class="clear"></div>
Dr. Laviola Jr.</a></div>
</td>
<td>
<div class="about-image-container">
<a href="http://www.eecs.ucf.edu/isuelab/people/chad.php">
<div class="outer-center"><div class="inner-center"><img alt="Chadwick Wingrave" src="http://www.eecs.ucf.edu/isuelab/images/photos/chad.jpg" class="about-image" /></div></div><div class="clear"></div>
Dr. Wingrave</a></div>
</td>
</tr>
</table>

<br /><br />

<table>
<tr>
<td>
<div class="about-image-container">
<a href="http://www.eecs.ucf.edu/isuelab/people/ross.php">
<div class="outer-center"><div class="inner-center"><img alt="Ross Arena" src="http://www.eecs.ucf.edu/isuelab/images/photos/rossa.jpg" class="about-image" /></div></div><div class="clear"></div>
Ross A.</a></div>
</td>
<td>
<div class="about-image-container">
<a href="http://www.eecs.ucf.edu/isuelab/people/michael.php">
<div class="outer-center"><div class="inner-center"><img alt="Michael Veazanchin" src="http://www.eecs.ucf.edu/isuelab/images/photos/michael.jpg" class="about-image" /></div></div><div class="clear"></div>
Michael V.</a></div>
</td>
<td>
<div class="about-image-container">
<a href="http://www.eecs.ucf.edu/isuelab/people/scott.php"><div class="outer-center"><div class="inner-center">
<img alt="Scott Tanner" src="http://www.eecs.ucf.edu/isuelab/images/photos/scott.jpg" class="about-image" /></div></div><div class="clear"></div>
Scott T.</a></div>
</td>
<td>
<div class="about-image-container">
<a href="http://www.eecs.ucf.edu/isuelab/people/nathan.php"><div class="outer-center"><div class="inner-center">
<img alt="Nathan Ochoa" src="http://www.eecs.ucf.edu/isuelab/images/photos/nathan.jpg" class="about-image" /></div></div><div class="clear"></div>
Nathan O.</a></div>
</td>
<td>
<div class="about-image-container">
<a href="http://www.eecs.ucf.edu/isuelab/people/kevin.php"><div class="outer-center"><div class="inner-center">
<img alt="Kevin Pfeil" src="http://www.eecs.ucf.edu/isuelab/images/photos/kevin.jpg" class="about-image" /></div></div><div class="clear"></div>
Kevin P.</a></div>
</td>
<td>
<div class="about-image-container">
<div class="outer-center"><div class="inner-center"><img alt="Chris Ross" src="<?=SITE_URL?>assets/images/chris.jpg" class="about-image" /></div></div><div class="clear"></div>
Chris R.</div>
</td>

</tr>
</table>
