<!DOCTYPE html>
<html lang="en">
<head>
	<title><? if(!isset($page_title) || empty($page_title)) { echo "Minds of Chimera"; } else { echo $page_title; } ?></title>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Minds of Chimera - One of the most advanced Minecraft servers available. Multiple developers are currently creating new ways to play Minecraft. This server is about creativity and advancing the Minecraft styles of play. Be a part of it." /> 
	<meta name="keywords" content="Minds of Chimera, minecraft, minecraft research, isue lab, ucf, minecraft server, interactive games" /> 
	<meta name="author" content="Ross Arena" />
	<meta name="robots" content="index, follow" /> 
	<meta name="googlebot" content="index, follow" /> 
	
	<link rel="SHORTCUT ICON" href="<?=SITE_URL ?>favicon.ico" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_URL?>assets/css/reset.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_URL?>assets/css/GGS.css" media="all" />
	<link rel="stylesheet/less" type="text/css" href="<?=SITE_URL?>assets/less/default-style.less" media="all" />

	<?
		if(isset($css)) {
			$styles = '';
			foreach($css as $path) {
				if(is_array($path)) {
					foreach($path as $inner_path) {
						$styles .= '<link rel="stylesheet" type="text/css" href="'.SITE_URL . $inner_path.'" media="all" />';
					}
				} else {
					$styles .= '<link rel="stylesheet" type="text/css" href="'.SITE_URL . $path.'" media="all" />';
				}
			}
			echo $styles;
		}
	?>
	
	<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript">
		var site_url = "<?=SITE_URL?>";
		var users_end = "<? if(isset($_SESSION['playerCount'])) { echo $_SESSION['playerCount']; } ?>";
		var user_posts_end = "<? if(isset($_SESSION['userPostsCount'])) { echo $_SESSION['userPostsCount']; } ?>";
		<? if(isset($owner)) { echo 'var user = "'.$owner . '";'; } ?>
		
		<? if(isset($orig_owner)) { echo 'var ownerPage = "'.$orig_owner . '";'; } else { echo 'var ownerPage = "";'; } ?>
	</script>

	<script type="text/javascript" src="<?=SITE_URL . "assets/scripts/jquery-1.10.1.min.js" ?>"></script>	
	<script type="text/javascript" src="<?=SITE_URL . "assets/scripts/less-1.3.3.min.js" ?>"></script>
	<script type="text/javascript" src="<?=SITE_URL . "assets/scripts/cache.js" ?>"></script>
	<script type="text/javascript">
		var $j = jQuery.noConflict();
	</script>
	<?
		if(isset($scripts)) {
			$script = '';
			foreach($scripts as $path) {
				$script .= '<script type="text/javascript" src="'.SITE_URL.$path.'"></script>';
			}
			echo $script;
		}
	?>
	<script type="text/javascript" src="<?=SITE_URL . "misc/popup/popup.js" ?>"></script>
	<script type="text/javascript" src="<?=SITE_URL . "assets/scripts/chimera-min.js" ?>"></script>
	<script type="text/javascript" src="<?=SITE_URL . "assets/scripts/waypoints.min.js" ?>"></script>

	<? 
		if(isset($inline_scripts)) {
			echo '<script type="text/javascript">$j(document).ready(function(){'.$inline_scripts.'});</script>';
		}
	?>

	<script type="text/javascript"><!--//--><![CDATA[//><!-
	/* prevents errors if angled brackets are found */
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-31885190-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	//--><!]]>
	</script>

	<script type="text/javascript"><!--//--><![CDATA[//><!-
	/* prevents errors if angled brackets are found */
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-34330432-1']);
	  _gaq.push(['_trackPageview']);

	//--><!]]>
	</script>
</head>
