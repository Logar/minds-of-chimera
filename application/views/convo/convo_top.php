<div class="overlay hide" id="overlay"></div>
<div class="user-msg-box user-msg-success" id="convo-msg-success">
	<a class="boxclose"></a>
	<h1>Page Updated</h1>
	<p>Congrats. Your changes were applied.</p>
</div>
<div class="user-msg-box" id="convo-msg-failure">
	<a class="boxclose"></a>
	<h1>Error</h1>
	<p>Oops, there was a problem. Please try again.</p>
</div>
<? if($internal_page == "create_convo") : ?>
<h1 class="float-left">Conversation Authoring</h1>
<a href="<?=site_url('convo')?>" class="float-right button">&lt;&lt; Convo Home</a>
		
<? else : ?>
<h1 class="float-left">Conversation Editing</h1>
<a href="<?=site_url('convo')?>" class="float-right button">&lt;&lt; Convo Home</a>
<? endif; ?>

<div class="clear"></div><br /><div id="convo-resp"></div>
<? 
	if($internal_page == "edit_convo") :
	
	$editAction = "convo/editConvo/";	
	if(isset($item[0]['id'])) :
		$editAction .= $item[0]['id']; 
	endif;
?>
<form enctype="multipart/form-data" method="post" action="<?=site_url($editAction)?>" id="convo-form">
<? else : ?>
<form enctype="multipart/form-data" method="post" action="<?=site_url('convo/createConvo')?>" id="convo-form">
<? endif; ?>
<div class="clearfix">
	<div class="convo-sec">
		<? if(isset($item[0]['id'])) : ?>
		<input type="hidden" name="convo_id" id="convo_id" value="<?php 
		if(isset($item[0]['id'])) : 
			  echo $item[0]['id']; 
		   endif; ?>" />
		<? endif; ?>
		<div class="float-left"><label class="float-right label-width2">Name:</label></div>
		
		<? if($internal_page == "edit_convo") : ?>
		<div class="edit-item-custom clearfix">
			<a class="edit-icon-link">
				<div class="edit-item-icon"></div>
				<div class="float-left">Edit</div>
			</a>
			<div class="edit-item-container">
				<div class="edit-item-inner" style="top: 0px; z-index: 500;">
					<!--<a class="convo-duplicate-item" id="convo-duplicate-<?php 
						if(isset($item[0]['id'])) { echo $item[0]['id']; } ?>">
					Duplicate</a><br /><hr>-->
					<a class="convo-delete-item" id="convo-delete-<?php 
					if(isset($item[0]['id'])) { echo $item[0]['id']; } ?>">
						<img src="<?=site_url('assets/images/delete_button.png')?>" alt="delete button" style="margin-right: 4px;" />
						Destroy!
					</a><br>
				</div>
			</div>
		</div>
		<? endif; ?>
		<input type="text" name="convo_name" class="float-left" style="width: 200px;" maxlength="60" value="<? if(isset($item[0]['name'])) : echo $item[0]['name']; endif; ?>" required />
		<div class="clear"></div><br />
		<div class="float-left"><label class="float-right label-width2">Description:</label></div>
		<textarea name="convo_desc" class="float-left textarea-medium textarea-noresize" maxlength="200"><? if(isset($item[0]['desc'])) : echo $item[0]['desc']; endif; ?></textarea>
	</div>
</div><br />
<div class="clearfix">
	<div id="convo-whennear-sec" class="convo-sec">
		<div class="clearfix">
			<div class="float-left" style="padding-right: 20px;">
				<h2>Start Conversation on approach?</h2>
			</div>
			<div class="float-left" style="padding-right: 10px;">
				Yes<br />
				<input type="radio" name="approach_radio" value="yes" class="approach-radio" 
				<? if(isset($item[0]['use_popup_on'])) :
			
					if($item[0]['use_popup_on'] == 1 || $item[0]['use_popup_on'] == 3) :
						echo 'checked="checked"';
						$approachActive = true;
					endif;
				endif; ?>
				/>
			</div>
			<div class="float-left">
				No<br />
				<input type="radio" name="approach_radio" value="no" class="approach-radio" />
			</div>
		</div><br />
		<div id="approach-choices-2" <? if(!isset($approachActive)) :
			echo 'class="hide"';
		endif; ?>>
			<div class="clearfix">
				<label class="float-left">Display Popup?</label>
				<div class="float-left" style="padding-right: 10px;">
					Yes<br />
					<input type="radio" name="approach_display_popup" value="yes" class="approach-display-popup" 
					<? if(isset($item[0]['use_popup_on'])) :
			
					if($item[0]['use_popup_on'] == 1 || $item[0]['use_popup_on'] == 3) :
						echo 'checked="checked"';
					endif;
				endif; ?>/>
				</div>
				<div class="float-left">
					No<br />
					<input type="radio" name="approach_display_popup" value="no" class="approach-display-popup" />
				</div>
				
			</div><br />
			<div class="clearfix">
				<label class="float-left">Display Text?</label>
				
				<div class="float-left" style="padding-right: 10px;">
					Yes<br />
					<input type="radio" name="approach_display_text" value="yes" class="approach-display-text" <?php
					if(isset($item[0]['neartext']) && !empty($item[0]['neartext'])) {
						echo 'checked="checked"';
						$approachDisplayText = true;
					}
					?>/>
				</div>
				<div class="float-left">
					No<br />
					<input type="radio" name="approach_display_text" value="no" class="approach-display-text" />
				</div>
			</div>
			<textarea id="approach-textarea" name="approach_textarea" class="<? if(!isset($approachDisplayText)) { echo "hide"; } ?> float-left textarea-medium textarea-noresize" maxlength="150"><?php
			if(isset($item[0]['neartext'])) {
				echo $item[0]['neartext'];
			}
			?></textarea>
		</div>
	</div>
</div><br />
<div class="clearfix">
	<div id="convo-onclick-sec" class="convo-sec">
		<div class="clearfix">
			<div class="float-left" style="padding-right: 20px;">
				<h2>Start Conversation on click?</h2>
			</div>
			<div class="float-left" style="padding-right: 10px;">
				Yes<br />
				<input type="radio" name="click_radio" value="yes" class="click-radio" 
				<? if(isset($item[0]['use_popup_on'])) :
			
					if($item[0]['use_popup_on'] == 2 || $item[0]['use_popup_on'] == 3) :
						echo 'checked="checked"';
						$clickActive = true;
					endif;
				endif; ?>
				/>
			</div>
			<div class="float-left">
				No<br />
				<input type="radio" name="click_radio" value="no" class="click-radio" />
			</div>
		</div><br />
		
		<div id="click-choices-2" <? if(!isset($clickActive)) :
			echo 'class="hide"';
		endif; ?>>
			<div class="clearfix">
				<label class="float-left">Display Popup?</label>
				<div class="float-left" style="padding-right: 10px;">
					Yes<br />
					<input type="radio" name="click_display_popup" value="yes" class="click-display-popup" 
					<? if(isset($item[0]['use_popup_on'])) :
				
						if($item[0]['use_popup_on'] == 2 || $item[0]['use_popup_on'] == 3) :
							echo 'checked="checked"';
						endif;
					endif; ?>
					/>
				</div>
				<div class="float-left">
					No<br />
					<input type="radio" name="click_display_popup" value="no" class="click-display-popup" />
				</div>
			</div><br />
			<div class="clearfix">
				<label class="float-left">Display Text?</label>
				<div class="float-left" style="padding-right: 10px;">
					Yes<br />
					<input type="radio" name="click_display_text" value="yes" class="click-display-text" />
				</div>
				<div class="float-left">
					No<br />
					<input type="radio" name="click_display_text" value="no" class="click-display-text" />
				</div>
			</div>
			<textarea id="click-textarea" name="click_textarea" class="hide float-left textarea-medium textarea-noresize" maxlength="150"></textarea>
		</div>
	</div>
</div><br />
<div class="clearfix">
	<div id="convo-onpopup-sec" class="convo-sec <?php if($user_mode == "create") { echo 'hide'; } ?>">
		<div class="clearfix">
			<h2 class="float-left">Conversation Popups</h2>
		</div>
		<div class="clearfix">
			<div class="row">
				<div class="col_22" id="all-entries-container">
				<?php
					if(isset($popup_entries)) {
						echo $popup_entries;
					}
				?>
				</div>
				<div class="col_14">
					<!--<canvas height="300" id="canvas-entry-map" class="width-100"></canvas>-->
				</div>
			</div>
		</div>
	</div>
</div>