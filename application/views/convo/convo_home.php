<div class="clearfix">
	<div class="row">
		<div class="col_26">
			<h1>Conversation Authoring</h1><br />
			<p>Create conversations you can have with your NPCs as you create quests and dreams. Or, create 
			slides for interacting on a Kiosk block you place. Apply these conversations to NPCs and Kiosks in the 
			game. From here, create, update or delete your conversations.</p><br />
			<div class="clearfix">
				<div class="float-right">
					<a href="<?=SITE_URL?>convo/page/create_convo" class="button create" >+ create</a>
				</div>
			</div>
			<div id="convos-container">
			<?php
				if(!empty($convo_items)) {
					foreach($convo_items as $item) {
						echo '<div class="convo-list-entity">';
						if($item['author_id'] == $session['pid']) {
							echo '<div class="edit-item-custom">
								<a href="'.SITE_URL.'convo/page/edit_convo/'.$item['id'].'" class="convo-edit-link" 
								id="convo-edit-'.$item['id'].'"><div class="edit-item-icon"></div></a></div>';
						}
						echo '<b>'.$item['name'].'</b><br /></div>';
					}
				}
			?>
			</div>
		</div>
	</div>
</div>