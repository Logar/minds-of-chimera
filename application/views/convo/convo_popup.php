<?php

	$entrySize = count($item);
	
	$entryCount = 0;
	
	if($entrySize == 0) {
		$entryCount = -1;
	}
	
	for($entryCount; $entryCount < $entrySize; $entryCount++) :
		
		if($user_mode !== "create") {
			if(isset($item[$entryCount]['order_num'])) {
				$groupNum = $item[$entryCount]['order_num'];
			}
		}
?>
<div id="convo-entry-container-<?=$groupNum?>" class="convo-entry-container" <?php
	if($groupNum > 1) {
		echo 'style="display:none;"';
	}
?>>
	<div style="border-right: 1px solid gray; border-bottom: 1px solid gray; width: 50px; background-color: #eee; float: left;">
		<div style="padding: 2px; text-align: center;">
			<?php
				if(isset($item[$entryCount]['entry_id'])) :
					echo '<input type="hidden" name="entity_id" value="'.$item[$entryCount]['entry_id'].'" />';
				endif;
			?>
			<h2><?=$groupNum?></h2>
		</div>
	</div>
	<div class="float-left" style="margin-left: 20px; margin-top: 5px;">
		<label>Entry Title:</label><br />
		<input type="text" name="group_<?=$groupNum?>[title]" value="<?php 
		if(isset($item[$entryCount]['title'])) { 
			echo $item[$entryCount]['title']; 
		}
		?>" style="width: 263px;" maxlength="32" />
		<input type="hidden" name="group_<?=$groupNum?>[order_number]" value="<?=$groupNum?>" />
	</div>
	<div class="relative-position float-right">
		<a class="absolute-position entry-delete" style="top: -5px; right: -5px;" id="entry-delete-<?php
		if(isset($item[$entryCount]['entry_id'])) { 
			echo $item[$entryCount]['entry_id'];
		}
		?>"><img src="<?=SITE_URL?>assets/images/close.png" alt="close button" /></a>
	</div>
	<div style="padding: 10px;">
		<div class="clearfix">
			<div class="instant-msg-container" style="width: 220px; display: none;">
				<div class="instant-msg-bubble">Creates a new entry in the conversation</div>
				<img src="<?=SITE_URL?>assets/images/down-arrow.png" style="position: absolute; top: 50px; right: 30px; float: right;">
			</div>
			<div class="instant-msg-container" style="width: 220px; display: none;">
				<div class="instant-msg-bubble">Deletes the current entry from the conversation</div>
				<img src="<?=SITE_URL?>assets/images/down-arrow.png" style="position: absolute; top: 50px; right: 180px; float: right;">
			</div>
		</div><br />
		<div class="clearfix">
			<div class="row">
				<div class="col_21">
					<label>Says:</label><br />
					<textarea name="group_<?=$groupNum?>[message]" class="float-left convo-says-textarea no-enter" maxlength="400"><?php 
					if(isset($item[$entryCount]['message'])) { 
						echo $item[$entryCount]['message']; 
					}
					?></textarea>
				</div>
				<div class="col_15">
					<div class="convo-faces-top" class="float-left">
						<div class="convo-faces-container">
						<?php if(isset($item[$entryCount]['image_url'])) { ?>
							<img src="<?=SITE_URL?>assets/images/plugins/convo/happy_face.png" alt="face" class="float-left <?php 
							if($item[$entryCount]['image_url'] == "/assets/images/plugins/convo/happy_face.png") { 
								echo "show"; 
							} else { 
								echo "hide"; 
							}
							?> convo-happy-face" style="width: 75px; height: 75px; margin-right: 5px;" />
							<img src="<?=SITE_URL?>assets/images/plugins/convo/neutral_face.png" alt="face" class="float-left <?php
							
							if($item[$entryCount]['image_url'] == "/assets/images/plugins/convo/neutral_face.png") { 
								echo "show"; 
							} else {
								echo "hide"; 
							}
							?> convo-neutral-face" style="width: 75px; height: 75px; margin-right: 5px;" />
							<img src="<?=SITE_URL?>assets/images/plugins/convo/sad_face.png" alt="face" class="float-left <?php
							if($item[$entryCount]['image_url'] == "/assets/images/plugins/convo/sad_face.png") { 
								echo "show"; 
							} 
							else { 
								echo "hide"; 
							}
						?> convo-sad-face" style="width: 75px; height: 75px; margin-right: 5px;" />
						<img src="<?=SITE_URL?>assets/images/plugins/convo/mad_face.png" alt="face" class="float-left <?php
							if($item[$entryCount]['image_url'] == "/assets/images/plugins/convo/mad_face.png") { 
								echo "show"; 
							} else { 
								echo "hide"; 
							}
						?> convo-mad-face" style="width: 75px; height: 75px; margin-right: 5px;" />
						<img src="<?=SITE_URL?>assets/images/plugins/convo/annoyed_face.png" alt="face" class="float-left <?php
							if($item[$entryCount]['image_url'] == "/assets/images/plugins/convo/annoyed_face.png") { 
								echo "show"; 
							} else { 
								echo "hide";
							}
						?> convo-annoyed-face" style="width: 75px; height: 75px; margin-right: 5px;" />
						<?php
							if($item[$entryCount]['image_url'] !== "/assets/images/plugins/convo/happy_face.png" && 
							$item[$entryCount]['image_url'] !== "/assets/images/plugins/convo/neutral_face.png" &&
							$item[$entryCount]['image_url'] !== "/assets/images/plugins/convo/sad_face.png" &&
							$item[$entryCount]['image_url'] !== "/assets/images/plugins/convo/mad_face.png" && 
							$item[$entryCount]['image_url'] !== "/assets/images/plugins/convo/annoyed_face.png") {
								echo '<img src="'.SITE_URL.$item[$entryCount]['image_url'].'" alt="face" class="float-left show" class="selected-face" style="width: 75px; height: 75px; margin-right: 5px;" />';
							}
						?>
						<div style="background-color: #ccc; width: 75px; height: 75px; margin-right: 5px;" class="float-left <?php
							if(empty($item[$entryCount]['image_url'])) { 
								echo "show"; 
							} 
							else { 
								echo "hide"; 
							} 
						?> convo-no-face"></div>
						<? } else { ?>
							<img src="<?=SITE_URL?>assets/images/plugins/convo/happy_face.png" alt="face" class="float-left show convo-face-image" id="convo-happy-face-<?=$groupNum?>" />
							<img src="<?=SITE_URL?>assets/images/plugins/convo/neutral_face.png" alt="face" class="float-left hide convo-face-image" id="convo-neutral-face-<?=$groupNum?>" />
							<img src="<?=SITE_URL?>assets/images/plugins/convo/sad_face.png" alt="face" class="float-left hide convo-face-image" id="convo-sad-face-<?=$groupNum?>" />
							<img src="<?=SITE_URL?>assets/images/plugins/convo/mad_face.png" alt="face" class="float-left hide convo-face-image" id="convo-mad-face-<?=$groupNum?>" />
							<img src="<?=SITE_URL?>assets/images/plugins/convo/annoyed_face.png" alt="face" class="float-left hide convo-face-image" id="convo-annoyed-face-<?=$groupNum?>" />
							<div id="convo-no-face-<?=$groupNum?>" class="float-left hide convo-face-image" style="background-color: #ccc;"></div>
						<? } ?>
						</div>
						<div class="float-left">
							<div class="clearfix">
								<a class="float-left small-dashed-box convo-faces" id="convo-no-link-<?=$groupNum?>">-</a>
								<a class="float-left small-dashed-box convo-faces" id="convo-sad-link-<?=$groupNum?>">Sad</a>
							</div>
							<div class="clearfix">
								<a class="float-left small-dashed-box convo-faces" id="convo-neutral-link-<?=$groupNum?>">Neutral</a>
								<a class="float-left small-dashed-box convo-faces" id="convo-annoyed-link-<?=$groupNum?>">Annoyed</a>
							</div>
							<div class="clearfix">
								<a class="float-left small-dashed-box convo-faces" id="convo-happy-link-<?=$groupNum?>">Happy</a>
								<a class="float-left small-dashed-box convo-faces" id="convo-mad-link-<?=$groupNum?>">Mad</a>
							</div>
							<div class="clearfix">
								<a class="float-left small-dashed-box convo-faces hide convo-myphoto-link" style="width: 138px; text-align: center;">My photo</a>
								<input type="hidden" id="convo-img-selected-<?=$groupNum?>" name="group_<?=$groupNum?>[convo_img_selected]" value="happy_face.png" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><br /><br />
		<div class="responses-top">
			<?php
				
				if(isset($item[$entryCount]['responseGroup']->response)) :
				$item[$entryCount]['responseGroup']->gotoentry_id = explode(",", $item[$entryCount]['responseGroup']->gotoentry_id);
				$item[$entryCount]['responseGroup']->response = explode(",", $item[$entryCount]['responseGroup']->response);
				//print_r($item[$entryCount]['responseGroup']);
				
				foreach($item[$entryCount]['responseGroup']->response as $resp) :
			?>
					<div class="response">
						<div class="response-inner">
							<div class="clearfix">
								<div class="float-left">
									<div class="js-remove-resp"></div>
									<div class="float-left">
										<textarea name="group_<?=$groupNum?>[responses][]" class="float-left textarea-small no-textarea-border" maxlength="90"><?php 
											if(isset($resp) && !empty($resp)) { 
												echo $resp;
											} 
										?></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="float-left" style="margin-left: 10px;">
							<a class="convo-entry-options-arrow"><div class="<?php if($user_mode == "create") { echo 'arrow-unlinked'; } else { echo 'arrow-linked'; } ?>"></div></a>
						</div>
						<div class="clear"></div>
						<div class="entry-options-container" style="display: none;">
							<div class="convo-entry-option">
								<a class="convo-create-entry">Create New Entry</a>
								<a class="no-response-options-link">
									<div class="no-response-options"></div>
								</a>
							</div>
							<div class="convo-entry-option">Goes to entry 
								<select name="group_<?=$groupNum ?>[gotos][1]" style="width: 140px;" class="select-existing">
									<?php
										$goto = "";

										for($j=0; $j < $entrySize; $j++) {
											if(isset($item[$entryCount]['responseGroup']->gotoentry_id[$j])) {
												
												$gotoentry_id = $item[$entryCount]['responseGroup']->gotoentry_id[$j];
												
												if($gotoentry_id == -1) {
													$goto .= '<option value="-1">End Conversation</option>'; 
												}
												
												if(isset($item[$j]['order_num'])) {
													$orderNum = $item[$j]['order_num'];

													if($groupNum == $orderNum) {
														$goto .= '<option value="'.$item[$entryCount]['entry_id'].'" disabled="disabled">'.$orderNum.'</option>'; 
													} else {
														$goto .= '<option value="'.$item[$entryCount]['entry_id'].'">'.$orderNum.'</option>';
													}
												}
											} else { 
												break;
											}
										}

										if(!empty($goto)) {
											echo $goto;
										}
									?>
								</select>
							</div>
						</div><br />
					</div>
				<?php endforeach; endif;?>
		</div>
		<div class="clearfix">
			<div class="float-left" style="margin-right: 150px;">
				<input type="button" value="+ add response" class="js-add-resp no-user-select" />
			</div>
		</div>
	</div>
</div>
<?php 
	endfor; ?>
