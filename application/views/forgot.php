<div id="forgot-success"></div>
<div id="before-reset">
	<h2>Lose something?</h2>
	<p>Enter your username or email to reset your password.</p><br />
	<div id="forgot-failure"></div>
	<form method="post" id="forgot-form" action="<?=SITE_URL?>reg/forgotPass">
		<div class="clearfix">
			<div class="float-left">
				<input type="text" name="forgotuser" placeholder="Username or Email" size="40" tabindex="9" required />
			</div>
		</div><br />
		<input type="submit" value="Reset my password" tabindex="10" />
	</form>
</div>
