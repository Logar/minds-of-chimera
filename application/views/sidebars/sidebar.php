<?php if($this->userSession['member'] !== false) : ?>
<div class="clearfix">
	<div class="sidebar_container">
		<div style="z-index: 900;">
			<div style="position: relative; float: left; width: 60px;">
				<a href="<?=SITE_URL?>gallery">
				<span style="cursor: pointer;">Photos</span><br/>
				<img src="<?=SITE_URL?>assets/images/photos_icon.png" alt="minecraft photos" /></a>
			</div>
			<div style="position: relative; float: left; width: 50px;">
				<a href="<?=SITE_URL?>map" title="Show map" style="cursor: pointer;">
				<span style="margin-left: 7px; cursor: pointer;">Maps</span><br>
				<img src="<?=SITE_URL?>assets/images/map-icon.png" alt="map" /></a>
			</div>
			<!--<div style="position: relative; float: left; width: 60px;">
				<a href="javascript:TINY.box.show({url:'<?=SITE_URL?>main/scores?playerid=<?=$this->userSession['pid']?>',width:700,height:600,opacity:30})" title="Scores" style="cursor: pointer;"><span style="margin-left: 8px; cursor: pointer;">Scores</span><br/><img src="<?=SITE_URL?>assets/images/awards-icon.png" alt="achievements" style="margin-left: 10px;" /></a>
				<a href="javascript:TINY.box.show({url:'<?=SITE_URL?>main/events',width:700,height:385,opacity:30})" title="Scores" style="cursor: pointer;"><span style="margin-left: 8px; cursor: pointer;">Scores</span><br/><img src="<?=SITE_URL?>assets/images/awards-icon.png" alt="achievements" style="margin-left: 10px;" /></a>
			</div>-->
			<div style="position: relative; float: left; width: 60px;">
				<a href="<?=SITE_URL?>convo" title="Kiosk" style="cursor: pointer;">
				<span style="margin-left: 10px; cursor: pointer;">Kiosk</span>
				<img src="<?=SITE_URL?>assets/images/kiosk-icon.png" alt="kiosk" style="margin-left: 6px; margin-top: 5px;"/></a>
			</div>
			<!--
			<div style="position: relative; float: left; width: 60px;">
				<a href="javascript:TINY.box.show({url:'<?=SITE_URL?>main/events',width:700,height:385,opacity:30})" title="Events" style="cursor: pointer;"><span style="margin-left: -3px; cursor: pointer;">Events</span><img src="<?=SITE_URL?>assets/images/events-icon.png" alt="events" style="margin-left: 0px; margin-top: 5px;"/></a>
			</div>-->
		</div>
	</div>
</div>
<div id="news_feed">
<?php 
	$content = '';
	
	$count = 0;
	$developer = false;
	$get_news_param = '';
	$feed_height = "height: 460px;";
	
	
	$get_news_param = "&news=true";
	$content .= '<div class="top-twtr-tweet"><div class="twtr-hd" style="margin: 10px;">
	<div class="twtr-profile-img-anchor">
		<img alt="profile" class="twtr-profile-img" src="'.SITE_URL.'/assets/images/isue_logo_small.png">
	</div>
	<h5>The Minds Team</h5><h4>Minds of Chimera</h4></div><div class="clear"></div>';

	$content .= '<div class="normal tiny-inner all-posts-news-container" style="'. $feed_height .'">';

	$developers_array = array(
		"cwingrav", 
		"ruckus_box", 
		"evali", 
		"chillerman91", 
		"julietnpn", 
		"EdBighead11",
		"thaigrl",
		"raidendex"
	);
	
	if(in_array($this->userSession['member'], $developers_array)) {
		$developer = true;
		$content .= '<form class="post-form" enctype="multipart/form-data" method="post" action="'.SITE_URL.'main/addPost/news">
			<textarea name="postBox" id="post-box-news" class="expand border-box" rows="1" 
			placeholder="Share some news with people..." style="width: 100%; resize: none; z-index: 10;" required></textarea>
			<div id="post-box-news-bottom"><input type="submit" value="Post" class="float-right" /></div>
			<div class="clear"></div></form><br />';
		$content .= '<div class="all-posts-news">';
	}
	
	$condition = '';
	
	for($i=0; $i < sizeof($posts); $i++) {
			
		$orig_author = $posts[$i]['author'];
		$author = ucwords($posts[$i]['author']);
		$post_id = $posts[$i]['post_id'];
		$date = $posts[$i]['post_date_formatted'];
		$post_content = $posts[$i]['post_content'];
		$profile_img_src = $posts[$i]['profile_img_src'];
	
		$profile_path = $posts[$i]['profile_path'];
		$like_count = $posts[$i]['like_count'];
			
		if($posts[$i]['like_list'] == 1) {
			$like_list = $posts[$i]['like_list'];
		} else {
			$like_list = 0;
		}
	
		$content .= '<div id="sidebar-post-container'.$post_id .'" class="post-container margin-bottom-10">
			<div id="sidebar-post'.$post_id.'"><div class="relative-position width-100"><div class="row clearfix">';
		if(!empty($profile_img_src)) {
			$content .= '<div class="col_6"><a href="'.$profile_path .'">
					<img src="'.$profile_img_src .'" class="post_profile_img" alt="profile_image" /></a></div>';
		} else {
			$content .= '<div class="col_6"><a href="'.$profile_path .'">
				<img src="'.SITE_URL.'assets/images/profile/blank_avatar_small.png" class="post_profile_img" alt="profile_image" /></a></div>';
		}
		$content .= '<div class="col_30"><a href="'.$profile_path .'"><b>'.$author .'</b></a>';
		$content .= '<br /><span class="wordwrap">'.$post_content .'</span><br />
				<span class="color-666">'.$date .'</span></div></div>';
			
		if($orig_author == $this->userSession['member']) {
			$content .= '<div class="post-options"><a class="delete" id="posts_' . $post_id . '">
					<img src="'.SITE_URL.'assets/images/delete_button.png" alt="delete button" /></a></div>';
		}
			
		$content .= '<div class="comment-container">';
		if($like_list == 1) {
			$content .= '<div class="single_comment_entity comment_choices">
					<a class="sidebar_unlike" id="sidebar_unlike_'.$post_id .'">Unlike</a>&nbsp;&#183;&nbsp;
							<a href="'.SITE_URL.'single/notify?postid='. $post_id.'" class="comment-link">Comment</a>';
		} else {
			$content .= '<div class="single_comment_entity comment_choices">
					<a class="sidebar_like" id="sidebar_like_'.$post_id .'">Dig it</a>&nbsp;&#183;&nbsp;
							<a href="'.SITE_URL.'single/notify?postid='. $post_id.'" class="view_comments">Comment</a>';
		}
		// optimize this
		$qry3 = mysql_query("SELECT COUNT(*) AS `cnt` FROM `comments` WHERE `post_id` = $post_id");
			
		if($qry3 != false) {
	
			$row3 = mysql_fetch_array($qry3);
			$cnt = $row3['cnt'];
		} else {
			$cnt = 0;
		}
			
		if($cnt > 0) {
			$content .= '&nbsp;&#183;&nbsp;<a href="'.SITE_URL.'single/notify?postid='. $post_id.'">
					<img src="'.SITE_URL.'assets/images/messages.png" alt="messages" /> '. $cnt. '</a>';
		}
			
		$msg_count = 0;
		if($like_count > 0) {
	
			if ($like_count >= 1) {
				if($this->userSession['member'] !== false) {
					$content .= '<span class="like-img">&nbsp;&#183;&nbsp;
						<img src="'.SITE_URL.'assets/images/likeup.png" alt="likeup" style="position: relative; top: 2px;" />';
				} else {
					$content .= '<span class="like-img">
						<img src="'.SITE_URL.'assets/images/likeup.png" alt="likeup" style="position: relative; top: 2px;" />';
				}
				$content .= '<span class="like_'.$post_id .'">';
				$new_like_count = $like_count;
				$content .= $new_like_count . '</span></span>';
			}
		}
		if($msg_count > 0) {
			$content .= '<span class="messages-img">&nbsp;&#183;&nbsp;
				<img src="'.SITE_URL.'assets/images/messages.png" alt="messages" style="position: relative; top: 2px;" /></span>';
		}
		$content .= '</div></div></div></div></div>';
	}
	if($developer == true) {
		$content .= '</div></div>';
	}

	$content .= '</div>&nbsp;</div><div class="clear"></div>';
	echo $content;
?>
</div>
<? endif; ?>
