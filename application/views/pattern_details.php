<div class="lb float-left" style="width: 100%; box-sizing: border-box; word-wrap:break-word;">
<div class="float-left"><h1><?=$patterns[0]['name']?></h1></div>
<div class="float-right"><?
if($ratings[0]['total_score'] != 0) {

$total_score = round($ratings[0]['total_score']);

if($total_score >= 1 || $total_score < 5) {
	echo '<div style="padding-top: 5px; padding-left: 10px; float: left;">';
if($total_score == 1) {
	echo '<span class="stars-1" style="float: left;"></span>&nbsp;&nbsp;';
} else if($total_score == 2) {
	echo '<span class="stars-2" style="float: left;"></span>&nbsp;&nbsp;';
} else if($total_score == 3) {
	echo '<span class="stars-3" style="float: left;"></span>&nbsp;&nbsp;';
} else if($total_score == 4) {
	echo '<span class="stars-4" style="float: left;"></span>&nbsp;&nbsp;';
} else if($total_score == 5) {
	echo '<span class="stars-5" style="float: left;"></span>&nbsp;&nbsp;';
}
	printf("%2.1f", $ratings[0]['total_score']);
	echo '</div>';
}
}
?></div>
</div><div class="clear"></div>
<br />
<div class="float-left">
Author: <a href="<?=site_url('profile/'.$patterns[0]['author'])?>"><?=$patterns[0]['author']?></a> on <?=$patterns[0]['createdon']?>
</div>
<div class="float-right"><a href="<?=site_url('mocrater') ?>"><img src="<?=site_url('assets/images/back-button.png')?>" alt="back button" />&nbsp;back</a></div>
<div class="clear"></div><br />

<?
	if($patterns[0]['author'] == $session['member']) { 
		echo '<span style="float: right;"><a href="'.site_url('mocrater/edit_pattern/'.$patterns[0]['id']).'" class="button">Update</a></span><div class="clear"></div>'; 
	}
?>
	<div>
		<div class="clearfix" style="width: 340px; margin-right: 7px; float: left; word-wrap:break-word;">
			<h2>Context:</h2>
			<p><?=$patterns[0]['context'] ?></p><br />
			<h2>Problem:</h2>
			<p><?=$patterns[0]['problem'] ?></p><br />
			<h2>Solution:</h2>
			<p><?=$patterns[0]['solution'] ?></p><br />
			<h2>Related Patterns:</h2>
			<? if(empty($relatedPatterns)) { 
				echo 'No related Patterns';
			} else { ?>
			<div style="border: 1px solid #ccc; padding: 5px; min-height: 335px; min-height: 50px; max-height: 100px; overflow: auto;">
			<?
				echo '<ul class="list">';
				for($i=0; $i<sizeof($relatedPatterns); $i++) {
					echo '<li><b>'.$relatedPatterns[$i]['name'].'</b></li>';
				}
				echo '</ul>';
			?>
			</div>
			<? } ?>
		</div>
		<div style="float: left;">
			<div class="img-container">
				<div class="mocrater-img-container-inner">
				<? 
					if(empty($patterns[0]['screen_src'])) {
						echo '<div style="text-align: center; margin: 0 auto; padding-top: 80px;"><h3 style="color: #888;">No Image</h3></div>';
					} else {
						if(file_exists('assets/images/profile/'.$patterns[0]['screen_src'])) {
							echo '<a href="'.site_url('assets/images/profile/'.$patterns[0]['screen_src']).'" target="'.site_url('assets/images/profile/'.$patterns[0]['screen_src']).'" rel="lightbox"><img src="'.site_url('assets/images/profile/'.$patterns[0]['screen_src']).'" alt="" style="width: 240px; height: 200px;" /></a>';
						} else {
							echo '<div style="text-align: center; margin: 0 auto; padding-top: 80px;"><h3 style="color: #888;">No Image</h3></div>';
						}
					}
				?>
				</div>
			</div>
		</div>
	</div><div class="clear"></div>
	<? if($ratings != 0 && is_array($ratings)) { ?>
	<br /><br />
	<a class="menu-tab" style="background: #eee; color: #000; height: 30px;">Feedback</a>
	<div style="border: 1px solid #ccc; padding: 5px; width: 380px; margin-top: 10px; position: relative; z-index: 100; background-color: #fff;">
	<?
		echo '<ul class="list">';
		for($i=0; $i<count($ratings); $i++) {
			echo '<li class="clearfix">';
			if($ratings[$i]['rating_t'] == 1) {
				echo '<b>Comment by </b>';
			}
			else if($ratings[$i]['rating_t'] == 2) {
				echo '<b>Pattern usage by </b>';
			}
			else if($ratings[$i]['rating_t'] == 3) {
				echo '<b>Design critique by </b>';
			}
			else if($ratings[$i]['rating_t'] == 4) {
				echo '<b>Pattern critique by </b>';
			}
			
			echo '<a href="#">'.$ratings[$i]['author'].'</a>';
			if($ratings[$i]['score'] == 1) {
				echo '<span class="stars-1" style="float: right;"></span><br />';
			} else if($ratings[$i]['score'] == 2) {
				echo '<span class="stars-2" style="float: right;"></span><br />';
			} else if($ratings[$i]['score'] == 3) {
				echo '<span class="stars-3" style="float: right;"></span><br />';
			} else if($ratings[$i]['score'] == 4) {
				echo '<span class="stars-4" style="float: right;"></span><br />';
			} else if($ratings[$i]['score'] == 5) {
				echo '<span class="stars-5" style="float: right;"></span><br />';
			}
			
			//echo '<img src="'.$ratings[$i]['screen_src'].'" alt="profile image" />';
			echo '<p style="text-indent: 15px;">' . $ratings[$i]['comment'] . '</p>';
			//echo $ratings['pattern_id'];
			//echo $ratings['pattern_name'];
			//echo $ratings['title'];
			echo' <span style="color: gray;">'.$ratings[$i]['createdon']. '</span>';
			
			if($ratings[$i]['X'] !== NULL || !empty($ratings[$i]['X'])) {
				echo '<span style="float: right;">@ '.$ratings[$i]['X'] .', '. $ratings[$i]['Y'] . ', '. $ratings[$i]['Z'].'</span>';
			}
		}
		echo '</ul>';
	?>
	</div>
	<? } ?>
<div class="clear"></div>
