<h2 class="page_title">Quest Guides</h2>

<div id="sites-canvas-main-content">
<table xmlns="http://www.w3.org/1999/xhtml" cellspacing="0" class="sites-layout-name-one-column sites-layout-hbox">
	<tbody><tr><td class="sites-layout-tile sites-tile-name-content-1"><div dir="ltr">
<h3 dir="ltr"><a name="TOC-Leocius-World-Quests"></a><span >Leocius World Quests</span></h3>
<span >Meant to get you started exploring in the world, many starter quests exist around Leocius. 
Here are the titles... will you find them all?</span>
<br><br>
<div style="height:400px;width:500px;background:#eee;padding:15px;overflow:auto">
<ol><li><span >Let's get you back on your feet</span></li>
<li><span >Cobblestone vs. Gravel</span></li>
<li><span >Door Thief</span></li>
<li><span >Boom Boom Ow!</span></li>
<li><span >MisterCactus</span></li>
<li><span >Extra-Caliber</span></li>
<li><span >Sandman</span></li>
<li><span >Roses</span></li>
<li><span >ObsidianMeridian</span></li>
<li><span >Party Rock!</span></li>
<li><span >Bowl of Coal</span></li>
<li><span >Rail Runner</span></li>
<li><span >Careful Now</span></li>
<li><span >Rock and Roll!</span></li>
<li><span >Robbin' Hood</span></li>
<li><span >Suspicious Intentions...</span></li>
<li><span >Nice and Leafy</span></li>
<li><span >Brick by-</span></li>
<li><span >Whole New Worlds</span></li>
<li><span >Just Like-</span></li>
<li><span >Picky Kitty</span></li>
<li><span >Dirt</span></li>
<li><span >NightLife</span></li>
<li><span >What did you call me?!</span></li>
<li><span >They got me seedin'</span></li>
<li><span >Bob the Digger.. can he dig it?!</span></li>
<li><span >Glassy</span></li>
<li><span >DigDig</span></li>
<li><span >DugDug</span></li>
<li><span >Revenge is a-</span></li>
<li><span >Sugar High</span></li>
<li><span >The Young and the Painted</span></li>
<li><span >Day at the Museum</span></li>
<li><span >Let the Beat pop~!</span></li>
<li><span >Crossfire Victim</span></li>
<li><span >Swordsmen</span></li>
<li><span >Slaughter House</span></li>
<li><span >Creepy-Crawlies</span></li>
<li><span >SSSSssss</span></li>
<li><span >Get Wood</span></li>
</ol>
</div>
<br>
<h3 dir="ltr"><a name="TOC-Haunted-Mansion"></a><span >Haunted Mansion</span></h3>
<span >Summary: Explore a foreboding mansion filled with menacing creatures and try to find out what happened to the Baron! Hidden within the various rooms are several switches which will unlock the ballroom, and the final mystery of this place...</span><br>
<span >How to Play: <br>Use the slash Command that follows:<br><br>/ow cp HM<br></span>
<h3 dir="ltr"><a name="TOC-Zombie-Attack"></a><span >Zombie Attack</span></h3>
<span >Summary: As a newly recruited guard in the castle, your training is suddenly cut short by a vicious attack by undead forces! Only by applying your redstone wire training will you be able to navigate the wrecked fortress and rescue the King in time!</span><br>
<span >How to Play: </span><span ><br>Use the slash Command that follows:<br>
/ow cp ZA</span><span ><br>
Did You Know? The puzzles you solve in Zombie Attack all use principles of basic computer architecture. You were learning something very complicated without realizing it! Redstone wire can be used to make all sorts of working examples of circuitry!</span></div></td></tr></tbody></table>
</div>
