<div class="clearfix">
	<div class="world-top" style="background-color: #4f7fe7;">
	
		<div class="twtr-hd" style="margin: 10px;">
			<div class="twtr-profile-img-anchor">
				<img alt="profile" class="twtr-profile-img" src="<?=site_url('/assets/images/world_icon.png')?>">
			</div>
			<div style="position: absolute; margin-top: 10px; margin-left: 60px;">
				<h4>The World</h4>
			</div><br />
		</div>
	</div>
</div>
<div class="landing-post-container">
	<br />
	<div class="clearfix">
		<div class="row">
			<div class="col_9">
				<div>
					<a href="<?=site_url('profile/'.$session['member'])?>">
					<img <? if(!empty($session['profile_img_src_small'])) { ?>src="<?=$session['profile_img_src_small'] ?>" 
					<? } else { ?>src="<?=SITE_URL?>assets/images/profile/steve_avatar_icon.png" <? } ?> alt="profile icon" width="36" height="36" /><span style="font-weight: bold; margin-left: 5px; margin-top: 15px; position: absolute;"><?=ucwords($session['member'])?></span></a>
					<div class="clear"></div><br />
					<img src="<?=site_url('assets/images/feed-photo-icon.png')?>" alt="status icon" style="padding-right: 5px;  position: relative; float: left; z-index: 15;" />
					<a href="<?=site_url('gallery/myPhotos/'. $session['member'])?>" class="float-left"><b>My Photos</b></a>
				</div>
			</div>
			<div class="col_27">
			<? if($session['member'] !== false): ?>
			<div style="margin-top: 5px; position: relative;">
				<div style="position: relative; left: 15px; float: left;">
					<img src="<?=site_url('assets/images/feed-status-icon.png')?>" alt="status icon" style="padding-right: 5px;  position: relative; float: left; z-index: 15;" />
					<a id="post-status" class="float-left"><b>Status</b></a>
				</div>
				<div style="position: relative; left: 35px; float: left;">
					<img src="<?=site_url('assets/images/feed-photo-icon.png')?>" alt="status icon" style="padding-right: 5px;  position: relative; float: left; z-index: 15;" />
					<a id="post-photo" class="float-left"><b>Photo</b></a>
				</div>
				<div class="clear"></div>
				<p id="img-result"></p>
				<div id="image_upload_form_container">
					<!--<form name="image_upload_form" id="gallery_upload_form" action="<?=site_url('gallery/upload_gallery_img')?>" method="post" enctype="multipart/form-data">	
						
						<input type="file" name="gallery_img" id="upload-img" style="width: 140px !important; height: 35px !important; position: absolute; top: 0; left: 0;" />
					</form>-->
					<div class="f1-upload-process" style="position: absolute; right: 0px; top: -8px;"><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></div>
					<form class="post-form" enctype="multipart/form-data" method="post" action="<?=site_url('main/addPost/home')?>">
						<textarea name="postBox" id="post-box" class="post-box" class="expand" rows="1" placeholder="Tell people what you are up to..." required></textarea>
						<div class="grippie"></div>
						<input type="hidden" name="location" value="<?=$session['member']?>" />
						<div id="post-box-bottom">
							<input type="file" name="post-feed-image" id="upload-feed-photo" style="left: 0;" />
							<input type="submit" value="Post" class="float-right" />
						</div>
						<div class="clear"></div>
					</form>
				</div>
			</div>
			<br />
			<div class="clear"></div>
			<div class="new-posts-loader" class="relative-position"><div class="f1-upload-process" style="position: absolute; right: 0px; top: -8px;"><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></div></div>
			<br />
			<? endif; ?>
				<div class="all-posts">
					<?=$post_feed ?>
				</div>
			</div>
		</div>
	</div>
	<div id="more-posts">

		<p id="more-posts-loader" class="hide"><img src="<?=site_url('assets/images/loading2.gif') ?>" alt="loading" /></p>
		<ul>
			<!-- Hijack this link for the infinite scroll -->
			<!--<li class="more"><a href="." title="Traditional navigation link">Next Page</a></li>-->
		</ul>
	</div><div class="clear"></div>
</div>

