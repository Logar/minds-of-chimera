<?php

	$getMore = 1;
	$comments = '';
	$postNum = sizeof($commentsArray);
	$row_count = 0;
	$like_count = 0;
	
	for($i=0; $i < $postNum; $i++) {

		$comment_id = $commentsArray[$i]['comment_id'];
		$post_id = $commentsArray[$i]['post_id'];
		$author = ucwords($commentsArray[$i]['author']);
		$comment_content = $commentsArray[$i]['comment_content'];
		$profile_img_src = $commentsArray[$i]['profile_img_src'];
		$profile_img_src_small = $commentsArray[$i]['profile_img_src'];
		$profile_path = $commentsArray[$i]['profile_path'];
		$date = $commentsArray[$i]['comment_date'];
		
		if(empty($profile_img_src)) {
			$profile_img_src = site_url('assets/images/profile/steve_avatar_small.png');
		}
		
		$comments .= '<div id="comment_'.$comment_id.'" class="post_'.$post_id.' single_comment_entity">
			<div style="position: relative; float: left; width: 100%;">
			<div class="row">
			<div class="col_4"><a href="'.$profile_path .'">
			<img src="'.$profile_img_src .'" class="comment_profile_img" /></a></div>
			<div class="col_32"><a href="'.$profile_path .'">
			<b>'.$author .'</b></a>&nbsp;
			<span style="word-wrap: break-word;">'.$comment_content .'</span>
			<br /><span style="color: #666;">'.$date .'</span>&nbsp;
			<!--<a class="like" id="like_'.$comment_id .'" name="likes">Like</a>-->
			</div></div>';
		if($author == $session['member']) { 
			$comments .= '<div class="post-options"><a class="delete" id="comments_' . $comment_id . '" >
				<img src="' . site_url('assets/images/delete_button.png') . '" alt="delete button" /></a></div>'; 
		}
		
		$comments .= '</div><div class="clear"></div></div>';
		$row_count++;
	}

	/*
	if(isset($_SESSION['pid'])) {
		$post_id = $commentsArray['post_id'];
		$profile_path = $_SESSION['profile_path'];
		$profile_img_src_small = $_SESSION['profile_img_src_small'];
		
		$comments .= '<div class="single_comment_entity commentbox-container" ';
		if(($like_count > 0) || ($row_count >= 1)) { 
			$comments .= 'style="display: block;"'; 
		}
		$comments .= '><form enctype="multipart/form-data" method="post" action="'.site_url('main/addComment/'.$post_id).'">';
		if(!empty($profile_img_src_small)) {
			$comments .= '<div class="before-comment-img" style="display: none; padding-right: 5px; float: left;">
			<a href="'.$profile_path.'"><img src="'.$profile_img_src_small.'" class="comment_profile_img" alt="profile_image" /></a>
			</div><div style="float: left; width: 300px;"><textarea name="comment" class="commentbox expand" id="comment_'.$post_id.'" rows="1" placeholder="Write a comment..."></textarea>
			<div class="grippie"></div></div><div class="clear"></div>';
		} else {
			$comments .= '<div class="before-comment-img" style="display: none; padding-right: 5px; float: left;">
			<a href="'.$profile_path.'"><img src="'.site_url('assets/images/profile/steve_avatar_small.png').'" class="comment_profile_img" alt="profile_image" /></a></div>
			<div style="float: left; width: 300px;"><textarea name="comment" class="commentbox expand" id="comment_'.$post_id .'" rows="1" placeholder="Write a comment..."></textarea>
			<div class="grippie"></div></div><div class="clear"></div>';
		}
		$comments .= '<input type="submit" value="" style="display: none;" /></form></div>';
	}*/
	$comments .= '</div><div class="clear"></div></div>';
	echo $comments;
?>