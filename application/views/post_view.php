<?php

$userNameUpper = ucwords($session['member']);
$postNum = sizeof($posts);
$mods_arr = array('ruckus_box', 'cwingrav');
$iscolor = false;

if(isset($_POST['colorbox'])) {
	$iscolor = $_POST['colorbox'];
}

if($iscolor == true) {
	echo '<div style="max-height: 620px; overflow-y: auto;"><div class="container_48">
			<div class="space_10"></div>';
}

if($posts[0]['is_news'] == 0) {

	if(sizeof($posts[0]) >= 1) {
		for($i=0; $i < $postNum; $i++) {
			
			$author = ucwords($posts[$i]['author']);
			$post_id = $posts[$i]['post_id'];
			$date = $posts[$i]['post_date_formatted'];
			$full_date = $posts[$i]['post_date_full'];
			$post_content = $posts[$i]['post_content'];
			$profile_img_src = $posts[$i]['profile_img_src'];
			
			if(isset($posts[$i]['is_flagged'])) {
				$flagged = $posts[$i]['is_flagged'];
			} else {
				$flagged = false;
			}
			
			if(isset($posts[$i]['main_img_src']) && isset($posts[$i]['medium_img_src']) && isset($posts[$i]['title'])) {
				$attached_img_big = $posts[$i]['main_img_src'];
				$attached_img_medium = $posts[$i]['medium_img_src'];
				$attached_img_title = $posts[$i]['title'];
			}
			
			$profile_path = $posts[$i]['profile_path'];
			$like_count = $posts[$i]['like_count'];
			
			if($posts[$i]['like_list'] == 1) {
				$like_list = $posts[$i]['like_list'];
			} else {
				$like_list = 0;
			}
			
		if($flagged == 0) {
	?>
	<div id="post-container-<?=$post_id ?>" class="post-container" <? if($i == $postNum - 1) { echo 'style="border: none;"';} ?>>
		<div id="post<?=$post_id ?>" class="post-position">
			<? if($iscolor == true) { ?>
				<div class="row relative-position">
			<? } else { ?>
			<div class="row">
			<? } ?>
			<? if(!empty($profile_img_src)) {
				if($iscolor == true) {
					echo '<div class="col_6">';
				} else {
					echo '<div class="col_5">';
				}
			?>
				<a href="<?=$profile_path ?>"><img src="<?=$profile_img_src // fix me, img not proper size?>" class="post_profile_img" alt="profile_image" /></a></div>
			<? } else { 
					if($iscolor == true) {
						echo '<div class="col_6">';
					} else {
						echo '<div class="col_5">';
					}
				?>
				<a href="<?=$profile_path ?>"><img src="<?=site_url('assets/images/profile/steve_avatar_small.png')?>" class="post_profile_img" /></a></div>
			<? }
				if($iscolor == true) {
						echo '<div class="col_30">';
					} else {
						echo '<div class="col_31">';
					}
			?>
				<a href="<?=$profile_path ?>" class="post-author"><b><?=$author ?></b></a><br />
				
			<?
				if($iscolor == true) {
					echo '<span class="color-666">'.$date.'</span></div><div class="clear"></div></div>
					<div class="col_12"><div class="wordwrap-padding">'.$post_content.'</div>';
				} else {
					echo '<div class="wordwrap">'.$post_content.'</div>';
				}
			?>
				
				<? if(isset($posts[$i]['main_img_src']) && isset($posts[$i]['medium_img_src'])) {
					
					if(empty($post_content)) {
						echo '<br />';
					} else {
						echo '<br />';
					}
					echo '<a href="'.$attached_img_big.'" target="'.$attached_img_big.'" rel="lightbox[gallery]" class="post-image"><img src="'.$attached_img_medium.'" alt="minds of chimera"></a><br />';
				}
				?>
				<div class="post-options">
				<? if(in_array($session['member'], $mods_arr)) { echo '<a class="flag" id="flagposts_' . $post_id . '">
				Flag</a>'; } ?>
				<? if($author == $userNameUpper && $iscolor == false) { echo '<a class="delete" id="post_' . $post_id . '">
				<img src="' . site_url('assets/images/delete_button.png') . '" alt="delete button" /></a>'; } ?>
				</div>
				
				<div class="top-comment-container">
					<div class="comment-options">
						<? if($userNameUpper != false) {
								if($like_list == 1) { ?>
						<div class="single_comment_entity"><a class="unlike" id="<?php echo 'unlike_'.$post_id; ?>">Unlike</a>&nbsp;&#183;&nbsp;<a class="comment-link">Comment</a>
						<? if($iscolor == false) { ?>
								&nbsp;&#183;&nbsp;<span class="color-666"><?=$date ?></span>
						<? } ?>
						</div>
						<? } else { ?>
						<div class="single_comment_entity"><a class="like" id="<?php echo 'like_'.$post_id; ?>">Dig it</a>&nbsp;&#183;&nbsp;<a class="comment-link">Comment</a>
						<? if($iscolor == false) { ?>
								&nbsp;&#183;&nbsp;<span class="color-666"><?=$date ?></span>
						<? } ?>
						</div>
						<?	}
						} 
						// Viewer currently not logged in. Display the post date only.
						else {
							echo '<div class="single_comment_entity"><span class="color-666">'.$date.'</span></div>';
						} ?>
						<? if($like_count > 0) {
								if($like_list == 1) {
						?>
						<? if($userNameUpper != false) { ?>
							<div class="single_comment_entity like_list">
							<img src="<?=site_url('/assets/images/likeup.png')?>" alt="likeup" class="float-left" /><span class="you_like">You <? if ($like_count > 1) { ?> and </span><span class="<?php echo 'like_'.$post_id; ?>"><? $new_like_count = $like_count-1; echo $new_like_count . "</span>"; if($new_like_count > 1) { ?> <span class="you_like">other</span> people dig this<? } else { echo ' <span class="you_like">other</span> person digs this'; } } else { echo 'dig this</span>'; } ?></div>
						<? } else { ?>
							<div class="single_comment_entity like_list">
							<img src="<?=site_url('/assets/images/likeup.png')?>" alt="likeup" class="float-left" /><span><? if($like_count > 1) { echo $like_count . " people like this"; } else { echo $like_count . " person likes this"; } ?></span></div>
						<? }
							} else { ?>
							<div class="single_comment_entity like_list">
							<img src="<?=site_url('/assets/images/likeup.png')?>" alt="likeup" class="float-left" /><span class="<?php echo 'like_'.$post_id; ?>"><?=$like_count ?></span> <? if($like_count > 1) { ?> people dig this<? } else { echo ' person digs this'; } ?></div>
						<? } 
						} ?>
					
					<?
						
					if(isset($posts[$i]['comments'][0]['view_more']) && !empty($posts[$i]['comments'][0]['view_more'])) {
						echo $posts[$i]['comments'][0]['view_more'];
					}
					?>
					</div>
					<div class="comment-container">
					<?
					$row_count = 0;
					$comments = array();
					
					for($j = 0; $j < sizeof($posts[$i]['comments']); $j++) {
						
						if(!isset($posts[$i]['comments'][$j]['comment_id'])) {
							break;
						}
						$comment_id = $posts[$i]['comments'][$j]['comment_id'];
						$post_id = $posts[$i]['comments'][$j]['post_id'];
						$author = ucwords($posts[$i]['comments'][$j]['author']);
						$comment_content = $posts[$i]['comments'][$j]['comment_content'];
						$date = $posts[$i]['comments'][$j]['comment_date_formatted'];
						$profile_img_src = $posts[$i]['comments'][$j]['profile_img_src'];
						$profile_path = $posts[$i]['comments'][$j]['profile_path'];
					?>
					<div id="comment_<?=$comment_id ?>" class="post_<?=$post_id ?> single_comment_entity">
						<div class="comment-position">
							<div class="row clearfix">
						<?
							if($iscolor == true) {
								echo '<div class="col_6">';
							} else {
								echo '<div class="col_4">';
							}
						?>
							<a href="<?=$profile_path ?>"><img src="<?=$profile_img_src ?>" class="comment_profile_img" alt="profile_image" /></a></div>
						<?
							if($iscolor == true) {
								echo '<div class="col_30">';
							} else {
								echo '<div class="col_32">';
							}
						?>
							<a href="<?=$profile_path ?>"><b><?=$author ?></b></a>&nbsp;
							<span class="wordwrap"><?=$comment_content ?></span><br />
							<span class="color-666"><?=$date ?></span>&nbsp;<!--<a class="like" id="like_<?=$comment_id ?>">Like</a>-->
							</div>
							</div>
							<? if($author == $userNameUpper && ($flagged !== 1 || $is_mod == 1)) { echo '<div class="post-options"><a class="delete" id="comments_' . $comment_id . '">
							<img src="' . site_url('assets/images/delete_button.png') . '" alt="delete button" /></a></div>'; } ?>
						</div><div class="clear"></div>
					</div>
					<? $row_count++; } ?>
					</div>
					<? if($userNameUpper != false) { ?>
					<div class="single_comment_entity commentbox-container" <? if(($like_count > 0) || ($row_count >= 1)) { echo ' display-block"'; } ?>>
						<form enctype="multipart/form-data" method="post" action="<?=site_url('main/addComment/'.$post_id) ?>">
							<? if(!empty($profile_img_src_small)) { ?>
								<div class="before-comment-img">
								<a href="<?=$profile_path ?>">
								<img src="<?=$profile_img_src_small?>" class="comment_profile_img" alt="profile_image" /></a>
								</div>
								<div class="float-left<? if($iscolor == true) { echo ' width-270;"'; } else { echo ' width-88'; } ?>"><textarea name="comment" class="commentbox expand" id="comment_<?php echo $post_id; ?>" rows="1" placeholder="Write a comment..."></textarea>
								<div class="grippie"></div></div><div class="clear"></div>
							<? } else { ?>
								<div class="before-comment-img"><a href="<?=$profile_path ?>"><img src="<?=site_url('assets/images/profile/steve_avatar_small.png')?>" class="comment_profile_img" alt="profile_image" /></a></div>
								<div style="float: left; <? if($iscolor == true) { echo 'width: 270px;'; } else { echo 'width: 88%;'; } ?>">
								<textarea name="comment" class="commentbox expand" id="comment_<?php echo $post_id; ?>" rows="1" placeholder="Write a comment..."></textarea>
								<div class="grippie"></div></div><div class="clear"></div>
							<? } ?>
							<input type="submit" value="" class="hide" />
						</form>
					</div>
					<? } ?>
				</div>
			</div></div><div class="clear"></div>
		</div>
	</div>
	<?
		if(sizeof($posts[0]) >= 1) {
			if($i == $postNum - 1) {
				echo '<input name="last_date" type="hidden" id="last-date" value="'.$full_date.'" />';
			}
		}
	?>
	<?
	} else { ?>
		<div id="post-container<?=$post_id ?>" class="post-container" 
		<? if($i == $postNum - 1) { echo 'style="border: none;"';} ?>>
		<img src="<?=site_url('assets/images/profile/blank_avatar_small.png')?>" class="post_profile_img" alt="profile_image" style="margin-right: 5px;" /><i>This post was flagged</i></div>
	<? }
		} // for loop for printing posts 
	} else {
	
		if(!isset($onthefly)) {
			echo '<div id="no-posts-container"><div class="warning-box" style="margin-top: 10px;">There are no posts on '.$owner.'\'s page</div>
			<img src="'.site_url('assets/images/post-something.png').'" alt="minecraft cow" /></div>';
		} else {
			return;
		}
	} 
} 
// print out news post recently added
else {
	$author = ucwords($posts[0]['author']);
	$post_id = $posts[0]['post_id'];
	$post_date = "Just Now";
	$full_date = $posts[0]['post_date_full'];
	$post_content = $posts[0]['post_content'];
	$profile_img_src = $posts[0]['profile_img_src'];
	$profile_path = $posts[0]['profile_path'];
	$postContent = '';
	
	$postContent .= '<div id="sidebar-post-container'. $post_id .'" class="post-container" style="margin-bottom: 10px;">
	<div id="sidebar-post'. $post_id .'">
	<div style="position: relative; width: 100%; margin-bottom: 8px;">
	<div class="col_6"><a href="'. $profile_path .'"><img src="'. $profile_img_src .'" class="post_profile_img" /></a></div>
	<div class="col_30" class="left-align"><a href="'. $profile_path .'"><b>'. $author .'</b></a>
	<br /><span class="wordwrap">'. $post_content .'</span><br /><span class="color-666">'. $post_date .'</span></div>
	<div class="clear"></div><a class="delete" id="posts_'. $post_id. '" style="display: inline;" ><img src="'. site_url('assets/images/delete_button.png').'" alt="delete button" /></a></div>';


	$postContent .= '<div class="comment-container ">
		<div class="single_comment_entity comment_choices"><a class="sidebar_like" id="sidebar_like_'. $post_id .'">Dig it</a>&nbsp;&#183;&nbsp;
		<a href="'.site_url('single/notify?postid='. $post_id).'" class="view_comments" name="comment">Comment</a></div>
		</div>';

	$postContent .= '</div>';
	echo $postContent;
}

if($iscolor == true) {
	echo '<div/></div>';
}
?>