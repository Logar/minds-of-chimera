<div class="clearfix">
	<div class="row">
		<div class="col_23">
			<h1>Welcome to the Community</h1><br />
			<img src="<?=SITE_URL?>assets/images/worldmap.gif" alt="world map" />
		</div>
		<div class="col_13">
			<br /><br />
			<div class="warning-box"><b>Please enter your pre-existing MineCraft username in order to unlock features for your account</b></div>
			<div id="error-msg"></div>
			<form name="register_form" enctype="multipart/form-data" method="post" action="<?=SITE_URL?>reg/userRegister" id="register-form">
				<table>
					<tr>
						<td><input type="text" name="register[fullname]" pattern="[A-Za-z\s]+" placeholder="Full name" value="<? if(isset($_POST['fullname'])) { echo $_POST['username']; }?>" size="30" required />
						</td>
					</tr>
					<tr>
						<td><input type="text" name="register[username]" placeholder="Username" value="<? if(isset($_POST['fullname'])) { echo $_POST['username']; }?>" size="30" required />
						</td>
					</tr>
					<tr>
						<td><input type="email" name="register[email]" class="email" value="<? if(isset($_POST['email'])) { echo $_POST['email']; }?>" placeholder="Email" size="30" required />
						</td>
					</tr>
					<tr>
						<td><input type="password" name="register[password1]" placeholder="Password" value="" size="30" required /><span></span></td>
					</tr>
					<tr>
						<td><input type="password" name="register[password2]" placeholder="Re-enter Password" value="" size="30" required /><span id="password_result"></span>
						</td>
					</tr>
					<tr>
					<td>
					<div class="clear"></div>
					<input id="signup" type="submit" value="Sign Up" />
					</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>