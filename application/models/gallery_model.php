<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/* filename: gallery_model.php */

/**
 * Model for accessing and changing user photo galleries
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
Class Gallery_model extends CI_Model {
	
	public function getPhotoGallery($per_page, $start) {
		
		$end = $start + $per_page;
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "
			SELECT `photo_id`, `main_img_src`, `thumbnail_img_src`, 
			`visibility`, `owner`, DATE_FORMAT(date_created,'%b %d %Y %h:%i %p') AS `date_formatted` 
			FROM `photo_gallery` 
			WHERE `visibility` = 'everyone' 
			ORDER BY(`date_created`) DESC
		";
		$qry1 = $this->db->query($qry_bind);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {

				$arr[$i]['photo_id'] = $row['photo_id'];
				$arr[$i]['main_img_src'] = $row['main_img_src'];
				$arr[$i]['thumbnail_img_src'] = $row['thumbnail_img_src'];
				$arr[$i]['owner'] = $row['owner'];
				$arr[$i]['date_created'] = $row['date_formatted'];
				$arr[$i]['visibility'] = $row['visibility'];
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getMyPhotoGallery($per_page, $start, $owner) {
		
		$end = $start + $per_page;
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT `player` FROM `all_users_view` WHERE `player`= ?";
		$qry1 = $this->db->query($qry_bind, array($owner));
		
		if($qry1->num_rows() > 0) {	
			
			// query binding
			$qry_bind = "
				SELECT `photo_id`, `main_img_src`, `thumbnail_img_src`, `visibility`, `owner`, 
				DATE_FORMAT(date_created,'%b %d %Y %h:%i %p') AS `date_formatted` 
				FROM `photo_gallery` 
				WHERE `owner`= ?
				ORDER BY(`date_created`) DESC
			";
			$qry2 = $this->db->query($qry_bind, array($owner));

			if($qry2->num_rows() > 0) {
				foreach ($qry2->result_array() as $row) {
					$arr[$i]['photo_id'] = $row['photo_id'];
					$arr[$i]['main_img_src'] = $row['main_img_src'];
					$arr[$i]['thumbnail_img_src'] = $row['thumbnail_img_src'];
					$arr[$i]['owner'] = $row['owner'];
					$arr[$i]['date_created'] = $row['date_formatted'];
					$arr[$i]['visibility'] = $row['visibility'];
					$i++;
				}
				return $arr;
			}
			return 0;
		} else {
			return -1;
		}
	}
	
	public function createGalleryPhoto($galleryImg) {
		
		// query binding
		$qry_bind = "
			INSERT INTO `photo_gallery` 
			(`main_img_src`, `thumbnail_img_src`, `owner`, `date_created`) 
			VALUES(?, ?, ?, ?)
		";
		
		$qry1 = $this->db->query($qry_bind, array($galleryImg['big_file_path'], $galleryImg['small_file_path'], 
			$galleryImg['photo_owner'], $galleryImg['photo_date']));
		
		$photo_id = $this->db->insert_id();
		
		return $photo_id;
	}
	
	public function deleteGalleryPhoto($galleryImg) {
		
		// query binding
		$qry_bind = "
			SELECT `main_img_src`, `thumbnail_img_src` FROM ? 
			WHERE `photo_id` = ? 
			AND `owner` = ?'
		";
		
		$qry1 = $this->db->query($qry_bind, array($galleryImg['table'], $galleryImg['id'], 
			$galleryImg['user']));
			
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
			
				$del_file = explode($site_url, $row['main_img_src']);
				$del_file[1] = '/'.$del_file[1];
				if(file_exists($del_file[1])) {
					unlink($del_file[1]);
				}
			
				$del_file = explode($site_url, $row['thumbnail_img_src']);
				$del_file[1] = '/'.$del_file[1];
				if(file_exists($del_file[1])) {
					unlink($del_file[1]);
				}
			}
		}
		
		// query binding
		$qry_bind = "
			DELETE FROM ?
			WHERE `photo_id` = ?
			AND `owner` = ?
		";
		
		$qry2 = $this->db->query($qry_bind, array($galleryImg['table'], $galleryImg['id'], 
			$galleryImg['user']));
			
		if($this->db->affected_rows() > 0) {
			$status = 1;
		} else {
			$status = 0;
		}
		return $status;
	}
	
	public function mc_changePhotoVisibility($galleryImg) {
		
		// query binding
		$qry_bind = "
			UPDATE `photo_gallery` SET `visibility`= ? 
			WHERE `photo_id`= ? AND `owner`= ?
		";
		
		$qry2 = $this->db->query($qry_bind, array($galleryImg['visibility'], $galleryImg['id'], 
			$galleryImg['user']));
			
		if($this->db->affected_rows() > 0) {
			$status = 1;
		} else {
			$status = 0;
		}
		return $status;
	}
}

?>
