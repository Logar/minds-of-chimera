<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: register_model.php */

/**
 * Model for registering users
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Register_model extends CI_Model {
	
	private $user;
	private $pid;
	
	public function __construct() {
		
		$sessionArray = $this->session->all_userdata();
		
		if(isset($sessionArray['member']) && !empty($sessionArray['member'])) {
			$this->user = $sessionArray['member'];
		} else {
			$this->user = false;
		}
		
		if(isset($sessionArray['pid']) && !empty($sessionArray['pid'])) {
			$this->pid = $sessionArray['pid'];
		} else {
			$this->pid = false;
		}
	}
	
	public function findRegisteredUser($currUser, $currEmail) {
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT * FROM `wp_users` WHERE `user_login`= ? OR `user_email`= ?";
		$qry1 = $this->db->query($qry_bind, array($currUser, $currEmail));
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				$arr[$i]['user_login'] = $row['user_login'];
				$arr[$i]['user_email'] = $row['user_email'];
				$arr[$i]['user_status'] = $row['user_status'];
				
				$i++;
			}
		}
		return false;
	}
	
	public function addNewUser($params) {

		// query binding
		$qry_bind = "INSERT INTO `wp_users` (user_login, user_pass, user_email, user_registered, display_name, 
		user_status, profile_img_src_big, profile_img_src, profile_img_src_small, profile_path) 
		VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$qry1 = $this->db->query($qry_bind, array($params['user_login'], $params['user_pass'], $params['user_email'],
		$params['user_registered'], $params['display_name'], $params['user_status'], $params['profile_img_src_big'], 
		$params['profile_img_src'], $params['profile_img_src_small'], $params['profile_path']));
		
		if($qry1->num_rows() > 0) {
			return true;
		}
		return false;
	}
	
	public function updateCurrentUser($params) {

		// query binding
		$qry_bind = "
			UPDATE `wp_users` SET user_pass = ?, user_email = ?, 
			user_registered = ?, user_status = ?, profile_img_src_big = ?,
			profile_img_src = ?, profile_img_src_small = ?, profile_path = ?
			WHERE user_login = ?
		";

		$qry1 = $this->db->query($qry_bind, array($params['user_pass'], $params['user_email'],
		$params['user_registered'], $params['user_status'], $params['profile_img_src_big'], 
		$params['profile_img_src'], $params['profile_img_src_small'], $params['profile_path'], $params['user_login']));
		
		if($qry1->num_rows() > 0) {
			return true;
		}
		return false;
	}
	
	public function setUserPass($newPass, $currUser) {
		
		// query binding
		$qry_bind = "UPDATE `wp_users` SET `user_pass` = ? WHERE `user_login`= ? ";
		$qry1 = $this->db->query($qry_bind, array($newPass, $currUser));
		
		if($this->db->affected_rows()) {
			return true;
		}
		
		return false;
	}
	
	public function updatePendingPassReset($currUser) {
		
		// query binding
		$qry_bind = "UPDATE `reset_passwords` SET `reset_done` = 1 WHERE `username`= ? ";
		$qry1 = $this->db->query($qry_bind, array($currUser));
		
		if($this->db->affected_rows()) {
			return true;
		}
		
		return false;
	}
	
	public function addPendingPassReset($currUser, $auth) {
		
		// query binding
		$qry_bind = "INSERT INTO `reset_passwords` (`username`, `auth`) VALUES(?, ?)";
		$qry1 = $this->db->query($qry_bind, array($currUser, $auth));
		
		if($this->db->affected_rows()) {
			return true;
		}
		
		return false;
	}
	
	public function findPendingPassReset($currUser, $auth) {
		
		// query binding
		$qry_bind = "SELECT * FROM `reset_passwords` WHERE `username`= ? AND `auth`= ? AND `reset_done` = 0";
		$qry1 = $this->db->query($qry_bind, array($currUser, $auth));
		
		if($qry1->num_rows() > 0) {
			return true;
		}
		
		return false;
	}
}

?>
