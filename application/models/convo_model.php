<?php 
if ( ! defined('BASEPATH')) exit ('No direct script access allowed');

/* filename: gallery_model.php */

/**
 * Model for the plugin Convo
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */	
class Convo_model extends CI_Model {
	
	/**
    * Stores the logged in username
    * @var string 
    */
	private $user;
	
	/**
    * Stores user's pid
    * @var string 
    */
	private $pid;
	
	/**
    * Stores locally accessed data
    * @var string array 
    */
	private $data;
		
	function __construct() {
		
		$sessionArray = $this->session->all_userdata();
		
		if(isset($sessionArray['member']) && !empty($sessionArray['member'])) {
			$this->user = $sessionArray['member'];
		} else {
			$this->user = false;
		}
		
		if(isset($sessionArray['pid']) && !empty($sessionArray['pid'])) {
			$this->pid = $sessionArray['pid'];
		} else {
			$this->pid = false;
		}
		
		$this->data = array();
		$this->load->library('doctrine');
    }
	
	public function addConvo($convo) {
		
		// query binding
		$qry_bind = "INSERT INTO `conversations` (`name`, `description`, `author_id`, `neartext`,
		`clicktext`) VALUES(?, ?, ?, ?, ?)";
		$qry1 = $this->db->query($qry_bind, array($convo['name'], $convo['desc'], $convo['pid'], $convo['neartext'],
		$convo['clicktext']));
		
		return $this->db->insert_id();
	}
	
	public function addConvoPopupEntity($convo, $convo_id) {
		// query binding
		$qry_bind = "INSERT INTO `popup_entries` (`convo_id`, `title`, `message`, `image_url`, `order_num`)
		VALUES (?, ?, ?, ?, ?)";
		
		$this->db->query($qry_bind, array($convo_id, $convo['title'], $convo['message'],
		$convo['convo_img_selected'], $convo['order_num']));
		
		$entry_id = $this->db->insert_id();
		
		// query binding
		$qry_bind = "UPDATE `conversations` SET `use_popup_on` = ? WHERE `id`= ?";
		$this->db->query($qry_bind, array($convo['popup_option'], $convo_id));

		if(isset($convo['responses'])) {
		
			if(isset($convo['gotos'])) {
				$i = 0;
				foreach($convo['responses'] as $resp) {
					
					if(isset($convo[$i]['gotos'])) {
						$goto_id = $convo[$i]['gotos'];
					} else {
						$goto_id = 1;
					}
					
					// query binding
					$qry_bind = "INSERT INTO `popup_responses` (`entry_id`, `convo_id`, `response`, `gotoentry_id`)
					VALUES(?, ?, ?, ?)";
					$this->db->query($qry_bind, array($entry_id, $convo_id, $resp, $goto_id));
					
					$i++;
				}
			} else {
				foreach($convo['responses'] as $resp) {
					
					// query binding
					$qry_bind = "INSERT INTO `popup_responses` (`entry_id`, `convo_id`, `response`) VALUES(?, ?, ?)";
					$this->db->query($qry_bind, array($entry_id, $convo_id, $resp));
				}
			}
		}
		return;
	}
	
	public function editConvo($convo) {
		
		$this->mc_deleteConvo($convo['convo_id']);
		return $this->addConvo($convo);
		
		/*
		if(isset($convo['use_popup_on'])) {
			
			// query binding
			$qry_bind = "SELECT `id` FROM `popup_entries` WHERE `convo_id` = ?";
			$qry3 = $this->db->query($qry_bind, array($convo['convo_id']));
			
			if($qry3->num_rows() > 0) {
				foreach ($qry3->result_array() as $row) {
					$start_entry = $row['id'];
				}
			} else {
				$start_entry = -1;
			}
		} else {
			$start_entry = -1;
		}
		
		if(isset($convo['use_popup_on'])) {
			
			// query binding
			$qry_bind = "UPDATE `conversations` SET `name` = ?, `description` = ?, 
			`neartext` = ?, `nearurl` = ?,
			`clicktext` = ?, `clickurl` = ?, `use_popup_on` = ?
			WHERE `id` = ? AND `author_id` = ?";
			
			$this->db->query($qry_bind, array($convo['name'], $convo['desc'], $convo['neartext'], 
			$convo['nearurl'], $convo['clicktext'], $convo['clickurl'], $convo['use_popup_on'], 
			$convo['convo_id'], $convo['pid']));

			if(isset($convo['entry_id']) && !empty($convo['entry_id'])) {
				// query binding
				$qry_bind = "SELECT `id` FROM `popup_entries` WHERE `id` = ?";
				$qry2 = $this->db->query($qry_bind, array($convo['entry_id']));
				
				if($qry2->num_rows() > 0) { 
					
					// query binding
					$qry_bind = "UPDATE `popup_entries` SET `title`= ?, 
					`message`= ?, `image_url`= ? 
					WHERE `convo_id`= ? AND `id`=?";
					
					if(empty($convo['convo_img_selected'])) {
						$convo['convo_img_selected'] = NULL;
					}
					
					$this->db->query($qry_bind, array($convo['popup_title'], $convo['popup_says'], 
					$convo['convo_img_selected'], $convo['convo_id'], $convo['entry_id']));
					
				} else if(isset($convo['use_popup_on'])) {
					
					// Get the order_num by counting all entries in conversation
					// query binding
					$qry_bind = "SELECT COUNT(*) AS num_of_entries FROM `popup_entries` 
					WHERE `convo_id`= ?";

					$numEntriesQuery = $this->db->query($qry_bind, array($convo['convo_id']));
					$numEntriesResults = $numEntriesQuery->row_array();
					$orderNum = $numEntriesResults['num_of_entries'] + 1;
					
					// query binding
					$qry_bind = "INSERT INTO `popup_entries` (`convo_id`, `title`, `message`, 
					`image_url`, `order_num`) VALUES (?, ?, ?, ?, ?)";
					
					if(empty($convo['convo_img_selected'])) {
						$convo['convo_img_selected'] = NULL;
					}
					
					$this->db->query($qry_bind, array($convo['convo_id'], $convo['popup_title'], 
					$convo['popup_says'], $convo['convo_img_selected'], $orderNum));

				}
			} else {
				// Get the order_num by counting all entries in conversation
				// query binding
				$qry_bind = "SELECT COUNT(*) AS num_of_entries FROM `popup_entries` 
				WHERE `convo_id`= ?";

				$numEntriesQuery = $this->db->query($qry_bind, array($convo['convo_id']));
				$numEntriesResults = $numEntriesQuery->row_array();
				$orderNum = $numEntriesResults['num_of_entries'] + 1;
				
				// query binding
				$qry_bind = "INSERT INTO `popup_entries` (`convo_id`, `title`, `message`, `image_url`, `order_num`) 
				VALUES (?, ?, ?, ?, ?)";
				
				if(empty($convo['convo_img_selected'])) {
					$convo['convo_img_selected'] = NULL;
				}
				
				$this->db->query($qry_bind, array($convo['convo_id'], $convo['popup_title'], $convo['popup_says'], 
				$convo['convo_img_selected'], $orderNum));
				
				$convo['entry_id'] = $this->db->insert_id();
				
				// query binding
				$qry_bind = "SELECT `id` FROM `popup_entries` WHERE `id` = ?";
				$qry2 = $this->db->query($qry_bind, array($convo['entry_id']));
			}
			
			if(isset($convo['responses'])) {
				foreach ($qry2->result_array() as $row) {
					$entry_id = $row['id'];
				}
				
				// query binding
				$qry_bind = "DELETE FROM `popup_responses` WHERE `entry_id`= ?";
				$this->db->query($qry_bind, array($convo['entry_id']));
				
				if(isset($convo['goto'])) {
					$i = 0;
					foreach($convo['responses'] as $resp) {
						
						if(isset($convo['goto'][$i])) {
							$goto_id = $convo['goto'][$i];
							
							// query binding
							$qry_bind = "INSERT INTO `popup_responses` (`entry_id`, `convo_id`, `response`, `gotoentry_id`) 
							VALUES(?, ?, ?, ?)";
							$this->db->query($qry_bind, array($convo['entry_id'], $convo['convo_id'], $resp, $goto_id));
						} else {
							
							// query binding
							$qry_bind = "INSERT INTO `popup_responses` (`entry_id`, `convo_id`, `response`) 
							VALUES(?, ?, ?)";
							$this->db->query($qry_bind, array($convo['entry_id'], $convo['convo_id'], $resp));
						}
						$i++;
					}
				} else {
					foreach($convo['responses'] as $resp) {
						
						// query binding
						$qry_bind = "INSERT INTO `popup_responses` (`entry_id`, `convo_id`, `response`) 
						VALUES(?, ?, ?)";
						$this->db->query($qry_bind, array($convo['entry_id'], $convo['convo_id'], $resp));
					}
				}
			}
		} else {
			
			// query binding
			$qry_bind = "UPDATE `conversations` SET `name` = ?, `description` = ?,
			`neartext` = ?, `nearurl` = ?,
			`clicktext` = ?, `clickurl` = ?
			WHERE `id` = ? AND `author_id` = ?";
			
			$this->db->query($qry_bind, array($convo['name'], $convo['desc'], $convo['neartext'], 
			$convo['nearurl'], $convo['clicktext'], $convo['clickurl'], 
			$convo['convo_id'], $convo['pid']));
		}
		return;*/
		
		return;
	}
	
	public function mc_deleteConvo($convo_id) {
		
		$status = 1;
		
		// query binding
		$qry_bind = "DELETE FROM `conversations` WHERE `id` = ?";
		$this->db->query($qry_bind, array($convo_id));
		
		// query binding
		$qry_bind = "DELETE FROM `popup_entries` WHERE `convo_id` = ?";
		$this->db->query($qry_bind, array($convo_id));
		
		// query binding
		$qry_bind = "DELETE FROM `popup_responses` WHERE `convo_id` = ?";
		$this->db->query($qry_bind, array($convo_id));
		
		return $status;
	}
	
	public function mc_deleteConvoEntry($entry_id) {
		
		$status = 0;
		
		// query binding
		$qry_bind = "DELETE FROM `popup_responses` WHERE `entry_id` = ?";
		$this->db->query($qry_bind, array($entry_id));
		
		// Must be 2nd for if stmnt because not all entries have responses
		// query binding
		$qry_bind = "DELETE FROM `popup_entries` WHERE `id` = ?";
		$this->db->query($qry_bind, array($entry_id));
		
		if($this->db->affected_rows() > 0) {
			$status = 1;
		}
		
		return $status;
	}

	public function dupConvo($convo) {
		
		// query binding
		$qry_bind = "SELECT * FROM `conversations` WHERE `id` = ?";
		$qry1 = $this->db->query($qry_bind, array($convo['id']));
		$results = $qry1->result_array();
		
		// query binding
		$qry_bind = "INSERT INTO `conversations` (`name`, `description`, `author_id`, `neartext`, `nearurl`,
		`clicktext`, `clickurl`) VALUES(?, ?, ?, ?, ?, ?, ?)";
		$this->db->query($qry_bind, array($results['name'], $results['description'], $convo['pid'],
		$results['neartext'], $results['nearurl'], $results['clicktext'], $results['clickurl']));

		return $results;
	}
	
	public function getConvos() {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT `id`, `author_id`, `name`, `description` FROM `conversations`
		WHERE `author_id`= ? ORDER BY `id` DESC";
		
		$qry2 = $this->db->query($qry_bind, array($this->pid)) or die(mysql_error());
		
		if($qry2->num_rows() > 0) {
			foreach ($qry2->result_array() as $row) {
				$arr[$i]['id'] = $row['id'];
				$arr[$i]['author_id'] = $row['author_id'];
				$arr[$i]['name'] = $row['name'];
				$arr[$i]['desc'] = $row['description'];
				
				$i++;
			}
			return $arr;
		} else {
			return;
		}
	}
	
	public function getEntireConvo($convo_id) {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT * FROM `conversations` WHERE `id` = ?";
		
		$qry1 = $this->db->query($qry_bind, array($convo_id)) or die(mysql_error());
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				$arr[$i]['id'] = $row['id'];
				$arr[$i]['author_id'] = $row['author_id'];
				$arr[$i]['name'] = $row['name'];
				$arr[$i]['desc'] = $row['description'];
				$arr[$i]['neartext'] = $row['neartext'];
				$arr[$i]['nearurl'] = $row['nearurl'];
				$arr[$i]['clicktext'] = $row['clicktext'];
				$arr[$i]['clickurl'] = $row['clickurl'];
				$arr[$i]['use_popup_on'] = $row['use_popup_on'];
				
				$i++;
			}
		}
		
		// query binding
		$qry_bind = "SELECT * FROM `popup_entries` WHERE `convo_id` = ? ORDER BY(`id`) ASC";
		$qry2 = $this->db->query($qry_bind, array($convo_id)) or die(mysql_error());
		$i = 0;
		if($qry2->num_rows() > 0) {
			foreach ($qry2->result_array() as $row) {
				$arr[$i]['entry_id'] = $row['id'];
				$arr[$i]['title'] = $row['title'];
				$arr[$i]['message'] = $row['message'];
				$arr[$i]['image_url'] = $row['image_url'];
				$arr[$i]['order_num'] = $row['order_num'];
				
				$i++;
			}
		}
		
		// query binding
		/*$qry_bind = "SELECT * FROM `popup_responses` WHERE `convo_id` = ? 
		AND `entry_id` = (
			SELECT min(`entry_id`) AS `entry_id_min` 
			FROM `popup_responses` 
			WHERE `convo_id`= ?
			GROUP BY (`convo_id`)
			ORDER BY (`id`) ASC
		)";*/
		//$qry_bind = "SELECT * FROM `popup_responses` WHERE `convo_id` = ?";
		
		$qry_bind = "SELECT id, convo_id, entry_id, 
			GROUP_CONCAT(gotoentry_id) AS gotoentry_id, 
			GROUP_CONCAT(response) AS response FROM `popup_responses` 
			WHERE `convo_id` = ? 
			GROUP BY (entry_id)
			ORDER BY (`id`) ASC";
		$qry3 = $this->db->query($qry_bind, array($convo_id)) or die(mysql_error());
		$i = 0;
		if($qry3->num_rows() > 0) {
			/*foreach ($qry3->result_array() as $row) {
				$arr[$i]['response'] = $row['response'];
				$arr[$i]['gotoentry_id'] = $row['gotoentry_id'];
				
				$i++;
			}*/
			$i = 0;
			foreach ($qry3->result() as $row) {
				$arr[$i]['responseGroup'] = $row;
				//print_r($arr[$i]['responseGroup']);
				$i++;
				//$arr[$i]['gotoentry_id'] = $row->gotoentry_id;
			}
			
			
		}
		
		return $arr;
	}
	
	public function getConvoEntryNames($convo_id) {
	
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT * FROM `popup_entries` WHERE `convo_id` = ? ORDER BY (`id`) ASC";
		$qry2 = $this->db->query($qry_bind, array($convo_id)) or die(mysql_error());

		if($qry2->num_rows() > 0) {
			foreach ($qry2->result_array() as $row) {
				$arr[$i]['entry_id'] = $row['id'];
				$arr[$i]['title'] = $row['title'];
				$arr[$i]['message'] = $row['message'];
				$arr[$i]['image_url'] = $row['image_url'];
				$arr[$i]['order_num'] = $row['order_num'];
				
				$i++;
			}
		}
		return $arr;
	}
	
	public function getSingleConvoEntry($convo_id, $entry_id) {
	
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT * FROM `popup_entries` WHERE `convo_id` = ? AND `id` = ? ORDER BY (`id`) ASC";
		$qry2 = $this->db->query($qry_bind, array($convo_id, $entry_id)) or die(mysql_error());
		$i = 0;
		if($qry2->num_rows() > 0) {
			foreach ($qry2->result_array() as $row) {
				$arr[$i]['entry_id'] = $row['id'];
				$arr[$i]['title'] = $row['title'];
				$arr[$i]['message'] = $row['message'];
				$arr[$i]['image_url'] = $row['image_url'];
				$arr[$i]['order_num'] = $row['order_num'];
				
				$i++;
			}
		}
		
		// query binding
		$qry_bind = "SELECT * FROM `popup_responses` WHERE `entry_id` = ? ORDER BY (`id`) ASC";
		$qry3 = $this->db->query($qry_bind, array($entry_id)) or die(mysql_error());

		$i = 0;
		if($qry3->num_rows() > 0) {
			foreach ($qry3->result_array() as $row) {
				$arr[$i]['response'] = $row['response'];
				$arr[$i]['gotoentry_id'] = $row['gotoentry_id'];
				
				$i++;
			}
		}
		
		return $arr;
	}
	
	public function currentOwner($convo) {
		
		// query binding
		$qry_bind = "SELECT * FROM `conversations` WHERE `author_id` = ? AND `id` = ?";
		$qry1 = $this->db->query($qry_bind, array($convo['pid'], $convo['convo_id']));
		
		if($qry1->num_rows() > 0) {
			return true;
		}
		
		return false;
	}
}
?>
