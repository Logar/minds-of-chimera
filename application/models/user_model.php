<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: user_model.php */

/**
 * Model for accessing and changing most user data
 * 
 * @TODO clean up sql in methods that are missing query bindings
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */	
class User_model extends CI_Model {
	
	/**
    * Stores the logged in username
    * @var string 
    */
	private $user;
	
	/**
    * Stores user's pid
    * @var string 
    */
	private $pid;
	
	/**
    * Stores locally accessed data
    * @var string array 
    */
	private $data;
		
	function __construct() {
		
		$sessionArray = $this->session->all_userdata();
		
		if(isset($sessionArray['member']) && !empty($sessionArray['member'])) {
			$this->user = $sessionArray['member'];
		} else {
			$this->user = false;
		}
		
		if(isset($sessionArray['pid']) && !empty($sessionArray['pid'])) {
			$this->pid = $sessionArray['pid'];
		} else {
			$this->pid = false;
		}
		
		$this->data = array();
    }
	
	public function getLevelsOf($_player) {
		
		// query binding
		$qry_bind = "SELECT * FROM `experiencetable` WHERE `name`= ?";
		$qry1 = $this->db->query($qry_bind, $_player);

		$retval = $qry1->row();
		return $retval;
	}
	
	public function getJobs($_player) {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT `experience`, `level`, `job` FROM `jobs` WHERE `username`= ?";
		$qry1 = $this->db->query($qry_bind, $_player);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				$arr[$i]['job'] = $row['job'];
				$arr[$i]['level'] = $row['level'];
				$arr[$i]['experience'] = $row['experience'];
				
				$i++;
			}
			return $arr;
		}
		
		return;
	}
	
	public function getTopJobPlayers()
	{
		$i = 0;
		$arr = array();
		
		/* DATABASE VIEW??
		 * SELECT g1.level, g1.username, g1.job, COUNT(*) AS rank
		 * FROM jobs AS g1
		 * JOIN jobs AS g2
		 * ON (g2.level) >= (g1.level)
		 * AND g1.job = g2.job
		 * GROUP BY g1.level,g1.job, g1.username
		 * HAVING rank <= 5
		 * ORDER BY g1.job, rank;
		 */

		/* Old Query
		 * SELECT `username`, `job`, `level` 
		 * FROM `jobs` AS j 
		 * WHERE j.level=(
		 * 		SELECT MAX(level) FROM jobs WHERE job=j.job
		 * ) 
		 * ORDER BY(level) DESC
		 * 
		 */

		// query binding
		$qry_bind = "SELECT level, username, job FROM top_jobs_view";
		$qry1 = $this->db->query($qry_bind);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				$arr[$i]['username'] = $row['username'];
				$arr[$i]['job'] = $row['job'];
				$arr[$i]['level'] = $row['level'];
				
				$i++;
			}
			return $arr;
		}
	}
	
	public function getMoneyOf($_player) {
		
		// query binding
		$qry_bind = "SELECT `balance` FROM `iconomy` WHERE `username`= ?";
		$qry1 = $this->db->query($qry_bind, $_player);
		
		if($qry1->num_rows() > 0) {
			$row = $qry1->row();
			return $row;
		}
		return false;
	}
	
	/* DATABASE VIEW: all_users_view
	 * SELECT wp.`id`, pt.`player`, wp.`user_status`, MAX(pt.`logintime`) AS `last_sc_login`, wp.`last_login` AS `last_web_login`, 
	 * wp.`profile_img_src_big`, wp.`profile_img_src`, wp.`profile_img_src_small`, wp.`profile_path` 
	 * FROM `PT_timelog` AS pt 
	 * LEFT OUTER JOIN `wp_users` AS wp 
	 * ON wp.`user_login`= pt.`player` 
	 * GROUP BY(pt.`player`) 
	 * ORDER BY wp.`user_status` DESC, `last_sc_login` DESC;
	 */
	public function mc_getMoreGamePlayers($start, $items) {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT * FROM `all_users_view` LIMIT ?, ?";
		$qry1 = $this->db->query($qry_bind, array($start, $items));
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				$arr[$i]['user_login'] = $row['player'];
				$arr[$i]['user_status'] = $row['user_status'];
				$arr[$i]['profile_img_src_small'] = $row['profile_img_src_small'];
				$arr[$i]['profile_path'] = $row['profile_path'];
				$timestamp = strtotime($row['last_sc_login']);
				
				if($timestamp == 0) {
					$arr[$i]['last_login'] = 'Never';
				} else {
					$arr[$i]['last_login'] = date("F jS Y h:i:s A", $timestamp);
				}
				$i++;
			}
			return $arr;
		}
		return false;
	}
	public function mc_getGamePlayersCount($cond) {
		
		// query binding
		$qry_bind = "SELECT COUNT(*) as `player_count` FROM `all_users_view` ? LIMIT 1";
		$qry1 = $this->db->query($qry_bind, $cond);
		
		if($qry1->num_rows() > 0) {
			$row = $qry1->row();
			return $row->player_count;
		}
	}
	
	public function getUserInfo($player) {
		
		// query binding
		$qry_bind = "SELECT * FROM `all_users_view` WHERE `player` = ? LIMIT 1";
		$qry1 = $this->db->query($qry_bind, $player);
		
		if($qry1->num_rows() > 0) {
			return $qry1->row();
		}
		
		return false;
	}
	
	public function getQuestStatus($_player) {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "
		SELECT `q_id`, `status` 
		FROM `uQuest_quest_status` as qstatus 
		WHERE qstatus.`player`= ?
		AND (
			qstatus.`status`='active' OR qstatus.`status`='complete'
		)";
		$qry1 = $this->db->query($qry_bind, $_player);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {

				$arr[$i]['q_id'] = $row['q_id'];
				$arr[$i]['status'] = $row['status'];
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function mc_getViewQueue($curr_user) {
		
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT * FROM `view_queue` WHERE `username` = ? AND `viewed` = 0 LIMIT 1";
		$qry1 = $this->db->query($qry_bind, $curr_user);
		
		if($qry1->num_rows() > 0) {
			
			$row = $qry1->row();
			
			$arr['id'] = $row->id;
			$arr['url'] = $row->url;
			
			
			// query binding
			$qry_bind = "UPDATE `view_queue` SET `viewed` = 1 WHERE `id` = ?";
			$qry2 = $this->db->query($qry_bind, $arr['id']);

			return $arr;
		}
		return false;
	}
	
	public function getRatings($pid) {
		
		$i = 0;
		$arr = array();
		$total_score = 0;
		// rating_comments_view sql
		/* SELECT rating_comments.id, wp_users.user_login AS author, patterns.name AS pattern_name, patterns.id 
		 * AS pattern_id, rating_comments.title, rating_comments.rating_t, rating_comments.score, locations.X, locations.Y, locations.Z, 
		 * rating_comments.createon, rating_comments.screen_src FROM rating_comments LEFT OUTER JOIN patterns 
		 * ON rating_comments.pattern_id = patterns.id LEFT JOIN locations ON locations.id = rating_comments.location_id 
		 * INNER JOIN wp_users ON wp_users.id = rating_comments.player_id WHERE is_on = 1;
		 */
		 
		// query binding
		$qry_bind = "SELECT * FROM `rating_comments_view` WHERE `pattern_id` = ? ORDER BY(`createon`) ASC";
		$qry1 = $this->db->query($qry_bind, $pid);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
		
				$arr[$i]['pattern_id'] = $row['pattern_id'];
				$arr[$i]['pattern_name'] = $row['pattern_name'];
				$arr[$i]['author'] = $row['author'];
				$arr[$i]['title'] = $row['title'];
				$arr[$i]['comment'] = $row['comment'];
				$arr[$i]['rating_t'] = $row['rating_t'];
				$arr[$i]['screen_src'] = $row['screen_src'];
				$arr[$i]['score'] = $row['score'];
				$total_score += $row['score'];
				$arr[$i]['X'] = $row['X'];
				$arr[$i]['Y'] = $row['Y'];
				$arr[$i]['Z'] = $row['Z'];
				$timestamp = strtotime($row['createon']);
				$arr[$i]['createdon'] = date("M j", $timestamp);
				$i++;
			}
			$arr[0]['total_score'] = $total_score / $i;
			return $arr;
		} else {
			return 0;
		}
	}
	
	public function getPatterns() {
		
		$i = 0;
		$arr = array();
		// pattern view sql
		// 1st one is the full one, 2nd is the original
		// SELECT p.id, u.user_login AS owner, p.name, p.description, p.screen_src, p.thumb_src, p.createdon, p.context, p.problem, p.solution, r.score, AVG(IF(r.rating_t != 3, NULL, r.score)) as total_score FROM `patterns` as p INNER JOIN `wp_users` as u ON u.id = p.player_id LEFT OUTER JOIN `rating_comments` as r ON r.pattern_id = p.id GROUP BY p.name;			
		/* SELECT p.id, u.user_login, p.name, p.description, p.screen_src, p.createdon, p.context, p.problem, p.solution, r.score FROM `patterns` as p INNER JOIN `wp_users` as u ON u.id = p.player_id FULL JOIN `rating_comments` as r ON r.pattern_id = p.id; */
		
		// query binding
		$qry_bind = "SELECT * FROM `patterns_view` ORDER BY(`name`) ASC";
		$qry1 = $this->db->query($qry_bind);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {

				$arr[$i]['id'] = $row['id'];
				$arr[$i]['name'] = $row['name'];
				$arr[$i]['context'] = $row['context'];
				$arr[$i]['author'] = $row['owner'];
				$timestamp = strtotime($row['createdon']);
				$arr[$i]['createdon'] = date("n/j/Y", $timestamp);
				$arr[$i]['total_score'] = $row['total_score'];
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getRelatedPatterns($currid) {
		
		$i = 0;
		$arr = array();
		// pattern view sql
		/* SELECT `patterns`.id, `related_patterns`.`pattern_id`, `name` FROM `patterns` INNER JOIN `related_patterns` ON `related_patterns`.`related_pattern_id` = `patterns`.`id`; */
		
		// query binding
		$qry_bind = "SELECT * FROM `related_patterns_view` WHERE `pattern_id` = ? ORDER BY(`name`) ASC";
		$qry1 = $this->db->query($qry_bind, $currid);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {

				$arr[$i]['id'] = $row['id'];
				$arr[$i]['name'] = $row['name'];
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getRelatedPatterns1D($currid) {
		
		$i = 0;
		$arr = array();

		/* DATABASE VIEW: related_patterns_view
		 * SELECT `patterns`.id, `related_patterns`.`pattern_id`, `name` 
		 * FROM `patterns` 
		 * INNER JOIN `related_patterns` 
		 * ON `related_patterns`.`related_pattern_id` = `patterns`.`id`;
		 */
		 
		// query binding
		$qry_bind = "SELECT * FROM `related_patterns_view` WHERE `pattern_id` = ? ORDER BY(`name`) ASC";
		$qry1 = $this->db->query($qry_bind, $currid);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				
				$arr[$i] = $row['name'];
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getAchievements($player_id) {
		$i = 0;
		$arr = array();

		// query binding
		$qry_bind = "SELECT a.`id`, a.`title`, a.`description`, a.`points`, pa.`player_id`, pa.`completed_date`
					FROM `achievements` AS a 
					LEFT OUTER JOIN `player_achievements` AS pa 
					ON a.`id` = pa.`achievement_id` 
					AND `player_id`= ? ORDER BY(a.`id`) ASC";
		$qry1 = $this->db->query($qry_bind, $player_id);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				
				$arr[$i]['id'] = $row['id'];
				$arr[$i]['title'] = $row['title'];
				$arr[$i]['description'] = $row['description'];
				$arr[$i]['points'] = $row['points'];
				
				if($row['completed_date'] != NULL) {
					$arr[$i]['completed'] = 1;
					$timestamp = strtotime($row['completed_date']);
					$arr[$i]['completed_date'] = date("m/d/y", $timestamp);
				} 
				else { 
					$arr[$i]['completed'] = 0;
				}
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getScoreboard() {
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "
		SELECT `player_id`, `player`, `total_points`
		FROM `player_points_view` ORDER BY (`total_points`) DESC";
		$qry1 = $this->db->query($qry_bind);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				
				$arr[$i]['player_id'] = $row['player_id'];
				$arr[$i]['player'] = $row['player'];
				$arr[$i]['total_points'] = $row['total_points'];
				
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getSinglePattern($pattern_id) {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT * FROM `patterns_view` WHERE `id`= ? LIMIT 1";
		$qry1 = $this->db->query($qry_bind, $pattern_id);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {

				$arr[$i]['id'] = $row['id'];
				$arr[$i]['name'] = $row['name'];
				$arr[$i]['context'] = $row['context'];
				$arr[$i]['screen_src'] = $row['screen_src'];
				$arr[$i]['problem'] = $row['problem'];
				$arr[$i]['solution'] = $row['solution'];
				$arr[$i]['author'] = $row['owner'];
				$timestamp = strtotime($row['createdon']);
				$arr[$i]['createdon'] = date("n/j/Y", $timestamp);
				$arr[$i]['score'] = '';//$row['score'];
				
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function updateRelatedPatterns($related_pattern_id, $patterns) {
		
		// query binding
		$qry_bind = "DELETE FROM `related_patterns` WHERE `pattern_id` = ?";
		$qry1 = $this->db->query($qry_bind, $related_pattern_id);
		
		foreach($patterns as $key=>$pattern_id) {
			
			// query binding
			$qry_bind = "INSERT INTO `related_patterns`(pattern_id, related_pattern_id) VALUES(?,?)";
			$this->db->query($qry_bind, array($related_pattern_id, $pattern_id));
		}
		return true;
	}
	
	public function updateSinglePattern($pattern_id, $data) {
		
		$result = mysql_query("UPDATE `patterns` SET `context` = '$data[context]', `problem` = '$data[problem]',
		`solution` = '$data[solution]' WHERE `id`=$pattern_id");
		return $result;
	}
	
	public function getAllQuests() {
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT id, `name`, `story_txt` FROM `uQuest_quests` GROUP BY (name)";
		$qry1 = $this->db->query($qry_bind);
		
		if($qry1->num_rows() > 0) {
			foreach ($qry1->result_array() as $row) {
				
				$arr[$i]['id'] = $row['id'];
				$arr[$i]['quest_name'] = $row['name'];
				$arr[$i]['story_txt'] = $row['story_txt'];
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getPlaytimeStatsOf($_player,$_world = null)
	{
		$retval = null;
		// query binding
		$qry_bind = "SELECT * FROM `PT_playerworldstats` WHERE `player`= ?";
		
		$qry1 = $this->db->query($qry_bind, $_player);
		// sum them
		if($_world == null) {
			$retval = $qry1->row(0);
			
			foreach($qry1->result() as $row) {	
				if($retval != $row) {
					$retval->died += $row->died;
					$retval->pk+= $row->pk;
					$retval->hk+= $row->hk;
					$retval->fk+= $row->fk;
					$retval->bplace+= $row->bplace;
					$retval->bdestroy += $row->bdestroy;
					$retval->moved += $row->moved;
				}
			}
		}
		else { // per world
			foreach($qry1->result() as $row) {
				if ($row->world == $_world) {
					$retval = $row;
				}
			}
		}
		return $retval;
	}
	
	public function getTotalPlaytime($curr_user) {

		/* DATABASE VIEW: pt_timelog_view
		 * SELECT `player`, (SUM(UNIX_TIMESTAMP(logouttime) - UNIX_TIMESTAMP(logintime))) / 3600 AS total_time 
		 * FROM `PT_timelog`
		 */
		 
		// query binding
		$qry_bind = "SELECT `total_time` FROM `pt_timelog_view` WHERE `player`= ?";
		$qry1 = $this->db->query($qry_bind, $curr_user);
		
		if($qry1->num_rows() > 0) {
			$row = $qry1->row();
			
			$total_time = $row->total_time;
			return $total_time;
		}
		return 0;
	}
	
	public function mc_addPost($post) {
		
		global $user;
		
		$post['content'] = preg_replace("/((https?:\/\/|www.)[a-z0-9\-\.\/\?=&amp;#~]+)/i",
		"<a href=\"$1\" class=\"inside-text-href\">$1</a>", $post['content']);
		
		if($post['post_type'] == "text") {
			
			// query binding
			$qry_bind = "
				INSERT INTO `posts`
				(`author_id`, `post_type`, `post_location`, `post_content`, `post_date`, `is_news`) 
				VALUES(?, 'text', ?, ?, ?)";
			$qry = $this->db->query($qry_bind, array($post['author_id'], $post['location'], 
				$post['content'], $post['date'], $post['is_news']
			));
			

			// query binding
			$qry_bind = "SELECT `post_id`, `post_content`,`author`, `post_date`, `profile_img_src`, 
					`profile_path`, `is_flagged` FROM `posts_view` WHERE `author_id`= ? 
					ORDER BY `post_id` DESC LIMIT 1";
			$qry1 = $this->db->query($qry_bind, $post['author_id']);
			
			if($qry1->num_rows() > 0) {
				
				$row = $qry1->row();
				
				// insert new notification for user
				if($post['location'] !== $user) {
					
					// query binding
					$qry_bind = "INSERT INTO `notifications`(`type`, `post_id`, `comment_id`, `author_id`, 
					`content`, `date`) VALUES('post', ?, null, ?, ?, ?)";
					
					$this->db->query($qry_bind, array($row->post_id, $post['author_id'], 
							$post['content'], $post['date']
					));
				}
				
				$postItems['post_id'] = $row->post_id;
				$postItems['post_date'] = $row->post_date;
				$postItems['post_content'] = $row->post_content;
				$postItems['author'] = $row->author;
				$postItems['profile_img_src'] = $row->profile_img_src;
				$postItems['profile_path'] = $row->profile_path;
				$postItems['is_flagged'] = $row->is_flagged;
			
				return $postItems;
			}
		} else if($post['post_type'] == "image") {
			
			// query binding
			$qry_bind = "INSERT INTO `posts` (`author_id`, `photo_id`, `post_type`, `post_location`, 
				`post_content`, `post_date`, `is_news`) VALUES(?, ?, 'image', ?, ?, ?)";
			$this->db->query($qry_bind, array($post['author_id'], $post['photo_id'], $post['location'],
					$post['content'], $post['date'], $post['is_news']
			));
			
			// query binding
			$qry_bind = "SELECT `post_id`, `post_content`, `author`, `post_date`, `profile_img_src`, 
					`profile_path`, `is_flagged` FROM `posts_view` WHERE `author_id`= ?
					ORDER BY `post_id` DESC LIMIT 1";
			
			$this->db->query($qry_bind, $post['author_id']);
			
			$row = $qry1->row();
			
			$postItems['post_id'] = $row->post_id;
			$postItems['post_date'] = $row->post_date;
			$postItems['post_content'] = $row->post_content;
			$postItems['author'] = $row->author;
			$postItems['profile_img_src'] = $row->profile_img_src;
			$postItems['profile_path'] = $row->profile_path;
			$postItems['is_flagged'] = $row->is_flagged;
			
			return $postItems;
		}
	}
	
	public function addPhoto($galleryImg) {
		
		// query binding
		$qry_bind = "INSERT INTO `photo_gallery`(`title`, `main_img_src`, `medium_img_src`, 
		`thumbnail_img_src`, `owner`, `date_created`) VALUES(?, ?, ?, ?, ?, ?)";
			
		$this->db->query($qry_bind, array($galleryImg['img_title'], 
		$galleryImg['big_file_path'], $galleryImg['medium_file_path'], $galleryImg['small_file_path'], 
		$galleryImg['photo_owner'], $galleryImg['photo_date']));
		
		$photo_id = $this->db->insert_id();
		
		return $photo_id;
	}
	
	public function mc_addComment($comment) {		
	
		// query binding
		$qry_bind = "INSERT INTO `comments`(`post_id`, `author_id`, `comment_content`, `comment_date`) 
			VALUES(?, ?, ?, ?)";
			
		$this->db->query($qry_bind, array($comment['post_id'], $comment['author_id'], 
			$comment['content'], $comment['date']));
		
		// query binding
		$qry_bind = "SELECT `comment_id`, `post_id`, `comment_content`, `profile_img_src`, 
			`profile_path` FROM `comments_view` WHERE `author_id`= ? 
			ORDER BY `comment_id` DESC LIMIT 1";
			
		$qry2 = $this->db->query($qry_bind, $comment['author_id']);
		$row = $qry2->row();
		
		// query binding
		// insert new notification for user
		$qry_bind = "INSERT INTO `notifications`(`type`, `post_id`, `comment_id`, `author_id`, 
		`content`, `date`) VALUES('comment', ?, ?, ?, ?, ?)";
			
		$this->db->query($qry_bind, array($row->post_id, $row->comment_id, $comment['author_id'], 
		$comment['content'], $comment['date']));
		
		$results = array();
		$results['new_comment'] = $row;
		
		return $results;
	}
	
	public function deletePostComment($post) {

		if($post['table'] == 'posts') {
		
			// query binding
			$qry_bind = "SELECT `photo_id` FROM `posts` WHERE `id`= ?";
			$qry1 = $this->db->query($qry_bind, $post['id']);
			
			$row = $qry1->row();
			
			if($row->photo_id !== null) {
				
				// query binding
				$qry_bind = "SELECT `photo_id`, `main_img_src`, `medium_img_src`, 
					`thumbnail_img_src` FROM `photo_gallery` WHERE `photo_id`= ?";
				$qry2 = $this->db->query($qry_bind, $row->photo_id);	
				
				if($qry2->num_rows() > 0) {
					$row2 = $qry2->row();
					
					$url = $row2->main_img_src;
					$url = substr($url, 1);
					
					if(file_exists($url)) {
						unlink($url);
					}
					
					$url = $row2->medium_img_src;
					$url = substr($url, 1);
					
					if(file_exists($url)) {
						unlink($url);
					}
					
					$url = $row2->thumbnail_img_src;
					$url = substr($url, 1);
					
					if(file_exists($url)) {
						unlink($url);
					}
					
					// query binding
					$qry_bind = "DELETE FROM `photo_gallery` WHERE `photo_id`= ?";
					$qry2 = $this->db->query($qry_bind, $row->photo_id);
				}
			}
			// query binding
			$qry_bind = "DELETE FROM `posts` WHERE `id`= ?";
			$this->db->query($qry_bind, $post['id']);
			
			// query binding
			$qry_bind = "DELETE FROM `notifications` WHERE `post_id`= ?";
			$this->db->query($qry_bind, $post['id']);
			
			// query binding
			$qry_bind = "DELETE FROM `comments` WHERE `post_id`= ?";
			$this->db->query($qry_bind, $post['id']);
		}
		else {
			// query binding
			$qry_bind = "DELETE FROM `comments` WHERE `id`= ?";
			$this->db->query($qry_bind, $post['id']);
			
			// query binding
			$qry_bind = "DELETE FROM `notifications` WHERE `comment_id`= ?";
			$this->db->query($qry_bind, $post['id']);
		}
	}
	
	public function updateNotificationCount() {	
		
		// query binding
		$qry_bind = "UPDATE `notifications_view` SET `viewed` = 1  WHERE `notify_user`= ? AND `viewed` = 0";
		$this->db->query($qry_bind, $this->user);
		
		if($this->db->affected_rows > 0) {
			$_SESSION['notifyCount'] = 0;
			return 1;
		} else {
			return 0;
		}
	}
	
	public function getNotifications($curr_user, $q) {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT COUNT(*) AS cnt FROM `notifications_view` 
			WHERE `notify_user` = ? AND `post_author` != ? AND `viewed` = 0";
		$qry1 = $this->db->query($qry_bind, array($curr_user, $curr_user));
		
		if($qry1->num_rows() > 0) {
			$row = $qry1->row();
			$arr[$i]['cnt'] = $row->cnt;
			
		}
		
		// query binding
		$qry_bind = "SELECT `type`, `post_id`, `comment_id`, `post_author`, `notify_user`, 
			`content`, `profile_img_src`, `profile_path`, `date` FROM `notifications_view` 
			WHERE (`notify_user`= ? AND `post_author` != ?) 
			ORDER BY `date` DESC";
		$qry2 = $this->db->query($qry_bind, array($curr_user, $curr_user));
		
		if($qry2->num_rows() > 0) {
			foreach ($qry2->result_array() as $row) {
				$arr[$i]['post_id'] = $row['post_id'];
				$arr[$i]['type'] = $row['type'];
				$arr[$i]['comment_id'] = $row['comment_id'];
				$arr[$i]['post_author'] = $row['post_author'];
				$arr[$i]['notify_user'] = $row['notify_user'];
				$arr[$i]['content'] = $row['content'];
				$arr[$i]['profile_img_src'] = $row['profile_img_src'];
				$arr[$i]['profile_path'] = $row['profile_path'];
				$arr[$i]['full_date'] = $row['date'];
				
				$timestamp = strtotime($row['date']);
				$date = $this->getHumanDate($timestamp);
				$arr[$i]['date'] = $date;
				
				if(empty($arr['profile_img_src'])) {
					$arr['profile_img_src'] = site_url('assets/images/profile/steve_avatar_small.png');
				}
				$i++;
			}
			return $arr;
		} else {
			return 0;
		}
	}
	
	public function mc_getLiveNotifications($curr_user, $last_date) {
		
		$i = 0;
		$arr = array();
		
		// query binding
		$qry_bind = "SELECT COUNT(*) AS cnt FROM `notifications_view` WHERE `notify_user` = ? 
					AND `post_author` != ? AND `viewed` = 0";
		$qry1 = $this->db->query($qry_bind, array($curr_user, $curr_user));
		
		if($qry1->num_rows() > 0) {
			$row = $qry1->row();
			$arr[$i]['cnt'] = $row->cnt;
			
		}
		
		// query binding
		$qry_bind = "SELECT `type`, `post_id`, `comment_id`, `post_author`, `notify_user`, 
					`content`, `profile_img_src`, `profile_path`, `date` FROM `notifications_view` 
					WHERE `notify_user` = ? AND `post_author` != ? AND `date` > ?
					ORDER BY `date` DESC";
		$qry2 = $this->db->query($qry_bind, array($curr_user, $curr_user, $last_date));
		
		if($qry2->num_rows() > 0) {
			foreach ($qry2->result_array() as $row) {
				$arr[$i]['post_id'] = $row['post_id'];
				$arr[$i]['type'] = $row['type'];
				$arr[$i]['comment_id'] = $row['comment_id'];
				$arr[$i]['post_author'] = $row['post_author'];
				$arr[$i]['notify_user'] = $row['notify_user'];
				$arr[$i]['content'] = $row['content'];
				$arr[$i]['profile_img_src'] = $row['profile_img_src'];
				$arr[$i]['profile_path'] = $row['profile_path'];
				$arr[$i]['full_date'] = $row['date'];
				
				$timestamp = strtotime($row['date']);
				$date = $this->getHumanDate($timestamp);
				$arr[$i]['date'] = $date;
				
				if(empty($arr['profile_img_src'])) {
					$arr['profile_img_src'] = site_url('/assets/images/profile/steve_avatar_small.png');
				}
				$i++;
			}
			return $arr;
		} else {
			return 0;
		}
	}
	
	public function getLastNotificationDate($curr_user) {
		
		// query binding
		$qry_bind = "SELECT `date` FROM `notifications_view` WHERE `notify_user`='$curr_user' 
					AND `post_author` != '$curr_user' ORDER BY `date` DESC";
		$qry = $this->db->query($qry_bind, array($curr_user, $curr_user));
		$row = $qry->row();
			
		return $row->date;
	}
	
	public function getPosts($curr_user, $user_profile, $q, $carry, $is_news) {
		
		if($this->user == false) {
			$like_owner = 'NA';
		} else {
			$like_owner = $this->user;
		}
		
		$i = 0;
		$arr = array();
		
		if($q == "non_user") {
			
			// query binding
			$qry_bind = "SELECT `post_id`, `photo_id`, `post_type`, `author`, `post_content`, `like_count`, 
						`profile_img_src`, `profile_path`, `like_list`, `post_date`, `is_flagged` 
						FROM `posts_view` WHERE `post_location`= ? AND `is_news`=0 
						ORDER BY `post_date` DESC LIMIT 20";
			$qry = $this->db->query($qry_bind, $curr_user);

		} else if($q == "curr_user") {
			
			// query binding
			$qry_bind = "SELECT `post_id`, `photo_id`, `post_type`, `author`, `post_content`, `like_count`,
						`profile_img_src`, `profile_path`, `like_list`, `post_date`, `is_flagged`
						FROM `posts_view` WHERE `is_news` = ? AND `post_location` = ?
						ORDER BY `post_date` DESC LIMIT 20";
			$qry = $this->db->query($qry_bind, array($is_news, $curr_user));
			
		} else if($q == "sidebar_feed") {
		
			if($is_news == 1) {
				$limit = 10;
			} else {
				$limit = 20;
			}
			
			// query binding
			$qry_bind = "SELECT `post_id`, `photo_id`, `post_type`, `author`, `post_content`, `like_count`, 
						`profile_img_src`, `profile_path`, `like_list` 
						REGEXP ? AS `like_list`, `post_date`, `is_flagged` 
						FROM `posts_view` WHERE `is_news` = ? ORDER BY `post_date` DESC LIMIT ?";
			$qry = $this->db->query($qry_bind, array($like_owner, $is_news, $limit));
		} else if($q == "single_post_user") {
			$postid = $carry;
			// query binding
			$qry_bind = "SELECT `post_id`, `author`, `post_content`, `like_count`, `profile_img_src`, 
						`profile_path`, `like_list` REGEXP ? AS `like_list`, `post_date`, 
						`is_flagged` FROM `posts_view` WHERE `post_id` = ? AND `is_news` = ?";
			$qry = $this->db->query($qry_bind, array($like_owner, $postid, $is_news));
		} else if($q == "single_post_non-user") {
			$postid = $carry;
			// query binding
			$qry_bind = "SELECT `post_id`, `author`, `post_content`, `like_count`, `profile_img_src`, 
						`profile_path`, `like_list`, `post_date`, `is_flagged` 
						FROM `posts_view` WHERE `post_id` = ? AND `is_news` = ?";
			$qry = $this->db->query($qry_bind, array($postid, $is_news));
		} else if($q == "single_post_any") {
			$postid = $carry;
			// query binding
			$qry_bind = "SELECT `post_id`, `author`, `post_content`, `like_count`, `profile_img_src`, 
						`profile_path`, `like_list`, `post_date`, `is_flagged`
						FROM `posts_view` WHERE `post_id` = ?";
			$qry = $this->db->query($qry_bind, $postid);
		} else if($q == "single_post_last") {
			$postid = $carry;
			// query binding
			$qry_bind = "SELECT `post_id`, `author`, `post_content`, `like_count`, `profile_img_src`, 
						`profile_path`, `like_list`, `post_date`, `is_flagged`
						FROM `posts_view` WHERE `is_news` = ? AND `post_id` > ?
						ORDER BY `post_id` DESC LIMIT 1";
			$qry = $this->db->query($qry_bind, array($is_news, $postid));
			
		} else if($q == "single_post_last_user") {
			$postid = $carry;
			// query binding
			$qry_bind = "SELECT `post_id`, `author`, `post_content`, `like_count`, `profile_img_src`, 
						`profile_path`, `like_list`, `post_date`, `is_flagged` 
						FROM `posts_view` WHERE `is_news` = ? AND `post_id` > ? 
						AND `post_location` = ? ORDER BY `post_id` DESC LIMIT 1";
			$qry = $this->db->query($qry_bind, array($is_news, $postid, $user_profile));
		}
		
		if($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row) {
			
				if(isset($row['post_type'])) {
					if($row['post_type'] == 'image') {
						
						// query binding
						$qry_bind = "SELECT `main_img_src`, `medium_img_src`, `title`
									FROM `photo_gallery` WHERE `photo_id` = ?";
						$qry2 = $this->db->query($qry_bind, $row['photo_id']);
						$row2 = $qry2->row();
						
						$arr[$i]['main_img_src'] = $row2['main_img_src'];
						$arr[$i]['medium_img_src'] = $row2['medium_img_src'];
						$arr[$i]['title'] = $row2['title'];
					}
				}
				$arr[$i]['post_id'] = $row['post_id'];
				$arr[$i]['author'] = $row['author'];
				$arr[$i]['post_content'] = $row['post_content'];
				$arr[$i]['like_count'] = $row['like_count'];
				$arr[$i]['profile_img_src'] = $row['profile_img_src'];
				$arr[$i]['profile_path'] = $row['profile_path'];
				$arr[$i]['like_list'] = $row['like_list'];
				$arr[$i]['is_flagged'] = $row['is_flagged'];
				
				$arr[$i]['post_date_full'] = $row['post_date'];
				
				$timestamp = strtotime($row['post_date']);
				$date = $this->getHumanDate($timestamp);
				$arr[$i]['post_date_formatted'] = $date;
				
				// Get all comments 
				$arr[$i]['comments'] = $this->getPostComments($arr[$i]['post_id']);
				
				// set if news or not
				$arr[0]['is_news'] = $is_news;
				
				$i++;
			}
			return $arr;
		}
		return false;
	}
	
	public function getPostComments($post_id) {
		
		// optimize this so we don't have to run a query to count first
		// query binding
		$qry_bind = "SELECT `comment_date`, COUNT(*) AS `cnt` 
					FROM `comments_view` WHERE `post_id` = ? ORDER BY `comment_date`";
		$qry1 = $this->db->query($qry_bind, $post_id);
		$row1 = $qry1->row();
		
		$cnt = $row1->cnt;
		
		$startCnt = $row1->cnt - 2;
		$comments = array();
		
		if($cnt >= 4) {
			
			// query binding
			$qry_bind = "SELECT `comment_id`, `post_id`, `author`, `comment_content`, 
						`profile_img_src`, `profile_path`, `comment_date` FROM `comments_view` 
						WHERE `post_id` = ? ORDER BY `comment_date` ASC LIMIT ?, 2";
			$qry2 = $this->db->query($qry_bind, array($post_id, $startCnt));
			
			$comments[]['view_more'] = '<div class="single_comment_entity view-all-comments-box">
				<a class="view-all-comments">View all '.$cnt.' comments</a>
				<div class="f1-upload-process" style="position: absolute;>
				<img src="'.site_url('assets/images/loading2.gif').'" alt="loading" /></div></div>';
		} else {
			
			// query binding
			$qry_bind = "SELECT `comment_id`, `post_id`, `author`, `comment_content`, `profile_img_src`, 
						`profile_path`, `comment_date` FROM `comments_view` WHERE `post_id` = ?
						ORDER BY `comment_date` ASC";
			$qry2 = $this->db->query($qry_bind, $post_id);
		}
		
		$i = 0;
		
		$row_count = 0;
		
		if($qry2->num_rows() > 0) {
			foreach ($qry2->result_array() as $row2) {
				$comments[$i]['comment_id'] = $row2['comment_id'];
				$comments[$i]['post_id'] = $row2['post_id'];
				$comments[$i]['author'] = ucwords($row2['author']);
				$comments[$i]['comment_content'] = $row2['comment_content'];
				$comments[$i]['profile_img_src'] = $row2['profile_img_src'];
				$comments[$i]['profile_path'] = $row2['profile_path'];
				
				$timestamp = strtotime($row2['comment_date']);
				$date = $this->getHumanDate($timestamp);
				
				$comments[$i]['comment_date_formatted'] = $date;
				
				if(empty($comments[$i]['profile_img_src'])) {
					$comments[$i]['profile_img_src'] = site_url('assets/images/profile/steve_avatar_small.png');
				}
				$i++;
			}
		}
		return $comments;
	}
	
	public function getUser($user, $md5_pass, $crypt_pass) {
		
		// query binding
		$qry_bind = "SELECT * FROM `wp_users` WHERE (`user_login` = ? OR `user_email` = ?) 
					AND (`user_pass` = ? OR `user_pass` = ?)";
		$loginResult = $this->db->query($qry_bind, array($user, $user, $crypt_pass, $md5_pass));

		if($loginResult->num_rows() > 0) {
			
			// query binding
			$qry_bind = "SELECT `pass_reset` FROM `wp_users` WHERE `pass_reset` = 0 
					AND (`user_login` = ? OR `user_email` = ?) AND `user_pass` = ?";
			$resetResult = $this->db->query($qry_bind, array($user, $user, $md5_pass));
			
			if($resetResult->num_rows() > 0) {
				
				// query binding
				$qry_bind = "UPDATE `wp_users` SET `pass_reset` = 1, `user_pass` = ?
						WHERE `user_login` = ? OR `user_email`= ?";
				$this->db->query($qry_bind, array($crypt_pass, $user, $user));
			}
			
			// query binding
			$qry_bind = "UPDATE `wp_users` SET `last_login` = now() 
						WHERE `user_login` = ? OR `user_email` = ?";
			$this->db->query($qry_bind, array($user, $user));
			
			return $loginResult->row();
		}
		else {
			return false;
		}
	}
	
	public function commentsGenerator($more_comments, $getMore) {
		
		$comments = '';
		
		// query binding
		$qry_bind = "SELECT comment_date, COUNT(*) AS cnt FROM comments_view
						WHERE post_id = ? ORDER BY comment_date";
		$qry1 = $this->db->query($qry_bind, $more_comments['post_id']);
		$row1 = $qry1->row();
		$cnt = $row1->cnt;
		
		if($more_comments['user'] != false) {
			$user = ucwords($more_comments['user']);
		} else {
			$user = false;
		}
		
		if($getMore == 1) {
			
			$cnt = $cnt - 2;
			
			// query binding
			$qry_bind = "SELECT comment_id, post_id, author, comment_content, comment_date, 
						profile_img_src, profile_path, comment_date FROM comments_view 
						WHERE post_id = ? ORDER BY comment_date ASC LIMIT ?";
			$qry2 = $this->db->query($qry_bind, array($more_comments['post_id'], $cnt));
		
		} else {
			
			if($cnt >= 4) {
				$startCnt = $cnt - 2;
				
				// query binding
				$qry_bind = "SELECT comment_id, post_id, author, comment_content, profile_img_src, 
							profile_path, comment_date FROM comments_view 
							WHERE post_id = ? ORDER BY comment_date ASC LIMIT ?, 2";
				$qry2 = $this->db->query($qry_bind, array($more_comments['post_id'], $startCnt));
				
				$comments .= '<div class="single_comment_entity view-all-comments-box">
						<a class="view-all-comments">View all '.$cnt.' comments</a></div>';
			} else {
				// query binding
				$qry_bind = "SELECT comment_id, post_id, author, comment_content, profile_img_src, 
							profile_path, comment_date FROM comments_view WHERE post_id = ?
							ORDER BY comment_date ASC";
				$qry2 = $this->db->query($qry_bind, $more_comments['post_id']);
			}
		}
		$row_count = 0;
		$like_count = 0;
		$comments_arr = array();
		
		foreach ($qry2->result_array() as $row2) {
			$comments_arr[$row_count]['comment_id'] = $row2['comment_id'];
			$comments_arr[$row_count]['post_id'] = $row2['post_id'];
			$comments_arr[$row_count]['author'] = ucwords($row2['author']);
			$comments_arr[$row_count]['comment_content'] = $row2['comment_content'];
			$comments_arr[$row_count]['profile_img_src'] = $row2['profile_img_src'];
			$comments_arr[$row_count]['profile_path'] = $row2['profile_path'];

			$timestamp = strtotime($row2['comment_date']);
			$comments_arr[$row_count]['comment_date'] = $this->getHumanDate($timestamp);
			
			if(empty($comments_arr[$row_count]['profile_img_src'])) {
				$comments_arr[$row_count]['profile_img_src'] = site_url('assets/images/profile/steve_avatar_small.png');
			}
			$row_count++;
		}
		
		return $comments_arr;
	}
	
	public function getPostsCount($user) {
		
		// query binding
		$qry_bind = "SELECT COUNT(*) as post_count FROM `posts_view` WHERE `post_location` = ?";
		$qry1 = $this->db->query($qry_bind, $user);
		$row = $qry1->row();
		
		return $row->post_count; 
	}
	
	public function mc_getMorePosts($start_date, $limit, $post_location) {
		$content = '';
		
		if($this->user !== false) {
			// query binding
			$qry_bind = "SELECT `post_id`, `author`, `post_content`, `like_count`, 
						`profile_img_src`, `profile_path`, `like_list` REGEXP (?) AS `like_list`, 
						`post_date`, `is_flagged` FROM `posts_view` WHERE `post_location`= ? 
						AND `post_date` < ? ORDER BY `post_date` DESC LIMIT ?";
			$qry1 = $this->db->query($qry_bind, array($this->user, $post_location, $start_date, $limit));
			
		} else {
			// query binding
			$qry_bind = "SELECT `post_id`, `author`, `post_content`, `like_count`, 
				`profile_img_src`, `profile_path`, `post_date`, `like_list`, `is_flagged` 
				FROM `posts_view` WHERE `post_location` = ? AND `post_date` < ? 
				ORDER BY `post_date` DESC LIMIT ?";
			$qry1 = $this->db->query($qry_bind, array($post_location, $start_date, $limit));
		}
		
		$i = 0;
		$arr = array();
		$is_news = 0;
		
		foreach ($qry1->result_array() as $row) {
			if(isset($row['post_type'])) {
				if($row['post_type'] == 'image') {
					
					// query binding
					$qry_bind = "SELECT `main_img_src`, `medium_img_src`, `title`
								FROM `photo_gallery` WHERE `photo_id` = ?";
					$qry2 = $this->db->query($qry_bind, $row['photo_id']);
				
					$row2 = $qry2->row();
					$arr[$i]['main_img_src'] = $row2->main_img_src;
					$arr[$i]['medium_img_src'] = $row2->medium_img_src;
					$arr[$i]['title'] = $row2->title;
				}
			}
			$arr[$i]['post_id'] = $row['post_id'];
			$arr[$i]['author'] = $row['author'];
			$arr[$i]['post_content'] = $row['post_content'];
			$arr[$i]['like_count'] = $row['like_count'];
			$arr[$i]['profile_img_src'] = $row['profile_img_src'];
			$arr[$i]['profile_path'] = $row['profile_path'];
			$arr[$i]['like_list'] = $row['like_list'];
			$arr[$i]['is_flagged'] = $row['is_flagged'];
			
			$arr[$i]['post_date_full'] = $row['post_date'];
			
			$timestamp = strtotime($row['post_date']);
			$date = $this->getHumanDate($timestamp);
			$arr[$i]['post_date_formatted'] = $date;
			
			// Get all comments 
			$arr[$i]['comments'] = $this->getPostComments($arr[$i]['post_id']);
			
			// set if news or not
			$arr[0]['is_news'] = $is_news;
			
			$i++;
		}
		return $content;
	}
	
	public function getHumanDate($long_date) {
				
		$currTime = time();
		$one_hour = $currTime - (60 * 60);
		$one_day = $currTime - (24 * 60 * 60);
		$three_days = $currTime - (4 * 24 * 60 * 60);
		$date = '';
		
		if($long_date <= $one_day) {
			if($long_date <= $three_days - 1) {
				$date = date("M j", $long_date);
			} else {
				$date = date("l", $long_date);
			}
		} else {
			
			if($long_date <= $one_hour) {
				if($long_date <= $one_hour - 59) {
				
					$t = intval(($currTime - $long_date) / (60 * 60));
					
					// if less than 2 hours
					if($t == 1) {
						$date = "about an hour ago";
					} else {
						$date = "about " . $t . " hours ago";
					}
				}
			} else if($long_date > $one_hour) {
				
				$elapsed = $currTime - $long_date;
				if($elapsed < 5) {
					$date = "Just now";
				}
				if($elapsed < 60) {
					$date = $elapsed . " seconds ago";
				} else {
					$date = intval(($currTime - $long_date) / 60) . " minutes ago";
				}
			}
		}
		return $date;
	}
}
?>
