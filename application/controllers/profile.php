<?php
/* filename: profile.php */

/* Include the Template class */
include('Template.php');

/**
 * Prepares user profile pages
 *
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Profile extends MOC_Controller
{	
	function __construct() {
		
		// Call the base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
        
        $this->data['page_title'] = "Minds of Chimera";
	}
	
	function index() {
		
		$this->data['page_title'] = $this->userSession['member'];
		$this->data['page'] = 'welcome';

		$this->data['css'] =  array('/assets/css/slimbox2.css', '/assets/css/colorbox.css');
		$this->data['scripts'] = array('/assets/scripts/lightbox/prototype.js', '/assets/scripts/slimbox2.js', '/assets/scripts/jquery.form.js');
		
		if(!isset($this->userSession['member'])) {
			
			// Check if the cookie exists
			if(isset($_COOKIE['mc_autologin']))
			{
				parse_str($_COOKIE['mc_autologin'], $received);

				$this->userdata = Profile::get_users_local($received);
				// Make a verification
				if($this->userdata !== false) 
				{
					//Login_control Successful. Regenerate session ID to
					//prevent session fixation attacks
					session_regenerate_id();
					
					$this->userSession['member'] = $this->userdata['user_login'];
					$this->userSession['profile_img_src_small'] = $this->userdata['profile_img_src_small'];
					$this->userSession['email'] = $this->userdata['user_email'];
					$this->userSession['pid'] = $this->userdata['id'];
					Profile::user_pages($this->userdata['user_login']);
				}
			} else {
				// load page partials
				$this->data['header'] = $this->load->view('_blocks/header', $this->data, true);
				$this->data['sidebar'] = $this->load->view('sidebar', $this->data, true);
				$this->data['menu'] = $this->load->view('menu', $this->data, true);
				$this->data['content']  = $this->load->view('splash', $this->data, true);
				$this->data['footer']  = $this->load->view('_blocks/footer', $this->data, true);
				$this->load->view('layouts/splash_layout',$this->data);
			}
		} else {
			$seg_user = $this->uri->segment(1);
			if(empty($seg_user)) {
				Profile::user_pages($this->userSession['member']);
			} else {
				Profile::user_pages($seg_user);
			}
		}
	}

	private function get_users_local($login) 
	{
		$md5_pass = 'deadpass';
		$crypt_pass = $login['hash'];
		
		$results = $this->user_model->getUser($login['usr'], $md5_pass, $crypt_pass);
		return $results;
	}

	public function user_pages($userName) {

		// Call the template class
		$template = new Template();
		
		$this->user_login = '';
		
		$this->userSession['userPostsCount'] =  $this->user_model->getPostsCount($userName);
		$total_playtime = abs($this->user_model->getTotalPlaytime($userName));
		
		if($total_playtime == 0) {
			$this->data['total_playtime'] = 0;
		} else {
			$this->data['total_playtime'] = round($total_playtime, 1);
		}
		
		$this->data['css'] =  array('/assets/css/colorbox.css');
		$this->data['scripts'] = array('/assets/scripts/lightbox/prototype.js', '/assets/scripts/jquery.colorbox.js', '/assets/scripts/jquery.form.js');
		$this->data['inline_scripts'] = '$j("a[rel]").colorbox({ transition:"none", width:"90%", height:"90%", maxWidth:"90%", maxHeight:"90%;"});';
		
		$this->data['page'] = "home";

		$this->data['page_title'] = ucwords($userName);
		
		$balance = $this->user_model->getMoneyOf($userName);
		if(!empty($userName)) {
			if($balance != false) {
				$this->data['money'] = floor($balance->balance);
			} else {
				$this->data['money'] = 0;
			}

			$this->userInfo = $this->user_model->getUserInfo($userName);
			
			if(isset($this->userInfo->user_status)) {
				$this->data['user_status'] = $this->userInfo->user_status;
			} else {
				$this->data['user_status'] = '';
			}
			
			if(isset($this->userInfo->profile_img_src)) {
				$this->data['profile_img_src'] = $this->userInfo->profile_img_src;
			} else {
				$this->data['profile_img_src'] = '';
			}
			
			if(isset($this->userInfo->profile_img_src_big)) {
				$this->data['profile_img_src_big'] = $this->userInfo->profile_img_src_big;
			} else {
				$this->data['profile_img_src_big'] = '';
			}
			
			if(isset($this->userInfo->profile_path)) {
				$profile_path = $this->userInfo->profile_path;
			} else {
				$profile_path = '';
			}

			$this->data['owner_profile_path'] = $profile_path;
			
			if(empty($profile_path) || $profile_path == NULL) {
				$this->data['owner'] = $this->uri->segment(2);
			} else {
				$s = explode("/", $profile_path);
				$this->data['owner'] = end($s);
			}
			
			if(empty($this->data['owner'])) {
				$this->data['owner'] = $userName;
			}
		
			$this->data['posts'] = $template->getPosts($this->data['owner']);
			
			//$lvls = $this->user_model->getLevelsOf($userName);
			$jobs = $this->user_model->getJobs($userName);
			$this->data['jobs'] = $jobs;

			$this->userStats = $this->user_model->getPlaytimeStatsOf($userName, null);

			if(isset($this->userStats->died)) {
				$this->data['died'] = $this->userStats->died;
			} else {
				$this->data['died'] = 0;
			}
			if(isset($this->userStats->pk)) {
				$this->data['pk'] = $this->userStats->pk;
			} else {
				$this->data['pk'] = 0;
			}
			if(isset($this->userStats->hk)) {
				$this->data['hk'] = $this->userStats->hk;
			} else {
				$this->data['hk'] = 0;
			}
			if(isset($this->userStats->fk)) {
				$this->data['fk'] = $this->userStats->fk;
			} else {
				$this->data['fk'] = 0;
			}
			if(isset($this->userStats->bplace)) {
				$this->data['bplace'] = $this->userStats->bplace;
			} else {
				$this->data['bplace'] = 0;
			}
			if(isset($this->userStats->bdestroy)) {
				$this->data['bdestroy'] = $this->userStats->bdestroy;
			} else {
				$this->data['bdestroy'] = 0;
			}
			if(isset($this->userStats->moved)) {
				$this->data['moved'] = ceil($this->userStats->moved);
			} else {
				$this->data['moved'] = 0;
			}
			
			if(empty($this->data['owner'])) {
				//$this->data['errorMsg'] = '<br /><div class="error-box">The username you searched for does not exist</div><br />';
				//$page_missing = true;
			}
			
		} else {
			$this->data['money'] = 0;
			
			$this->userInfo = $this->user_model->getUserInfo($userName);
			$this->data['user_status'] = $this->userInfo->user_status;
			if(!empty($this->userInfo->player)) {
				$this->data['profile_img_src'] = $this->userInfo->profile_img_src;
				$profile_path = $this->userInfo->profile_path;
				$this->data['owner_profile_path'] = $profile_path;

				if(empty($profile_path) || $profile_path == NULL) {
					$this->data['owner'] = $this->uri->segment(2);
				} else {
					$s = explode("/", $profile_path);
					$this->data['owner'] = end($s);
				}
				
				if(empty($this->data['owner'])) {
					$this->data['owner'] = $userName;
				}
				
				$this->data['posts'] = $template->getPosts($this->data['owner']);
				
				$this->data['died'] = 0;
				$this->data['pk'] = 0;
				$this->data['hk'] = 0;
				$this->data['fk'] = 0;
				$this->data['bplace'] = 0;
				$this->data['bdestroy'] = 0;
				$this->data['moved'] = 0;
			}
			if(empty($this->data['owner'])) {
				$page_missing = true;
			}
		}
		
		if(isset($page_missing)) {
				
			$template->makePageTemplate("page_missing", "one_col", $this->data);
		} else {
			$this->data['post_feed']  = $this->load->view('post_view', $this->data, true);
			$template->makePageTemplate("profile_view", "one_col", $this->data);
		}
		
	}
}
?>
