<?php
/* filename: news.php */

/* Include the Template class */
include('Template.php');

/**
 * Controller for displaying news to users
 * @TODO clean up stray sql and html
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class News extends MOC_Controller
{	
	function __construct() {
		
        // Call base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
        
        $this->data['page_title'] = "Minds of Chimera";
	}
	
	/**
	 * Depreciated: new function is located in sidebar.php
	 * @param array $param
	 */
	function feed($param) {
		
		echo "Started News Feed function<br />";
		$this->timer = new FuncTimer();
		$start = $this->timer->getStartTime();
		
		$content = '';
		$count = 0;
		
		if($param == "devteam") {
			$content .= '<div class="top-twtr-tweet">
					<div class="twtr-hd" style="margin: 10px;">
					<div class="twtr-profile-img-anchor">
					<img alt="profile" class="twtr-profile-img" src="'.SITE_URL.'/assets/images/isue_logo_small.png"></div><h5>The Minds Team</h5><h4>Minds of Chimera</h4></div><div class="clear"></div>';
		}
		
		if($param == "devteam") {
			$content .= '<div class="normal tiny-inner all-posts-news-container">';
			
			if($this->userSession['member'] !== false) {
				
				$developers = array("cwingrav", "ruckus_box", "evali", "chillerman91", 
						"julietnpn", "EdBighead11", "thaigrl", "raidendex");
				if(in_array($this->userSession['member'], $developers)) {
					$content .= '<form class="post-form" enctype="multipart/form-data" method="post" 
							action="'.SITE_URL.'main/addPost/news">
							<textarea name="postBox" id="post-box-news" class="expand" rows="1" 
									placeholder="Share some news with people..." style="width: 303px; 
									resize: none; z-index: 10;"></textarea>
								<div id="post-box-news-bottom"><input type="submit" value="Post" 
									style="float: right;" /></div><div class="clear"></div></form><br />';
					$content .= '<div class="all-posts-news">';
				}
			}
		} 
		$qry1 = '';
		if($param == "devteam") {

			$qry1 = mysql_query("SELECT `post_id`, `author`, `post_content`, `like_count`, 
					`profile_img_src`, `profile_path`, `like_list` REGEXP '($this->userSession['member'])' 
					AS `like_list`, DATE_FORMAT(post_date,'%b %d %Y %h:%i %p') AS `post_date_formatted` 
					FROM `posts` WHERE `status`=1 ORDER BY `post_date` DESC LIMIT 6") or die(mysql_error());
			$qry2 = mysql_query("SELECT COUNT(`post_id`) as num_posts FROM `posts` 
					WHERE `status`=1") or die(mysql_error());
			$count_results = mysql_fetch_array($qry2);
		}
		if(!empty($qry1)) {
			while ($row = mysql_fetch_array($qry1)) 
			{ 
				$author = $row['author'];
				$date = $row['post_date_formatted'];
				$post_id = $row['post_id'];
				$profile_img_src = $row['profile_img_src'];
				$profile_path = $row['profile_path'];
				$like_count = $row['like_count'];
				
				if(isset($row['like_list'])) {
					$like_list = $row['like_list'];
				}
				$content .= '<div id="post-container'.$post_id .'" class="post-container" 
						style="margin-bottom: 10px;"><div id="post'.$post_id.'">
								<div style="position: relative; width: 100%;">';
				if(!empty($profile_img_src)) {
					$content .= '<div class="grid_10"><a href="'.$profile_path .'">
							<img src="'.$profile_img_src .'" class="post_profile_img" /></a></div>';
				} else {
					$content .= '<div class="grid_10"><a href="'.$profile_path .'">
							<img src="'.SITE_URL.'assets/images/profile/steve_avatar_small.png" 
									class="post_profile_img" /></a></div>';
				}
				$content .= '<div class="grid_37"><a href="'.$profile_path .'"><b>'.$author .'</b></a>';
				$content .= '<br /><span style="word-wrap: break-word;">'.$row['post_content'] .'</span>
						<br /><span style="color: #666;">'.$date .'</span></div><div class="clear"></div>';
				
				if($author == $this->userSession['member']) {
					$content .= '<a class="delete" id="posts_' . $post_id . '">
							<img src="'.SITE_URL.'assets/images/delete_button.png" alt="delete button" />
							</a>'; 
				}
				
				$content .= '</div>';
				
				$content .= '<div style="text-align: left;"><div class="comment-container">';
				if($this->userSession['member'] !== false) { 
					if(!empty($like_list)) {
						$content .= '<div class="single_comment_entity comment_choices">
								<a class="sidebar_unlike" id="unlike_'.$post_id .'" name="unlike">Unlike</a>
								&nbsp;&#183;&nbsp;<a href="javascript:TINY.box.show({url:\''.SITE_URL.'misc/popup/post.php?post='
								. $post_id .'\',width:500,height:400,opacity:30})" class="comment-link">Comment</a>';
					} else {
						$content .= '<div class="single_comment_entity comment_choices">
								<a class="sidebar_like" id="like_'.$post_id .'" name="likes">Dig it</a>
								&nbsp;&#183;&nbsp;<a href="javascript:TINY.box.show({url:\''.SITE_URL.'misc/popup/post.php?post='
								. $post_id .'\',width:500,height:400,opacity:30})" class="view_comments">Comment</a>';
					}
				}
				// optimize this
				$qry3 = mysql_query("SELECT COUNT(*) AS `cnt` FROM `comments` 
						WHERE `post_id` = $post_id");
				$row3 = mysql_fetch_array($qry3);
				$cnt = $row3['cnt'];
				
				if($cnt > 0) {
					$content .= '&nbsp;&#183;&nbsp;<a href="javascript:TINY.box.show({url:\''.SITE_URL.
					'misc/popup/post.php?post='. $post_id .'\',width:500,height:400,opacity:30})">
					<img src="'.SITE_URL.'assets/images/messages.png" alt="messages" /> '. $cnt. '</a>';
				}
				
				$msg_count = 0;
				if($like_count > 0) {

					if ($like_count >= 1) {
						if($this->userSession['member'] !== false) {
							$content .= '<span class="like-img">&nbsp;&#183;&nbsp;<img src="'.SITE_URL.
							'assets/images/likeup.png" alt="likeup" style="position: relative; top: 2px;" />';
						} else {
							$content .= '<span class="like-img">
									<img src="'.SITE_URL.'assets/images/likeup.png" alt="likeup" 
											style="position: relative; top: 2px;" />';
						} 
						$content .= '<span class="like_'.$post_id .'">';
						$new_like_count = $like_count; 
						$content .= $new_like_count . '</span></span>'; 
					}
				} 
				if($msg_count > 0) {
					$content .= '<span class="messages-img">&nbsp;&#183;&nbsp;<img src="'.SITE_URL.'assets/images/messages.png" alt="messages" style="position: relative; top: 2px;" /></span>';
				}
				if($this->userSession['member'] !== false) {
					$content .= '</div></div></div></div></div>'; 
				} else {
					$content .= '</div></div></div></div>';
				}
			}
		}
		$content .= '</div>&nbsp;<div class="clear"></div>';
		
		$end = $this->timer->getEndTime();
		$content .= $this->timer->getTotalTime($this->timer->start, $this->timer->end);
		
		if($param == "devteam") {
			$content .= '</div>';
			echo json_encode(array("sidebar"=>$content, "count"=>$count_results['num_posts']));
			exit;
		} else {
			echo json_encode(array("sidebar"=>$content));
			exit;
		}
	}
	
	function news_count() {
		$qry1 = mysql_query("SELECT COUNT(`post_id`) as num_posts FROM `posts` WHERE `status`=1") or die(mysql_error());
		$count_results = mysql_fetch_array($qry1);
		
		echo json_encode(array("count"=>$count_results['num_posts']));
		exit;
	}
}
?>
