<?php
/* filename: landing.php */

/* Include the Template class */
include('Template.php');

/**
 * Controller for displaying first page user sees after logged in.
 * Also the same page for the World Feed
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Landing extends MOC_Controller
{
	function __construct() {
		
		// Call the base class constructor
        parent::__construct();
        $this->load->model('user_model');
		
        $this->data['page_title'] = 'Minds of Chimera';
    }
	
	function index() {
		
		$this->data['page_title'] = $this->userSession['member'];

		array_push($this->data['css'], '/assets/css/slimbox2.css', '/assets/css/colorbox.css');
		array_push($this->data['scripts'], '/assets/scripts/lightbox/prototype.js', '/assets/scripts/waypoints.min.js', '/assets/scripts/slimbox2.js', '/assets/scripts/jquery.form.js');
		array_push($this->data['inline_scripts'], '
			
		');
		
		if($this->userSession['member'] == false) {
			
			// Check if the cookie exists
			if(isset($_COOKIE['mc_autologin']))
			{
				parse_str($_COOKIE['mc_autologin'], $received);

				$userdata = Landing::getCurrentUser($received);
				// Make a verification
				if($userdata !== false) 
				{
					$query_type = '';
					$notify_arr = $this->user_model->getNotifications($userdata->user_login, $query_type);
						
					if(empty($notify_arr[0]['cnt'])) {
						$notify_arr[0]['cnt'] = 0;
					}
					
					if(!empty($autologin)){
						setcookie("mc_autologin", 'usr='.$userdata->user_login.'&hash='.$userdata->user_pass, time()+60*60*24*30);
					}
						
					// Login_control Successful. Regenerate session ID to
					// prevent session fixation attacks
					session_regenerate_id();
						
					$newUserArray = array(
							'pid' 					=> $userdata->id,
							'member' 				=> $userdata->user_login,
							'email' 				=> $userdata->user_email,
							'profile_img_src_small' => $userdata->profile_img_src_small,
							'profile_path' 			=> $userdata->profile_path,
							//'notifyList'			=> $notify_arr,
							'notifyCount' 			=> $notify_arr[0]['cnt']
					);

					$this->session->set_userdata($newUserArray);
					Landing::userPages($userdata->user_login);
				}
			} else {
				
				/**
				 * Call the template class
				 */
				$template = new Template();
				
				$template->makePageTemplate("splash", "two_cols", $this->data);
			}
		} else {
			$seg_user = $this->uri->segment(1);
			if(empty($seg_user)) {
				Landing::userPages($this->userSession['member']);
			} else {
				Landing::userPages($seg_user);
			}
		}
	}
	
	private function getCurrentUser($login) {
		
		$md5_pass = 'deadpass';
		$crypt_pass = $login['hash'];
		
		$results = $this->user_model->getUser($login['usr'], $md5_pass, $crypt_pass);
		return $results;
	}
	
	public function userPages($userName) {
		
		$user_login = '';
		// Call the template class
		$template = new Template();
		
		$this->session->set_userdata('userPostsCount', $this->user_model->getPostsCount($userName));
		
		array_push($this->data['css'], '/assets/css/colorbox.css');
		array_push($this->data['scripts'], '/assets/scripts/lightbox/prototype.js', '/assets/scripts/jquery.colorbox.js', '/assets/scripts/jquery.form.js');
		array_push($this->data['inline_scripts'], '$j("a[rel]").colorbox({ transition:"none", width:"90%", height:"90%", maxWidth:"90%", maxHeight:"90%;"});');
		
		$page = "landing_view";

		$this->data['page_title'] = ucwords($userName);
		$this->data['posts'] = $template->getPosts($userName);
		
		$this->data['post_feed']  = $this->load->view('post_view', $this->data, true);
		
		$template->makePageTemplate($page, "two_cols", $this->data);
		
		
	}
}
?>
