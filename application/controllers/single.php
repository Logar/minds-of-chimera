<?php
/* filename: single.php */

/* Include the Template class */
include('Template.php');

/**
 * Controller for accessing and changing user settings
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Single extends MOC_Controller
{	
	function __construct() {
		
         // Call the base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
        
        $this->data['page_title'] = "Minds of Chimera";
	}
	
	function index() {
		// Call the template class
		$template = new Template();
		
		if(isset($_GET['news'])) {
			$getNews = 1;
		} else {
			$getNews = 0;
		}
		$posts = $template->getSinglePost($this->userSession['member'], $_GET['postid'], $getNews);
		
		Single::displayPosts($posts, 1);
	}
	
	function notify() {
		
		// Call the template class
		$template = new Template();
		
		if($this->userSession['member'] == false) {
			header( 'Location: '.SITE_URL);
		}
		
		if(isset($_GET['news'])) {
			$getNews = 1;
		} else {
			$getNews = 0;
		}
		
		if(isset($_GET['postid'])) {
		
			if(!empty($_GET['postid'])) {
				$posts = $template->getSinglePost($this->userSession['member'], $_GET['postid'], -1);
				
				if($posts == false) {
					$this->error();
				} else {
					$this->displayPosts($posts, 0);
				}
			} else {
				$this->error();
			}
		} else {
			$this->error();
		}
	}

	public function displayPosts($posts, $onthefly) {
		
		$this->data['posts'] = $posts;
		$content = $this->load->view('post_view', $this->data, true);
		
		if($onthefly == 1) {
			echo $content;
		} else {
			$content .= '<div class="space_40"></div>';
			$this->data['page'] = "home";
			$this->data['page_title'] = ucwords($this->userSession['member']);
			
			// Call the template class
			$template = new Template();
				
			$template->makePageTemplate("post_view", "two_cols", $this->data);
		}
	}

	public function error() {
		
		$this->data['page'] = "Page Not Found";
		$this->data['page_title'] = "Page Not Found";
		
		// Call the template class
		$template = new Template();
			
		$template->makePageTemplate("page_missing", "two_cols", $this->data);
	}
}
