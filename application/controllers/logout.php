<?
/* filename: logout.php */

/**
 * Controller for logging a user out
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Logout extends MOC_Controller {
	
	function __destruct() {
		
		/**
		 * @TODO remove all PHP session variables and swap with CodeIgniter
		 * session variables
		 * Initialize the session in case PHP session variables exist
		 */
		if(!isset($_SESSION)) {
			session_start();
		}
		
		// Unset any PHP session variables.
		$_SESSION = array();
		
		// Unset all CodeIgniter session variables
		$this->session->sess_destroy();
		
		// Finally, destroy the session.
		session_destroy();
		
		/**
		 * Delete cookies - the time must be in the past,
		 * so just negate what you added when creating the
		 * cookie.
		 */
		
		if(isset($_COOKIE['mc_autologin'])){
			setcookie ("mc_autologin", "", time()-60*60*24*30);
		}
		
		header("Location: " . SITE_URL);
		exit;
	}
}

?>
