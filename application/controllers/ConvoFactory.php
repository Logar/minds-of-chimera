<?php
/* filename: ConvoFactory.php */

/**
 * Singleton Factory for creating Convo objects
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.1
 */
class ConvoFactory extends MOC_Controller
{
	private static $factory = null;
	
	public static function getConvoFactoryInstance() {
		if(static::$factory === null) {
			return new ConvoFactory();
		} else {
			return static::$factory;
		}
	}
	
    public function home($data) {
    	return $this->load->view('convo/convo_home', $data, true);
    }
    
    public function top($data) {
    	return $this->load->view('convo/convo_top', $data, true);
    }
    
    public function bottom($data) {
    	return $this->load->view('convo/convo_bottom', $data, true);
    }
    
    public function popup($data) {
    	return $this->load->view('convo/convo_popup', $data, true);
    }
}
?>
