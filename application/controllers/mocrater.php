<?php
/* filename: mocrater.php */

/* Include the Template class */
include('Template.php');

/**
 * Controller for plugin MOCRater
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Mocrater extends MOC_Controller
{
	public function __construct() {
		
        // Call the base class constructor
        parent::__construct();
        $this->load->model('user_model');
		
        $this->data['css'] = array('assets/css/rater.css');		
        $this->data['page_title'] = 'Minds of Chimera';
    }
	
	public function index() {
		
		$this->data['page_title'] = 'MocRater';
		
		$page = $this->uri->segment(2);
		
		if(empty($page)) {
			
			if($this->userSession['member'] == false) {
				header("Location: " . SITE_URL);
			}
			$page = "all_patterns";
			
			$patterns = $this->user_model->getPatterns();
			if($patterns != false) {
				$this->data['patterns'] = $patterns;
			} else {
				$this->data['warningMsg'] = true;
			}
		} else {
			
			$page = "all_patterns";
			
		}
		
		$this->data['page'] = $page;
		
		$this->data['scripts'] = array('/assets/scripts/sorttable.js', '/assets/scripts/lightbox/prototype.js', '/assets/scripts/slimbox2.js');
		array_push($this->data['css'], array('/assets/css/slimbox2.css'));
		
		// Call the template class
		$template = new Template();
			
		$template->makePageTemplate($page, "two_cols", $this->data);
	}
	
	public function page() {
		
		$this->data['page_title'] = 'MocRater';
		$page = $this->uri->segment(2);
		
		if($page == 'quest_guides') {

			$this->data['page'] = "quest guides";
		} 
		
		// Call the template class
		$template = new Template();
			
		$template->makePageTemplate($page, "two_cols", $this->data);
	}
	
	public function display($pattern_id) {
		
		$this->data['page_title'] = 'MocRater';
		$this->data['scripts'] = array('/assets/scripts/sorttable.js', '/assets/scripts/lightbox/prototype.js', '/assets/scripts/slimbox2.js');
		array_push($this->data['css'], array('/assets/css/slimbox2.css'));
		
		if(!empty($pattern_id)) {
			
			if(!isset($this->userSession['member'])) {
				header("Location: /");
			}
			$page = "pattern_details";
			
			$this->data['ratings'] = $this->user_model->getRatings($pattern_id);
			$patterns = $this->user_model->getSinglePattern($pattern_id);
			$this->data['relatedPatterns'] = $this->user_model->getRelatedPatterns($pattern_id);
			if($patterns != false) {
				$this->data['patterns'] = $patterns;
			} else {
				$this->data['warningMsg'] = true;
			}
		}
		$this->data['page'] = $page;
		
		// Call the template class
		$template = new Template();
		
		$template->makePageTemplate($page, "two_cols", $this->data);
	}
	
	public function edit_pattern($pattern_id) {

		$this->data['page_title'] = 'MocRater';
		$this->data['scripts'] = array('/assets/scripts/sorttable.js', '/assets/scripts/lightbox/prototype.js', '/assets/scripts/slimbox2.js', '/assets/scripts/jquery.limit-1.2.source.js');

		if(!empty($pattern_id)) {
			
			if(!isset($this->userSession['member'])) {
				header("Location: /");
			}
			$page = "edit_pattern";
			
			$this->data['currPatterns'] = $this->user_model->getRelatedPatterns1D($pattern_id);
			$this->data['relatedPatterns'] = $this->user_model->getPatterns();
			$pattern = $this->user_model->getSinglePattern($pattern_id);
			if($pattern != false) {
				$this->data['patterns'] = $pattern;
			} else {
				$this->data['warningMsg'] = true;
			}
		}
		$this->data['page'] = $page;

		// Call the template class
		$template = new Template();
		
		$template->makePageTemplate($page, "two_cols", $this->data);
	}
	
	public function update_pattern($pattern_id) {
		
		$this->data['page_title'] = 'MocRater';
		$this->data['scripts'] = Array('/assets/scripts/sorttable.js', '/assets/scripts/lightbox/prototype.js', '/assets/scripts/slimbox2.js');
		array_push($this->data['css'], array('/assets/css/slimbox2.css'));
		
		if(!empty($pattern_id)) {
			
			if($this->userSession['member'] == false) {
				header("Location: ".SITE_URL."?resp=login");
			}
			
			if(isset($_POST['related-patterns'])) {
				$new_related_patterns = $_POST['related-patterns'];
			}
			$this->user_model->updateRelatedPatterns($pattern_id, $new_related_patterns);

			$rdata = array();

			$context = $this->removeNewline($_POST['context']);
			$problem = $this->removeNewline($_POST['problem']);
			$solution = $this->removeNewline($_POST['solution']);
			
			$rdata['context'] = $context;
			$rdata['problem'] = $problem;
			$rdata['solution'] = $solution;
			
			$update_result = $this->user_model->updateSinglePattern($pattern_id, $rdata);
			if($update_result != false) {
				header("Location: ".SITE_URL."mocrater/display/".$pattern_id);
				exit;
			} else {
				header("Location: ".SITE_URL."mocrater/edit_pattern/".$pattern_id."?resp=failed");
				exit;
			}
		} else {
			header("Location: ".SITE_URL."mocrater/edit_pattern/".$pattern_id."?resp=failed");
			exit;
		}
	}
	
	public function removeNewline($input) {
		$t = trim($input);
		$t = str_replace("\n", " ", $t);
		$t = str_replace("\r", "", $t);
		return $t;
	}
}
?>
