<?php
/* filename: errors.php */

/* Include the Template class */
include('Template.php');

/**
 * Default controller for displaying page errors
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Errors extends MOC_Controller
{	
	function __construct() {
		
        // Call the base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
	}
	
	function index() {
		
		$this->data['page_title'] = 'Page Not Found';
		$page = "page_missing";
		
		// Call the template class
		$template = new Template();
			
		$template->makePageTemplate($page, "two_cols", $this->data);
	}
}
?>
