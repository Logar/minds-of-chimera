<?php
/* filename: main.php */

/* Include the Template class */
include('Template.php');

/**
 * Prepares most generic pages and provides information on upcoming plugins
 * @TODO move stray HTML and mysql queries from controller to respective models and views
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Main extends MOC_Controller
{	
	function __construct() {
		
		// Call the base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
	}
	
	/** 
	* Construct local pages
	* @access	public
	* @return   Void
	*/	
	public function page() {
		
		$page = $this->uri->segment(1);
		
		$user_only_pages = array('quests', 'profile', 'stats');
		$this->data['page'] = $page;
		
		$this->data['errorMsg'] = '';
		if(in_array($page, $user_only_pages)) {
			
			if($this->userSession['member'] == false) {
				redirect(SITE_URL, 'refresh');
			}
		}
		if(isset($errorMsg) && !empty($errorMsg)) {
			$this->data['errorMsg'] = $errorMsg;
		}
		if($page == 'videos') {
			$this->data['scripts'] = array('/assets/video/js/mootools.js', '/assets/video/js/swfobject.js', 
			'/assets/video/js/videobox.js');
			
			$this->data['css'] = array('/assets/video/css/videobox.css');
		}
		else if($page == 'all_users') {
			
			$this->data['sub_page'] = ucwords($this->uri->segment(2));
			
			$cond = '';
			if($this->data['sub_page'] == "Members") {
				$cond = "WHERE id != 'NULL'";
			}
			$this->userSession['playerCount'] = $this->user_model->mc_getGamePlayersCount($cond);
			$users_array = $this->user_model->mc_getMoreGamePlayers(0, 30);
			
			$this->data['users_array'] = $users_array;
			$this->data['page'] = "all users";
			
		}
		else if($page == 'quests') {
			
			$quests_yml = Main::quest_parser("/home/isue/moc/craftbukkit/plugins/Citizens/quests.yml");
			$profile_quests = Main::quest_parser("/home/isue/moc/craftbukkit/plugins/Citizens/".$this->userSession['member'].".yml");
			
			$questDesc = array();
			$questNames = array();
			$i = 0;
			
			if(!empty($quests_yml)) {
				foreach($quests_yml as $key1=>$value) {
					$questNames[$i] = $key1;
					$subarray = current($value);
					$questDesc[$i] = $subarray['description'];
			
					$i++;
				}
			}
			$this->data['questNames'] = $questNames;
			$this->data['questDesc'] = $questDesc;
			
			$total_quests = count($questNames);
			$this->data['totalQuests']= $total_quests;
			
			if(!empty($profile_quests)) {
				
				$curr_arr = Main::search_for_array($profile_quests, "current");
				$active_quests = $curr_arr[0];
				$active_count = count(Main::search_for_array($active_quests, "name"));
				
				$completed_arr = Main::search_for_array($profile_quests, "completed");
				$completed_quests = $completed_arr[0];
				$completed_count = sizeof($completed_quests);

				if($completed_count == 0) {
					$percent_left = 1 * 100;
				} else {
					$done = $total_quests - $completed_count; 
					$percent_left = round(($done / $total_quests) * 100, 2);
				}
				
				$this->data['activeQuests'] = $active_quests;
				$this->data['completedQuests'] = $completed_quests;
				$this->data['activeCount'] = $active_count;
				$this->data['completedCount'] = $completed_count;
				$this->data['percentLeft'] = $percent_left;
			} else {
				$this->data['percentLeft'] = 100;
				$this->data['activeCount'] = 0;
				$this->data['completedCount'] = 0;
			}
			$this->data['page'] = "quests";
			
		} 
		else if($page == 'settings') {
			
			$page = "settings_view";
			$this->data['userName'] = SITE_URL . $this->userSession['member'];
			$this->data['name'] = 'Steve';
			$this->data['email'] = $this->userSession['email'];
		}
		else if($page == 'quest_guides') {

			$this->data['page'] = "quest guides";
		} 
		else if($page == 'community_projects') {

			$this->data['page'] = "community projects";
		}
		else if($page == 'learn') {

			$this->data['page'] = "learn to play";
		}
		else if($page == 'mumble_help') {

			$this->data['page'] = "mumble help";
		}
		else if($page == 'splash') {
			$this->data['page'] = "Minds of Chimera";
		}
		else if($page == 'plugin_info') {

			$this->data['page'] = "Plugin Information";
		}
		else if ($page == 'map') {
			$this->data['page'] = "Map";
			$page = 'map';
		}
		else if ($page == 'forgot') {
			$this->data['page'] = "Forgot Pass";
			$page = 'forgot';
		}
		
		// Call the template class
		$template = new Template();
		
		$template->makePageTemplate($page, "two_cols", $this->data);
	}
	
	/** 
	* Parses yml file searching for completed, not completed, and in progress quests
	* @access	private
	* @param	string $filepath - path of file to parse
	* @return   String array - array containing quests information
	*/	
	private function quest_parser($filepath) {
		
		$yml_contents = '';
		
		if(file_exists($filepath)) {
			$yml_contents = yaml_parse_file($filepath);
		}
		return $yml_contents;
	}
	
	/** 
	* Count number of occurences of a value in an array
	* @access	private
	* @param	any $array
	* @param	any $value
	* @return   string array
	*/	
	private function array_count_this_value($array, $value) {
		$values = array(); 
		$values = array_count_values($array); 
		
		return $values[$value]; 
	} 
	
	/** 
	* Search an array for a specific key
	* @access	private
	* @param	any $array
	* @param	any $key
	* @return   string array
	*/	
	private function search_for_array($array, $key) {
		$results = array();

		Main::search_array_r($array, $key, $results);

		return $results;
	}
	
	/** 
	* Search an array for a specific key; RECURSIVE function
	* @access	private
	* @param	any $array
	* @param	any $key
	* @param	string address of &$results
	* @return   Void
	*/	
	private function search_array_r($array, $key, &$results) {
		if(!is_array($array))
			return;
			
		if(isset($array[$key])) {
			$results[] = $array[$key];
		}
		foreach ($array as $subarray)
			Main::search_array_r($subarray, $key, $results);
	}
	
	/** 
	* Create a specific user page from some choices
	* @access	public
	* @param	string $page_name
	* @return   Void or string
	*/	
	function user_pages($page_name) {
		
		if(isset($this->userSession['pid']) && !empty($this->userSession['pid'])) {
			$name = $this->userSession['member'];
			if ($page_name == "inventory") {
				/**
				 * @TODO remove or implement
				 */
			} else if ($page_name == "foreman_status") {
				/**
				 * @TODO remove or implement
				 */
			} else if ($page_name == "stats") {
				$this->data['name'] = $name;
				$userStats = $this->user_model->getPlaytimeStatsOf($name, null);
			
				$this->data['died'] = $userStats->died;
				$this->data['pk'] = $userStats->pk;
				$this->data['hk'] = $userStats->hk;
				$this->data['fk'] = $userStats->fk;
				$this->data['bplace'] = $userStats->bplace;
				$this->data['bdestroy'] = $userStats->bdestroy;
				$this->data['moved'] = $userStats->moved;
			}
			return;
		} else {
			return '<div class="error-box">You must be logged in to access this page</div>';
		}
	}
	
	/** 
	* Add post created by the user
	* @access	public
	* @param	string $param
	* @return   Void
	*/	
	public function addPost($param) {
		
		$is_news = 0;
		if($param == "news") {
			$is_news = 1;
		} else {
			$is_news = 0;
		}
		
		if(empty($this->userSession['pid'])) {
			redirect(SITE_URL, 'refresh');
		}
		
		if(isset($_FILES["post-feed-image"])) {
			$file = $_FILES["post-feed-image"];
			
			if(!empty($file)) {
				$new_post = array();
				$new_post['author_id'] = $this->userSession['pid'];
				$new_post['is_news'] = $is_news;
				
				$stripped_tag = strip_tags($_POST['postBox']);
				$stripped_slashes = stripslashes($stripped_tag);
				$new_post['content'] = $stripped_slashes;
				
				if(!empty($_POST['location'])) {
					$new_post['location'] = $_POST['location'];
				} else {
					$new_post['location'] = "news";
				}
				$new_post['date'] = date("Y-m-d H:i:s");
				Main::uploadFeedPhoto($_FILES["post-feed-image"], $new_post, 0);
				exit;
			}
		}
		if(!empty($_POST['postBox']))
		{
			$new_post = array();
			$new_post['author_id'] = $this->userSession['pid'];
			$new_post['is_news'] = $is_news;
			$stripped_tag = strip_tags($_POST['postBox']);
			$new_post['content'] = stripslashes($stripped_tag);
			$new_post['post_type'] = "text";
			
			if(!empty($_POST['location'])) {
				$new_post['location'] = $_POST['location'];
			} else {
				$new_post['location'] = "news";
			}
			$new_post['date'] = date("Y-m-d H:i:s");
			
			$results = $this->user_model->mc_addPost($new_post);
			
			$postItems = array();
			
			$postItems[0]['post_id'] = $results['post_id'];
			$postItems[0]['author'] = $this->userSession['member'];
			
			$postItems[0]['post_content'] = $results['post_content'];
			$postItems[0]['like_count'] = 0;
			$postItems[0]['profile_img_src'] = $this->userSession['profile_img_src_small'];
			$postItems[0]['profile_path'] = $this->userSession['profile_path'];
			$postItems[0]['like_list'] = "";
			
			$postItems[0]['post_date_full'] = $results['post_date'];
			$postItems[0]['post_date_formatted'] = "Just Now";
			$postItems[0]['is_news'] = $is_news;
			
			// No comments exist yet
			$postItems[0]['comments'] = "";
			
			$this->data['posts'] = $postItems;
			
			$post = $this->load->view('post_view', $this->data, true);
			echo json_encode(array("post_content"=>$post, "is_news"=>$is_news));
			exit;
		} else {
		
			exit;
		}
	}
	
	/** 
	* Add a comment created by user
	* @access	public
	* @return   Void
	*/	
	public function addComment() {
		
		if(empty($this->userSession['pid'])) {
			redirect(SITE_URL, 'refresh');
		}
		
		if(!empty($_POST['comment_content']))
		{
			$post_id = $_POST['table_id'];
			if(!empty($post_id)) {
				
				$new_comment = array();
				
				$new_comment['post_id'] = $post_id;
				$new_comment['author_id'] = $this->userSession['pid'];
				
				$stripped_tag = strip_tags($_POST['comment_content']);
				$stripped_slashes = stripslashes($stripped_tag);
				$new_comment['content'] = $stripped_slashes;
				
				$new_comment['date'] = date("Y-m-d H:i:s");
				
				$results = $this->user_model->mc_addComment($new_comment);
				//Main::notification_email($this->userSession['member'], $post_id, "comment", $_POST['comment_content']);
				
				echo json_encode(array("comment_id"=>$results['new_comment']['comment_id'], "post_id"=>$new_comment['post_id'], "author"=>$this->userSession['member'], "date"=>"Just now", "comment_content"=>$new_comment['content'], "profile_img_src"=>$results['new_comment']['profile_img_src'], "profile_path"=>$results['new_comment']['profile_path']));
				exit;
			}
		}
	}
	
	/** 
	* If setting enabled, send a notification email to the user if a user comments or posts
	* on something they posted
	* @access	public
	* @return   Void
	*/	
	public function notificationEmail() {
		
		$from_user = $this->userSession['member'];
		$post_id = $_GET['post_id'];
		$type = $_GET['type'];
		$content = $_GET['content'];
		
		require_once("/usr/share/php/Mail.php");
		require_once("/usr/share/php/Mail/mime.php");
		
		$sender = "noreply@mindsofchimera.com";
		
		$qry1 = mysql_query("SELECT `author` FROM `posts_view` WHERE `post_id`= $post_id");

		if(mysql_num_rows($qry1) > 0) {
			$result1 = mysql_fetch_array($qry1);
			
			$owner = $result1['author'];
			
			if($from_user !== $owner) {
				$qry2 = mysql_query("SELECT `notify`, `user_email` FROM `wp_users` WHERE `user_login`='$owner'");
				
				if(mysql_num_rows($qry2) > 0) {
					$result2 = mysql_fetch_array($qry2);
					
					if($result2['notify'] == 'email_all') {
						
						$recipient = $result2['user_email'];
						$subject = "Minds of Chimera: User Notification";

						$html = "<html><head>
						<style type=\"text/css\">
						blockquote.block-post {
							margin-top: 10px;
							margin-bottom: 10px;
							margin-left: 50px;
							padding-left: 15px;
							border-left: 3px solid #ccc;
						}
						</style>
						</head><body>";
						$html .= '<div style="background: #F0F0F0; width: 100%;"><div style="margin-top: 40px;">&nbsp;</div>';
						$html .= '<div style="border: 1px solid #CCC; background: #fff; width: 90%; display: block; margin: 0 auto;">
						<div style="background: #81acff;"><img src="http://mindsofchimera.com/assets/images/moc_email_banner.jpg" alt="moc email banner" /></div>
						<div style="padding: 10px;">Hello '.ucwords($owner).',<br /><br />';
						
						if($type == 'comment') {
							$action = 'commented';
						} else if($type == 'post') {
							$action = 'posted';
						}
						
						$location = "post";
						
						$html .= "<b>".ucwords($from_user)."</b> $action on your $location: <br />";
						$html .= '<blockquote class="block-post">"'.$content.'"</blockquote><br />';

						$html .= "Thanks,<br />";
						$html .= "The MOC Team<br /></div></div><div style=\"width: 90%; display: block; margin: 0 auto; font-size: 11px;\"><p>If you would like to disable this and future notifications from <a href=\"http://mindsofchimera.com\">mindsofchimera.com</a>. Please go into \"Settings\" and select \"No\" for Email Notifications. Thanks.</p></div></div>";
						$html .= "</body></html>";

						$host = "ssl://smtp.gmail.com";
						$port = "465";
						$userName = "moc.noreply@gmail.com";
						$password = "3mindsnobrains";
						$sender = "noreply@mindsofchimera.com";
						$headers = array ('From' => $sender,
										'To' => $recipient,
										'Subject' => $subject);
						$mime = new Mail_mime();
						$mime->setHTMLBody($html);
						$body = $mime->get();
						$smtp = Mail::factory('smtp',
										array ('host' => $host,
										'port' => $port,
										'auth' => true,
										'username' => $userName,
										'password' => $password));
						$headers = $mime->headers($headers);
						$mail = $smtp->send($recipient, $headers, $body);
						
						/*
						 *@TODO respond if message failed or not using AJAX
						 *
						if(!$mail) {
							echo json_encode(array("resp"=>"failed"));
							exit;
						} else if(!$mail) {
							echo json_encode(array("resp"=>"success"));
							exit;
						}
						*/
					}
				}
			}
		}
		return;
	}
	
	/** 
	* Upload a post containing an image
	* @access	public
	* @param	string $file
	* @param	string array $postInfo
	* @return   int
	*/
	public function uploadFeedPhoto($file, $postInfo) {
		
		$status = 0;
		$msg = "";
		
		$destination_path = "assets/images/profile/$this->userSession['member']/";
		$web_path = "/assets/images/profile/$this->userSession['member']/";
		
		if(isset($file)) {
			
			if($file["size"] < 16777216) {
				if (($file["type"] == "image/gif")
					|| ($file["type"] == "image/jpeg")
					|| ($file["type"] == "image/pjpeg") 
					|| ($file["type"] == "image/png")) {
					
					$img = getimagesize($file["tmp_name"]);
					$minimum = array('width' => '206', 'height' => '206');
					$width= $img[0];
					$height =$img[1];

					if($width < $minimum['width']) {
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						exit;
					} else if ($height <  $minimum['height']){
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						exit;
					}
					if($file["error"] > 0) {
						$status = 0;
					}
					else {
						$ext = '';
						
						$currTime = time();
						if($file["type"] == "image/gif") {
							$ext = ".gif";
						} else if($file["type"] == "image/jpeg") {
							$ext = ".jpg";
						} else if($file["type"] == "image/png") {
							$ext = ".png";
						} else if($file["type"] == "image/pjpeg") {
							$ext = ".png";
						}
						
						$new_md5 = md5($currTime);
						$file_name = $new_md5 . "_b" . $ext;
						$absolute_path = $destination_path . $file_name;
						$medium_file = $new_md5 . "_m" . $ext;
						$small_file = $new_md5 . "_s" . $ext;

						
						$small_file_abs_path = $destination_path . 'thumbnails/' . $small_file;
						$medium_file_abs_path = $destination_path . $medium_file;
						
						move_uploaded_file($file["tmp_name"], $absolute_path);
						$big_file_path = $web_path . $file_name;
						$medium_file_path = $web_path . $medium_file;
						$small_file_path = $web_path .  'thumbnails/' . $small_file;
						

						require_once('misc/phpthumb/ThumbLib.inc.php');

						try
						{
							$thumb = PhpThumbFactory::create($absolute_path);
							$thumb->resize(800, 800);
							$thumb->cropFromCenter(700)->save($absolute_path);
							
							$thumb = PhpThumbFactory::create($absolute_path);
							$thumb->adaptiveResize(355, 355);
							$thumb->cropFromCenter(355, 355)->save($medium_file_abs_path);
							
							$thumb = PhpThumbFactory::create($absolute_path);
							$thumb->adaptiveResize(206, 206);
							$thumb->cropFromCenter(206, 206)->save($small_file_abs_path);
							
							$galleryImg = array();
							$galleryImg['big_file_path'] = $big_file_path;
							$galleryImg['medium_file_path'] = $medium_file_path;
							$galleryImg['small_file_path'] = $small_file_path;
							$galleryImg['photo_date'] = date("Y-m-d H:i:s");
							$galleryImg['prefilter_date'] = date("m-d-y");
							$galleryImg['photo_owner'] = $this->userSession['member'];
							$galleryImg['img_title'] = "Posted by ".$galleryImg['photo_owner'] . ' on ' . $galleryImg['prefilter_date'];
							$postInfo['photo_id'] = $this->user_model->addPhoto($galleryImg);
							$postInfo['post_type'] = "image";
							$results = $this->user_model->mc_addPost($postInfo);
							
			
							$postItems = array();
							
							$postItems[0]['post_id'] = $results['post_id'];
							$postItems[0]['author'] = $this->userSession['member'];
							$postItems[0]['post_content'] = $results['post_content'];
							$postItems[0]['like_count'] = 0;
							$postItems[0]['profile_img_src'] = $this->userSession['profile_img_src_small'];
							$postItems[0]['profile_path'] = $this->userSession['profile_path'];
							$postItems[0]['like_list'] = "";
							$postItems[0]['main_img_src'] = $big_file_path;
							$postItems[0]['medium_img_src'] = $medium_file_path;
							$postItems[0]['title'] = $galleryImg['img_title'];
							
							$postItems[0]['post_date_full'] = $results['post_date'];
							$postItems[0]['post_date_formatted'] = "Just Now";
							$postItems[0]['is_news'] = 0;
							
							// No comments exist yet
							$postItems[0]['comments'] = "";
							
							$this->data['posts'] = $postItems;
							/*
							$postItems['big_file_path'] = $big_file_path;
							$postItems['medium_file_path'] = $medium_file_path;
							$postItems['img_title'] = $galleryImg['img_title'];
							$postItems['post_date'] = "Just now";
							$postItems['post_type'] = "image";
							$postItems['is_news'] = 0;
							*/
							
							$post = $this->load->view('post_view', $this->data, true);
							
							echo $post;
							
							$status = 1;
							exit;
						}
						catch (Exception $e)
						{
							// handle error here however you'd like
							$status = $e->getMessage();
						}
					}
				}
				else {
					$status = 4;
				}
			} else {
				$status = 2;
			}
		} else { 
			$status = 0;
		}
		echo $status;
	}
	
	public function updateNotifications() {
		
		$status = 0;
		
		if($this->userSession['member'] !== false) {
			$status = $this->user_model->updateNotificationCount();
		}
		
		echo json_encode(array('status'=>$status));
	}
	
	public function getPhotoComments() {
		
		$content = '';
		
		if(isset($_POST['id'])) {
			$post_id = $_POST['id'];

			if($this->userSession['member'] !== false) {
				$q = "single_post_user";
			} else {
				$q = "single_post_non-user";
			}

			$this->data['posts'] = $this->user_model->getPosts($this->userSession['member'], '', $q, $post_id, 0);
			
			$content .= $this->load->view('post_view', $this->data, true);
		} else {
			$content .= "<p>Failed to load the comments for this photo. Please check your connection and try again.</p>";
		}
		echo json_encode(array('content'=>$content));
	}
	
	public function getGamePlayersCount() {
		
		$player_count = $this->userSession['playerCount'];
		echo json_encode(array("count"=>$player_count));
		exit;
	}
	
	public function getMoreGamePlayers() {
		
		$newContent = '';
		
		if(isset($_POST['start'])) {
			$start = $_POST['start'];
		} else {
			$start = 31;
		}
		if(isset($_POST['end'])) {
			$end = $_POST['end'];
		} else {
			$end = 61;
		}
		$users_array = $this->user_model->mc_getMoreGamePlayers($start, $end);
		for($i=0; $i < sizeof($users_array); $i++) {
	
			$name = $users_array[$i]['user_login'];
			$profile_img_src_small = $users_array[$i]['profile_img_src_small'];
			$profile_path = $users_array[$i]['profile_path'];
			$last_login = $users_array[$i]['last_login'];
			
			if(empty($profile_img_src_small)) {
				$profile_img_src_small = site_url('assets/images/profile/steve_avatar_small.png');
			}
			if(empty($profile_path)) {
				$profile_path = site_url('profile/'.$name);
			}
			if($users_array[$i]['user_status'] == NULL) {
				$newContent .= '<div>';
			} else {
				$newContent .= '<div>';
			}
			$newContent .= '
			<div style="float: left; padding-right: 10px;">
			<a href="'.$profile_path.'"><img src="'.$profile_img_src_small.'" class="post_profile_img" /></a>
			</div>
			<span style="float: left; text-align: left;"><a href="'.$profile_path.'"><b>'.$name.'</b></a></span>
			<span style="float: right; padding-right: 5px;">Last Server Login: '.$last_login.'</span><br />
			</div>';
		}
		echo json_encode(array("content"=>$newContent));
		exit;
	}
	
	// get a live post from the database
	public function getLiveFeed() {

		$this->data = array();
		
		$curr_user = $this->userSession['member'];
		$status = 0;
		$condition = $_POST['postid'];
		
		if(isset($_POST['user'])) {
			$this->data['posts'] = $this->user_model->getPosts($curr_user, $_POST['user'], "single_post_last_user", $condition, $status);
		} else {
			if(!empty($curr_user)) {
				$this->data['posts'] = $this->user_model->getPosts($curr_user, '', "single_post_last", $condition, $status);
			}
		}
		
		if($this->data['posts'] !== false) {
			$this->data['onthefly'] = true;
			$post = $this->load->view('post_view', $this->data, true);
		} else {
			$post = '';
		}
		
		echo json_encode(array("content"=>$post));
		exit;
	}
	
	// get a live notification from the database
	public function getLiveNotifications() {

		$this->data = array();
		
		$curr_user = $this->userSession['member'];
		$status = 0;
		$last_date = $_POST['lastdate'];
		
		$notify_arr = $this->user_model->mc_getLiveNotifications($curr_user, $last_date);
		
		if($notify_arr !== 0) {
			if(empty($notify_arr[0]['cnt'])) {
				$this->userSession['notifyCount'] = 0;
			} else {
				$this->userSession['notifyCount'] =  $notify_arr[0]['cnt'];
			}
			
			$this->data['old_notifications'] = $notify_arr; 
			$notifications = $this->load->view('notification_view', $this->data, true);
			
			$all_notify_arr = $this->user_model->getNotifications($curr_user, '');
			
			$this->userSession['notifyList'] = $all_notify_arr;
			$new_last_date = $this->user_model->getLastNotificationDate($curr_user);
		} else {
			$notifications = '';
			$new_last_date = $_POST['lastdate'];
		}
		
		echo json_encode(array("content"=>$notifications, "lastdate"=>$new_last_date));
		exit;
	}
	
	public function getViewQueue() {

		$status = 0;

		$viewArr = $this->user_model->mc_getViewQueue($this->userSession['member']);
		
		if($viewArr !== false) {
			$status = 1;
			echo json_encode(array("status"=>$status, "url"=>$viewArr['url']));
		} else {
		
			echo json_encode(array("status"=>$status, "url"=>"none"));
		}
		exit;
	}
	
	public function remainingComments() {
		
		$postId = $_POST['post_id'];
		
		$remainComments = array();
		$remainComments['post_id'] = $postId;
		$remainComments['member'] = $this->userSession['member'];
		$results = '';
		
		if(!empty($postId)) {
			$results = $this->user_model->commentsGenerator($remainComments, 1);
		}
		
		$this->data['commentsArray'] = $results;
		$comments = $this->load->view('remaining_comments', $this->data, true);
		echo json_encode(array("remaining_comments"=>$comments));
	}
	
	function deletePost() {
		$rm_post['id'] = $_POST['id'];
		$rm_post['table'] = $_POST['table'];
		
		$this->user_model->deletePostComment($rm_post);
	}
	
	function rating() {

		$choice = $_POST['choice'];
		if($choice == 'like' || $choice == 'sidebar_like') {
			
			if($_POST['id']) {
				$id = $_POST['id'];
				
				$date = date("Y-m-d H:i:s");
				
				// update likes
				mysql_query("INSERT INTO `post_likes` (`user_id`, `post_id`) VALUES($this->userSession['pid'], $id)");
				$qry1 = mysql_query("SELECT `post_content` FROM `posts` WHERE `id` = $id");
				$results1 = mysql_fetch_array($qry1);
				
				mysql_query("INSERT INTO `notifications`(`type`, `post_id`, `comment_id`, `author_id`, `content`, `date`) VALUES('like', $id, null, $this->userSession['pid'], '$results1[post_content]', '$date')") or die(mysql_error());
				
				// Getting latest likes
				$result = mysql_query("SELECT `like_count` FROM `posts_view` WHERE `post_id` = $id");
				$row = mysql_fetch_array($result);
				
				$likes = $row['like_count'];
				echo json_encode(array("status"=>1,"like_count"=>$likes,"append"=>"unlike"));
				exit;
			}	
		}
		else if($choice == 'unlike' || $choice == 'sidebar_unlike') {
				
			if($_POST['id']) {
				$id = $_POST['id'];
				
				mysql_query("DELETE FROM `post_likes` WHERE `post_id` = $id AND `user_id` = $this->userSession['pid']");
				mysql_query("DELETE FROM `notifications` WHERE `post_id` = $id AND `author_id` = $this->userSession['pid']");
				
				// Getting latest likes
				$result = mysql_query("SELECT `like_count` FROM `posts_view` WHERE `post_id` = '$id'");
				$row = mysql_fetch_array($result);
				
				$likes = $row['like_count'];
				echo json_encode(array("status"=>1,"like_count"=>$likes,"append"=>"like"));
				exit;
			}
		}
	}
	
	public function upload_profile_img() {
		
		$status = 0;
		$msg = "";
		
		$destination_path = "assets/images/profile/$this->userSession['member']/";
		$web_path = "/assets/images/profile/$this->userSession['member']/";
		
		if(isset($_FILES['profile_img'])) {
			
			if($_FILES['profile_img']["size"] < 16777216) {
				if (($_FILES['profile_img']["type"] == "image/gif")
					|| ($_FILES['profile_img']["type"] == "image/jpeg")
					|| ($_FILES['profile_img']["type"] == "image/pjpeg") 
					|| ($_FILES['profile_img']["type"] == "image/png")) {
					/*
					$img = getimagesize($_FILES['profile_img']['tmp_name']);
					$minimum = array('width' => '206', 'height' => '206');
					$width= $img[0];
					$height =$img[1];

					if ($width < $minimum['width'] ){
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						exit;
					} else if ($height < $minimum['height']){
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						exit;
					}
					*/
					
					if ($_FILES['profile_img']["error"] > 0) {
						echo $_FILES['profile_img']["error"];
						$status = 0;
					}
					else
					{
						//echo "Upload: " . $_FILES["file"]["name"] . "<br />";
						//echo "Type: " . $_FILES["file"]["type"] . "<br />";
						//echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
						//echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";
						$ext = '';
						
						$currTime = time();
						if($_FILES['profile_img']["type"] == "image/gif") {
							$ext = ".gif";
						} else if($_FILES['profile_img']["type"] == "image/jpeg") {
							$ext = ".jpg";
						} else if($_FILES['profile_img']["type"] == "image/png") {
							$ext = ".png";
						} else if($_FILES['profile_img']["type"] == "image/pjpeg") {
							$ext = ".png";
						}
						
						$file_name = md5($currTime);
						
						$big_name = $file_name . "_b" . $ext;
						$med_name = $file_name . "_m" . $ext;
						$small_name = $file_name . "_s" . $ext;

						$big_abs_path = $destination_path . $big_name;
						$med_abs_path = $destination_path . $med_name;
						$small_abs_path = $destination_path . 'thumbnails/' .  $small_name;
						
						move_uploaded_file($_FILES['profile_img']["tmp_name"], $big_abs_path);
						$big_file_path = $web_path . $big_name;
						$med_file_path = $web_path . $med_name;
						$small_file_path = $web_path . 'thumbnails/' . $small_name;
						$this->userSession['profile_img_src_small'] = $small_file_path;
						
						require_once('misc/phpthumb/ThumbLib.inc.php');

						try {
							$thumb = PhpThumbFactory::create($big_abs_path);
							$thumb->resize(800, 800)->save($big_abs_path);
							
							$thumb = PhpThumbFactory::create($big_abs_path);
							$thumb->adaptiveResize(206, 206);
							$thumb->cropFromCenter(206, 206)->save($med_abs_path);
							
							$thumb = PhpThumbFactory::create($big_abs_path);
							$thumb->adaptiveResize(50, 50);
							$thumb->cropFromCenter(50, 50)->save($small_abs_path);

							$qry1 = mysql_query("SELECT `profile_img_src_big`, `profile_img_src`, `profile_img_src_small` FROM `wp_users` WHERE `user_login` = '$this->userSession[member]'");
							if(mysql_num_rows($qry1) > 0) {
								$result1 = mysql_fetch_array($qry1);
								
								if(!empty($result1['profile_img_src_big'])) {
									$url = substr($result1['profile_img_src_big'], 1);
									if(file_exists($url)) {
										unlink($url);
									}
								}
								if(!empty($result1['profile_img_src'])) {

									$url = substr($result1['profile_img_src'], 1);
									if($url != "assets/images/profile/steve_avatar.png") {
										if(file_exists($url)) {
											unlink($url);
										}
									}
								}
								if(!empty($result1['profile_img_src_small'])) {
									
									$url = substr($result1['profile_img_src_small'], 1);
									if(file_exists($url)) {
										unlink($url);
									}
								}
							}
							mysql_query("UPDATE `wp_users` SET `profile_img_src_big` = '$big_file_path', `profile_img_src` = '$med_file_path', `profile_img_src_small` = '$small_file_path' WHERE `user_login` = '$this->userSession[member]'");
							$status = 1;
							if(isset($this->userSession['curr_img_src'])) {
								unset($this->userSession['curr_img_src']);						
							}
							$this->userSession['curr_img_src'] = $med_file_path;
							
							$str = $big_file_path . " " . $big_file_path . " " . $med_file_path;
							
							echo $str;
						}
						catch (Exception $e)
						{
							// handle error here however you'd like
							$status = $e->getMessage();
						}
					}
				}
				else {
					$status = 3;
				}
			} else {
				$status = 2;
			}
		} else { 
			$status = 0;
		}
		
		return;
	}
	
	function getMorePosts() {
		
		if(isset($_POST['start'])) {
			$start = $_POST['start'];
		} else {
			$start = 11;
		}
		
		if(isset($_POST['items'])) {
			$items = $_POST['items'];
		} else {
			$items = 10;
		}
		
		if(isset($_POST['user_page'])) {
			$post_location = $_POST['user_page'];
		} else {
			$post_location = 'NA';
		}
		
		$results = $this->user_model->mc_getMorePosts($start, $items, $post_location);
		
		echo json_encode(array("content"=>$results));
		exit;
	}
	
	public function flagPost() {
		$id = $_POST['id'];
		mysql_query("UPDATE `posts` SET `is_flagged` = 1 WHERE `id` = $id");
	
	}
	
	public function awards() {
		$content = '';
		$player_id = $_GET['playerid'];
		$results = $this->user_model->getAchievements($player_id);
		$content .= '<div style="float: left; padding-right: 10px;"><div style="width: 20px; height: 20px; background: #D7EFEF; float: left; margin-right: 5px; border: 1px solid gray;"></div>= Incomplete</div>
<div style="float: left; padding-right: 10px;"><div style="width: 20px; height: 20px; background: #F9F99F; float: left; margin-right: 5px; border: 1px solid #000;"></div> = Completed</div>
<div class="clear"></div><br />';
		$content .= '<div style="margin: 0 auto; width: 540px; max-height: 500px; overflow: auto;">';
		$content .= '<ul class="awardslist">';
		for($i=0; $i < sizeof($results); $i++) {
			if($results[$i]['completed'] == 1) {
				$content .= '<li class="completed clearfix"><b>'.$results[$i]['title'].'</b><span class="points-box">'.$results[$i]['points'].'XP</span><div class="clear"></div><div style="word-wrap:break-word; width:500px;">'.$results[$i]['description'].'</div><br /><span style="float: right;"><b><i>Completed: '.$results[$i]['completed_date'].'</i></b></span></li>';
			}
			else {
				$content .= '<li class="incomplete clearfix"><b>'.$results[$i]['title'].'</b><span class="points-box">'.$results[$i]['points'].'XP</span><div class="clear"></div><div style="word-wrap:break-word; width:500px;">'.$results[$i]['description'].'</div></li>';
			}
		}
		$content .= '</ul>';
		$content .= '</div>';
		echo json_encode(array("content"=>$content));
	}
	
	public function topJobs() {

		$data['results'] = $this->user_model->getTopJobPlayers();
		$content = $this->load->view('top_jobs', $data, true);
		
		echo json_encode(array("content"=>$content));
	}
	
	public function scores() {
			
		$content = '';
		$player_id = $_GET['playerid'];
		$results = $this->user_model->getScoreboard();

		if(!isset($_GET['live'])) {
			$content .= '<h1 class="scoreboard-title">Scoreboard</h1><br />';
			$content .= '<a href="'. site_url('main/scores?playerid='.$player_id.'&live') .'" class="score-tab score-menu-tab">Scoreboard</a><a href="'. site_url('main/awards?playerid='.$player_id) .'" class="score-tab score-menu-tab">Achievements</a><a href="'. site_url('main/topJobs') .'" class="score-tab score-menu-tab">Jobs</a><a href="'. site_url('main/stats') .'" class="score-tab score-menu-tab">Stats</a><a href="'. site_url('main/dreams') .'" class="score-tab score-menu-tab">Dreams</a><a href="'. site_url('main/groupActivities') .'" class="score-tab score-menu-tab">Group Activities</a><div class="clear"></div>';
			$content .= '<div class="top-scoreboard" style="position: relative; border: 1px solid gray; padding: 10px; max-height: 550px; margin-top: -5px; z-index: 900; background-color: #fff;">';
		}
		$content .= '<p>Achievement points are obtained by completing important activities in the game. <a class="find-me">See your achievement status</a>.<br /></p><br />';
		$content .= '<div style="margin: 0 auto; width: 600px; max-height: 460px; overflow: auto; overflow-x: visible;"><div style="padding-left: 40px; width: 340px; float: left;"><b>Player</b></div><div><b>Points</b></div><hr style="width: 440px;">';
		$content .= '<ol style="margin-left: 20px;">';
		for($i=0; $i < sizeof($results); $i++) {
			if($results[$i]['player_id'] == $player_id) {
				$content .= '<li class="my-ranking"><div style="width: 300px; float: left; color: blue;"><b>'.$results[$i]['player'].'</b></div><span style="padding-left: 40px; color: blue;"><b>'.$results[$i]['total_points'].'XP</b></span><div class="clear"></div></li>';
			}
			else {
				$content .= '<li><div style="width: 300px; float: left;">'.$results[$i]['player'].'</div><span style="padding-left: 40px;">'.$results[$i]['total_points'].'XP</span><div class="clear"></div></li>';
			}
		}
		if(isset($_GET['live'])) {
			echo json_encode(array("content"=>$content));
		} else {
			
			$content .= '</ol></div></div><br />';
			echo $content;
		}
	}
	
	/**
	 * Displays map of Minecraft
	 * 
	 * @TODO Remove redundant layout code and consolidate code
	 * @access public
	 * @return Void
	 */
	public function showMap() {
		$page = "map";
		$this->data['page_title'] = "Map";
		
		// Call the template class
		$template = new Template();
		
		$template->makePageTemplate($page, "one_col", $this->data);
	}
	
	public function maintenance() {
		$content = '';
		
		$content .= '<style>body { margin: 0 auto; } </style><div style="position: relative; width: 100%; height: 100%; background: #000; margin: 0 auto;"><div style="margin: 0 auto; width: 820px;"><img src="'.site_url('assets/images/under_maintenance.jpg').'" style="margin: 0 auto; text-align: center;"/></div></div>';
		echo $content;
	}
}
?>
