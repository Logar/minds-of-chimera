<?php
/* filename: settings.php */

/**
 * Controller for accessing and changing user settings
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Settings extends MOC_Controller
{	
	function __construct() {
		
		// Call the base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
        
        if($this->userSession['member'] == false) {
        	// Login required! Only accessible to logged in users
        	header("Location: " . SITE_URL);
        }
        
        $this->data['page_title'] = "Minds of Chimera";
	}
	
	public function index() {
		
		$this->data['page_title'] = $this->userSession['member'];
		$this->data['page'] = 'welcome';
		
		$this->data['css'] =  array('/assets/css/slimbox2.css', '/assets/css/colorbox.css');
		$this->data['scripts'] = array('/assets/scripts/lightbox/prototype.js', '/assets/scripts/slimbox2.js', '/assets/scripts/jquery.form.js');
	}
}
?>
