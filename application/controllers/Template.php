<?php
/* filename: Template.php */

/**
* Class for generating page template
*
* @author     Ross Arena
* @copyright  2012-2013 University of Central Florida
* @license    http://www.php.net/license/3_01.txt  PHP License 3.01
* @version    Release: 1.0
*/
class Template extends MOC_Controller
{
	private static $template = null;
	
	public function __construct() {
		
		// Call the base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
        
        $this->data['page_title'] = "Minds of Chimera";
	}
	
	public static function getTemplateInstance() {
		if(self::$template === null) {
			self::$template = new Template();
		}	
		return self::$template;
	}
	
	/**
	 * Generates the view templates for all pages
	 * 
	 * @param string $page
	 * @param string $layoutType
	 * @access public
	 */
	public function makePageTemplate($page, $layoutType, $data) {
		
		$data['page_title'] = $this->data['page_title'];
	
		$data['inline_scripts'] = array();
		
		if($page !== "none") {
			$data['content'] = $this->load->view($page, $data, true);
		}
		if($this->userSession['member'] !== false) {
			
			$data['newsFeed'] = $this->user_model->getPosts($this->userSession['member'], '', "sidebar_feed", '', 1);
	
			if(isset($this->userSession['notifyList'])) {
				$data['old_notifications'] = $this->userSession['notifyList'];
			} else {
				$data['old_notifications'] = '';
			}
			$data['notification_view'] = $this->load->view('notification_view', $data, true);
		}
	
		$data['layoutType'] = $layoutType;
	
		if($layoutType == "two_cols") {
			/**
			 * Load the sidebar for the 2nd column
			 */
			if($this->userSession['member'] == false) {
				if(isset($_GET['login_failed'])) {
					$data['login_failed'] = true;
				}
				if(isset($_GET['user_banned'])) {
					$data['user_banned'] = true;
				}
				$data['sidebar'] = $this->twig->render('sidebars/login', $data, false);
			} else {
				
				$data['sidebar'] = $this->load->view('sidebars/sidebar', $data, true);
			}
		}
	
		/**
		 * Let's have Twig handle the layout pages
		 * (pages: base.html.twig, footer.html.twig, header.html.twig, and main_layout.html.twig)
		 */
		$this->twig->render('layouts/main_layout', $data);
	}
	
	/**
	 * @todo make this cleaner
	 * @param unknown $curr_user
	 * @return unknown
	 */
	public function getPosts($currUser) {
	
		$status = 0;
		$condition = '';
	
		if(!empty($currUser)) {
			$posts = $this->user_model->getPosts($currUser, '', "curr_user", $condition, $status);
		} else {
			$posts = $this->user_model->getPosts($currUser, '', "non_user", $condition, $status);
		}
	
		return $posts;
	}
	
	public function getSinglePost($curr_user, $post_id, $status) {
	
		$carry = $post_id;
		
		if($status == -1) {
			$posts = $this->user_model->getPosts($curr_user, '', "single_post_any", $carry, $status);
		} else {
			if(isset($curr_user) && !empty($curr_user)) {
				$posts = $this->user_model->getPosts($curr_user, '', "single_post_user", $carry, $status);
			} else {
				$posts = $this->user_model->getPosts($curr_user, '', "single_post_non-user", $carry, $status);
			}
		}
	
		return $posts;
	}
}

?>