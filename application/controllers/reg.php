<?php
/* filename: reg.php */

/* Include the Template class */
include('Template.php');

/**
 * Controller for accessing and changing user settings
 * 
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Reg extends MOC_Controller
{	
	function __construct() {
		
		// Call the base class constructor
        parent::__construct();
        
        // Load the userdata model
        $this->load->model('user_model');
	}
	
	public function userRegister() {
		
		if(isset($_POST['register'])) {
			
			$register = $_POST['register'];
			
			if(!empty($register['fullname']) && isset($register['password1']) && isset($register['password2']) && isset($register['username']) && isset($register['email']) &&
			!empty($register['password1']) && !empty($register['password2']) && !empty($register['username']) && !empty($register['email'])) {
				
				if($register['password1'] == $register['password2']) {
					$pass = Reg::generatePassword($register['password1']);
					
					$date = date("c");
					$user = $register['username'];
					$email = $register['email'];
					
					$userResults = $this->register_model->findRegisteredUser($user, $email);
					$minecraftUserExists = $this->register_model->findMineCraftUser($user);
					
					if($userResults !== false){
						
						if($userResults['user_status'] == -1) {
							
							$register['user_email'] = $register['email'];
							$register['user_login'] = $register['username'];
							$register['profile_path'] = '/profile/' . $register['username'];
							$register['profile_img_src'] = '/assets/images/profile/steve_avatar.png';
							$register['profile_img_src_small'] = '/assets/images/profile/steve_avatar_small.png';
							$register['profile_img_src_big'] = '/assets/images/profile/steve_avatar_big.png';
							
							$this->register_model->updateCurrentUser($register);
						
							Reg::makeProfileDirectory($register['username']);
							Reg::firstLogin($register['username'], $register['password1']);
						} else {
							echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">There is already an account with that username</div>'));
							exit;
						}
					}
					else if($userResults == false) {
						echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">There is already an account with that email</div>'));
						exit;
					}
					else {
						$register['user_pass'] = $pass;
						$register['user_registered'] = $date;
						$register['user_status'] = 0;
						$register['profile_img_src_big'] = '/assets/images/profile/steve_avatar_big.png';
						$register['profile_img_src'] = '/assets/images/profile/steve_avatar.png';
						$register['profile_img_src_small'] = '/assets/images/profile/steve_avatar_small.png';
						$register['profile_path'] = '/profile/' . $register['username'];
						
						$this->register_model->addNewUser($register);
						
						Reg::makeProfileDirectory($register['username']);
						Reg::firstLogin($register['username'], $register['password1']);
					}
				} else {
					echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">Passwords do not match</div>'));
					exit;
				}
			} else {
				echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">Missing fields. Please fill in all fields</div>'));
				exit;
			}
		}
	}
	
	public function page() {
		
		if($this->userSession['member'] == false) {
			
			$this->data['page_title'] = "Sign Up";
			$this->data['page'] = "Sign Up";
		
			$page = "register";
			
			// Call the template class
			$template = new Template();
			
			$template->makePageTemplate($page, "one_col", $this->data);
		} else {
			header('Location: '.SITE_URL);
		}
	}
	
	private function makeProfileDirectory($username) {
	
		if(!file_exists("assets/images/profile/$username")) {
			mkdir("assets/images/profile/$username", 0755, true);
		}
		
		if(!file_exists("assets/images/profile/$username/thumbnails")) {
			mkdir("assets/images/profile/$username/thumbnails", 0755, true);
		}
	}
	
	public function firstLogin($username, $pass) {
		
		$login['username'] = $username;
		$login['password'] = $pass;
		$userdata = Reg::getCurrentUser($login);
		
		//Login_control Successful. Regenerate session ID to
		//prevent session fixation attacks
		session_regenerate_id();
		
		$newUserArray = array(
			'pid' 					=> $userdata->id,
			'member' 				=> $userdata->user_login,
			'email' 				=> $userdata->user_email,
			'profile_img_src_small' => $userdata->profile_img_src_small,
			'profile_path' 			=> $userdata->profile_path,
			'notifyCount' 			=> 0
		);

		$this->session->set_userdata($newUserArray);

		echo json_encode(array("status"=>1, "msg"=>'<div class="center-block"><img src="'.SITE_URL.'assets/images/loading.gif" alt="loading" /><br /><p>Creating your new account</p></div>'));
		exit;
	}
	
	public function login() {
		
		$login = $_POST['login'];
		$autologin = $login['autologin'];
		$userdata = Reg::getCurrentUser($login);
		
		if($userdata->user_status == -2) {
			header("Location: ".SITE_URL."?user_banned");
			exit;
		}
		else if($userdata !== false) {
			//$notifications = $this->user_model->getNotifications($userdata->user_login,'');
			
			if(empty($notify_arr[0]['cnt'])) {
				$notify_arr[0]['cnt'] = 0;
			}
						
			if(!empty($autologin)){
				setcookie("mc_autologin", 'usr='.$userdata->user_login.'&hash='.$userdata->user_pass, time()+60*60*24*30);
			}
			
			//Login_control Successful. Regenerate session ID to
			//prevent session fixation attacks
			//session_regenerate_id();
			
			$newUserArray = array(
					'pid' 					=> $userdata->id,
					'member' 				=> $userdata->user_login,
					'email' 				=> $userdata->user_email,
					'profile_img_src_small' => $userdata->profile_img_src_small,
					'profile_path' 			=> $userdata->profile_path,
					//'notifyCount' 			=> $notifications[0]['cnt']
			);
			
			$this->session->set_userdata($newUserArray);
			
			header("Location: ".SITE_URL);
			exit;
		}
		else {
			header("Location: ".SITE_URL."?login_failed");
			exit;
		}
	}
	
	public function forgotPass() {
		
		if(!empty($_POST['forgotuser']) && isset($_POST['forgotuser'])) {
			
			$user = $_POST['forgotuser'];
			
			$auth = Reg::generateResetPassAuth($user);
			
			$registeredUserExists = $this->register_model->findRegisteredUser($user, $user);
		
			if($registeredUserExists !== false) {
				
				$this->register_model->addPendingPassReset($currUser, $auth); 
				
				require_once("/usr/share/php/Mail.php");
				require_once("/usr/share/php/Mail/mime.php");
				
				$sender = "noreply@mindsofchimera.com";

				$recipient = $registeredUserExists['user_email'];
				
				$subject = "Minds of Chimera: Recover Your Password";
				
				$html = "<html><body>";
				$html .= "Hello $user,<br /><br />";
				$html .= "You received this email because you or someone pretending to be you requested for a password reset 
				for the user <b>$user</b>. Please click on the following link to reset your password: ";
				$html .= '<a href="http://mindsofchimera.com/reg/resetPass?user='.$user.'&auth='.$auth.'">Reset my password</a><br /><br />';
				//$html .= 'If you did not request a password reset, please click on the following link: <a href="http://mindsofchimera.com/reg/resetPass?user='.$user.'&auth='.$auth.'">Do not reset my password</a><br /><br />';
				$html .= "Thanks,<br />";
				$html .= "The MOC Team";
				$html .= "</body></html>";

				$host = "ssl://smtp.gmail.com";
				$port = "465";
				$username = "moc.noreply@gmail.com";
				$password = "3mindsnobrains";
				$headers = array ('From' => $sender,
								'To' => $recipient,
								'Subject' => $subject);
				$mime = new Mail_mime();
				$mime->setHTMLBody($html);
				$body = $mime->get();
				$smtp = Mail::factory('smtp',
								array ('host' => $host,
								'port' => $port,
								'auth' => true,
								'username' => $username,
								'password' => $password));
				$headers = $mime->headers($headers);
				$mail = $smtp->send($recipient, $headers, $body);
				
				if($mail) {
					echo json_encode(array("status"=>1, "msg"=>'<h3>Success!</h3><div class="success-box">Check your email for the password and then return to log in.</div><br />'));
					exit;
				} else {
					echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">Oops something went wrong...please try again</div>'));
					exit;
				}
			}
			else {
				echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">No username or email exists with that name</div>'));
				exit;
			}
		} 
		else {
			echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">Field can not be left blank</div>'));
			exit;
		}
	}
	
	public function resetPass() {

		if(isset($_GET['user']) && isset($_GET['auth']) && !empty($_GET['user']) && !empty($_GET['auth'])) {
			
			$user = $_GET['user'];
			$auth = $_GET['auth'];
			
			$findPendingPassReset = $this->register_model->findPendingPassReset($currUser, $auth);
			
			if($findPendingPassReset !== false) {

				$this->data['page_title'] = "Reset Your Password";
				$page = "reset_pass_view";
				$this->data['auth'] = $findPendingPassReset['auth'];
				
			} else {
				$this->data['page_title'] = "Page Not Found";
				$page = "page_missing";
			}
				
			// Call the template class
			$template = new Template();
			
			$template->makePageTemplate($page, "one_col", $this->data);
		}
	}
	
	public function resetPassForm() {

		if(isset($_POST['auth'])) {
			
			$auth_decoded = base64_decode($_POST['auth']);
			$pieces = explode("-", $auth_decoded);
			$currUser = $pieces[1];
			
			$auth = $_POST['auth'];
			$plain_password = $_POST['pass'];
			
			if(!empty($currUser) && !empty($auth) && !empty($plain_password)) {
				
				$passResetCheck = $this->register_model->findPendingPassReset($currUser, $auth);
				
				if($passResetCheck == true) {
					$new_pass = Reg::generatePassword($plain_password);
					
					$setUserCheck = $this->register_model->setUserPass($new_pass);
					if($setUserCheck == true) {
						$this->register_model->updatePendingPassReset($currUser);
					}
				}
				
				$this->data['page_title'] = "Minds of Chimera";
				$page = "home";
				
				// Call the template class
				$template = new Template();
					
				$template->makePageTemplate($page, "two_cols", $this->data);
			}
		}
	}
	
	private function generatePassword($plain_password) {
	
		$crypt_pass = crypt($plain_password, '$6$rounds=7000H!mc[Z=v-3*1~!`$rad1Efe%$#@!&)(?!$');
		$crypt_pass = base64_encode($crypt_pass);
		
		return $crypt_pass;
	}
	
	private function generateResetPassAuth($user) {
	
		//$salt = '$%#92IQEWC';
		$timestamp = time() . '-' . $user;
		//$md5stamp = md5($timestamp . $salt);
		return base64_encode($timestamp);
	}
	
	private function getCurrentUser($login) {

		$user = $login['username'];
		$plain_password = $login['password'];
		
		/**
		 * md5 the password, so we can search for any old md5 password hashes
		 * If a password is found, replace it with a sha-2 encrypted password
		 */
		$md5pass = md5($plain_password);
		
		/**
		 * Generate a sha-2 hash of the password
		 */
		$sha2pass = Reg::generatePassword($plain_password);

		$userArray = $this->user_model->getUser($user, $md5pass, $sha2pass);
		
		return $userArray;
	}
}
?>
