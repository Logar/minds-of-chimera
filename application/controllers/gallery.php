<?php
/* filename: gallery.php */

/* Include the Template class */
include('Template.php');
/**
 * Controller for displaying public and private photo galleries
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Gallery extends MOC_Controller
{
	function __construct() {
		
		// Call the base class constructor
        parent::__construct();
		
        $this->data['page_title'] = "Photo Gallery";
    }
	
	function index() {
		
		$this->data['page_title'] = "Photo Gallery";

		$this->data['owner'] = false;
		$this->data['member'] = $this->userSession['member'];
		$this->load->library('pagination');
		
		$this->load->model('gallery_model');
		
		$config['base_url'] = SITE_URL . 'gallery/index';
		$config['total_rows'] = $this->db->get('photo_gallery')->num_rows();
		$config['per_page'] = 12;
		$config['num_links'] = 20;
		$config['prev_link'] = '&laquo prev';
		$config['next_link'] = 'next &raquo';
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		//$this->data['records'] = $this->db->get('photo_gallery', $config['per_page'], $this->uri->segment(3));
		//$start = $this->uri->segment(3);
		$start = $this->uri->segment(3);
		
		if(empty($start)) {
			$start = 0;
		}
		$this->data['gallery_records'] = $this->gallery_model->getPhotoGallery($config['per_page'], $start);
		
		$page = $this->uri->segment(1);
		$this->data['page'] = $page;
		
		$this->data['css'] =  array('/assets/css/colorbox.css');
		$this->data['scripts'] = array('/assets/scripts/lightbox/prototype.js', '/assets/scripts/jquery.colorbox.js', '/assets/scripts/jquery.form.js');
		$this->data['inline_scripts'] = '$j("a[rel]").colorbox({ transition:"none", width:"90%", height:"90%", maxWidth:"90%", maxHeight:"90%;"});';
		
		$page = "gallery_view";
		
		// Call the template class
		$template = new Template();
			
		$template->makePageTemplate($page, "one_col", $this->data);
	}
	
	function photos($curr_gallery) {
		
		$this->data['page_title'] = $curr_gallery;
		$curr_gallery = $curr_gallery;
		
		$this->data['owner'] = $curr_gallery;
		$this->load->library('pagination');
		
		$this->load->model('gallery_model');
		
		$config['base_url'] = SITE_URL . 'gallery/index';
		$config['total_rows'] = $this->db->get('photo_gallery')->num_rows();
		$config['per_page'] = 12;
		$config['num_links'] = 20;
		$config['prev_link'] = '&laquo prev';
		$config['next_link'] = 'next &raquo';
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		//$this->data['records'] = $this->db->get('photo_gallery', $config['per_page'], $this->uri->segment(3));
		//$start = $this->uri->segment(3);
		$start = $this->uri->segment(3);
		if(empty($start)) {
			$start = 0;
		}
		$this->data['gallery_records'] = $this->gallery_model->getMyPhotoGallery($config['per_page'], $start, $curr_gallery);
		if($this->data['gallery_records'] == -1) {
			$page_missing = true;
		}
		$page = $this->uri->segment(1);
		$this->data['page'] = $page;
		
		$this->data['css'] =  array('/assets/css/colorbox.css');
		$this->data['scripts'] = array('/assets/scripts/lightbox/prototype.js', '/assets/scripts/jquery.colorbox.js', '/assets/scripts/jquery.form.js');
		$this->data['inline_scripts'] = '$j("a[rel]").colorbox({ transition:"none", width:"90%", height:"90%", maxWidth:"90%", maxHeight:"90%;"});';
		
		if(isset($page_missing)) {
			$this->data['page_title'] = "Page Not Found";
			$page = "page_missing";
		} else {
			$page = "private_gallery";
		}
		// Call the template class
		$template = new Template();
			
		$template->makePageTemplate($page, "one_col", $this->data);
	}
	
	public function upload_gallery_img() {
		
		$status = 0;
		$msg = "";
		
		//$destination_path = "/home/httpd/html/chimera/assets/images/gallery/";
		$destination_path = "assets/images/gallery/";
		$web_path = SITE_URL . "assets/images/gallery/";
		
		if(isset($_FILES['gallery_img'])) {
			
			if($_FILES['gallery_img']["size"] < 16777216) {
				if (($_FILES['gallery_img']["type"] == "image/gif")
					|| ($_FILES['gallery_img']["type"] == "image/jpeg")
					|| ($_FILES['gallery_img']["type"] == "image/pjpeg") 
					|| ($_FILES['gallery_img']["type"] == "image/png")) {
					
					$img = getimagesize($_FILES['gallery_img']["tmp_name"]);
					$minimum = array('width' => '206', 'height' => '206');
					$width= $img[0];
					$height =$img[1];

					if ($width < $minimum['width'] ){
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						exit;
					} else if ($height <  $minimum['height']){
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						exit;
					}
					if ($_FILES['gallery_img']["error"] > 0) {
						//json_encode(array('status'=>$_FILES['gallery_img']["error"]));
						$status = 0;
					}
					else
					{
						//echo "Upload: " . $_FILES["file"]["name"] . "<br />";
						//echo "Type: " . $_FILES["file"]["type"] . "<br />";
						//echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
						//echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";
						$ext = '';
						
						$currTime = time();
						if($_FILES['gallery_img']["type"] == "image/gif") {
							$ext = ".gif";
						} else if($_FILES['gallery_img']["type"] == "image/jpeg") {
							$ext = ".jpg";
						} else if($_FILES['gallery_img']["type"] == "image/png") {
							$ext = ".png";
						} else if($_FILES['gallery_img']["type"] == "image/pjpeg") {
							$ext = ".png";
						}
						
						$new_md5 = md5($currTime);
						$file_name = $new_md5 . $ext;
						$absolute_path = $destination_path . $file_name;
						$small_file = $new_md5 . "_small" . $ext;
						$small_file_abs_path = $destination_path . 'thumbnails/' . $small_file;
						
						move_uploaded_file($_FILES['gallery_img']["tmp_name"], $absolute_path);
						$big_file_path = $web_path . $file_name;
						$small_file_path = $web_path .  'thumbnails/' . $small_file;
						

						require_once('misc/phpthumb/ThumbLib.inc.php');

						try
						{
							$thumb = PhpThumbFactory::create($absolute_path);
							$thumb->resize(800, 800);
							$thumb->cropFromCenter(700)->save($absolute_path);
							
							$thumb = PhpThumbFactory::create($absolute_path);
							$thumb->adaptiveResize(206, 206);
							$thumb->cropFromCenter(206, 206)->save($small_file_abs_path);
							
							$galleryImg = array();
							$galleryImg['big_file_path'] = $big_file_path;
							$galleryImg['small_file_path'] = $small_file_path;
							$galleryImg['photo_date'] = date("Y-m-d H:i:s");
							$galleryImg['prefilter_date'] = date("m-d H:i");
							$galleryImg['photo_owner'] = $this->user;
							$galleryImg['img_title'] = "Posted by ".$galleryImg['photo_owner'] . ' on ' . $galleryImg['prefilter_date'];
							$photo_id = $this->gallery_model->createGalleryPhoto($galleryImg);
							$status = 1;
							
							/*
							json_encode(array('status'=>'<div class="gallery-img-container"><div class="img-container-inner">
							<a class="delete_photo" id="photo_'.$photo_id.'" style="display: none;"><img src="'.site_url('assets/images/delete_button.png') .'" alt="delete button" /></a>
							<a href="'.$big_file_path.'" target="'.$big_file_path.'" rel="lightbox[gallery]" title="'.$galleryImg['img_title'].'"><img src="'.$small_file_path.'" alt="minds of chimera"></a></div></div>'));
							*/
							echo '<div class="img-container"><div class="gallery-img-container-inner">
							<div class="edit-item-container"><div class="edit-photo-icon-container"><div class="edit-item-icon"></div></div><div class="edit-item-inner"><a class="delete_photo" id="photo_'. $photo_id .'" >
							<img src="'. SITE_URL .'assets/images/delete_button.png" alt="delete button" />Destroy!</a><br /><div class="clear"></div><form name="permissions-form-'. $photo_id .'" id="permissions-form-'.$photo_id .'" action="#" method="post" enctype="multipart/form-data">
							<input type="radio" name="view_permissions_'. $photo_id .'" value="everyone" checked="checked" class="visibility-setting" />Public <br /><input type="radio" name="view_permissions_'.$photo_id .'" value="private" class="visibility-setting" />Private<br /></form></div></div>
							<a class="delete_photo" id="photo_'.$photo_id.'" style="display: none; position: relative; float: right; right: 15px;"><img src="'.site_url('assets/images/delete_button.png') .'" alt="delete button" style="position: absolute;" /></a>
							<a href="'.$big_file_path.'" target="'.$big_file_path.'" rel="lightbox[gallery]" title="'.$galleryImg['img_title'].'"><img src="'.$small_file_path.'" alt="minds of chimera"></a></div></div>';
							exit;
						}
						catch (Exception $e)
						{
							// handle error here however you'd like
							$status = $e->getMessage();
						}
					}
				}
				else {
					$status = 3;
				}
			} else {
				$status = 2;
			}
		} else { 
			$status = 0;
		}
		echo $status;
	}
	
	public function deletePhoto() {
		
		$galleryImg['user'] = $this->user;
		$galleryImg['id'] = $_POST['id'];
		$galleryImg['table'] = "photo_gallery";
		$galleryImg['status'] = 0;
		
		$status = $this->gallery_model->deleteGalleryPhoto($galleryImg);
		echo json_encode(array('status'=>$status));
	}
	
	public function changePhotoVisibility() {
		
		$galleryImg['user'] = $this->user;
		$galleryImg['id'] = $_POST['id'];
		$galleryImg['visibility'] = $_POST['visibility'];
		
		$status = $this->gallery_model->mc_changePhotoVisibility($galleryImg);
		echo json_encode(array('status'=>$status));
	}
}

?>
