<?php
/* filename: convo.php */

/* Include the Template class */
include('Template.php');

/* Include the ConvoFactory */
include('ConvoFactory.php');

/**
 * Controller for the Plugin Convo
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.1
 */
class Convo extends MOC_Controller
{
	private static $template = null;
	private static $factory;
	
	public function __construct() {
		
		 // Call the base class constructor
        parent::__construct();
        $this->load->model('convo_model');
		
		self::$template = Template::getTemplateInstance();
		self::$factory = ConvoFactory::getConvoFactoryInstance();
		
		
		if($this->userSession['member'] == false) {
			// Login required! Only accessible to logged in users
			header("Location: " . SITE_URL);
		}
		
		$this->data['inline_scripts'] = array();
		$this->data['scripts'] = array('assets/scripts/ConvoMap.js', 'assets/scripts/ConvoEntry.js', 'assets/scripts/convo-main.js');
		
		$this->data['inline_scripts'] = array_push($this->data['inline_scripts'], '<script type="text/javascript">
		$j(function(){
			var hash = window.location.hash;
			console.log(hash);
			if(hash == "#popup") {
				$j("html, body").animate({
					scrollTop: $j(".convo-entry-container").offset().top
				}, 10);
				
				showOverlay("#convo-msg-success");
			}
			
			function showOverlay(id) {
				$j("#overlay").fadeIn("fast",function(){
					$j(id).show();
				});
			}
			
			function hideOverlay(id) {
				$j("#overlay").fadeOut("fast",function(){
					$j(id).hide();
				});
			}
		});
		</script>');
		$this->data['css'] = array('assets/less/convo.less');
        $this->data['page_title'] = 'Minds of Chimera';
    }
	
	function index() {
		
		$this->data['page_title'] = 'Convo';
		
		$this->data['page'] = "Convo";
		$this->data['convo_items'] = $this->convo_model->getConvos();
		
		$this->data['content'] = self::$factory->home($this->data);

		self::$template->makePageTemplate("none", "one_col", $this->data);
	}
	
	function page() {

		$this->data['page_title'] = 'Convo';
		$page = $this->uri->segment(3);
		
		if($page == "create_convo") {
			$this->data['internal_page'] = "create_convo";
			$this->data['item'] = array();
			$this->data['item2'] = array();
			$this->data['user_mode'] = "create";
		} else {
			$this->data['internal_page'] = "edit_convo";
			$convo_id = $this->uri->segment(4);
			
			$this->data['user_mode'] = "edit";
			
			$convo['convo_id'] = $convo_id;
			$convo['pid'] = $this->userSession['pid'];
			$owner = $this->convo_model->currentOwner($convo);
			
			if($owner == false) {
				header("Location: ".SITE_URL."convo");
				exit;
			}
			
			$this->data['user_mode'] = "edit";
			
			$this->data['item'] = $this->convo_model->getEntireConvo($convo_id);
			$this->data['item2'] = array();
		}
		
		if(isset($this->data['item'][0]['use_popup_on'])) {

			if($this->data['item'][0]['use_popup_on'] !== -1) {
				$this->data['groupNum'] = 1;
				$this->data['popup_entries'] = self::$factory->popup($this->data);
			}
		}
		
		$this->data['content'] = self::$factory->top($this->data);
		
		
		$this->data['content'] .= self::$factory->bottom($this->data);
			
		self::$template->makePageTemplate("none", "one_col", $this->data);
	}
	
	public function appendConvoResp() {
	
		$status = 1;
		$convoArray = $this->convo_model->getConvoEntryNames($_POST['convo_id']);
		$size = sizeof($convoArray);
		$options = '';
		
		for($i=0; $i < $size; $i++) {
			$options .= '<option value="'.$convoArray[$i]['entry_id'].'">'.$convoArray[$i]['title'].'</option>'; 
		}
		
		$response = '<div class="response">
			<div class="response-inner">
					<div class="clearfix"><div class="float-left">
						<div class="js-remove-resp"></div>
							<div class="float-left">
								<textarea name="group_1[responses][]" class="float-left textarea-small no-textarea-border" maxlength="90"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="float-left" style="margin-left: 10px;">
					<a class="convo-entry-options-arrow"><div class="arrow-unlinked"></div></a>
				</div><div class="clear"></div><br />
			</div>';
		echo json_encode(array("status"=>$status, "content"=>$response));
	}
	
	public function createConvo() {
		
		if(isset($this->userSession['pid'])) {
			
			$msg = '';
			$status = 0;
			$convo = array();
			$convo['pid'] = $this->userSession['pid'];
							
			if(!empty($_POST['convo_name'])) {
				
				$convo['name'] = $_POST['convo_name'];
				
				if(isset($_POST['convo_desc']) && !empty($_POST['convo_desc'])) {
					$convo['desc'] = $_POST['convo_desc'];
				} else {
					$convo['desc'] = "";
				}
				
				$convo['neartext'] = $_POST['approach_textarea'];
				$convo['clicktext'] = $_POST['click_textarea'];
				
				$convoId = $this->convo_model->addConvo($convo);
				
				if(isset($_POST['approach_display_popup'])) {
					$approachPopup = $_POST['approach_display_popup'];
				} else {
					$approachPopup = "";
				}

				if(isset($_POST['click_display_popup'])) {
					$clickPopup = $_POST['click_display_popup'];
				} else {
					$clickPopup = "";
				}

				if($approachPopup == "yes" || $clickPopup == "yes") {
					
					$popup_option = -1;
					
					if($approachPopup == "yes" && $clickPopup == "yes") {
						$popup_option = 3;
					}
					else if($approachPopup == "yes") {
						$popup_option = 1;
					}
					else if($clickPopup == "yes") {
						$popup_option = 2;
					}
					
					$this->createPopupEntity($groupName = "group_1", 1, $convoId, $popup_option);
				}
				
				$status = 1;
			}
		} else {
			$status = 0;
			$msg = '<div class="error-box">Please login</div>';
		}
		
		echo json_encode(array("status"=>$status, "msg"=>$msg));
	}
	
	public function createPopupEntity($groupName = "group_1", $currentGroupNumber, $convoId, $popup_option) {
			
		if(isset($_POST[$groupName])) {
			$group = $_POST[$groupName];
			
			if(!empty($group)) {
				$entity['title'] =  $group['title'];
				$entity['message'] = $group['message'];
				$entity['popup_option'] = $popup_option;
				$entity['order_num'] = $group['order_number'];
				
				$img_selected = trim($group['convo_img_selected']);
				if(empty($img_selected)) {
					$entity['convo_img_selected'] = null;
				} else {
					$entity['convo_img_selected'] = "/assets/images/plugins/convo/" . $img_selected;
				}
				
				if(isset($group['responses'])) {
					$entity['responses'] = $group['responses'];
				}
				if(isset($group['gotos'])) {
					$entity['gotos'] = $group['gotos'];
				}
				
				$this->convo_model->addConvoPopupEntity($entity, $convoId);
				$currentGroupNumber++;
				
				return $this->createPopupEntity("group_".$currentGroupNumber, $currentGroupNumber, $convoId, $popup_option);
			}
		} else {
			return;
		}
	}
	
	public function editPopupEntity($groupName = "group_1", $currentGroupNumber, $convoId, $popup_option) {
			
		if(isset($_POST[$groupName])) {
			$group = $_POST[$groupName];
			
			if(!empty($group)) {
				$entity['title'] =  $group['title'];
				$entity['message'] = $group['message'];
				$entity['popup_option'] = $popup_option;
				$entity['order_num'] = $group['order_number'];
				
				$img_selected = trim($group['convo_img_selected']);
				if(empty($img_selected)) {
					$entity['convo_img_selected'] = null;
				} else {
					$entity['convo_img_selected'] = "/assets/images/plugins/convo/" . $img_selected;
				}
				
				if(isset($group['responses'])) {
					$entity['responses'] = $group['responses'];
				}
				if(isset($group['gotos'])) {
					$entity['gotos'] = $group['gotos'];
				}
				
				$this->convo_model->addConvoPopupEntity($entity, $convoId);
				$currentGroupNumber++;
				
				return $this->editPopupEntity("group_".$currentGroupNumber, $currentGroupNumber, $convoId, $popup_option);
			}
		} else {
			return;
		}
	}
	
	public function editConvo() {
		
		if(isset($this->userSession['pid'])) {
			
			$msg = '';
			$status = 0;
			$convo = array();
			$convo['pid'] = $this->userSession['pid'];
							
			if(!empty($_POST['convo_name'])) {
				
				$convo['name'] = $_POST['convo_name'];
				
				if(isset($_POST['convo_desc']) && !empty($_POST['convo_desc'])) {
					$convo['desc'] = $_POST['convo_desc'];
				} else {
					$convo['desc'] = "";
				}
				
				$convo['neartext'] = $_POST['approach_textarea'];
				$convo['clicktext'] = $_POST['click_textarea'];
				
				$convo['convo_id'] = $_POST['convo_id'];
				$convoId = $this->convo_model->editConvo($convo);
				
				if(isset($_POST['approach_display_popup'])) {
					$approachPopup = $_POST['approach_display_popup'];
				} else {
					$approachPopup = "";
				}

				if(isset($_POST['click_display_popup'])) {
					$clickPopup = $_POST['click_display_popup'];
				} else {
					$clickPopup = "";
				}

				if($approachPopup == "yes" || $clickPopup == "yes") {
					
					$popup_option = -1;
					
					if($approachPopup == "yes" && $clickPopup == "yes") {
						$popup_option = 3;
					}
					else if($approachPopup == "yes") {
						$popup_option = 1;
					}
					else if($clickPopup == "yes") {
						$popup_option = 2;
					}
					
					$this->createPopupEntity($groupName = "group_1", 1, $convoId, $popup_option);
				}
				
				$status = 1;
			}
		} else {
			$status = 0;
			$msg = '<div class="error-box">Please login</div>';
		}
		
		echo json_encode(array("status"=>$status, "msg"=>$msg));
	}
	/*
	public function editConvo() {

		if(isset($this->userSession['pid'])) {
			
			$msg = '';
			$status = 0;
			$convo = array();
			$convo['pid'] = $this->userSession['pid'];
							
			if(!empty($_POST['convo_name'])) {
				
				$convo['name'] = $_POST['convo_name'];
				
				if(isset($_POST['convo_desc']) && !empty($_POST['convo_desc'])) {
					$convo['desc'] = $_POST['convo_desc'];
				} else {
					$convo['desc'] = "";
				}
				
				$convo['neartext'] = $_POST['approach_textarea'];
				$convo['clicktext'] = $_POST['click_textarea'];
				
				$convoId = $_POST['convo_id']; //$this->convo_model->addConvo($convo);
				
				if(isset($_POST['approach_display_popup'])) {
					$approachPopup = $_POST['approach_display_popup'];
				} else {
					$approachPopup = "";
				}

				if(isset($_POST['click_display_popup'])) {
					$clickPopup = $_POST['click_display_popup'];
				} else {
					$clickPopup = "";
				}

				if($approachPopup == "yes" || $clickPopup == "yes") {
					
					$popup_option = -1;
					
					if($approachPopup == "yes" && $clickPopup == "yes") {
						$popup_option = 3;
					}
					else if($approachPopup == "yes") {
						$popup_option = 1;
					}
					else if($clickPopup == "yes") {
						$popup_option = 2;
					}
					
					//$this->convo_model->mc_editConvo($convo);
					$this->createPopupEntity($groupName = "group_1", 1, $convoId, $popup_option);
				}
				
				$status = 1;
			}
		} else {
			$status = 0;
			$msg = '<div class="error-box">Please login</div>';
		}
		
		echo json_encode(array("status"=>$status, "msg"=>$msg));
	}
	*/
	
	public function getConvoEntry() {
		
		$this->data = array();
		$singleConvoEntry = array();
		$this->data['session'] = $this->userSession;
		
		$this->data['convo_id'] = $_POST['convoId'];
		$this->data['entry_id'] = $_POST['entryId'];
		$this->data['user_mode'] = $_POST['userMode'];
		
		if(isset($_POST['groupNum'])) {
			$this->data['groupNum'] = $_POST['groupNum'];
		}
		
		if(!empty($this->data['entry_id'])) {
			//$singleConvoEntry = $this->convo_model->getSingleConvoEntry($this->data['convo_id'], $this->data['entry_id']);
			$convoEntries = $this->convo_model->getEntireConvo($this->data['convo_id']);
			
			$this->data['item'] = $convoEntries;
		} else {
			$this->data['item'] = array();
		}
		
		$convoEntryNames = $this->convo_model->getConvoEntryNames($this->data['convo_id']);
		$this->data['item2'] = $convoEntryNames;
		
		$content = self::$factory->popup($this->data);
		
		if(isset($singleConvoEntry[0]['order_num']) && !empty($singleConvoEntry[0]['order_num'])) {
			echo json_encode(array("content"=>$content, "orderNum"=>$singleConvoEntry[0]['order_num'], 
			"entryId"=>$singleConvoEntry[0]['entry_id']));
		} else {
			$orderNum = 0;
			echo json_encode(array("content"=>$content));
		}
	}
	
	public function dupConvo() {
		
		$msg = '';
		$status = 0;
		$convo = array();
		
		if(!empty($_POST['itemid'])) {
			
			if(isset($this->userSession['pid'])) {
				$convo['pid'] = $this->userSession['pid'];
				$convo['id'] = $_POST['itemid'];
				$pieces = $this->convo_model->dupConvo($convo);
				
				$msg = '<div style="border: 1px solid #ccc; padding: 10px; margin-bottom: 5px;">

		
			<div class="edit-item-custom edit-item-container ">
				<a><div class="edit-item-icon"></div></a>
				<div class="edit-photo-inner">
					<a class="convo-edit-item" id="convo-duplicate-'.$pieces['id'].'">Edit</a><br />
					<a class="convo-duplicate-item" id="convo-duplicate-'.$pieces['id'].'">Duplicate</a><br /><br />
					<hr><br />
					<a class="convo-delete-item" id="convo-delete-'.$pieces['id'].'">
					<img src="'.site_url("assets/images/delete_button.png").'" alt="delete button" style="margin-right: 4px;">Destroy!</a><br>

					<div class="clear"></div>
					
				</div>
			</div>' .$pieces['name']. '</b><br /></div>';
				
				$status = 1;
			} else {
				$status = 0;
			}
		} else {
			$status = 0;
		}
		
		echo json_encode(array("status"=>$status, "content"=>$msg));
	}
	
	public function deleteConvo() {
		
		$status = 0;
		
		$id = $_POST['itemid'];
		$status = $this->convo_model->mc_deleteConvo($id);
	
		echo json_encode(array("status"=>$status));
	}
	
	public function deleteConvoEntry() {
		
		$status = 0;
		
		$entry_id = $_POST['entry_id'];
		
		$status = $this->convo_model->mc_deleteConvoEntry($entry_id);
	
		echo json_encode(array("status"=>$status));
	}
	

	/* @TODO Complete Graphiz function 
	 * Creates image and map from the dot file
	 * &$_image -> image name
	 * &$_map   -> map file text (not the file name)
	 * &$_tag   -> a nice tag with all the above
	 */
	function GV_createImageAndMap($_txt,$_uid,$_id,$_dot,&$_image,&$_map,&$_tag)
	{
		$retval = 0;

		// Create a png of this
		$a = array();

		// use to write the image name
		$ctime = microtime(true);
		$readbase = "images/tmp/".$_txt.$_uid."_".$_id."_main_".$ctime.".";
		$fh = fopen($readbase."dot",'w');
		fwrite($fh,$_dot);
		fclose($fh);
		$r = "/opt/local/bin/dot -Tcmapx -o".$readbase."map -Tpng -o".$readbase."png ".$readbase."dot";
		exec($r,$a,$retval);

		if($retval != 0) {
			/*
			log_message('debug',"Error: $retval :".print_r($a,true));
			log_message('debug',"Error: dot: '$_dot'");
			log_message('debug',"<div>Graphviz error:</div><div>cmd:".$r."</div><div>Error:".$retval."</div><div>Error Output:".print_r($a,true)."</div>");
			*/
		}
		else {
			$fh = fopen($readbase."map",'r');
			$_map = fread($fh,filesize($readbase."map")); // read the map file into $map
			fclose($fh);
			$_image = $readbase."png";
			$_tag = "<img src='".$readbase."png' usemap='#".$_txt.$_uid."_".$_id."' />".$_map;

		}
		return $retval;
	}
	
	public function uploadPhoto($file, $destination_path, $web_path) {
		
		$resp['status'] = 0;
		$resp['msg'] = "";

		if(count($file) >= 1) {
			
			if($file["size"] < 16777216) {
				if (($file["type"] == "image/gif")
					|| ($file["type"] == "image/jpeg")
					|| ($file["type"] == "image/pjpeg") 
					|| ($file["type"] == "image/png")) {
					
					$img = getimagesize($file["tmp_name"]);
					$minimum = array('width' => '75', 'height' => '75');
					$width = $img[0];
					$height = $img[1];

					if ($width < $minimum['width'] ){
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						return $resp;
					} else if ($height <  $minimum['height']){
						echo "<script type=\"text/javascript\">alert(\"Image dimensions are too small. Minimum dimensions are {$minimum['width']} x {$minimum['height']}px.\");</script>";
						return $resp;
					}
					if ($file["error"] > 0) {
						$resp['status'] = 0;
						return $resp;
					}
					else
					{
						$ext = '';
						
						$currTime = time();
						if($file["type"] == "image/gif") {
							$ext = ".gif";
						} else if($file["type"] == "image/jpeg") {
							$ext = ".jpg";
						} else if($file["type"] == "image/png") {
							$ext = ".png";
						} else if($file["type"] == "image/pjpeg") {
							$ext = ".png";
						}
						
						$new_md5 = md5($currTime);
						$file_name = $new_md5 . $ext;
						$absolute_path = $destination_path . $file_name;
						
						move_uploaded_file($file["tmp_name"], $absolute_path);
						$file_abs_path = $web_path . $file_name;
						

						require_once('misc/phpthumb/ThumbLib.inc.php');

						try
						{
							$thumb = PhpThumbFactory::create($absolute_path);
							$thumb->adaptiveResize(200, 200);
							$thumb->cropFromCenter(200, 200)->save($file_abs_path);
							
							$resp['status'] = 1;
							$resp['msg'] = $file_abs_path;
							
							return $resp;
						}
						catch (Exception $e)
						{
							// handle error here however you'd like
							$resp['status'] = $e->getMessage();
							return $resp;
						}
					}
				}
				else {
					$resp['status'] = 4;
					return $resp;
				}
			} else {
				$resp['status'] = 2;
				return $resp;
			}
		} else { 
			$resp['status'] = 4;
			$resp['msg'] = "file not there";
			return $resp;
		}
	}
	
	public function displayUploadedImage() {
		
		$file = $_POST['tmp'];
		$width = $_POST['width'];
		$height = $_POST['height'];
		
		$status = 0;
		$msg = "";

		if(!empty($file)) {

			require_once('misc/phpthumb/ThumbLib.inc.php');

			try
			{
				$thumb = PhpThumbFactory::create($file);
				$thumb->adaptiveResize($width, $height);
				$thumb->cropFromCenter($width, $height);
				
				$status = 1;
				$msg = $thumb;
			}
			catch (Exception $e)
			{
				// handle error here however you'd like
				$msg = $e->getMessage();
			}
		}
		echo json_encode(array("status"=>$status, "msg"=>$msg));
	}
	
	function removeNewline($input) {
		
		$t = trim($input);
		$t = str_replace("\n", " ", $t);
		$t = str_replace("\r", "", $t);
		return $t;
	}
}
?>
