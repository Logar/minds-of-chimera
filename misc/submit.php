<?
	if(!isset($_SESSION))
	{
		session_start();
	}
	
	require_once('connect.php');

	if(isset($_POST['register']))
	{
		
		$register = $_POST['register'];
		if(isset($_POST['password1']) && isset($register['username']) && isset($register['email']) &&
		!empty($_POST['password1']) && !empty($register['username']) && !empty($register['email']))
		{
			if($_POST['password1'] == $_POST['password2'])
			{
				$pass = md5($_POST['password2']);
				$date = date("c");
				$user = $register['username'];
				$email = $register['email'];
				$escaped_user = mysql_real_escape_string($user);
				$escaped_email = mysql_real_escape_string($email);

				$registerUser = sprintf("SELECT * FROM `wp_users` WHERE `user_login`='%s'",
					$escaped_user);
				$registerEmail = sprintf("SELECT * FROM `wp_users` WHERE `user_email`='%s'",
					$escaped_email);
				
				$userExists = sprintf("SELECT * FROM `cb_player` WHERE `name`='%s'",
					$escaped_user);
			
				$qry1 = mysql_query($registerUser);
				$qry2 = mysql_query($registerEmail);
				$qry3 = mysql_query($userExists);
				
				if(mysql_num_rows($qry1) > 0){
					echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">There is already an account with that username</div>'));
					exit;
				}
				else if(mysql_num_rows($qry2) > 0) {
					echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">There is already an account with that email</div>'));
					exit;
				}
				/*else if(mysql_num_rows($qry3) == 0) {
					echo '<div class="error-box">Please use your existing username or create a new username on the Minds of Chimera server</div>';
				}*/
				else {
					mysql_insert('wp_users', array(
						'user_login' => $register['username'],
						'user_pass' => $pass,
						'user_email' => $register['email'],
						'user_registered' => $date,
						'user_status' => 0
					));
					echo json_encode(array("status"=>1, "msg"=>'<div class="success-box">Your account has been successfully created!</div>'));
					exit;
				}
			} else {
				echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">Passwords do not match</div>'));
				exit;
			}
		}else {
			echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">Missing fields. Please fill in all fields</div>'));
			exit;
		}
	}
	else if(isset($_POST['login']))
	{
		$login = $_POST['login'];
		$userdata = get_users_local($login);

		if($userdata !== false) 
		{
			//Login_control Successful. Regenerate session ID to
			//prevent session fixation attacks
			session_regenerate_id();
			$_SESSION['member'] = $userdata['user_login'];
			$_SESSION['email'] = $userdata['user_email'];
			$_SESSION['pid'] = $userdata['ID'];

			//$user = wp_signon( $login_data, true ); 
			/*$creds = array();
			$creds['user_login'] =  $userdata['user_login'];
			$creds['user_password'] = $plain_password;
			$creds['remember'] = true;
			$user = wp_signon( $creds, false );
			if ( is_wp_error($user) ) {
			   //echo $user->get_error_message();
			}
			wp_set_current_user($user->ID); //Here is where we update the global user variables*/
			//Write session to disc
			session_write_close();
		}
		else {
			echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">Incorrect username or password</div>'));
			exit;
		}
	}
	else if(isset($_POST['forgot']))
	{
		$forgot = $_POST['forgot'];
		$user = $forgot['username'];
		$escaped_user = mysql_real_escape_string($user);
		
		$query = sprintf("SELECT * FROM `wp_users` WHERE `user_login`='%s' OR `user_email`='%s'",
			$escaped_user,
			$escaped_user);
		$query = mysql_query("SELECT `user_login`, `user_email` FROM `wp_users` WHERE `user_login` = '$user' OR `user_email` = '$user' AND `user_pass`='$new_pass'") or die(mysql_error());
		if(mysql_num_rows($query) > 0) {
			echo json_encode(array("status"=>1, "msg"=>'<div class="success-box"><h3>Success!</h3>Check your email for the password and then return to log in.</div><br />'));
			exit;
		}
		else {
			echo json_encode(array("status"=>0, "msg"=>'<div class="error-box">No username or email exists with that name</div><br />'));
			exit;
		}
	}
	
	function mysql_insert($table, $inserts) {
		$values = array_map('mysql_real_escape_string', array_values($inserts));
		$keys = array_keys($inserts);
			
		return mysql_query('INSERT INTO `'.$table.'` (`'.implode('`,`', $keys).'`) VALUES (\''.implode('\',\'', $values).'\')');
	}
	$plain_password = '';
	function get_users_local($login) {
		global $plain_password;

		$user = $login['username'];
		$plain_password = $login['password'];
		
		// begin normal code
		$new_pass = md5($plain_password);
		$escaped_user = mysql_real_escape_string($user);
		$escaped_pass = mysql_real_escape_string($new_pass);
		
		/*
		// begin wp code
		/*$userdata = get_user_by('login', $user);
		$loginResult = wp_check_password($plain_password, $userdata->user_pass, $userdata->ID);
		*/
		
		// normal code
		$query = sprintf("SELECT * FROM `wp_users` WHERE `user_login`='%s' AND `user_pass`='%s' OR `user_email`='%s'",
			$escaped_user,
			$escaped_pass,
			$escaped_user);
		$loginResult = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($loginResult) > 0) {
			
			$result = mysql_fetch_array($loginResult);
			return $result;
		}
		else {
			return false;
		}
		// wp code again
		/*if($loginResult == 1) {
			return $userdata;
		}
		else {
			return false;
		}*/
	}
?>
