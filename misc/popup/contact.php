<form name="contact-form" id="contact-form" method="post" accept-charset="utf-8" action="/misc/popup/contact_sub.php" >

	<div class="table-header"><h3 style="width: 80%;">Contact Us</h3></div>
	<!-- the result of the search will be rendered inside this div -->
	<div id="result"></div>
	
	<label class="sec-label">FULL NAME:</label><br />
	<div class="col-right" style="width: 400px;">
		<div class="pair cell-space-10"><input type="text" size="15" name="contact[first]"><label> First Name <span class="required">*</span></label></div>
		<div class="pair cell-space-10"><input type="text" size="15" name="contact[last]"><label> Last Name </label></div>
		<div class="pair"><input type="text" size="4" name="contact[suffix]" style="width: 30px;"><label> Suffix </label></div>
	</div><div class="clear"></div>
	<label class="sec-label"> PHONE: </label><br />
	<div class="col-right">
		<div class="pair">
			<span style="float: left; color: #777;">(</span><input type="tel" name="contact[phone_areaCode]" size="3" maxlength="3" style="float: left; width: 30px;"><span style="float: left; color: #777;">)</span><div class="clear"></div>
			<label> Area Code </label>
		</div>
		<div class="pair hypen" style="color: #777;">
			-
		</div>
		<div class="pair">
			<input type="tel" name="contact[phone_number]" size="8" maxlength="8" id="telCheck" style="width: 60px;">
			<label> Phone Number </label>
		</div>
	</div><div class="clear"></div>
	<label class="sec-label"> EMAIL: <span class="required">*</span></label><br />

	<div class="col-right">
		<input type="email" class="validate[Email]" name="contact[emailAddress]" size="30" placeholder="ex: myname@example.com" style="width: 250px;">
	</div><div class="clear"></div>

	<label class="sec-label"> MESSAGE: <span class="required">*</span></label><br />
	<div class="col-right">
		<textarea name="contact[message]" rows="4" cols="30"></textarea>
	</div><div class="clear"></div>
	<div style="margin: 0 auto; margin-top: 10px; width: 20%;"><input type="submit" value="Submit"></div>
</form>	
