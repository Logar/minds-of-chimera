<?
	
	$error = '<div class="error-box">
				<b>Missing fields</b><br />Please make sure to fill out all required fields. 
			  </div>';
	$error2 = '<div class="error-box">
				<b>Failed to send message</b><br />Please press submit and retry. 
			  </div>';
	$error3 = '<div class="error-box">
				Please check either Chris or Javid to email. 
			  </div>';
	$success = '<div class="success-box">
				  Your message has been successfully sent. Thank you.
				</div>';
		
	//print_r($_POST['contact']);
	$contact = $_POST['contact'];
	
	if(!empty($contact['first']) && !empty($contact['emailAddress']) && !empty($contact['message']))
	{
		$fullname = $contact['first'] . ' ' . $contact['last'] . ' ' . $contact['suffix'];
		$phone = $contact['phone_areaCode'] . '-' . $contact['phone_number'];
		$sender = $contact['emailAddress'];
		$message = $contact['message'];
		$recipients = array();
		
		if(!empty($contact[0]['mailTo']) && !empty($contact[1]['mailTo'])) {
			$recipients = "tetradonshar@gmail.com";
			$recipients = "tetradonshar@aim.com";
		} else if(!empty($contact[0]['mailTo'])) {
			// Chris only
			$recipients = "tetradonshar@gmail.com";
		} else if(!empty($contact[1]['mailTo'])) {
			// Javid only
			$recipients = "tetradonshar@aim.com";
		} else {
			echo $error3;
			exit;
		}
		
		foreach($recipients as $recipient) {
			$subject = "Homicidal Rabbit";
			$message .= PHP_EOL;
			$message .= "Contact Info:".PHP_EOL;
			
			$message .= 'Name: '. $fullname . PHP_EOL;
				
			$message .= 'Email: '. $sender . PHP_EOL;
			
			if(!empty($phone))
				$message .= 'Phone: '. $phone . PHP_EOL;
			
			$headers = 'From: '. $sender . "\r\n";

			$mail = mail($recipient, $subject, $message, $headers);
			 
			if(!$mail) {
				echo $error2;
				exit;
			}
		}
		
		$subject = 'Thank you';
		$message = "Dear ".$fullname.",".PHP_EOL."Thank you for contacting us at Homicidal Rabbit. We will respond back to you as soon as possible, usually within 48 hours. "
		. PHP_EOL . PHP_EOL ."Sincerely,".PHP_EOL."Homicidal Rabbit"; 
		
		$recipient = $sender;
		$sender = 'tetradonshar@gmail.com';
		
		$headers = 'From: '. $sender . "\r\n";

		$automail = mail($recipient, $subject, $message, $headers);
		
		if(!$automail) {
			echo $error2;
			exit;
		}
		else {
			echo $success;
		}
	}
	else {
		echo $error;
	}
?>
