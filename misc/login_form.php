<div id="login-register-password">

	<ul class="tabs_login">
		<li><a href="#tab1_login" class="tab_link active_login">Login</a></li>
		<li><a href="#tab2_login" class="tab_link">Register</a></li>
		<li><a href="#tab3_login" class="tab_link">Forgot?</a></li>
	</ul>
	<div class="tab_container_login">
		<div id="tab1_login" class="tab_content_login">

			<?
				if(isset($_GET['register'])) { 
					$register = $_GET['register'];

					if ($register == true) {
							echo '<h3>Success!</h3>
							<p>Check your email for the password and then return to log in.</p>';
					} 
				}
				if(isset($_GET['reset'])) { 
					$reset = $_GET['reset']; 
					if ($reset == true) {
							echo '<h3>Success!</h3>
							<p>Check your email to reset your password.</p>';
					} 
				} 
				else if(!isset($_GET['reset']) || !isset($_GET['register'])) {
					echo "<h3>Don't have an account?</h3>
					<p>Log in or sign up! It&rsquo;s fast &amp; <em>free!</em></p><br />";
				} 
			?>

			<form method="post" action="<?php bloginfo('url') ?>/wp-login.php" class="wp-user-form">
				<div class="float-left">
					<label class="sec-label">Username: </label>
				</div>
				<div class="col-right">
					<input type="text" name="user_login" value="<?php echo esc_attr(stripslashes($user_login)); ?>" size="20" id="user_login" tabindex="101" />
				</div><div class="clear"></div>
				<div class="float-left">
					<label class="sec-label">Password: </label>
				</div>
				<div class="col-right">
					<input type="password" name="pwd" value="" size="20" id="user_pass" tabindex="12" />
				</div><div class="clear"></div><br />
				<div class="login_fields" style="margin-left: 80px;">
					<label for="rememberme">
						<input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" tabindex="13" /> Remember me
					</label>
					<input type="submit" name="user-submit" value="Login" tabindex="14" />
					<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
					<input type="hidden" name="user-cookie" value="1" />
				</div>
			</form>
		</div>
		<div id="tab2_login" class="tab_content_login" style="display:none;">
			<h3>Register for this site!</h3>
			<p>Sign up now to get access to the medical form.</p>
			<form method="post" action="<?php bloginfo('url') ?>/register.php" class="wp-user-form">
				<div class="float-left">
					<label class="sec-label">Username: </label>
				</div>
				<div class="col-right">
					<input type="text" name="user_login" value="<?php echo esc_attr(stripslashes($user_login)); ?>" size="20" id="user_login" tabindex="101" />
				</div><div class="clear"></div>
				<div class="float-left">
					<label for="user_email">Your Email: </label>
				</div>
				<div class="col-right">
					<input type="text" name="user_email" value="<?php echo esc_attr(stripslashes($user_email)); ?>" size="25" id="user_email" tabindex="102" />
				</div><div class="clear"></div>
				<div class="login_fields" style="margin-left: 80px;">
					<input type="submit" name="user-submit" value="Sign up!" class="user-submit" tabindex="103" />
					<?php $register = $_GET['register']; if($register == true) { echo '<p>Check your email for the password!</p>'; } ?>
					<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?register=true" />
					<input type="hidden" name="user-cookie" value="1" />
				</div>
			</form>
		</div>
		<div id="tab3_login" class="tab_content_login" style="display:none;">
			<h3>Lose something?</h3>
			<p>Enter your username or email to reset your password.</p>
			<form method="post" action="" class="wp-user-form">
				<div class="float-left">
					<label for="user_login" class="hide">Username or Email: </label>
				</div>
				<div class="col-right">
					<input type="text" name="user_login" value="" size="20" id="user_login" tabindex="1001" />
				</div><div class="clear"></div>
				<div class="login_fields" style="margin-left: 120px;">
					<input type="submit" name="user-submit" value="Reset my password" class="user-submit" tabindex="1002" />
					<?php $reset = $_GET['reset']; if($reset == true) { echo '<p>A message will be sent to your email address.</p>'; } ?>
					<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?reset=true" />
					<input type="hidden" name="user-cookie" value="1" />
				</div>
			</form>
		</div>
	</div>

</div>
</div>
