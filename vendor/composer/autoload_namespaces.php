<?php

// autoload_namespaces.php generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_Extensions_' => array($vendorDir . '/twig/extensions/lib'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Doctrine\\ORM' => array($vendorDir . '/doctrine/orm/lib'),
    'Doctrine\\DBAL' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common' => array($vendorDir . '/doctrine/common/lib'),
    'Assetic' => array($vendorDir . '/kriswallsmith/assetic/src'),
);
