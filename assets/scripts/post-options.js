	$j(document).on('click', '.like', function() {
		
		var _this = $j(this);
		var _this_id = this.id;
		var className = $j(this).attr('class');
		var id = this.id.split("_")[1];
		var dataString = 'id='+ id + '&choice='+ className;

		$j.ajax
		({
			type: "POST",
			url: site_url + "main/rating",
			data: dataString,
			cache: false,
			dataType: "json",
			success: function(html)
			{
				if(html.status != 0) {
					if(html.like_count > 1) {
						var ele = _this.parent().children('.like_'+_this_id);
						ele.prepend('You and ');
					} else {
						var ele = _this.closest('.comment-container');
						var newEle = '<div class="single_comment_entity like_list"><img src="'+site_url+'assets/images/likeup.png" alt="likeup" style="float: left;" /><span class="' + _this_id + '">You</span> dig this</div>';
						ele.prepend(newEle);
					}
					if(html.append == "unlike") {
						var newEle = '<a class="unlike" id="unlike_'+id+'">Unlike</a>';
						var parent = _this.parent();
						_this.remove();
						parent.prepend(newEle);
					}
				}
			} 
		});
		return false;
	});
	
	$j(document).on('click', '.sidebar_like', function()
	{
		var _this = $j(this);
		var _this_id = this.id;
		var className = $j(this).attr('class');
		var id = this.id.split("_")[2];
		var dataString = 'id='+ id + '&choice='+ className;
		
		$j.ajax
		({
			type: "POST",
			url: site_url + "main/rating",
			data: dataString,
			cache: false,
			dataType: "json",
			success: function(html)
			{
				if(html.status != 0) {
					if(html.like_count > 1) {
						var ele = _this.closest('.comment_choices').find('.like_'+id);
						ele.empty().append(html.like_count);
					} else {
						var ele = _this.closest('.comment_choices');
						var newEle = '<span class="like-img">&nbsp;&#183;&nbsp;<img src="'+site_url+'assets/images/likeup.png" alt="likeup" />'+html.like_count+'</span>';
						ele.append(newEle);
					}
					
					if(html.append == "unlike") {
						var newEle = '<a class="sidebar_unlike" id="sidebar_unlike_'+id+'">Unlike</a>';
						var parent = _this.parent();
						_this.remove();
						parent.prepend(newEle);
					}
				}
			} 
		});
		return false;
	});
	
	$j(document).on('click', '.sidebar_unlike', function()
	{
		var _this = $j(this);
		var _this_id = this.id;
		var className = $j(this).attr('class');
		var id = this.id.split("_")[2];
		var dataString = 'id='+ id + '&choice='+ className;

		$j.ajax
		({
			type: "POST",
			url: site_url + "main/rating",
			data: dataString,
			cache: false,
			dataType: "json",
			success: function(html)
			{
				if(html.status != 0) {
					if(html.like_count >= 1) {
						var ele = _this.closest('.comment_choices').find('.like_'+id);
						ele.empty().append(html.like_count);
					} else if(html.like_count == 0) {
						var ele = _this.parent().children('.like-img');
						ele.remove();
					}
					if(html.append == "like") {
						var newEle = '<a class="sidebar_like" id="sidebar_like_'+id+'">Dig It</a>';
						var parent = _this.parent();
						_this.remove();
						parent.prepend(newEle);
					}
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return false;
	});
	
	$j(document).on('click', '.unlike', function()
	{
		var _this = $j(this);
		var _this_id = this.id;
		var className = $j(this).attr('class');
		var id = this.id.split("_")[1];
		var dataString = 'id='+ id + '&choice='+ className;

		$j.ajax
		({
			type: "POST",
			url: site_url + "main/rating",
			data: dataString,
			cache: false,
			dataType: "json",
			success: function(html)
			{
				if(html.status != 0) {
					if(html.like_count >= 1) {
						var ele = _this.closest('.comment-container').find('.like_list');
						var likes = ele.children('.like_'+id);
						var you_like = ele.children('.you_like');

						likes.text(html.like_count);
						you_like.remove();
					} else if(html.like_count == 0) {
						var ele = _this.closest('.comment-container').find('.like_list');
						ele.remove();
					}
					if(html.append == "like") {
						var newEle = '<a class="like" id="like_'+id+'">Dig it</a>';
						var parent = _this.parent();
						_this.remove();
						parent.prepend(newEle);
					}
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return false;
	});
	
	$j('.delete').on('click', 'a', function(e) {
		e.preventDefault();
		
		var table = this.id.split("_")[0];
		var id = this.id.split("_")[1];
		
		var answer = confirm("Destory selected post?");
		if (answer)
		{
			var dataString = 'id='+ id + '&table='+ table;
			$j.ajax
			({
				type: "POST",
				url: site_url + "main/deletePost",
				data: dataString,
				cache: false,
				success: function(html)
				{
					// do nothing
				} 
			});
			
			if(table == 'posts') {
				//if ($j(this).closest(".post-container").length > 0) { }
				$j(this).closest(".post-container").remove();
			}
			else
				$j(this).closest("#comment_"+ id).remove();
		}
		return false;
	});
	
	$j('.flag').on('click', 'a', function(e) {
		e.preventDefault();
		
		var id = this.id.split("_")[1];
		
		var answer = confirm("Are you sure you want to flag this post?");
		if (answer)
		{
			var dataString = 'id='+ id;
			$j.ajax
			({
				type: "POST",
				url: site_url + "main/flagPost",
				data: dataString,
				cache: false,
				success: function(html)
				{
					var get_post = $j(this).closest(".post-container").remove();
					$j('.all-posts').prepend('<div id="post-container" class="post-container"><img src="'+site_url+'assets/images/profile/blank_avatar_small.png" class="post_profile_img" alt="profile_image" style="margin-right: 5px;" /><i>This post was flagged</i></div>');

				}, 
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}
			});
		}
	});
	
	$j('.delete_photo').on('click', 'a', function(e) {
		e.preventDefault();
		var _this = $j(this);
		var id = this.id.split("_")[1];
		
		var answer = confirm("Destory this photo?");
		if (answer) {
			
			var dataString = 'id='+ id;
			$j.ajax
			({
				type: "POST",
				url: site_url + "gallery/deletePhoto",
				data: dataString,
				dataType: "json",
				cache: false,
				success: function(html)
				{
					if(html.status == 1) {
						_this.closest(".img-container").remove();
					} else {
						alert('Failed to delete image. Please try again.');
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log(textStatus, errorThrown);
				}
			});
			
		}
		return false;
	});