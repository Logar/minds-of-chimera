$j(function() {
	
	window.ConvoMap = function(canvasId) {
		
		var ctx = $j(canvasId).get(0).getContext('2d');
		
		var circles = [
		    { x:  50, y:  50, r: 25 },
		    { x: 250, y:  50, r: 25 },
		    { x: 250, y: 250, r: 25 },
		    { x:  50, y: 250, r: 25 },
		];
		
		var rootNode = { centerX: 150, centerY: 30, width: 100, height: 40 };
		
		function drawEllipse(node) {	
			ctx.beginPath();
			  
			ctx.moveTo(node.centerX, node.centerY - node.height/2); // A1
			  
			ctx.bezierCurveTo(
					node.centerX + node.width/2, node.centerY - node.height/2, // C1
					node.centerX + node.width/2, node.centerY + node.height/2, // C2
					node.centerX, node.centerY + node.height/2); // A2
			
			ctx.bezierCurveTo(
					node.centerX - node.width/2, node.centerY + node.height/2, // C3
					node.centerX - node.width/2, node.centerY - node.height/2, // C4
					node.centerX, node.centerY - node.height/2); // A1
			 
			ctx.closePath();
			ctx.fillStyle = "white";
			ctx.fill();
			ctx.lineWidth = 2;
		    ctx.strokeStyle = '#000';
		    ctx.stroke();
		}
		
		function drawOval(data) {
		    ctx.beginPath();
		    ctx.arc(data.x, data.y, data.r, 0, Math.PI * 2);
		    ctx.fill();
		}
		
		function drawLine(from, to) {
		    ctx.beginPath();
		    ctx.moveTo(from.x, from.y);
		    ctx.lineTo(to.x, to.y);
		    ctx.stroke();
		}
		
		//drawOval(mainCircle);
		
		//drawEllipse(rootNode);
		/*$j.each(circles, function() {
		    drawCircle(this);
		    drawLine(mainCircle, this);
		});*/
	}
});