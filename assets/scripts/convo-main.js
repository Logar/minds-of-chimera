$j(function() {
	
	// Instantiate a Convo Entry object
	var convo = new ConvoEntry();
	
	// Instantiate a Convo Map object
	//var map = new ConvoMap('#canvas-entry-map');

	$j(".instant-msg-link").hover(
	
		function(){
			$j(this).prevAll('.instant-msg-container:first').show();
		},
		function(){
			$j(this).prevAll('.instant-msg-container:first').hide();
		}
	);
	
	$j('.approach-radio').parent().on("click", '.approach-radio', function(event){
		
		var choice = $j(this).val();
		
		if(choice == "yes") {
			$j('#approach-choices-2').show();
		} else {
			$j('#approach-choices-2').hide();
			$j('#approach-choices-2 input[value="no"]').attr('checked', true);
		}
	});
	
	$j('.click-radio').parent().on("click", '.click-radio', function(event){
		
		var choice = $j(this).val();
		
		if(choice == "yes") {
			$j('#click-choices-2').show();
		} else {
			$j('#click-choices-2').hide();
			$j('#click-choices-2 input[value="no"]').attr('checked', true);
		}
		return true;
	});
	
	$j(document).on("change", '.click-display-popup', function(event){

		if($j(this).val() == "yes") {
			$j('#convo-onpopup-sec').show();
			
			if($j('.convo-entry-container').length == 0) {
				var that = $j(this);
				convo.updateGroupNum("add");
				convo.createEntry($j(this), '', 'create');
			} else {
				this.scrollToEntry($j(".convo-entry-container:last-child"));
			}
			
		} else {
			$j('#convo-onpopup-sec').hide();
		}
	});
	
	$j('.click-display-text').parent().on("change", '.click-display-text', function(event){

		if($j(this).val() == "yes") {
			$j('#click-textarea').show();
		} else {
			$j('#click-textarea').hide();
		}
	});
	
	$j('.approach-display-popup').parent().on("change", '.approach-display-popup', function(event){

		if($j(this).val() == "yes") {
			$j('#convo-onpopup-sec').show();
			
			if($j('.convo-entry-container').length == 0) {
				var that = $j(this);
				
				convo.updateGroupNum("add");
				convo.createEntry($j(this), '', 'create');
			}
			            
		} else {
			$j('#convo-onpopup-sec').hide();
		}
	});
	
	$j('.approach-display-text').parent().on("change", '.approach-display-text', function(event){

		if($j(this).val() == "yes") {
			$j('#approach-textarea').show();
		} else {
			$j('#approach-textarea').hide();
		}
	});
	
	$j(document).on("click", '.convo-faces', function(event) {
		event.preventDefault();
		
		var currentId = $j(this).attr('id').split("-");
		var entryNum = currentId[3];
		var emotion = currentId[1];
		var $convoFacesTop = $j(this).closest('.convo-faces-top');
		var oldFace = $convoFacesTop.find('.show:first');
		
		$j(oldFace).removeClass('show').addClass('hide');
		//$j('#convo-img-selected-'+entryNum).val(emotion+"_face.png");
		$convoFacesTop.find(".convo-"+emotion+"-face").removeClass('hide').addClass('show');
	});

	$j("a.edit-icon-link").on("click", function(e) {
		e.preventDefault();
		var curr = $j(this).next('.active');
		var any = $j(this).parents('#convos-container').find(".active");
		
		if(curr.length > 0) {
			
			$j(curr).toggleClass("active");
			
		} else if(any.length > 0) {
		
			$j(any).toggleClass("active");
			$j(this).next('.edit-item-container').toggleClass("active");
		} else {
			$j(this).next('.edit-item-container').toggleClass("active");
		}
	});
	
	$j(".convo-duplicate-item").parent().on("click", ".convo-duplicate-item", function(event) {
		event.preventDefault();
		var id = this.id.split("-")[2];
		var dataString = 'itemid='+ id;
		
		$j.ajax
		({
			type: "POST",
			data: dataString,
			url: site_url + 'convo/dupConvo',
			dataType: "json",
			cache: false,
			success: function(resp)
			{
				if(resp.status == 1) {
					$j('#convos-container').append(resp.content);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return true;
	});
	
	$j(".convo-delete-item").parent().on("click", ".convo-delete-item", function(event) {
		var _this = $j(this);
		event.preventDefault();
		var id = this.id.split("-")[2];
		var convo_id = $j("#convo_id").val();
		var dataString = 'itemid='+ id;
		
		$j.ajax
		({
			type: "POST",
			data: dataString,
			url: site_url + 'convo/deleteConvo',
			dataType: "json",
			cache: false,
			success: function(resp)
			{
				if(resp.status == 1) {
					window.location.href = site_url + "convo";
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return true;
	});
	
	$j("#convo-onpopup-sec").on("click", ".convo-create-entry", function(event) {
		event.preventDefault();
		
		// Increment the global count for the number of entries
		convo.updateGroupNum("add");
		var groupCount = convo.getGroupNum();
		
		// Call the function for performing all UI actions required to change a response goto
		convo.changeResponseGoto($j(this), groupCount);
		
		convo.addSelectOptions(groupCount);
		convo.createEntry($j(this), '', 'create');
	});
	
	$j(document).on("click", ".convo-entry-options-arrow", function(event) {
		event.preventDefault();
		
		$j(this).hide();
		
		var $currentResponse = $j(this).closest('.response');
		var $currentEntryOptions = $currentResponse.find('.entry-options-container');
		
		if($currentEntryOptions.length == 0) {
			var $convoEntryOptionsPosition = $currentResponse.children('br');
			
			var convoEntryOptions = '<div class="entry-options-container">'+
			'<div class="convo-entry-option">';
			
			if($j(this).children('.arrow-linked-current').length == 0) {
				convoEntryOptions += '<a class="convo-create-entry">Create New Entry</a>';
			}
			convoEntryOptions +='<a class="no-response-options-link"><div class="no-response-options"></div></a></div>';
			
			if(convo.groupNum > 1) {
				convoEntryOptions += '<div class="convo-entry-option"><a class="convo-join-entry">Join to Existing</a></div></div>';
			} 
			$convoEntryOptionsPosition.before(convoEntryOptions);
		} else {
			$currentEntryOptions.show();
		}
	});
	
	$j(document).on("change", ".select-existing", function(event) {
		event.preventDefault();
		
		var ele = event.target.options[event.target.selectedIndex];
		var optionVal = ele.value;
		
		if(optionVal !== "-1") {
			optionVal = ele.text;
		}
		// Call the function for performing all UI actions required to change a response goto
		convo.changeResponseGoto($j(this), optionVal);
	});
	
	$j(document).on("click", ".convo-join-entry", function(event) {
		event.preventDefault();
		
		var $parent = $j(this).parent();
		var $topParent = $j(this).closest('.convo-entry-container');
		var responsesCount = convo.getCurrentEntryResponsesCount($topParent);
		
		// check if current entity already has a select element
		var $currentSelect = $j(this).closest('.entry-options-container').find('select');
		var topParentId = $topParent.attr('id').split("-")[3];
		
		if($currentSelect.length > 0) {
			$currentSelect.show();
		}
		else if(convo.groupNum > 1) {
			$parent.append(convo.getEntrySelect(topParentId, responsesCount));
		}
		
		// Finished using $(this), now we can delete it
		$j(this).remove();
	});
	
	$j(document).on("click", ".no-response-options-link", function(event) {
		event.preventDefault();
		
		$j(this).closest('.entry-options-container').hide();
		$j(this).closest('.response').find('.convo-entry-options-arrow').show();
	});

	
	$j(document).on("click", ".optionlinked", function(event) {
		event.preventDefault();
		
		var $currentArrow = convo.getCurrentLink();
		
		// In case the entry options container is opened, set the arrow to visible again with the .show()
		$currentArrow.parent().addClass('convo-entry-options-arrow').show();
		$currentArrow.removeClass('arrow-linked-current').addClass('arrow-unlinked');
		
		// Remove the no longer linked entry options container
		$currentArrow.closest('.response').find('.entry-options-container').remove();

		convo.unsetCurrentLink();
		
		$j('#convo-entry-container-'+convo.groupNum).hide();
		
		$j(this).remove();
	});
	
	$j("#convo-show-map").parent().on("change", "#convo-show-map", function(event) {

		$j("#convo-show-map option:selected").each(function () {
			entryId = $j(this).val();
		});

		convo.createEntry($j(this), '', 'change');
	});
	
	$j(".entry-delete").hover(
	
		function(){
			$j(this).prev('.instant-msg-container').show();
		},
		function(){
			$j(this).prev('.instant-msg-container').hide();
		}
	);
	
	$j(document).on("click", ".entry-delete", function(event) {
		event.preventDefault();
		var entryId = this.id.split("-")[2];

		if(entryId !== '') {
			var convo_id = $j("#convo_id").val();
			var answer = confirm("Destory this conversation entry?");
			if (answer) {
				if(convo.userMode == "create") {
					
					$j('.optionlinked').remove();
					var $currentEntryContainer = $j(this).closest('.convo-entry-container');
					
					if($currentEntryContainer.attr('id').split("-")[3] == 1) {
						
						$currentEntryContainer.find("input[type=text], textarea").val("");
					} else {
						//$currentEntryContainer.prev('.optionlinked').remove();
						$currentEntryContainer.remove();
					}
					convo.groupNum--;
					convo.removeSelectOptions($j(this).closest('.convo-entry-container').attr('id').split("-")[3]);
				}
				else if(convo.userMode == "edit") {
					var dataString = 'entry_id='+ entryId;
					$j.ajax
					({
						type: "POST",
						url: site_url + "convo/deleteConvoEntry",
						data: dataString,
						dataType: "json",
						cache: false,
						success: function(html)
						{
							if(html.status == 1) {
								window.location.href = site_url + "convo/page/edit_convo/"+convo_id;
							} else {
								console.log("Failed to delete this conversation entry");
							}
						},
						error: function(jqXHR, textStatus, errorThrown) {
							//console.log(textStatus, errorThrown);
						}
					});
				}
			}
		} else {
			alert("No conversation entry exists yet. You can not delete this.");
		}
		return false;
	});
	
	//When the message box is closed, fade out
	$j(".close").parent().on("click", ".close", function(){
		$j(this).closest(".dim").fadeOut();
		return false;
	});
	
	function startLoader(parent){
		
		$j(parent).children('.f1-upload-process').css({"display": "block"});
		return true;
	}
	
	function stopLoader(parent){
	
		$j(parent).children('.f1-upload-process').css({"display": "none"});
		return true;   
	}
	
	var $check_convo_form = false;
	
	$j(document).on("submit", "#convo-form2", function(event) {
		event.preventDefault();

		var status = 0;
		
		// get some values from elements on the page: 
		var form = $j(this), goto_url = form.attr('action');
		
		var urlArray = goto_url.split('/');
		
		// get an associative array of just the values.
		var values = {};
		$j(this).find('input,textarea').each(function() {
			values[this.name] = $j(this).val();
		});

		if($j("input[name=approach_display_popup]").val() == "yes" || $j("input[name=click_display_popup]").val() == "yes") {

			if(values['popup_title'] == '') {
				status = 0;
			} else if(values['popup_says'] == '') {
				status = 0;
			} else {
				status = 1;
			}
			
			if(status == 0) {
				alert('Please fill out all the fields if you want to use the pop-up option');
			}
		}
		
		if(status == 1) {
			
			var convo_id = $j("#convo_id").val();
			var dataSent = $j(form).serialize();
			
			$j.ajax
			({
				type: "POST",
				url: goto_url,
				data: dataSent,
				dataType: "json",
				cache: false,
				success: function(resp) {
					
					if(resp.status == 1) {
						if(urlArray[urlArray.length-1] == "createConvo") {
							window.location.href = site_url + "convo";
						} else {
							window.location.href = site_url + "convo/page/edit_convo/" + convo_id;
							//showOverlay("#convo-msg-success");
						}

					} else {
						showOverlay("#convo-msg-failure");
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
					showOverlay("#convo-msg-failure");
				}
			});
			
		} else {
			console.log('validation failed');
			return false;
		}
		
	});
	
	$j('.boxclose').click(function(){
        var $parent = $j(this).parent('.user-msg-box');
		hideOverlay($parent);
    });
	
	function showOverlay(id) {
		$j('#overlay').fadeIn('fast',function(){
            $j(id).show();
        });
	}
	
	function hideOverlay(id) {
		$j('#overlay').fadeOut('fast',function(){
            $j(id).hide();
        });
	}
	
	$j(".js-add-resp-create").parent().on("click", ".js-add-resp-create", function(event) {
		
		event.preventDefault();
		
		var $responsesTop = $j(this).closest('.responses-top');
		var count = $responsesTop.find('.response').length;
		if(count < 4) {
			var response = '<div class="response">'+
			'<div class="response-inner">'+
					'<div class="clearfix"><div class="float-left">'+
						'<div class="js-remove-resp"></div>'+
							'<div class="float-left">'+
								'<textarea name="group_'+convo.groupNum+'[responses][]" class="float-left textarea-small no-textarea-border" maxlength="90"></textarea>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="float-left" style="margin-left: 10px;">'+
					'<a class="convo-entry-options-arrow"><div class="arrow-unlinked"></div></a>'+
				'</div><div class="clear"></div><br />'+
			'</div>';
			$responsesTop.append(response);
		}
	});
	
	$j(document).on("click", ".js-add-resp", function(event) {
		
		event.preventDefault();
		
		var $responsesTop = $j(this).closest('.convo-entry-container').find('.responses-top');
		
		var count = $responsesTop.find('.response').length;

		if(count < 4) {
			if($j('#convo_id').doesExist()) {
				var c_id = $j('#convo_id').val();
				var dataString = 'convo_id='+ c_id;
				$j.ajax
				({
					type: "POST",
					url: site_url + "convo/appendConvoResp",
					data: dataString,
					dataType: "json",
					cache: false,
					success: function(resp) {
						$responsesTop.append(resp.content);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus, errorThrown);
					}
				});
			} else {
				var response = '<div class="response">'+
						'<div class="response-inner">'+
							'<div class="clearfix"><div class="float-left">'+
								'<div class="js-remove-resp"></div>'+
									'<div class="float-left">'+
										'<textarea name="group_'+convo.groupNum+'[responses][]" class="float-left textarea-small no-textarea-border" maxlength="90"></textarea>'+
									'</div>'+

								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="float-left" style="margin-left: 10px;">'+
							'<a class="convo-entry-options-arrow"><div class="arrow-unlinked"></div></a>'+
						'</div><div class="clear"></div><br />'+
					'</div>';
				$responsesTop.append(response);
			}
		}
	});
	
	$j.fn.doesExist = function() {
		return $j(this).length > 0;
	};
 
	$j(document).on("click", ".js-remove-resp", function(event) {
		
		event.preventDefault();
		$j(this).closest('.response').remove();
	});
});