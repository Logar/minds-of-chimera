$j(function() {
	
	$j(window).scroll(function() {
		var fromTop = $j(document).scrollTop();
		//267
		
		if(fromTop >= 190) {
			//console.log(fromTop);
			$j('#fixed_feed').css({"position" : "fixed", "top" : "0" });
		} else {
			//console.log(fromTop);
			$j('#fixed_feed').css({"position" : "relative", "max-width" : "100%"});
		}
	});
	
	$j('.go-top').click(function() {
		$j('html, body').animate({scrollTop:0}, 0);
		return false;
	});
	
	function appendMoreUsers() {
		$j('#all-users-loader').css({"display": "block"});
		$j.ajax
		({
			type: "POST",
			url: site_url + 'main/getMoreGamePlayers',
			dataType: "json",
			cache: false,
			data: {
				start: users_startIndex,
				items: users_limit
			},
			success: function(resp)
			{
				$j('#all-users-content').append(resp.content);
				$j('#all-users-loader').css({"display": "none"});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return true;
	}
	
	function appendMorePosts() {
		//console.log('this is post date in append function '+posts_startDate);
		$j('#more-posts-loader').css({"display": "block"});
		$j.ajax
		({
			type: "POST",
			url: site_url + 'main/getMorePosts',
			dataType: "json",
			cache: false,
			data: {
				start: posts_startDate,
				items: posts_limit,
				user_page: user
			},
			success: function(resp)
			{
				//console.log(resp.content)
				$j('.all-posts').append(resp.content);
				$j("textarea[class*=expand]").TextAreaExpander();
				$j('#more-posts-loader').css({"display": "none"});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return true;
	}
	// show delete button for post
	$j(".post-container").on({
		mouseenter:
			function()
			{
				$j(this).find(".post-options:first").show();
			},
		mouseleave:
			function()
			{
				$j(this).find(".post-options:first").hide();
			}
		}
	);
	
	$j(".post-container-bottom").on({
		mouseenter:
			function()
			{
				$j(this).find(".delete:first").show();
			},
		mouseleave:
			function()
			{
				$j(this).find(".delete:first").hide();
			}
		}
	);
	
	$j(".single_comment_entity").on({
		mouseenter:
			function()
			{
				$j(this).find(".post-options:first").show();
			},
		mouseleave:
			function()
			{
				$j(this).find(".post-options:first").hide();
			}
	});
	
	$j(".gallery-img-container-inner").on({
		
		mouseenter:
			function(){
				$j(this).find('.edit-item-container:first').show();
			},
		mouseleave:
			function(){
				$j(this).find('.edit-item-container:first').hide();
			}
	});
	
	$j("li.toplink").hover(
	
		function(){
			//Following events are applied to the subnav itself (moving subnav up and down)
			$j(this).prev('span').eq(0).css({"background" : "url('"+ site_url + "assets/images/switch-on-bg.png') no-repeat", "margin-top" : "0px"});
			
			//$j(this).children('ul.subnav').stop(true,true).slideDown(1).show(); //Drop down the subnav on click
		},
		function(){
			//$j(this).children('ul.subnav').stop(true,true).slideUp(5).show(); //Drop down the subnav on click
			$j(this).prev('span').eq(0).css({"background" : "url('"+ site_url + "assets/images/switch-off-bg.png') no-repeat", "margin-top" : "-7px"});
		}
	);
	
	$j("#profile_img").hover(
	
		function(){
			$j('#profile_upload_form').delay(100).stop(true,true).show();
		},
		function(){
			$j('#profile_upload_form').delay(10).stop(true,true).hide();
		}
	);
	
	$j(".instant-msg-link").hover(
	
		function(){
			$j(this).prevAll('.instant-msg-container:first').show();
		},
		function(){
			$j(this).prevAll('.instant-msg-container:first').hide();
		}
	);
	
	$j('#post-photo').click(function(event){
		event.preventDefault();
		//$j('.post-form').hide();
		$j('#post-box').css({"min-height" : "60px"});
		$j('.grippie').show();
		$j('#post-box-bottom').show();
		$j('#upload-feed-photo').css({"display" : "block"});
	});
	
	$j('#post-status').click(function(event){
		event.preventDefault(); 
		$j('.post-form').show();
		$j('#upload-feed-photo').hide();
	});
	
	$j("#notify-sign").click(function(){
		var getClass = $j('#notification-list').hasClass("active");
		
		if(getClass == true) {
			$j('#notification-list').toggleClass("active");
			$j('#notification-list').hide();
		} else {
			$j('#notification-list').toggleClass("active");
			$j('#notification-list').show();
			
			if($j("#notification-count").length > 0) {
				$j.ajax
				({
					type: "POST",
					url: site_url + "main/updateNotifications",
					dataType: "json",
					cache: false,
					success: function(html)
					{
						if(html.status == 1) {
							$j('#notification-count').text("");
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						//console.log(textStatus, errorThrown);
					}
				});
			}
		}
		return false;
	});
	
	$j(".trigger_dropdown_links").click(function(){
		var hasClass = $j('#profile_dropdown_container').hasClass("active");
		
		if(hasClass == true) {
			$j('#profile_dropdown_container').toggleClass("active");
			$j('#profile_dropdown_container').hide();
		} else {
			$j('#profile_dropdown_container').toggleClass("active");
			$j('#profile_dropdown_container').show();
		}
		return false;
	});
	
	$j(".gallery-img-container-inner").on({
		mouseleave:
			function()
			{
				$j(this).find(".edit-item-inner").hide();
			}
		}
	);
	
	$j(".tab_link").click(function() {
		var old_id = $j(".active_login").attr("href");
		$j(old_id).hide();
		
		var new_id = $j(this).attr("href");
		$j(".active_login").removeClass("active_login");
		$j(this).addClass("active_login");
		
		$j(new_id).show();
		return false;
	});
	
	$j("#signup-btn").click(function() {
		
		$j('.signup-space').hide();
		$j('.register_container').show();
	});
	
	$j(".back-login").click(function() {
		
		$j('.signup-space').show();
		$j('.register_container').hide();
	});
	
	//Adjust height of overlay to fill screen when page loads
	$j("#fullsize-map").css("height", $j(window).height());

	//When the link that triggers the message is clicked fade in overlay/msgbox
	$j("#expand-map").click(function(){
		$j("#fullsize-map").fadeIn();
		return false;
	});

	//When the message box is closed, fade out
	$j(".close").click(function(){
		$j(this).closest(".dim").fadeOut();
		return false;
	});
	
	$j("#post-box").click(function() {
		$j(this).css({"min-height" : "60px"});
		$j(".grippie").show();
		$j("#post-box-bottom").show();
	});
	
	$j("#post-box-news").on("click", function() {
		$j(this).css({"min-height" : "60px"});
		$j(this).find(".grippie").show();
		$j("#post-box-news-bottom").show();
	});
	
	var systemActive = true;
	var timeout = 18000;

	// function to execute upon timeout
	function waitForUserAction() {
		console.log("Waiting for user to take action");
		systemActive = false;
	}

	//function to reset the timeout
	var reset = function() {
		window.clearTimeout(timeoutHandle);
		timeoutHandle = window.setTimeout(waitForUserAction, timeout);
		console.log("resetting timer");
		systemActive = true;
	};
		
	// timeout timer
	var timeoutHandle = window.setTimeout(waitForUserAction, timeout);
	
	var currTime = $j.now();
	var newTime = currTime + 5000;
	
	//bind browser events
	$j(window).scroll(function() {
		currTime = $j.now();
		
		if(currTime >= newTime) {
			newTime = currTime + 5000;
			reset();
		}

	});
	
	$j(document).click(function() {
		currTime = $j.now();
		
		if(currTime >= newTime) {
			newTime = currTime + 5000;
			reset();
		}
	});
	
	function checkWaitQueue() {

		if(systemActive == true) {
			
			$j.ajax({
				type: "POST",
				dataType: "json",
				data: '',
				url: "/main/getViewQueue",
				async: true, /* If set to non-async, browser shows page as "Loading.."*/
				cache: false,
				timeout: 50000, /* Timeout in ms */

				success: function(html) {
					if(html.status != 0) {
						console.log(html.url);
						TINY.box.show({url: html.url,width:700,height:385,opacity:30});
						console.log('Got a view!!');
					} else {
						console.log('Got no views back.');
					}
					
					setTimeout(
						checkWaitQueue, /* Request next message */
						8000
					);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					
					console.log(textStatus + " (" + errorThrown + ")");
					setTimeout(
						checkWaitQueue, /* Try again after.. */
						15000); /* milliseconds (15seconds) */
				}
			});

		} else {
			setTimeout(
				checkWaitQueue,
				5000
			);
		}
    };
	
	waitForNewPost();
	// left off here
	function waitForNewPost() {
        
		if(systemActive == true) {
		
			if($j('.all-posts').length > 0) {
				var obj = $j('.all-posts').children('.post-container:first');
				
				if(obj != '' && obj.length > 0) {
					var id_name = obj.attr('id');
					if(id_name != '') {
						var regex = /(\d+)/g;
						var id = id_name.match(regex);
						var dataString = 'postid='+ id;
						
						if(ownerPage != '') {
							dataString = dataString + '&user='+ownerPage;
						}
						
						//console.log("The current owner: "+ ownerPage);
						
						$j.ajax({
							type: "POST",
							dataType: "json",
							data: dataString,
							url: "/main/getLiveFeed",
							async: true, /* If set to non-async, browser shows page as "Loading.."*/
							cache: false,
							timeout: 50000, /* Timeout in ms */

							success: function(html) {
								if(html.content != '') {
									startLoader('.new-posts-loader');
									
									$j('.all-posts').prepend(html.content);
									//console.log('asked for new post and got something back!!!!');
									stopLoader('.new-posts-loader');
								} else {
									//console.log('asked for new post and got nothing back');
								}
								
								setTimeout(
									waitForNewPost, /* Request next message */
									8000
								);
							},
							error: function(XMLHttpRequest, textStatus, errorThrown){
								
								//console.log(textStatus + " (" + errorThrown + ")");
								setTimeout(
									waitForNewPost, /* Try again after.. */
									15000); /* milliseconds (15seconds) */
							}
						});
					}
				}
			}
		} else {
			setTimeout(
				waitForNewPost,
				5000
			);
		}
    };
	
	waitForNewNotification();
	
	function waitForNewNotification() {
        
		if(systemActive == true) {
			if($j('.all-posts').length > 0) {
				var date = $j('.last-notification-date').val();
				
				var dataString = 'lastdate='+ date;
				//console.log(date);
				
				$j.ajax({
					type: "POST",
					dataType: "json",
					data: dataString,
					url: "/main/getLiveNotifications",
					async: true, /* If set to non-async, browser shows page as "Loading.."*/
					cache: false,
					timeout: 50000, /* Timeout in ms */

					success: function(html) { /* called when request to barge.php completes */
						if(html.content != '') {
							//console.log(html.content);
							
							if($j('#no-notifications-text').length > 0) {
								$j('#no-notifications-text').remove();
							}
							$j('.last-notification-date').val(html.lastdate);
							$j('#notification-posts-top').prepend(html.content);
							
							var innerText = $j('#notification-count').text();
							var innerCount = '';
							
							if(innerText != '') {
								innerCount = parseInt(innerText);
							}
							
							innerCount = innerCount + 1;
							if(innerCount != '') {
								$j('#notification-count').text(innerCount);
							}
							//console.log('asked for new notification and something back!!!!');
						} else {
							//console.log('asked for new notification and got nothing back');
						}
						
						setTimeout(
							waitForNewNotification, /* Request next message */
							5000
						);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown){
						
						//console.log(textStatus + " (" + errorThrown + ")");
						setTimeout(
							waitForNewNotification, /* Try again after.. */
							15000); /* milliseconds (15seconds) */
					}
				});
			}
		} else {
			setTimeout(
				waitForNewNotification,
				5000
			);
		}
    };

	$j(document).on("click", ".view-all-comments", function(event) {
		
		// stop form from submitting normally
		event.preventDefault();
		
		startLoader('.view-all-comments-box');
		
		var self = $j(this);
		var pid = self.closest('.post-container').attr('id').split("-")[2];
		
		var dataString = "post_id="+pid;
		
		// Send the data using post and put the results in a div
		$j.ajax({
			type: "POST",
			dataType: "json",
			data: dataString,
			url: "main/remainingComments",
			async: true,
			cache: false,
			timeout: 50000,
			
			success: function(resp) {
				console.log('success');
				
				if(resp.remaining_comments !== '') {
					var commentContainer = self.closest('.top-comment-container').children('.comment-container:first');
					self.parent().closest('.view-all-comments-box').remove();
					commentContainer.prepend(resp.remaining_comments);
					stopLoader('.view-all-comments-box');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				console.log(textStatus + " (" + errorThrown + ")");
			}
		});
		/*
		$j.post(site_url + "main/remainingComments", { post_id: pid},
			function(data) {
				if(data.remaining_comments !== '') {
					var commentContainer = self.closest('.top-comment-container').children('.comment-container:first');
					self.parent().closest('.view-all-comments-box').remove();
					commentContainer.prepend(data.remaining_comments);
					stopLoader('.view-all-comments-box');
				}
				return false;
			}, "json"
		);*/
	});
	
	$j("#register-form").submit(function(event) {
		
		// stop form from submitting normally
		event.preventDefault(); 
		var form = $j(this), currUrl = form.attr('action');
		
		var dataString = $j(this).serialize();
		$j.ajax
		({
			type: "POST",
			url: currUrl,
			data: dataString,
			dataType: "json",
			cache: false,
			success: function(resp) {
				if(resp.status == 1) {
					console.log('got back');
					window.location.href = site_url;
				} else {
					$j("#error-msg").empty().append(resp.msg);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
		
		return false;
	});
	
	function redirect() {
		window.location.href = site_url;
	}
	
	$j(document).on("click", ".pagination-ajax2 a", function(event) {

		// stop form from submitting normally
		event.preventDefault(); 
		var href = $j(this).attr('href');
		var pattern = /[0-9]+/g;
		var pid = href.match(pattern);
		
		//console.log(pid[0]);
		var dataString = 'start='+ pid[0];
		$j.ajax
		({
			type: "POST",
			url: site_url + 'main/pagination_users',
			data: dataString,
			dataType: "json",
			cache: false,
			success: function(data)
			{
				$j("#all-users").empty().append(data.content);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return false;
	});
	
	$j("#forgot-form").submit(function(event) {

		// stop form from submitting normally
		event.preventDefault(); 
		
		var form = $j(this);
		var path = form.attr('action');
		var values = form.serialize();
		
		//console.log(values);
		$j.ajax
		({
			type: "POST",
			url: path,
			data: values,
			dataType: "json",
			cache: false,
			success: function(data)
			{
				if(data.status == 1) {
					$j("#before-reset").empty();
					$j("#forgot-success").append(data.msg);
				} else {
					$j("#forgot-failure").empty().append(data.msg);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return false;
	});
	
	$j("iframe").each(function(){
		var ifr_source = $j(this).attr('src');
		var wmode = "wmode=transparent";
		if(ifr_source.indexOf('?') != -1) {
			var getQString = ifr_source.split('?');
			var oldString = getQString[1];
			var newString = getQString[0];
			$j(this).attr('src',newString+'?'+wmode+'&'+oldString);
		}
		else $j(this).attr('src',ifr_source+'?'+wmode);
	});
	
	$j(document).on("change", '.visibility-setting', function(e) {
		
		e.preventDefault();
		var id = this.name.split("_")[2];
		var visibility = $j(this).val();
		var dataString = 'id='+ id + '&visibility='+visibility;
		//console.log(id);
		//console.log(visibility);
		
		$j.ajax
		({
			type: "POST",
			url: site_url + "gallery/changePhotoVisibility",
			data: dataString,
			cache: false,
			dataType: "json",
			success: function(html)
			{
				//console.log(html.status);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
		return false;
	});
	
	$j(document).on("click", '.comment-link', function() {
		var greatGrandparent = $j(this).parent().parent().parent();
		var ele = greatGrandparent.find('.commentbox-container:first');
		
		//console.log(ele);
		
		ele.show();
			var child = ele.find('.commentbox:first');
			
			child.focus();
	
		//ele.show().focus();
		ele.find('.before-comment-img').show();
	});
	
	function startLoader(parent){
		
		$j(parent).children('.f1-upload-process').css({"display": "block"});
		return true;
	}
	
	function stopLoader(parent){
	
		$j(parent).children('.f1-upload-process').css({"display": "none"});
		return true;   
	}
	
	$j('input[type=file][name=profile_img]').on("change", function(e) {
		
		e.preventDefault();
		
		$j('#profile_img_form_container').addClass("grayed-out");
		startLoader('#profile_img_form_container');
		
		$j('#profile_upload_form').ajaxSubmit({ success: profileImgResponse });
        
	});
	
	$j('input[type=file][name=gallery_img]').on("change", function(e) {
		
		e.preventDefault();
		startLoader();
		
		var newTarget = "new-image"+times_image_uploaded;
		$j('#gallery').prepend('<div id="'+newTarget+'"></div>');

		$j('#gallery_upload_form').ajaxSubmit({ success: galleryPhotoResponse});
        
	});
	// post-submit callback 
	function galleryPhotoResponse(responseText, statusText, xhr, $form)  {
		
		$j('#gallery').prepend(responseText);
		//var inner_photo_content = '<div class="edit-item-container"><div class="edit-photo-icon-container"><div class="edit-item-icon"></div></div><div class="edit-photo-inner"><a class="delete_photo" id="photo_'+ photo_id +'" ><img src="' + site_url +'assets/images/delete_button.png" alt="delete button" />Destroy!</a><br /><div class="clear"></div><form name="permissions-form-'+ photo_id +'" id="permissions-form-'+photo_id +'" action="#" method="post" enctype="multipart/form-data"><input type="radio" name="view_permissions_'+ photo_id +'" value="everyone" checked="checked" class="visibility-setting" />Public <br /></form></div></div>';
		$j("a[rel]").colorbox({ transition:"none", width:"90%", height:"90%", maxWidth:"90%", maxHeight:"90%;"});
		/*alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
			'\n\nThe output div should have already been updated with the responseText.');*/
		/*$j("[rel^='lightbox']").slimbox({  }, null, function(el) { 
				return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel)); });*/
		stopLoader();
	}
	
	function profileImgResponse(responseText, statusText, xhr, $form) {
		
		var arr = responseText.split(/ +/);

		$j('#profile-img-link').attr({
			href: arr[0],
			target: arr[1]
		});
		$j('#profile-img').attr({src: arr[2]});
		$j("a[rel]").colorbox({ transition:"none", width:"90%", height:"90%", maxWidth:"90%", maxHeight:"90%;"});
		
		//stopLoader('#profile_img_form_container');
		//j('#profile_img_form_container').removeClass("grayed-out");
	}
	
	function postBoxResponse(responseText, statusText, xhr, $form) {
		$j('.all-posts').prepend(responseText);
	}
	
	$j('.score-tab').on('click', 'a', function(e) {
		e.preventDefault();
		var href = $j(this).attr('href');
		var part1 = href.substring(href.lastIndexOf('/') + 1);
		var parts = part1.split('?');
		$j.ajax
		({
			type: "POST",
			url: href,
			dataType: "json",
			cache: false,
			success: function(html) {
				
				var curr_page = parts[0];
				
				if(parts[0] == "scores") {
						curr_page = "Scoreboard";
				} else if(parts[0] == "awards") {
					curr_page = "Achievements";
				} else if(parts[0] == "topJobs") {
					curr_page = "Jobs";
				} else if(parts[0] == "stats") {
					curr_page = "Stats";
				} else if(parts[0] == "dreams") {
					curr_page = "Dreams";
				} else if(parts[0] == "groupActivities") {
					curr_page = "Group Activities";
				}
				$j("h1.scoreboard-title").empty().append(curr_page);
				$j(".top-scoreboard").empty().append(html.content);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus, errorThrown);
			}
		});
	});
	
	$j('.no-enter').keypress(function(event) {

		if (event.keyCode == 10 || event.keyCode == 13) 
			event.preventDefault();
	});
	
	$j('.find-me').on('click', 'a', function(e) {
		e.preventDefault();
		$j('.my-ranking').scrollTop();
	});
	
	
	
	function notify(data) {
		var post_id = data.post_id;
		var comment_content = data.comment_content;
		console.log(post_id + " " + comment_content);
		var dataString = 'post_id='+ post_id + '&type=comment&content=' +comment_content;
		$j.ajax
		({
			type: "GET",
			url: site_url + "main/notificationEmail",
			data: dataString,
			dataType: "json",
			cache: false,
			success: function(con)
			{
				//console.log("Email: "+con.resp);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
	}
	
	$j('.boxclose').click(function(){
        var $parent = $j(this).parent('.user-msg-box');
		hideOverlay($parent);
    });
	
	function showOverlay(id) {
		$j('#overlay').fadeIn('fast',function(){
            $j(id).show();
        });
	}
	
	function hideOverlay(id) {
		$j('#overlay').fadeOut('fast',function(){
            $j(id).hide();
        });
	}
	
	$j.fn.doesExist = function() {
		return $j(this).length > 0;
	};
 
	$j(document).on("submit", ".post-form", function(event) {
	
		var parent_id = $j(this).parent().attr('id');
		console.log(parent_id);
		
		// stop form from submitting normally
		event.preventDefault(); 
		
		startLoader('#' + parent_id);
		
		// get some values from elements on the page: 
		var form = $j(this), url = form.attr('action');
		var dataSent = $j(this).serializeArray();
		
		startLoader('#' + parent_id);
		
		if($j('#upload-feed-photo').val()) {
			
			$j(this).ajaxSubmit({ success: postBoxResponse });
			stopLoader('.post-form');
        } 
		else {
		
			// Send the data using post and put the results in a div
			$j.post(url, dataSent, function(data) {
					
				if(data != '') {

					var is_news = data.is_news;
					var newPost = data.post_content;
					
					if(is_news == 0) {
						$j('#post-box').val("");
						
						if($j('#no-posts-container').length > 0) {
							$j('#no-posts-container').remove();
						}
						$j('.all-posts').prepend(newPost);
						stopLoader('#' + parent_id);
					}
					else if(is_news == 1) {
						$j('#post-box-news').val("");
						$j('.all-posts-news').prepend(newPost);
						stopLoader('#' + parent_id);
					}
				} else {
					stopLoader('#' + parent_id);
				}
			}, "json");
		}
	});
	
	/* @todo pig element remove, remove in the future or re-implement
	var capslock = false;
	
	$j(window).keydown(function(e) {
		var pigImg = $j("#pig");
		
		var code = (e.keyCode ? e.keyCode : e.which);
		
		var position = pigImg.position();
		
		if(code == 20) {
			capslock = true;
		}
		
		if(capslock == true) {
			if(code == 65) {
				pigImg.css({"left": position.left + 10});
			}
			else if(code == 68) {
				pigImg.css({"left": position.left - 10});
			}
			else if(code == 87) {
				pigImg.css({"top": position.top - 10});
			}
			else if(code == 83) {
				pigImg.css({"top": position.top + 10});
			}
		}
	});
	*/
	$j(document).on("keypress", 'textarea.commentbox', function(e) {
		
		var _this = $j(this);
		
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) 
		{
			var find_id = $j(this).attr('id');
			var pattern = /[0-9]+/g;
			var table_id = find_id.match(pattern);
		
			var comment = $j(this).val();

			var dataString = 'table_id='+ table_id + '&comment_content='+ comment;
		
			$j.ajax
			({
				type: "POST",
				url: site_url + "main/addComment",
				data: dataString,
				dataType: "json",
				cache: false,
				success: function(resp)
				{
					var comment_id = resp.comment_id;
					var post_id = resp.post_id;
					var author = resp.author;
					var date = resp.date;
					var comment_content = resp.comment_content;
					var profile_img_src = resp.profile_img_src;
					var profile_path = resp.profile_path;
					
					if(profile_img_src == '') {
						profile_img_src = site_url + "assets/images/profile/steve_avatar_small.png";
					}
					var newComment = '<div id="comment_'+ comment_id + '" class="post_' + post_id +' single_comment_entity"><div style="position: relative; float: left; width: 100%;">'
						+	'<div class="grid_5"><a href="' + profile_path + '"><img src="' + profile_img_src + '" class="comment_profile_img" /></a></div><div class="grid_42"><a href="' + profile_path + '"><b>'+ author +'</b></a>&nbsp;<span style="word-wrap: break-word;">'+ comment_content + '</span><br /><span style="color: #666;">'+ date +'</span>&nbsp;'+ '<!--<a class="like" id="like_'+ comment_id + '">Like</a>--></div><div class="post-options"><a class="delete" id="comments_' + comment_id + '"><img src="' + site_url + 'assets/images/delete_button.png" alt="delete button" /></a></div></div><div class="clear"></div>'	
						+	'</div>';
					var ele = _this.parents('.top-comment-container').children('.comment-container');
					ele.append(newComment);
					_this.val('');
					notify(resp);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log(textStatus, errorThrown);
				}
			});
		}
	});
});

//Adjust height of overlay to fill screen when browser gets resized
$j(window).bind("resize", function(){
	$j("#forgot-box").css("height", $j(window).height());
	//$j("#bg").css("height", $j(window).height());
	//$j("#bg").css("width", $j(window).width());
});

//Adjust height of overlay to fill screen when browser gets resized
$j(window).bind("resize", function(){
	$j("#fullsize-map").css("height", $j(window).height());
});
