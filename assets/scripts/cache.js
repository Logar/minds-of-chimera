/**
 * CacheJS - implements a key/val store with expiry.
 * Swappable storage modules (array, cookie, localstorage)
 * Homepage: http://code.google.com/p/cachejs
 */
var cache = function() {

    /* public */
    var my = {
        /**
         * Sets the storage object to use.
         * On invalid store being passed, current store is not affected.
         * @param new_store store.
         * @return boolean true if new_store implements the required methods and was set to this cache's store. else false
         */
        setStore: function(new_store) {
            if(typeof new_store=="function") {
                new_store = new_store();
                if(new_store.get && new_store.set && new_store.kill && new_store.has) {
                    store = new_store;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        },
        /**
         * Returns true if cache contains the key, else false
         * @param key string the key to search for
         * @return boolean
         */
        has: function(key) {
            return store.has(key);
        },
        /**
         * Removes a key from the cache
         * @param key string the key to remove for
         * @return boolean
         */
        kill: function(key) {
            store.kill(key);
            return store.has(key);
        },
        /**
         * Gets the expiry date for given key
         * @param key string. The key to get
         * @return mixed, value for key or NULL if no such key
         */
        getExpiry: function(key) {
            var exp = get(key,EXPIRES);
            if(exp!=false && exp!=null) {
                exp = new Date(exp);
            }
            return exp;
        },
        /**
         * Sets the expiry date for given key
         * @param key string. The key to set
         * @param expiry; RFC1123 date or false for no expiry
         * @return mixed, value for key or NULL if no such key
         */
        setExpiry: function(key, expiry) {
            if(store.has(key)) {
                storedVal = store.get(key);
                storedVal[EXPIRES] = makeValidExpiry(expiry);
                store.set(key,storedVal);
                return my.getExpiry(key);
            } else {
                return NOSUCHKEY;
            }
        },
        /**
         * Gets a value from the cache
         * @param key string. The key to fetch
         * @return mixed or NULL if no such key
         */
        get: function(key) {
            return get(key,VALUE);
        },
        /**
         * Sets a value in the cache, returns true on sucess, false on failure.
         * @param key string. the name of this cache object
         * @param val mixed. the value to return when querying against this key value
         * @param expiry RFC1123 date, optional. If not set and is a new key, or set to false, this key never expires
         *                       If not set and is pre-existing key, no change is made to expiry date
         *                       If set to date, key expires on that date.
         */
        set: function(key, val, expiry) {

            if(!store.has(key)) {
                // key did not exist; create it
                storedVal = Array();
                storedVal[EXPIRES] = makeValidExpiry(expiry);
                store.set(key,storedVal);
            } else {
                // key did already exist
                storedVal = store.get(key);
                if(typeof expiry!="undefined") {
                    // If we've been given an expiry, set it
                    storedVal[EXPIRES] = makeValidExpiry(expiry);
                } // else do not change the existent expiry
            }

            // always set the value
            storedVal[VALUE] = val;
            store.set(key,storedVal);

            return my.get(key);
        }
    };
    /* /public */

    /* private */
    var store = arrayStore();
    var NOSUCHKEY = null;
    var VALUE = 0;
    var EXPIRES = 1;

    function get(key,part) {
        if(store.has(key)) {
            // this key exists:

            // get the value
            storedVal = store.get(key);

            var now = new Date();
            if(storedVal[EXPIRES] && Date.parse(storedVal[EXPIRES])<=now) {
                // key has expired
                // remove from memory
                store.kill(key);
                // return NOSUCHKEY
                return NOSUCHKEY;
            } else if(typeof storedVal[part]!="undefined") {
                // not expired or never expires, and part exists in store[key]
                return storedVal[part];
            } else {
                // part is not a member of store[key]
                return NOSUCHKEY;
            }
        } else {
            // no such key
            return NOSUCHKEY;
        }
    }

    function makeValidExpiry(expiry) {
        if(!expiry) {
            // no expiry given; change from "undefined" to false - this value does not expire.
            expiry = false;
        } else {
            // force to date type
            expiry = new Date(expiry);
        }

        return expiry;
    }

    /* /private */

    return my;
};

/*
    http://www.JSON.org/json2.js
    2008-07-15

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html

    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the object holding the key.

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array, then it will be used to
            select the members to be serialized. It filters the results such
            that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.

    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.
*/

/*jslint evil: true */

/*global JSON */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", call,
    charCodeAt, getUTCDate, getUTCFullYear, getUTCHours, getUTCMinutes,
    getUTCMonth, getUTCSeconds, hasOwnProperty, join, lastIndex, length,
    parse, propertyIsEnumerable, prototype, push, replace, slice, stringify,
    test, toJSON, toString
*/

if (!this.JSON) {

// Create a JSON object only if one does not already exist. We create the
// object in a closure to avoid creating global variables.

    JSON = function () {

        function f(n) {
            // Format integers to have at least two digits.
            return n < 10 ? '0' + n : n;
        }

        Date.prototype.toJSON = function (key) {

            return this.getUTCFullYear()   + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate())      + 'T' +
                 f(this.getUTCHours())     + ':' +
                 f(this.getUTCMinutes())   + ':' +
                 f(this.getUTCSeconds())   + 'Z';
        };

        String.prototype.toJSON =
        Number.prototype.toJSON =
        Boolean.prototype.toJSON = function (key) {
            return this.valueOf();
        };

        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            escapeable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap,
            indent,
            meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            },
            rep;


        function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

            escapeable.lastIndex = 0;
            return escapeable.test(string) ?
                '"' + string.replace(escapeable, function (a) {
                    var c = meta[a];
                    if (typeof c === 'string') {
                        return c;
                    }
                    return '\\u' + ('0000' +
                            (+(a.charCodeAt(0))).toString(16)).slice(-4);
                }) + '"' :
                '"' + string + '"';
        }


        function str(key, holder) {

// Produce a string from holder[key].

            var i,          // The loop counter.
                k,          // The member key.
                v,          // The member value.
                length,
                mind = gap,
                partial,
                value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

            if (value && typeof value === 'object' &&
                    typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

            if (typeof rep === 'function') {
                value = rep.call(holder, key, value);
            }

// What happens next depends on the value's type.

            switch (typeof value) {
            case 'string':
                return quote(value);

            case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value) ? String(value) : 'null';

            case 'boolean':
            case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

                return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

            case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

                if (!value) {
                    return 'null';
                }

// Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

// If the object has a dontEnum length property, we'll treat it as an array.

                if (typeof value.length === 'number' &&
                        !(value.propertyIsEnumerable('length'))) {

// The object is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || 'null';
                    }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                    v = partial.length === 0 ? '[]' :
                        gap ? '[\n' + gap +
                                partial.join(',\n' + gap) + '\n' +
                                    mind + ']' :
                              '[' + partial.join(',') + ']';
                    gap = mind;
                    return v;
                }

// If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === 'object') {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        k = rep[i];
                        if (typeof k === 'string') {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                } else {

// Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

                v = partial.length === 0 ? '{}' :
                    gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                            mind + '}' : '{' + partial.join(',') + '}';
                gap = mind;
                return v;
            }
        }

// Return the JSON object containing the stringify and parse methods.

        return {
            stringify: function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

                var i;
                gap = '';
                indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

                if (typeof space === 'number') {
                    for (i = 0; i < space; i += 1) {
                        indent += ' ';
                    }

// If the space parameter is a string, it will be used as the indent string.

                } else if (typeof space === 'string') {
                    indent = space;
                }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

                rep = replacer;
                if (replacer && typeof replacer !== 'function' &&
                        (typeof replacer !== 'object' ||
                         typeof replacer.length !== 'number')) {
                    throw new Error('JSON.stringify');
                }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

                return str('', {'': value});
            },


            parse: function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

                var j;

                function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                    var k, v, value = holder[key];
                    if (value && typeof value === 'object') {
                        for (k in value) {
                            if (Object.hasOwnProperty.call(value, k)) {
                                v = walk(value, k);
                                if (v !== undefined) {
                                    value[k] = v;
                                } else {
                                    delete value[k];
                                }
                            }
                        }
                    }
                    return reviver.call(holder, key, value);
                }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

                cx.lastIndex = 0;
                if (cx.test(text)) {
                    text = text.replace(cx, function (a) {
                        return '\\u' + ('0000' +
                                (+(a.charCodeAt(0))).toString(16)).slice(-4);
                    });
                }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

                if (/^[\],:{}\s]*$/.
test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                    j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                    return typeof reviver === 'function' ?
                        walk({'': j}, '') : j;
                }

// If the text is not JSON parseable, then a SyntaxError is thrown.

                throw new SyntaxError('JSON.parse');
            }
        };
    }();
}

/**
 * wraps JSON so we can use a JSON encoder which uses toString and fromString or parse and stringify
 */
var JSONWrapper = function() {
    var my = {
        /**
         * passes control to the JSON object; defaults to JSON.stringify
         */
        toString: function() {
            return JSON.stringify(arguments);
        },
        /**
         * passes control to the JSON object; defaults to JSON.parse
         */
        fromString: function() {
            return JSON.parse(arguments);
        },
        /**
         * sets toString handler
         * @param func reference to toString function, eg this.set_toString(JSON.stringify);
         */
        set_toString: function(func) {
            my.toString = function() {
                // send all arguments to the desired function as an array
                var args = Array.prototype.slice.call(arguments);
                return func(args);
            }
        },
        /**
         * sets fromString handler
         * @param func reference to fromString function, eg this.set_toString(JSON.parse);
         */
        set_fromString: function(func) {
            my.fromString = function() {
                // send all arguments to the desired function as an array
                var args = Array.prototype.slice.call(arguments);
                return func(args);
            }
        }
    };

    return my;
};

/**
 * arrayStore - the default Cache storage
 */
var arrayStore = function (){
    var myStore = Array();

    var my = {
        has: function(key) {
            return (typeof myStore[key]!="undefined");
        },
        get: function(key) {
            return myStore[key];
        },
        set: function(key,val) {
            myStore[key] = val;
        },
        kill: function(key) {
            delete myStore[key];
        }
    };

    return my;
};

/**
 * localStorageStore.
 */
var localStorageStore = function() {
    var prefix = "CacheJS_LS"; // change this if you're developing and want to kill everything ;0)

    var my = {
        has: function(key) {
            return (localStorage[prefix+key]!=null);
        },
        get: function(key) {
            if(!my.has(key)) {
                return undefined;
            } else {
                return JSON.parse(localStorage[prefix+key]);
            }
        },
        set: function(key,val) {
            if(val===undefined) {
                my.kill(key);
            } else {
                localStorage[prefix+key] = JSON.stringify(val);
            }
        },
        kill: function(key) {
            //delete localStorage[prefix+key]; // not supported in IE8
            localStorage.removeItem(prefix+key);
        }
    };

    if(window.localStorage) {
        return my;
    } else {
        // localStorage not supported on this browser; degrade to arrayStore.
        return arrayStore();
    }
};

/**
 * Cookie Monster Want Cookies.
 * I don't recommend the use of this store really; cookies have limited length, and you can only have a limited number of cookies per domain
 * It's really only included to show how flexible the pluggable storage system is.
 */
var cookieStore = function() {
    // uses cookie functions from http://www.quirksmode.org/js/cookies.html
    var prefix = "CacheJS_CS";

    var my = {
        has: function(key) {
            return (my.get(key)!==undefined);
        },
        get: function(key) {
            var nameEQ = prefix + "=";
            var ca = document.cookie.split(';');
            for(var i=0 ; i < ca.length ; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') {
                    c = c.substring(1,c.length);
                }
                if (c.indexOf(nameEQ) == 0) {
                    // found our cookie; split it out for the specified key
                    cookieContents = JSON.parse(c.substring(nameEQ.length,c.length));
                    if(key) {
                        return cookieContents[key];
                    } else {
                        return cookieContents;
                    }
                }
            }
            return undefined;
        },
        set: function(key, val) {
            cookieContents = my.get();
            if(cookieContents==null) {
                cookieContents = Object();
            }
            cookieContents[key] = val;
            document.cookie = prefix+"="+JSON.stringify(cookieContents)+"; path=/";
        },
        kill: function(key) {
            my.set(key,undefined);
        }
    };

    return my;
};
