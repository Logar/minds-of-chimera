function checkWaitQueue() {

		if(systemActive == true) {
			
			$j.ajax({
				type: "POST",
				dataType: "json",
				data: '',
				url: "/main/getViewQueue",
				async: true, /* If set to non-async, browser shows page as "Loading.."*/
				cache: false,
				timeout: 50000, /* Timeout in ms */

				success: function(html) {
					if(html.status != 0) {
						console.log(html.url);
						TINY.box.show({url: html.url,width:700,height:385,opacity:30});
						console.log('Got a view!!');
					} else {
						console.log('Got no views back.');
					}
					
					setTimeout(
						checkWaitQueue, /* Request next message */
						8000
					);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					
					console.log(textStatus + " (" + errorThrown + ")");
					setTimeout(
						checkWaitQueue, /* Try again after.. */
						15000); /* milliseconds (15seconds) */
				}
			});

		} else {
			setTimeout(
				checkWaitQueue,
				5000
			);
		}
    };
	
	waitForNewPost();
	// left off here
	function waitForNewPost() {
        
		if(systemActive == true) {
		
			if($j('.all-posts').length > 0) {
				var obj = $j('.all-posts').children('.post-container:first');
				
				if(obj != '' && obj.length > 0) {
					var id_name = obj.attr('id');
					if(id_name != '') {
						var regex = /(\d+)/g;
						var id = id_name.match(regex);
						var dataString = 'postid='+ id;
						
						if(ownerPage != '') {
							dataString = dataString + '&user='+ownerPage;
						}
						
						//console.log("The current owner: "+ ownerPage);
						
						$j.ajax({
							type: "POST",
							dataType: "json",
							data: dataString,
							url: "/main/getLiveFeed",
							async: true, /* If set to non-async, browser shows page as "Loading.."*/
							cache: false,
							timeout: 50000, /* Timeout in ms */

							success: function(html) {
								if(html.content != '') {
									startLoader('.new-posts-loader');
									
									$j('.all-posts').prepend(html.content);
									//console.log('asked for new post and got something back!!!!');
									stopLoader('.new-posts-loader');
								} else {
									//console.log('asked for new post and got nothing back');
								}
								
								setTimeout(
									waitForNewPost, /* Request next message */
									8000
								);
							},
							error: function(XMLHttpRequest, textStatus, errorThrown){
								
								//console.log(textStatus + " (" + errorThrown + ")");
								setTimeout(
									waitForNewPost, /* Try again after.. */
									15000); /* milliseconds (15seconds) */
							}
						});
					}
				}
			}
		} else {
			setTimeout(
				waitForNewPost,
				5000
			);
		}
    };
	
	waitForNewNotification();
	
	function waitForNewNotification() {
        
		if(systemActive == true) {
			if($j('.all-posts').length > 0) {
				var date = $j('.last-notification-date').val();
				
				var dataString = 'lastdate='+ date;
				//console.log(date);
				
				$j.ajax({
					type: "POST",
					dataType: "json",
					data: dataString,
					url: "/main/getLiveNotifications",
					async: true, /* If set to non-async, browser shows page as "Loading.."*/
					cache: false,
					timeout: 50000, /* Timeout in ms */

					success: function(html) { /* called when request to barge.php completes */
						if(html.content != '') {
							//console.log(html.content);
							
							if($j('#no-notifications-text').length > 0) {
								$j('#no-notifications-text').remove();
							}
							$j('.last-notification-date').val(html.lastdate);
							$j('#notification-posts-top').prepend(html.content);
							
							var innerText = $j('#notification-count').text();
							var innerCount = '';
							
							if(innerText != '') {
								innerCount = parseInt(innerText);
							}
							
							innerCount = innerCount + 1;
							if(innerCount != '') {
								$j('#notification-count').text(innerCount);
							}
							//console.log('asked for new notification and something back!!!!');
						} else {
							//console.log('asked for new notification and got nothing back');
						}
						
						setTimeout(
							waitForNewNotification, /* Request next message */
							5000
						);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown){
						
						//console.log(textStatus + " (" + errorThrown + ")");
						setTimeout(
							waitForNewNotification, /* Try again after.. */
							15000); /* milliseconds (15seconds) */
					}
				});
			}
		} else {
			setTimeout(
				waitForNewNotification,
				5000
			);
		}
    };