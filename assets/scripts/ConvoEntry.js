$j(function() {
	
	/****************************************************************************/
	/* ConvoEntry Class */
	/****************************************************************************/
	window.ConvoEntry = function () {
		
		var changesLog = new Array();
		var prevEntryLinks = new Array();
		
		this.changesMade = false;
		this.userMode = "create";
		this.currentArrow = "";
		
		if($j('.convo-entry-container').length > 0) {
			this.groupNum = $j('.convo-entry-container').length;
		} else {
			this.groupNum = 0;
		}
		
		
		this.setChangesMade = function() {
			this.changesMade = true;
		};
		
		this.addChanges = function(ele) {
			changesLog.push(ele);
		};
		
		this.removeChange = function(ele) {
			changesLog.remove(ele);
		};
		
		this.getChangesLog = function() {
			return this.changesLog;
		};
		
		this.getGroupNum = function() {
			
			return this.groupNum;
		};
		
		this.updateGroupNum = function(option) {
			if(option == "add") {
				this.groupNum = this.groupNum + 1;
			} else if(option == "remove") {
				this.groupNum = this.groupNum - 1;
			}
		};
		
		function remove(ele) {
			var what, a = arguments, L = a.length, ax;
			while (L && this.length) {
				what = a[--L];
					while ((ax = this.indexOf(what)) !== -1) {
						this.splice(ax, 1);
					}
			}
			return this;
		}
		
	}
	
	ConvoEntry.prototype.getChangesMade = function() {
		return this.changesMade;
	};
	
	ConvoEntry.prototype.getCurrentLink = function() {
		return this.currentArrow;
	};
	
	ConvoEntry.prototype.setCurrentLink = function(arrowObj) {
		this.currentArrow = arrowObj;
	};
	
	ConvoEntry.prototype.unsetCurrentLink = function() {
		this.currentArrow = "";
	};
	
	ConvoEntry.prototype.getConnector = function() {
		var connector = '<a class="optionlinked">'+
			'<div class="unjoinButton" title="Unlink these two paragraphs">Unlink</div></a>';
		return connector;
	};
	

	ConvoEntry.prototype.getEntrySelect = function(topParentId, responsesCount) {
		var selectBlock = "";
		
		selectBlock += 'Goes to entry <select name="group_'+topParentId+'[gotos]['+responsesCount+']" style="width: 140px;" class="select-existing">';
		
		for(var i=1; i <= this.groupNum; i++) {
			
			if(i == 1) {
				selectBlock += '<option value="none" selected="selected">End Conversation</option>';
			}
			
			if(i == topParentId) {
				selectBlock += '<option value="'+i+'" disabled="disabled">'+i+'</option>';
			} else {
				selectBlock += '<option value="'+i+'">'+i+'</option>';
			}
		}
		selectBlock += '</select>';
		return selectBlock;
	};
	
	ConvoEntry.prototype.getPrevEntries = function() {
		
		return this.prevEntryLinks;
	};
	
	ConvoEntry.prototype.setPrevEntries = function(prevEntryContainers) {
		
		var hiddenEntryIds = new Array();
		
		$j.each(prevEntryContainers, function(index, value) {
			hiddenEntryIds.push($j(value).attr('id').split('-')[3]);
		});
		
		hiddenEntryIds.sort();
		
		$j.each(hiddenEntryIds, function(index, value) {
			this.prevEntryLinks += '<div class="float-left" style="margin-left: 10px;"><a class="convo-prev-entry">Entry #'+value+'</a></div>';
		});
	};
	
	ConvoEntry.prototype.getCurrentEntryResponsesCount = function ($topParent) {
		
		if($topParent.find('.response').length > 0) {
			return responsesCount = $topParent.find('.response').length;
		}
		return 0;
	};
	
	ConvoEntry.prototype.createEntry = function(that, entryId, choice) {
		
		var self = this;
		var convoId = $j('#convo_id').attr('value');
		var dataString = 'convoId='+ convoId +"&entryId="+ entryId+"&groupNum="+this.groupNum+"&userMode="+choice;
		
		$j.ajax
		({
			type: "POST",
			url: site_url + 'convo/getConvoEntry',
			data: dataString,
			dataType: "json",
			cache: false,
			success: function(resp) 
			{
				if(choice == "change") {
					$j('#shown-entry-text').text(resp.orderNum);
					$j('#shown-entry').val(resp.entryId);
				} else {
					var currNum = $j('#convo-show-map').children('option').length + 1;
					var ele = $j('#convo-show-map').append('<option value="'+ currNum +'" selected="selected" class="convo-option-new"></option>');
					$j('#shown-entry-text').text(currNum);
					$j('#shown-entry').val('');
				}
				
				$j('#all-entries-container').append(resp.content);
				$j('#all-entries-container').children('.convo-entry-container:last-child').show();
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
		
		//this.scrollToEntry($j('.convo-entry-container:last-child'));
		
		return true;
	};
	
	ConvoEntry.prototype.scrollToEntry = function($current) {
		
		$j('html, body').animate({
			scrollTop: $current.offset().top
		}, 1000);
	};
	
	ConvoEntry.prototype.addSelectOptions = function(entryNum) {
		
		var $selects = $j('#convo-onpopup-sec').find('select');
		
		$j.each($selects, function(index, element) {
			if($j(element).find("option[value='"+entryNum+"']").length < 1) {
				$j(element).append('<option value="'+entryNum+'">'+entryNum+'</option>');
			}
		});
	};
	
	ConvoEntry.prototype.removeSelectOptions = function(entryOrderNum) {
		
		var $selects = $j('#convo-onpopup-sec').find('select');
		
		$selects.find("option[value='"+entryOrderNum+"']").remove();
	};
	
	// Function for performing all UI actions required to change a response goto
	ConvoEntry.prototype.changeResponseGoto = function($self, optionVal) {
		
		var currentContainer = $self.closest(".convo-entry-container").attr('id');
		var $arrow = $self.closest('.response').find('.convo-entry-options-arrow').children(":first");
		var $currentEntryOptionsContainer = $self.closest('.entry-options-container');
		var $currentJoinEntry = $currentEntryOptionsContainer.find('.convo-join-entry');
		var responsesCount = this.getCurrentEntryResponsesCount($j(currentContainer));
		var topParentId = currentContainer.split("-")[3];
		
		if($currentJoinEntry.length > 0) {
			$currentJoinEntry.parent().append(this.getEntrySelect(topParentId, responsesCount));
			$currentJoinEntry.remove();
		} else if ($currentEntryOptionsContainer.find('.select-existing').length < 1) {
			$currentEntryOptionsContainer.append('<div class="convo-entry-option">'+ this.getEntrySelect(topParentId, responsesCount)+'</div>');
		}
		
		$currentEntryOptionsContainer.hide();
		//$j('.convo-entry-options-arrow').show();

		$currentEntryOptionsContainer.parent('.response').find('.convo-entry-options-arrow').show();
		//$j('.optionlinked').remove();
		
		//var currentEntryEq = optionVal - 1;
		//var $current = $j('#convo-onpopup-sec').children('#convo-entry-container:eq('+currentEntryEq+')');
		
		var $current = $j('#convo-entry-container-'+optionVal);
		
		console.log("#"+currentContainer);
		// Hide all convo entry containers except the current parent
		//var $prevEntryContainers = $j('.convo-entry-container').not("#"+currentContainer);
		var $prevEntryContainers = $j('.convo-entry-container');
		
		$prevEntryContainers.hide();
		
		if($j('#convo-onpopup-sec').find('.optionlinked').length == 0) {
			$j("#"+currentContainer).after(this.getConnector());
		} else {
			$j('.optionlinked').remove();
			
			if(topParentId < optionVal) {
				$j("#"+currentContainer).after(this.getConnector());
			} else {
				$j("#"+currentContainer).before(this.getConnector());
			}
		}
		/*if(optionVal !== "none") {
			$j("#"+currentContainer).after(this.getConnector());
		}*/
		
		$j("#"+currentContainer).show();
		
		this.setPrevEntries($prevEntryContainers);
		
		if($j('#convo-entry-container-'+optionVal).length > 0) {
			$j('#convo-entry-container-'+optionVal).show();
		}		
		
		$j('#convo-onpopup-sec').find('.arrow-linked-current').removeClass("arrow-linked-current").addClass("arrow-linked");
		
		this.setCurrentLink($arrow);
		$arrow.removeClass().addClass("arrow-linked-current");
		
		if($current.length > 0) {
			$j('html, body').animate({
				scrollTop: $current.offset().top
			}, 1000);
		}
		
		return;
	}
	
	/****************************************************************************/
	/* END ConvoEntry Class */
	/****************************************************************************/
});
