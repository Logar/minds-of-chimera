function postRequester() {
	
	var myCache = cache();
	// tell the cache to use an array for its internal storage system
	
	myCache.setStore(localStorageStore);
	var users_startIndex = 29, users_limit = 10;
	var posts_startIndex = 11;
	var posts_startDate = '9009-01-20', posts_limit = 10;
	var times_image_uploaded = 1;
	var post_img_id = '';
	
	var $loading = $j('<div class="loading"><p>Loading more items&hellip;</p></div>'),
	$all_users_bottom = $j('#all-users-bottom'),
	opts = {
		offset: '100%'
	};
	
	$all_users_bottom.waypoint(function(event, direction) {
		$all_users_bottom.waypoint('remove');
		//$j('#all-users').append($loading);
		
		$j.get($j('.more a').attr('href'), function(data) {
			var $data = $j(data);
			
			//$loading.detach();
			$j('.more').replaceWith($data.find('.more'));
			
			if(users_startIndex < users_end) {
				appendMoreUsers();
				users_startIndex = users_startIndex + 10;
			}
			$all_users_bottom.waypoint(opts);
		});
	}, opts);
	
	$more_posts = $j('#more-posts'),
	opts2 = {
		offset: 'top-in-view'
	};
	//someElements.bind('waypoint.reached', function(event, direction) {
	//$more_posts.waypoint(function(event, direction) {
	$more_posts.waypoint(function(event, direction) {
		alert("You've hit my waypoint! ow!");
		if(direction == "down") {
			//$j('#all-users').append($loading);

			$j.get($j('.more a').attr('href'), function(data) {
				var $data = $j(data);
				
				//$loading.detach();
				$j('.more').replaceWith($data.find('.more'));
				
				// user_posts_end defined in the head
				if(posts_startIndex < user_posts_end) {
					
					var nextDate = $j('#last-date').val();
					//console.log('this next date '+nextDate);
					posts_startIndex = posts_startIndex + 10;
					if(nextDate == '') {
						nextDate = posts_startDate;
					}
					posts_startDate = nextDate;
					appendMorePosts();
				}
				$more_posts.waypoint(opts2);
			});
		}
		$j.waypoints('refresh');
	}, opts2);
	
	$go_top = $j('#go-top'),
	$j.waypoints.settings.scrollThrottle = 30;
		
	$j('#main-wrapper').waypoint(function(event, direction) {
		$j('#go-top').toggleClass('hidden', direction === "up");
	}, {
			offset: '-100%'
		}).find('#top-bar').waypoint(function(event, direction) {
			$j(this).parent().toggleClass('sticky', direction === "down");
		event.stopPropagation();
	});
}